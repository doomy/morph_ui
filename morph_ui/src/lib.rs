#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(not(feature = "std"))]
extern crate alloc;

#[macro_use]
extern crate downcast;

pub mod context;
pub mod events;
#[cfg(feature = "font")]
pub mod font {
    //! Provides text style to render from font (exported from embedded_font).
    pub use embedded_font::*;

    pub use super::font_loader::*;
}
pub mod eg;
pub mod error;
pub mod shell;
pub mod stdlib;
pub mod themes;
pub mod utils;
pub mod widget;
pub mod widgets;

#[cfg(feature = "font")]
mod font_loader;

pub use eg::prelude::{DrawTarget, Drawable};

pub use context::*;
pub use events::*;
pub use shell::State;
pub use utils::*;
pub use widget::states::*;
pub use widget::*;
pub use widgets::*;
