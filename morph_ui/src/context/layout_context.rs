use crate::eg::geometry::Size;

/// Provides local information to calculate the desired size of a widget.
#[derive(Debug, Clone, PartialEq)]
pub struct LayoutContext {
    available_size: Size,
    v_stretch: bool,
    h_stretch: bool,
}

impl LayoutContext {
    /// Creates a new `LayoutContext`.
    pub fn new(available_size: Size, v_stretch: bool, h_stretch: bool) -> Self {
        LayoutContext {
            available_size,
            v_stretch,
            h_stretch,
        }
    }

    /// Returns the available size that comes from the parent.
    pub fn available_size(&self) -> Size {
        self.available_size
    }

    /// Returns the available width.
    pub fn available_width(&self) -> u32 {
        self.available_size.width
    }

    /// Returns the available height.
    pub fn available_height(&self) -> u32 {
        self.available_size.height
    }

    /// Returns a value that indicates if the widget should vertical stretch.
    pub fn v_stretch(&self) -> bool {
        self.v_stretch
    }

    /// Returns a value that indicates if the widget should horizontal stretch.
    pub fn h_stretch(&self) -> bool {
        self.h_stretch
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn text_available_size() {
        let ltx = LayoutContext::new(Size::new(2, 3), false, false);
        assert_eq!(Size::new(2, 3), ltx.available_size());
    }

    #[test]
    fn text_available_width() {
        let ltx = LayoutContext::new(Size::new(2, 3), false, false);
        assert_eq!(2, ltx.available_width());
    }

    #[test]
    fn text_available_height() {
        let ltx = LayoutContext::new(Size::new(2, 3), false, false);
        assert_eq!(3, ltx.available_height());
    }

    #[test]
    fn text_v_stretch() {
        let ltx = LayoutContext::new(Size::zero(), true, false);
        assert!(ltx.v_stretch());
    }

    #[test]
    fn text_h_stretch() {
        let ltx = LayoutContext::new(Size::zero(), false, true);
        assert!(ltx.h_stretch());
    }
}
