//! Provides a set of context structs, that provides context for the methods of the widget trait.

mod clipboard_context;
mod debug_context;
mod draw_context;
mod event_context;
mod init_context;
mod keyboard_context;
mod layout_context;
mod state_context;

pub use clipboard_context::*;
pub use debug_context::*;
pub use draw_context::*;
pub use event_context::*;
pub use init_context::*;
pub use keyboard_context::*;
pub use layout_context::*;
pub use state_context::*;
