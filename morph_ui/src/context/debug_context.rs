use crate::{
    eg::{
        draw_target::DrawTarget,
        pixelcolor::PixelColor,
        prelude::Primitive,
        primitives::{PrimitiveStyleBuilder, Rectangle},
        Drawable,
    },
    widget::Widget,
};

#[cfg(feature = "std")]
use crate::stdlib::time::Instant;

#[cfg(feature = "std")]
use crate::stdlib::collections::BTreeMap;

/// This enum is used to
#[derive(Debug, PartialEq)]
pub enum Debug {
    /// Prints out the widget graph.
    PrintGraph,

    /// Prints the operation in the pipeline (layout render) with time measure.
    PrintPipeline,

    /// Draw a blue raster for each widget to better debug layout.
    DrawRaster,
}

/// Add some debug helpers to morph_ui.
#[derive(Debug)]
pub struct DebugContext<C>
where
    C: PixelColor,
{
    print_graph: bool,
    print_pipeline: bool,
    draw_raster: bool,
    raster_color: Option<C>,
    active: bool,

    #[cfg(feature = "std")]
    instants: BTreeMap<String, Instant>,
}

impl<C> Default for DebugContext<C>
where
    C: PixelColor,
{
    fn default() -> Self {
        DebugContext {
            print_graph: false,
            print_pipeline: false,
            draw_raster: false,
            raster_color: None,
            active: false,

            #[cfg(feature = "std")]
            instants: BTreeMap::new(),
        }
    }
}

impl<C> DebugContext<C>
where
    C: PixelColor,
{
    /// Creates a new debug context from the given flags.
    pub fn new(flags: &[Debug], raster_color: Option<C>) -> Self {
        DebugContext {
            print_graph: flags.contains(&Debug::PrintGraph),
            print_pipeline: flags.contains(&Debug::PrintPipeline),
            draw_raster: flags.contains(&Debug::DrawRaster),
            raster_color,
            active: false,

            #[cfg(feature = "std")]
            instants: BTreeMap::new(),
        }
    }

    #[cfg(debug_assertions)]
    fn active(&self) -> bool {
        self.active
    }

    #[cfg(not(debug_assertions))]
    fn active(&self) -> bool {
        false
    }

    /// Toggles the debug between active and inactive.
    pub fn toggle_active(&mut self) {
        self.active = !self.active;
    }

    /// Prints the start of the pipeline output.
    pub fn print_pipeline_start(&self) {
        if !self.print_pipeline || !self.active() {
            return;
        }

        #[cfg(feature = "std")]
        println!("\n--------");

        #[cfg(feature = "std")]
        println!("# Start execution of pipeline\n");
    }

    /// Prints the end of the pipeline output.
    pub fn print_pipeline_end(&self) {
        if !self.print_pipeline || !self.active() {
            return;
        }

        #[cfg(feature = "std")]
        println!("--------\n");
    }

    /// Prints the start of the given fn and start measure time.
    pub fn print_pipeline_fn_start(&mut self, func: &str) {
        if !self.print_pipeline || !self.active() || func.is_empty() {
            return;
        }

        #[cfg(feature = "std")]
        self.instants.insert(String::from(func), Instant::now());

        #[cfg(feature = "std")]
        println!("* fn {} start", func);
    }

    /// Prints the end of the given fn and print the duration of the execution of the fn.
    pub fn print_pipeline_fn_end(&mut self, func: &str) {
        if !self.print_pipeline || !self.active() || func.is_empty() {
            return;
        }

        #[cfg(feature = "std")]
        if let Some(instant) = self.instants.remove(func) {
            println!("* fn {} end after {:?}.", func, instant.elapsed());
        }
    }

    /// Prints the tree for the given root.
    pub fn print_graph<S, D>(&self, root: &dyn Widget<S, D>)
    where
        S: 'static,
        D: DrawTarget + 'static,
    {
        if !self.print_graph || !self.active {
            return;
        }

        #[cfg(feature = "std")]
        println!("\n..........");

        #[cfg(feature = "std")]
        println!("# Widget graph\n");

        self.print_widget(root, 0);

        #[cfg(feature = "std")]
        println!("..........\n");
    }

    fn print_widget<S, D>(&self, root: &dyn Widget<S, D>, depth: usize)
    where
        S: 'static,
        D: DrawTarget + 'static,
    {
        #[cfg(feature = "std")]
        println!("{}{}", "| ".repeat(depth), root.widget_string());

        for child in root.get_children() {
            self.print_widget(child, depth + 1);
        }
    }

    /// Draws a rectangle for the given bounds.
    pub fn draw_widget_raster<D, S>(&self, bounds: Rectangle, dt: &mut D) -> Result<(), D::Error>
    where
        D: DrawTarget<Color = C>,
    {
        if !self.draw_raster || !self.active() {
            return Ok(());
        }

        if let Some(raster_color) = self.raster_color {
            bounds
                .into_styled(
                    PrimitiveStyleBuilder::new()
                        .stroke_width(1)
                        .stroke_color(raster_color)
                        .build(),
                )
                .draw(dt)?;
        }

        Ok(())
    }
}
