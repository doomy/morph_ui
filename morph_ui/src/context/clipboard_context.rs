use crate::stdlib::string::String;

#[derive(Default, Debug, PartialEq)]
pub struct ClipboardContext {
    pub content: String,
    pub internal_update: bool,
}

impl ClipboardContext {
    /// Creates a new clipboard context.
    pub fn new() -> Self {
        ClipboardContext::default()
    }

    /// Reads the current content of the clipboard.
    pub fn read(&self) -> &String {
        &self.content
    }

    /// Write content to the clipboard.
    pub fn write(&mut self, content: impl Into<String>) {
        let content = content.into();

        if self.content == content {
            return;
        }

        self.content = content;
        self.internal_update = true;
    }
}
