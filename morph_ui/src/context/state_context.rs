use crate::stdlib::collections::BTreeSet;

/// Provides access to the application state and manage the render and layout update flags.
#[derive(Debug, Clone)]
pub struct StateContext<S> {
    render_update: bool,
    layout_update: bool,
    rebuild: bool,
    state: S,
    redraw_widgets: BTreeSet<u32>,
}

impl<S> StateContext<S> {
    /// Creates a new `StateContext`.
    pub fn new(state: S) -> Self {
        StateContext {
            render_update: true,
            layout_update: true,
            rebuild: true,
            state,
            redraw_widgets: BTreeSet::new(),
        }
    }

    /// Triggers render and layout update.
    pub fn trigger_update(&mut self, id: Option<u32>) {
        self.render_update = true;
        self.layout_update = true;

        if let Some(id) = id {
            self.redraw_widgets.insert(id);
        }
    }

    /// Returns if there is a render update.
    pub fn render_update(&self) -> bool {
        self.render_update
    }

    /// Triggers render update.
    pub fn trigger_render_update(&mut self, id: Option<u32>) {
        self.render_update = true;
        if let Some(id) = id {
            self.redraw_widgets.insert(id);
        }
    }

    /// Returns if there is a layout update.
    pub fn layout_update(&self) -> bool {
        self.layout_update
    }

    /// Triggers a rebuild of the ui.
    pub fn trigger_rebuild(&mut self) {
        self.rebuild = true;
    }

    /// Returns `true` if the ui should rebuild.
    pub fn rebuild(&self) -> bool {
        self.rebuild
    }

    /// Triggers layout update.
    pub fn trigger_layout_update(&mut self) {
        self.layout_update = true;
    }

    pub(crate) fn reset_rebuild(&mut self) {
        self.rebuild = false;
    }

    /// Reset the update flags.
    pub(crate) fn reset_update(&mut self) {
        self.layout_update = false;
        self.render_update = false;
        self.rebuild = false;
    }

    /// Updates the update fags from an other `StateContext`.
    pub fn update_from_other_context<OS>(&mut self, otx: &StateContext<OS>) {
        self.layout_update = self.layout_update || otx.layout_update;
        self.render_update = self.render_update || otx.render_update;
    }

    /// Returns a reference of the state.
    pub fn state(&self) -> &S {
        &self.state
    }

    /// Returns a mutable reference of the state.
    pub fn state_mut(&mut self) -> &mut S {
        &mut self.state
    }

    pub fn has_render_update(&mut self, id: u32) -> bool {
        self.redraw_widgets.remove(&id)
    }
}
