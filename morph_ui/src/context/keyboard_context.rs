use crate::{
    events::*,
    stdlib::{string::String, vec::*},
    utils::Cursor,
};

/// This enum is used to send focus requests to the `KeyboardContext`.
pub enum FocusRequest {
    /// Focus the widget with the given id.
    Focus(u32),
    /// Move focus to the next widget.
    Next,
    /// Move focus to the previous widget.
    Previous,
    /// Removes focus form the widget with the given id.
    Remove(u32),
}

/// Defines requests to open and close on screen keyboard.
#[derive(Debug, Clone)]
pub enum KeyboardRequest {
    /// There is no request.
    None,

    /// Request to open keyboard.
    Open(String, u32, Cursor, bool),

    /// Request to close keyboard.
    Close(String, u32, Cursor),
}

impl Default for KeyboardRequest {
    fn default() -> Self {
        KeyboardRequest::None
    }
}

/// Used to interact with keyboard state e.g. keyboard modifiers and focus.
#[derive(Debug, Default, Clone)]
pub struct KeyboardContext {
    pub(crate) focused_widget: Option<u32>,
    focus_lost_widgets: Vec<u32>,
    pressed_keys: Vec<Key>,
    pub(crate) focusable_widgets: Vec<u32>,
    pub(crate) keyboard_request: KeyboardRequest,
}

impl KeyboardContext {
    /// Creates a new `KeyboardContext`.
    pub fn new() -> Self {
        Self {
            focused_widget: None,
            focus_lost_widgets: Vec::new(),
            pressed_keys: Vec::new(),
            focusable_widgets: Vec::new(),
            keyboard_request: KeyboardRequest::None,
        }
    }

    /// Update the list of current pressed keys and check of focus manipulation. Returns `true` if there is a focus manipulation.
    pub fn update_keys(&mut self, e: &KeyEvent) -> bool {
        if e.pressed() {
            // Move focus to next on tab.
            if e.key() == Key::Tab {
                if self.is_shift_pressed() {
                    return self.send_focus_request(FocusRequest::Previous);
                } else {
                    return self.send_focus_request(FocusRequest::Next);
                }
            }

            if !self.pressed_keys.contains(&e.key()) {
                self.pressed_keys.push(e.key());
            }
        } else {
            self.pressed_keys.retain(|k| k != &e.key());
        }

        false
    }

    /// Manipulate focus by controller button input (left / right). Returns `true` if there is a focus manipulation.
    pub fn update_controller_buttons(&mut self, e: &ControllerButtonEvent) -> bool {
        if e.pressed() {
            if e.button() == ControllerButton::DPadRight {
                return self.send_focus_request(FocusRequest::Next);
            }

            if e.button() == ControllerButton::DPadLeft {
                return self.send_focus_request(FocusRequest::Previous);
            }
        }

        false
    }

    /// Returns `true` if the given key is pressed otherwise `false`.
    pub fn is_key_pressed(&self, key: Key) -> bool {
        self.pressed_keys.contains(&key)
    }

    /// Returns `true` if control left or control right key is pressed.
    pub fn is_ctrl_pressed(&self) -> bool {
        self.is_key_pressed(Key::ControlLeft) || self.is_key_pressed(Key::ControlRight)
    }

    /// Returns `true` if the home key is pressed.
    pub fn is_home_pressed(&self) -> bool {
        self.is_key_pressed(Key::Home)
    }

    /// Returns `true` if shift left or shift right is pressed.
    pub fn is_shift_pressed(&self) -> bool {
        self.is_key_pressed(Key::ShiftLeft) || self.is_key_pressed(Key::ShiftRight)
    }

    #[cfg(not(target_os = "macos"))]
    /// Returns `true` if home is pressed on macos, on other operating systems if ctrl is pressed.
    pub fn is_os_ctrl_pressed(&self) -> bool {
        self.is_ctrl_pressed()
    }

    #[cfg(target_os = "macos")]
    /// Returns `true` if home is pressed on macos, on other operating systems if ctrl is pressed.
    pub fn is_os_ctrl_pressed(&self) -> bool {
        self.is_home_pressed()
    }

    /// Sends a keyboard request.
    pub fn send_keyboard_request(&mut self, request: KeyboardRequest) {
        self.keyboard_request = request;
    }

    /// Returns a reference of the current keyboard request.
    pub fn keyboard_request(&self) -> &KeyboardRequest {
        &self.keyboard_request
    }

    /// Sends a focus request to the `KeyboardContext`. Returns `true` if the operation is a success otherwise `false`.
    pub fn send_focus_request(&mut self, request: FocusRequest) -> bool {
        let old_focused_widget = self.focused_widget;
        let mut success = false;

        match request {
            FocusRequest::Focus(id) => {
                if self.focusable_widgets.contains(&id) {
                    self.focused_widget = Some(id);
                    success = true;
                }
            }
            FocusRequest::Next => {
                if !self.focusable_widgets.is_empty() {
                    self.focused_widget = Some(self.focusable_widgets[0]);
                }

                if let Some(focused_widget) = old_focused_widget {
                    if let Some(index) = self
                        .focusable_widgets
                        .iter()
                        .position(|&w| w == focused_widget)
                    {
                        if index < self.focusable_widgets.len() - 1 {
                            self.focused_widget = Some(self.focusable_widgets[index + 1]);
                        }
                    }
                }

                success = true
            }
            FocusRequest::Previous => {
                if self.focusable_widgets.is_empty() {
                    self.focused_widget =
                        Some(self.focusable_widgets[self.focusable_widgets.len() - 1]);
                }

                if let Some(focused_widget) = old_focused_widget {
                    if let Some(index) = self
                        .focusable_widgets
                        .iter()
                        .position(|&w| w == focused_widget)
                    {
                        if index > 0 {
                            self.focused_widget = Some(self.focusable_widgets[index - 1]);
                        }
                    }
                }

                success = true
            }
            FocusRequest::Remove(id) => {
                if self.focused_widget.is_some() && self.focused_widget.unwrap() == id {
                    self.focused_widget = None;
                    success = true;
                }
            }
        }

        if self.focused_widget != old_focused_widget {
            if let Some(old_focused_widget) = old_focused_widget {
                self.focus_lost_widgets.push(old_focused_widget);
            }
        }

        success
    }

    /// Checks if the given widget has the current focus.
    pub fn has_focus(&self, widget: u32) -> bool {
        self.focused_widget.is_some() && self.focused_widget.unwrap() == widget
    }

    /// Returns the id of the focused widget. If no widget is focused `None` will be returned.
    pub fn focused_id(&self) -> Option<u32> {
        self.focused_widget
    }

    /// Checks if the given widget lost focus.
    pub fn lost_focus(&mut self, widget: u32) -> bool {
        self.focus_lost_widgets.contains(&widget)
    }

    /// Clears all focus lost widgets from the list.
    pub(crate) fn clear_focus(&mut self) {
        self.focus_lost_widgets.clear();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_key_pressed() {
        let mut ktx = KeyboardContext::new();

        ktx.update_keys(&KeyEvent::new(KEY_ENTER, true));

        assert!(ktx.is_key_pressed(Key::Enter));

        ktx.update_keys(&KeyEvent::new(KEY_ENTER, false));

        assert!(!ktx.is_key_pressed(Key::Enter));
    }

    #[test]
    fn test_is_ctrl_pressed() {
        let mut ktx = KeyboardContext::new();

        ktx.update_keys(&KeyEvent::new(KEY_CONTROL_LEFT, true));

        assert!(ktx.is_key_pressed(Key::ControlLeft));

        ktx.update_keys(&KeyEvent::new(KEY_CONTROL_LEFT, false));

        assert!(!ktx.is_key_pressed(Key::ControlLeft));
    }

    #[test]
    fn test_is_home_pressed() {
        let mut ktx = KeyboardContext::new();

        ktx.update_keys(&KeyEvent::new(KEY_HOME, true));

        assert!(ktx.is_key_pressed(Key::Home));

        ktx.update_keys(&KeyEvent::new(KEY_HOME, false));

        assert!(!ktx.is_key_pressed(Key::Home));
    }

    #[test]
    fn test_is_shift_pressed() {
        let mut ktx = KeyboardContext::new();

        ktx.update_keys(&KeyEvent::new(KEY_SHIFT_LEFT, true));

        assert!(ktx.is_key_pressed(Key::ShiftLeft));

        ktx.update_keys(&KeyEvent::new(KEY_SHIFT_LEFT, false));

        assert!(!ktx.is_key_pressed(Key::ShiftLeft));

        ktx.update_keys(&KeyEvent::new(KEY_SHIFT_RIGHT, true));

        assert!(ktx.is_key_pressed(Key::ShiftRight));

        ktx.update_keys(&KeyEvent::new(KEY_SHIFT_RIGHT, false));

        assert!(!ktx.is_key_pressed(Key::ShiftRight));
    }

    #[test]
    fn test_focus() {
        let mut ktx = KeyboardContext::new();
        ktx.focusable_widgets.push(0);

        ktx.send_focus_request(FocusRequest::Focus(0));

        assert_eq!(ktx.focused_id(), Some(0));

        ktx.send_focus_request(FocusRequest::Remove(0));

        assert_eq!(ktx.focused_id(), None);

        assert!(ktx.lost_focus(0));
    }
}
