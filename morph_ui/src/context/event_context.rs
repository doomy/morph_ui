use crate::*;

/// Provides everything that is needed to handle the event operations of a widget.
pub struct EventContext<'a, S> {
    stx: &'a mut StateContext<S>,
    ktx: &'a mut KeyboardContext,
    ctx: &'a mut ClipboardContext,
}

impl<'a, S> EventContext<'a, S> {
    /// Creates a new `EventContext`.
    pub fn new(
        stx: &'a mut StateContext<S>,
        ktx: &'a mut KeyboardContext,
        ctx: &'a mut ClipboardContext,
    ) -> Self {
        Self { stx, ktx, ctx }
    }

    /// Creates a new `EventContext` from a [`StateContext`].
    pub fn from_stx<I>(etx: &'a mut EventContext<I>, stx: &'a mut StateContext<S>) -> Self {
        EventContext {
            stx,
            ktx: etx.ktx,
            ctx: etx.ctx,
        }
    }

    /// Returns a reference to the `StateContext`.
    pub fn stx(&self) -> &StateContext<S> {
        self.stx
    }

    /// Returns a mutable reference to the `StateContext`.
    pub fn stx_mut(&mut self) -> &mut StateContext<S> {
        self.stx
    }

    /// Returns a reference to the `KeyboardContext`.
    pub fn ktx(&self) -> &KeyboardContext {
        self.ktx
    }

    /// Returns a mutable reference to the `KeyboardContext`.
    pub fn ktx_mut(&mut self) -> &mut KeyboardContext {
        self.ktx
    }

    /// Returns a reference to the `ClipboardContext`.
    pub fn ctx(&self) -> &ClipboardContext {
        self.ctx
    }

    /// Returns a mutable reference to the `ClipboardContext`.
    pub fn ctx_mut(&mut self) -> &mut ClipboardContext {
        self.ctx
    }

    /// Returns mutable references to all field of the `EventContext`.
    pub fn all(
        &mut self,
    ) -> (
        &mut StateContext<S>,
        &mut KeyboardContext,
        &mut ClipboardContext,
    ) {
        (self.stx, self.ktx, self.ctx)
    }
}
