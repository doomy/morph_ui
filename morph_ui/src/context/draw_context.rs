use crate::{
    eg::{
        draw_target::{Clipped, DrawTargetExt},
        geometry::Dimensions,
        pixelcolor::PixelColor,
        primitives::Rectangle,
    },
    *,
};

/// A `Display` can be used as `DrawTarget`. It can be wrap a `DrawTarget` or a clipped `DrawTarget`.
pub enum Display<'a, C, D, T, E>
where
    C: DrawTarget<Color = T, Error = E>,
    D: DrawTarget<Color = T, Error = E>,
    T: PixelColor,
{
    /// Wraps a `DrawTarget` without clipping.
    Display(&'a mut D),

    /// Wraps a `DrawTarget` with clipping.
    Clipped(C),
}

impl<'a, C, D, T, E> DrawTarget for Display<'a, C, D, T, E>
where
    C: DrawTarget<Color = T, Error = E>,
    D: DrawTarget<Color = T, Error = E>,
    T: PixelColor,
{
    type Color = T;

    type Error = E;

    fn fill_contiguous<I>(&mut self, area: &Rectangle, colors: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Self::Color>,
    {
        match self {
            Display::Display(d) => d.fill_contiguous(area, colors),
            Display::Clipped(c) => c.fill_contiguous(area, colors),
        }
    }

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = embedded_graphics::Pixel<Self::Color>>,
    {
        match self {
            Display::Display(d) => d.draw_iter(pixels),
            Display::Clipped(c) => c.draw_iter(pixels),
        }
    }

    fn fill_solid(&mut self, area: &Rectangle, color: Self::Color) -> Result<(), Self::Error> {
        match self {
            Display::Display(d) => d.fill_solid(area, color),
            Display::Clipped(c) => c.fill_solid(area, color),
        }
    }
}

impl<'a, C, D, T, E> Dimensions for Display<'a, C, D, T, E>
where
    C: DrawTarget<Color = T, Error = E>,
    D: DrawTarget<Color = T, Error = E>,
    T: PixelColor,
{
    fn bounding_box(&self) -> Rectangle {
        match self {
            Display::Display(d) => d.bounding_box(),
            Display::Clipped(c) => c.bounding_box(),
        }
    }
}

/// Provides everything that is needed to handle the draw operation of a widget.
#[derive(Debug)]
pub struct DrawContext<'a, D, S>
where
    D: DrawTarget,
{
    draw_target: &'a mut D,
    stx: &'a mut StateContext<S>,
    ktx: &'a mut KeyboardContext,
    clip_box: Option<Rectangle>,
    local_redraw: bool,
    dbx: &'a mut DebugContext<D::Color>,
}

impl<'a, D, S> DrawContext<'a, D, S>
where
    D: DrawTarget,
{
    /// Creates a new `DrawContext`.
    pub fn new(
        display: &'a mut D,
        stx: &'a mut StateContext<S>,
        ktx: &'a mut KeyboardContext,
        local_redraw: bool,
        dbx: &'a mut DebugContext<D::Color>,
    ) -> Self {
        Self {
            draw_target: display,
            stx,
            ktx,
            clip_box: None,
            local_redraw,
            dbx,
        }
    }

    /// Creates a new `DrawContext` from a [`StateContext`].
    pub fn from_stx<I>(dtx: &'a mut DrawContext<D, I>, stx: &'a mut StateContext<S>) -> Self {
        Self {
            stx,
            draw_target: dtx.draw_target,
            ktx: dtx.ktx,
            local_redraw: dtx.local_redraw,
            dbx: dtx.dbx,
            clip_box: dtx.clip_box,
        }
    }

    /// Creates a new `DrawContext` from the given clip box.
    pub fn from_clip_box(
        display: &'a mut D,
        stx: &'a mut StateContext<S>,
        ktx: &'a mut KeyboardContext,
        clip_box: Rectangle,
        local_redraw: bool,
        dbx: &'a mut DebugContext<D::Color>,
    ) -> Self {
        Self {
            draw_target: display,
            stx,
            ktx,
            clip_box: Some(clip_box),
            local_redraw,
            dbx,
        }
    }

    // todo: refactor to use reference of DrawContext instead!!!
    /// Creates a new draw context from an other context
    pub fn from_draw_context(
        dtx: (
            &'a mut D,
            &'a mut StateContext<S>,
            &'a mut KeyboardContext,
            &'a mut DebugContext<D::Color>,
        ),
        clip_box: Option<Rectangle>,
        local_redraw: bool,
    ) -> Self {
        Self {
            draw_target: dtx.0,
            stx: dtx.1,
            ktx: dtx.2,
            clip_box,
            local_redraw,
            dbx: dtx.3,
        }
    }

    /// Returns a reference of the draw target.
    pub fn draw_target(&self) -> &D {
        self.draw_target
    }

    /// Returns a mutable reference of the draw target.
    pub fn draw_target_mut(&mut self) -> &mut D {
        self.draw_target
    }

    /// Returns a reference to the `StateContext`.
    pub fn stx(&self) -> &StateContext<S> {
        self.stx
    }

    /// Returns a mutable reference to the `StateContext`.
    pub fn stx_mut(&mut self) -> &mut StateContext<S> {
        self.stx
    }

    /// Returns a reference to the `KeyboardContext`.
    pub fn ktx(&self) -> &KeyboardContext {
        self.ktx
    }

    /// Returns a mutable reference to the `KeyboardContext`.
    pub fn ktx_mut(&mut self) -> &mut KeyboardContext {
        self.ktx
    }

    /// Returns a reference to the `DebugContext`.
    pub fn dbx(&self) -> &DebugContext<D::Color> {
        self.dbx
    }

    /// Returns mutable references to all field of the `DrawContext`.
    pub fn all(
        &mut self,
    ) -> (
        &mut D,
        &mut StateContext<S>,
        &mut KeyboardContext,
        &mut DebugContext<D::Color>,
    ) {
        (self.draw_target, self.stx, self.ktx, self.dbx)
    }

    /// Returns a display with an inner draw target that can be used for drawing. The display respects the clip box if it is set.
    pub fn display<'b>(&'b mut self) -> Display<Clipped<'b, D>, D, D::Color, D::Error> {
        if let Some(clip_box) = self.clip_box {
            return Display::Clipped(self.draw_target.clipped(&clip_box));
        }

        Display::Display(self.draw_target)
    }

    /// Returns the current clip box. Clip box defines a clip area.
    pub fn clip_box(&self) -> Option<Rectangle> {
        self.clip_box
    }

    /// Check if the calling widget show be redrawn.
    pub fn has_render_update(&mut self, id: Option<u32>) -> bool {
        self.local_redraw || id.is_some() && self.stx_mut().has_render_update(id.unwrap())
    }
}
