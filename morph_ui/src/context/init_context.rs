use crate::context::*;

/// Access to functionality that is used to initialize a widget.
pub struct InitContext<'a, S> {
    id_count: &'a mut u32,
    stx: &'a mut StateContext<S>,
    ktx: &'a mut KeyboardContext,
}

impl<'a, S> InitContext<'a, S> {
    /// Creates a new `InitContext`.
    pub fn new(
        id_count: &'a mut u32,
        stx: &'a mut StateContext<S>,
        ktx: &'a mut KeyboardContext,
    ) -> Self {
        Self { id_count, stx, ktx }
    }

    /// Creates a new `InitContext` from a [`StateContext`].
    pub fn from_stx<I>(itx: &'a mut InitContext<I>, stx: &'a mut StateContext<S>) -> Self {
        Self {
            id_count: itx.id_count,
            stx,
            ktx: itx.ktx,
        }
    }

    /// Generates a new widget id.
    pub fn get_id(&mut self) -> u32 {
        *self.id_count += 1;
        *self.id_count - 1
    }

    /// Returns a reference to the `StateContext`.
    pub fn stx(&self) -> &StateContext<S> {
        self.stx
    }

    /// Returns a mutable reference to the `StateContext`.
    pub fn stx_mut(&mut self) -> &mut StateContext<S> {
        self.stx
    }

    /// Returns a reference to the `KeyboardContext`.
    pub fn ktx(&self) -> &KeyboardContext {
        self.ktx
    }

    /// Returns a mutable reference to the `KeyboardContext`.
    pub fn ktx_mut(&mut self) -> &mut KeyboardContext {
        self.ktx
    }
}
