//! This module provides additional components to extend the feature set of embedded-graphics.

mod qr_code;

pub use qr_code::*;
