use crate::eg::pixelcolor::*;

/// Defines the style of a QrCode.
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[non_exhaustive]
pub struct QrCodeStyle<C>
where
    C: PixelColor,
{
    /// Fill color (background color) of the qr code.
    ///
    /// If `fill_color` is set to `None` no fill will be drawn.
    pub fill_color: Option<C>,

    /// Color (foreground color) of the qr code.
    ///
    /// If `fill_color` is set to `None` no color will be drawn.
    pub color: Option<C>,
}
