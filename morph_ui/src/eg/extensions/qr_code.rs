use crate::{
    eg::{
        draw_target::DrawTarget, geometry::*, pixelcolor::PixelColor, prelude::*,
        primitives::Rectangle, Drawable,
    },
    error::Error,
    stdlib::*,
};

mod qr_code_style;

pub use qr_code_style::*;

/// QrCode drawable.
///
/// A qr code drawable can be used to draw a qr code.
#[derive(Clone, PartialEq, Eq)]
pub struct QrCode<C>
where
    C: PixelColor,
{
    /// Qr code.
    pub qr_code: qrcodegen::QrCode,

    text: String,

    /// The position
    pub top_left: Point,

    /// The qr code style.
    style: QrCodeStyle<C>,

    /// The scale factor of the qr code
    pub scale: u32,
}

impl<C> QrCode<C>
where
    C: PixelColor,
{
    /// Creates a qr code with default style and error correction level.
    pub fn new(text: &str, top_left: Point) -> Result<Self, Error> {
        QrCode::with_error_correction_level(
            text,
            top_left,
            qrcodegen::QrCodeEcc::Medium,
            QrCodeStyle {
                fill_color: None,
                color: None,
            },
        )
    }

    /// Gets the text of the qr code.
    pub fn get_text(&self) -> &str {
        self.text.as_str()
    }

    /// Creates a qr code with style and error correction level.
    pub fn with_error_correction_level(
        text: &str,
        top_left: Point,
        ecl: qrcodegen::QrCodeEcc,
        style: QrCodeStyle<C>,
    ) -> Result<Self, Error> {
        Ok(QrCode {
            qr_code: qrcodegen::QrCode::encode_text(text, ecl).map_err(|_| Error::QrCodeError)?,
            top_left,
            style,
            scale: 4,
            text: String::from(text),
        })
    }

    /// Creates a qr code with style and default error correction level.
    pub fn with_style(text: &str, top_left: Point, style: QrCodeStyle<C>) -> Result<Self, Error> {
        QrCode::with_error_correction_level(text, top_left, qrcodegen::QrCodeEcc::Medium, style)
    }

    /// Return whether the qr code contains a given point.
    pub fn contains(&self, point: Point) -> bool {
        self.bounding_box().contains(point)
    }

    /// Returns a new `Rectangle` containing the intersection of `self` and `other`.
    pub fn intersection(&self, other: &Rectangle) -> Rectangle {
        self.bounding_box().intersection(other)
    }

    /// Gets the style of the qr code.
    pub fn get_style(&self) -> QrCodeStyle<C> {
        self.style
    }

    /// Sets the style of the qr code.
    pub fn set_style(&mut self, style: QrCodeStyle<C>) {
        self.style = style;
    }

    /// Returns the size of the qr code
    pub fn size(&self) -> Size {
        Size::new(
            self.qr_code.size() as u32 * self.scale,
            self.qr_code.size() as u32 * self.scale,
        )
    }
}

impl<C> Dimensions for QrCode<C>
where
    C: PixelColor,
{
    fn bounding_box(&self) -> Rectangle {
        Rectangle::new(
            self.top_left,
            Size::new(
                self.qr_code.size() as u32 * self.scale,
                self.qr_code.size() as u32 * self.scale,
            ),
        )
    }
}

impl<C> Transform for QrCode<C>
where
    C: PixelColor,
{
    fn translate(&self, by: Point) -> Self {
        Self {
            top_left: self.top_left + by,
            ..self.clone()
        }
    }

    fn translate_mut(&mut self, by: Point) -> &mut Self {
        self.top_left += by;

        self
    }
}

impl<C> Drawable for QrCode<C>
where
    C: PixelColor,
{
    type Color = C;

    type Output = ();

    fn draw<D>(&self, target: &mut D) -> Result<Self::Output, D::Error>
    where
        D: DrawTarget<Color = Self::Color>,
    {
        let mut pixels = Vec::new();
        let scale = self.scale as i32;

        for y in 0..self.qr_code.size() {
            for x in 0..self.qr_code.size() {
                if self.qr_code.get_module(x, y) {
                    for s_y in (y * scale)..(y * scale + scale) {
                        for s_x in (x * scale)..(x * scale + scale) {
                            if let Some(color) = self.style.color {
                                pixels.push(Pixel(self.top_left + Point::new(s_x, s_y), color));
                            }
                        }
                    }
                }
            }
        }

        if let Some(fill_color) = self.style.fill_color {
            target.fill_solid(&self.bounding_box(), fill_color)?;
        }

        target.draw_iter(pixels)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::pixelcolor::Rgb888;

    #[test]
    fn test_dimensions() {
        let qr: QrCode<Rgb888> = QrCode::new("test", Point::new(10, 20)).unwrap();

        assert_eq!(
            qr.bounding_box(),
            Rectangle::new(Point::new(10, 20), Size::new(84, 84))
        );
    }

    #[test]
    fn test_rectangle_intersection() {
        let qr: QrCode<Rgb888> = QrCode::new("test", Point::new(10, 20)).unwrap();
        let rect2 = Rectangle::new(Point::new_equal(25), Size::new(30, 40));

        assert_eq!(
            qr.intersection(&rect2),
            Rectangle::new(Point::new_equal(25), Size::new(30, 40))
        );
    }

    #[test]
    fn test_rectangle_no_intersection() {
        let qr: QrCode<Rgb888> = QrCode::new("test", Point::new(10, 20)).unwrap();
        let rect2 = Rectangle::new(Point::new_equal(100), Size::new(30, 40));

        assert_eq!(
            qr.intersection(&rect2),
            Rectangle::new(Point::zero(), Size::zero())
        );
    }

    #[test]
    fn test_rectangle_complete_intersection() {
        let qr: QrCode<Rgb888> = QrCode::new("test", Point::new(10, 20)).unwrap();
        let rect2 = qr.bounding_box();

        assert_eq!(qr.intersection(&rect2), qr.bounding_box());
    }

    #[test]
    fn test_rectangle_contained_intersection() {
        let qr: QrCode<Rgb888> = QrCode::new("test", Point::new(10, 10)).unwrap();
        let rect2 = Rectangle::with_corners(Point::new_equal(5), Point::new(100, 100));

        assert_eq!(qr.intersection(&rect2), qr.bounding_box());
    }
}
