/// This trait is use to define an application state.
pub trait State {
    /// Run on `Shell` will trigger update of the state.
    fn update(&mut self) {}
}
