use crate::{
    eg::{geometry::Point, text::renderer::*},
    stdlib::{ops::Deref, *},
    *,
};

mod state;
pub use state::*;

pub type WidgetBox<S, D> = Box<dyn Widget<S, D>>;
pub type WidgetBuilder<S, D> = dyn Fn(&mut S) -> WidgetBox<S, D> + 'static;

pub struct Shell<S, D>
where
    S: State + PartialEq + Clone,
    D: DrawTarget,
{
    ui: Option<Box<dyn Widget<S, D>>>,
    ui_builder: Option<Box<WidgetBuilder<S, D>>>,

    keyboard: Option<Box<dyn Widget<S, D>>>,

    show_keyboard: bool,

    display: Option<D>,

    mouse_position: Point,
    mouse_button_states: (bool, bool, bool),

    events: EventQueue,

    stx: StateContext<S>,
    ktx: KeyboardContext,
    keyboard_ktx: KeyboardContext,
    ctx: ClipboardContext,

    id_count: u32,

    background_color: Option<D::Color>,

    redraw_all: bool,

    dbx: DebugContext<D::Color>,
}

impl<S, D> Shell<S, D>
where
    S: State + PartialEq + Clone + 'static,
    D: DrawTarget + 'static,
{
    pub fn new(state: S) -> Self {
        Self {
            ui: None,
            ui_builder: None,
            keyboard: None,
            show_keyboard: false,
            display: None,
            mouse_position: Point::default(),
            mouse_button_states: (false, false, false),
            events: EventQueue::new(),
            stx: StateContext::new(state),
            ktx: KeyboardContext::new(),
            keyboard_ktx: KeyboardContext::new(),
            ctx: ClipboardContext::new(),
            id_count: 1,
            background_color: None,
            redraw_all: true,
            dbx: DebugContext::default(),
        }
    }

    pub fn from_display(display: D, state: S) -> Self {
        Self {
            ui: None,
            ui_builder: None,
            keyboard: None,
            show_keyboard: false,
            display: Some(display),
            mouse_position: Point::default(),
            mouse_button_states: (false, false, false),
            events: EventQueue::new(),
            stx: StateContext::new(state),
            ktx: KeyboardContext::new(),
            keyboard_ktx: KeyboardContext::new(),
            ctx: ClipboardContext::new(),
            id_count: 0,
            background_color: None,
            redraw_all: true,
            dbx: DebugContext::default(),
        }
    }

    /// Sets the debug context.
    pub fn set_debug_context(&mut self, dbx: DebugContext<D::Color>) {
        self.dbx = dbx;
    }

    /// Builder method that is used to configure debug output. Only available on debug builds.
    #[must_use]
    pub fn debug_flags(mut self, flags: &[Debug], raster_color: Option<D::Color>) -> Self {
        self.dbx = DebugContext::new(flags, raster_color);
        self
    }

    /// Sets the draw target that is used internal to draw the widgets.
    pub fn set_display(&mut self, display: D) {
        self.redraw_all = true;
        self.display = Some(display);
        self.stx.trigger_update(None);
    }

    /// Injects the scancode of the current pressed key.
    pub fn key(&mut self, scan_code: u8, pressed: bool) {
        self.events.enqueue(KeyEvent::new(scan_code, pressed));
    }

    /// Injects the value of the current text input event.
    pub fn text_input(&mut self, text_input: char) {
        self.events.enqueue(TextInputEvent::new(text_input));
    }

    /// Injects the current position of the mouse pointer.
    pub fn mouse(&mut self, x: i32, y: i32) {
        self.events.enqueue(MoveEvent::new((x, y)));
        self.mouse_position = Point::new(x, y);
    }

    /// Injects the current state of mouse buttons. If `true` the corresponding button
    /// is pressed.
    pub fn mouse_button(&mut self, left: bool, middle: bool, right: bool) {
        let (mouse_button, pressed) = {
            if left != self.mouse_button_states.0 {
                (MouseButton::Left, left)
            } else if middle != self.mouse_button_states.1 {
                (MouseButton::Middle, middle)
            } else {
                (MouseButton::Right, right)
            }
        };

        self.events
            .enqueue(MouseEvent::new(self.mouse_position, mouse_button, pressed));

        self.mouse_button_states = (left, middle, right);
    }

    /// Injects the scroll offset of the current event
    pub fn scroll(&mut self, offset_x: i32, offset_y: i32) {
        self.events.enqueue(ScrollEvent::new((offset_x, offset_y)));
    }

    /// Injects a file drop event.
    pub fn drop_file(&mut self, path: String) {
        self.events
            .enqueue(DropEvent::new(path, DropKind::File, self.mouse_position));
    }

    /// Injects a text drop event.
    pub fn drop_text(&mut self, text: String) {
        self.events
            .enqueue(DropEvent::new(text, DropKind::Text, self.mouse_position));
    }

    /// Injects a controller button event.
    pub fn controller_button(
        &mut self,
        button: impl Into<ControllerButton>,
        pressed: bool,
        device: u32,
    ) {
        self.events
            .enqueue(ControllerButtonEvent::new(button.into(), pressed, device));
    }

    /// Injects a controller axis motion event.
    pub fn controller_axis_motion(&mut self, axis: impl Into<Axis>, value: i32, device: u32) {
        self.events
            .enqueue(ControllerAxisMotionEvent::new(axis.into(), value, device));
    }

    fn build(&mut self) {
        let mut ui = None;
        if let Some(ui_builder) = &self.ui_builder {
            ui = Some(ui_builder(self.stx.state_mut()));
        }

        self.ktx.focused_widget = None;
        self.ktx.focusable_widgets.clear();
        self.ktx.clear_focus();

        if let Some(ui) = &mut ui {
            if let Some(old_ui) = &mut self.ui {
                ui.init_and_update(
                    Some(old_ui.deref().deref()),
                    &mut InitContext::new(&mut self.id_count, &mut self.stx, &mut self.ktx),
                );
            } else {
                ui.init_and_update(
                    None,
                    &mut InitContext::new(&mut self.id_count, &mut self.stx, &mut self.ktx),
                );
            }
        }

        self.redraw_all = true;

        self.ui = ui;
    }

    #[must_use]
    pub fn ui<F: Fn(&mut S) -> Box<dyn Widget<S, D>> + 'static>(mut self, ui_builder: F) -> Self {
        self.ui_builder = Some(Box::new(ui_builder));
        self.build();
        self
    }

    /// Builder method that is used to set the on screen keyboard.
    ///
    /// If a keyboard is set each `TextField` that gets focus will open the keyboard.
    #[must_use]
    pub fn keyboard<F, R>(mut self, keyboard_builder: F) -> Self
    where
        R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
        F: Fn(&mut S) -> Box<Keyboard<S, D, R>>,
    {
        self.keyboard = Some(keyboard_builder(self.stx.state_mut()));
        self
    }

    #[must_use]
    pub fn ui_box<F: Fn(&mut S) -> Box<dyn Widget<S, D>> + 'static>(
        mut self,
        ui_builder: Box<F>,
    ) -> Self {
        self.ui_builder = Some(ui_builder);
        self.build();
        self
    }

    /// Builder method used to set the background color of the shell.
    #[must_use]
    pub fn background_color(mut self, background_color: D::Color) -> Self {
        self.background_color = Some(background_color);
        self
    }

    pub fn set_background_color(&mut self, background_color: D::Color) {
        self.background_color = Some(background_color);
    }

    /// Returns a mutable reference of the clipboard context.
    pub fn ctx_mut(&mut self) -> &mut ClipboardContext {
        &mut self.ctx
    }

    fn layout(&mut self) {
        self.dbx.print_pipeline_fn_start("layout");

        let ui = if self.show_keyboard {
            &mut self.keyboard
        } else {
            &mut self.ui
        };

        if let Some(ui) = ui {
            if let Some(display) = &mut self.display {
                ui.layout(LayoutContext::new(display.bounding_box().size, true, true));
            }
        }

        self.dbx.print_pipeline_fn_end("layout");
    }

    fn draw(&mut self) -> Result<(), D::Error> {
        self.dbx.print_pipeline_fn_start("draw");

        if self.redraw_all {
            if let Some(background_color) = self.background_color {
                if let Some(display) = &mut self.display {
                    display.clear(background_color)?;
                }
            }
        }

        let ui = if self.show_keyboard {
            &mut self.keyboard
        } else {
            &mut self.ui
        };

        let ktx = if self.show_keyboard {
            &mut self.keyboard_ktx
        } else {
            &mut self.ktx
        };

        if let Some(ui) = ui {
            if let Some(display) = &mut self.display {
                ui.update_and_draw(
                    Point::default(),
                    &mut DrawContext::new(
                        display,
                        &mut self.stx,
                        ktx,
                        self.redraw_all,
                        &mut self.dbx,
                    ),
                )?;
            }
        }

        ktx.clear_focus();
        self.redraw_all = false;

        self.dbx.print_pipeline_fn_end("draw");

        if let Some(root) = &self.ui {
            self.dbx.print_graph(root.as_ref());
        }

        Ok(())
    }

    fn event(&mut self) -> Result<(), D::Error> {
        self.dbx.print_pipeline_fn_start("event");

        let ktx = if self.show_keyboard {
            &mut self.keyboard_ktx
        } else {
            &mut self.ktx
        };

        while let Some(event) = self.events.dequeue() {
            // active debug output on Ctrl + D or Cmd + D on macOS
            #[cfg(debug_assertions)]
            if let Ok(e) = event.downcast_ref::<KeyEvent>() {
                if e.key() == Key::D && e.pressed() && ktx.is_os_ctrl_pressed() {
                    self.dbx.toggle_active();
                    self.redraw_all = true;
                    self.stx.trigger_update(None);
                    if let Some(root) = &self.ui {
                        self.dbx.print_graph(root.as_ref());
                    }
                }
            }

            let ui = if self.show_keyboard {
                &mut self.keyboard
            } else {
                &mut self.ui
            };

            if let Some(ui) = ui {
                // Update the keyboard state.
                if let Ok(e) = event.downcast_ref::<KeyEvent>() {
                    if ktx.update_keys(e) {
                        self.stx.trigger_render_update(ktx.focused_id());
                    }
                }

                if let Ok(e) = event.downcast_ref::<ControllerButtonEvent>() {
                    if ktx.update_controller_buttons(e) {
                        self.stx.trigger_render_update(ktx.focused_id());
                    }
                }
                ui.event(
                    event.deref(),
                    &mut EventContext::new(&mut self.stx, ktx, &mut self.ctx),
                );
            }
        }

        if self.keyboard.is_some() {
            match ktx.keyboard_request() {
                KeyboardRequest::Open(_, _, _, _) => {
                    if !self.show_keyboard {
                        self.show_keyboard = true;
                        let ktx = &mut self.keyboard_ktx;
                        ktx.keyboard_request = self.ktx.keyboard_request.clone();
                        self.ktx.send_keyboard_request(KeyboardRequest::None);
                        if let Some(keyboard) = &mut self.keyboard {
                            keyboard.init(None, &mut InitContext::new(&mut 0, &mut self.stx, ktx));
                        }

                        ktx.focused_widget = None;

                        self.stx.trigger_layout_update();
                        self.redraw_all = true;
                        self.run()?;
                    }
                }
                KeyboardRequest::Close(text, id, cursor) => {
                    if self.show_keyboard {
                        self.show_keyboard = false;
                        self.events
                            .enqueue(KeyboardEvent::new(text.clone(), *cursor, *id));
                        self.redraw_all = true;
                        self.run()?;
                    }
                }
                _ => {}
            }
        }

        self.dbx.print_pipeline_fn_end("event");

        Ok(())
    }

    pub fn display_mut(&mut self) -> Option<&mut D> {
        if let Some(display) = &mut self.display {
            return Some(display);
        }

        None
    }

    pub fn run(&mut self) -> Result<(), D::Error> {
        self.dbx.print_pipeline_start();
        self.dbx.print_pipeline_fn_start("loop");
        let old_state = self.stx.state().clone();

        self.event()?;
        self.stx.state_mut().update();

        if *self.stx.state() != old_state || self.stx.rebuild() {
            self.stx.reset_rebuild();
            self.build();
            self.stx.trigger_update(None);
        }

        if self.stx.layout_update() {
            self.layout();
        }

        if self.stx.render_update() {
            self.draw()?;
        }

        if self.stx.rebuild() {
            self.run()?;
        }

        self.stx.reset_update();

        self.dbx.print_pipeline_fn_end("loop");
        self.dbx.print_pipeline_end();

        Ok(())
    }
}
