pub mod binary;
pub mod rgb888;

use embedded_graphics::prelude::ImageDrawable;

use crate::{
    eg::{pixelcolor::*, text::renderer::*},
    stdlib::boxed::Box,
    widgets::*,
    DrawTarget,
};

pub fn rgb565_from_rgb888(r: u8, g: u8, b: u8) -> Rgb565 {
    Rgb565::from(Rgb888::new(r, g, b))
}

/// Helper trait that is used to access theme functionality from state.
pub trait ThemeHelper<D, R, T>
where
    D: DrawTarget + 'static,
    R: TextRenderer + CharacterStyle + 'static,
    T: ThemeHelper<D, R, T>,
{
    fn theme(&self) -> &T;

    /// Returns a [`Column`] with default settings.
    fn column<S: 'static>(&self) -> Box<Column<S, D>> {
        Column::new()
    }

    /// Returns a [`Row`] with default settings.
    fn row<S: 'static>(&self) -> Box<Row<S, D>> {
        Row::new()
    }

    /// Returns a [`ConstraintBox`] with default settings.
    fn constraint_box<S: 'static>(&self) -> Box<ConstraintBox<S, D>> {
        ConstraintBox::new()
    }

    /// Returns a [`EventAdapter`] with default settings.
    fn event_adapter<S: 'static>(&self) -> Box<EventAdapter<S, D>> {
        EventAdapter::new()
    }

    /// Returns a [`ImageWidget`] with default settings.
    fn image<S: 'static, I: ImageDrawable>(&self) -> Box<ImageWidget<S, D, I>> {
        ImageWidget::new()
    }

    /// Returns a [`Stretch`] with default settings.
    fn stretch<S: 'static>(&self) -> Box<Stretch<S, D>> {
        Stretch::new()
    }

    /// Returns a [`Spacer`] with default settings.
    fn spacer<S: 'static>(&self) -> Box<Spacer<S, D>> {
        Spacer::new()
    }

    /// Returns a [`Wrap`] with default settings.
    fn wrap<S: 'static>(&self) -> Box<Wrap<S, D>> {
        Wrap::new()
    }

    /// Returns a button with primary button styling (orb_dark).
    fn primary_button<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().primary_button()
    }

    /// Returns a button with secondary button styling (orb_dark).
    fn button<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().button()
    }

    /// Returns a button with dark button styling (orb_dark).
    fn dark_button<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().dark_button()
    }

    /// Returns a button with dark button styling (orb_dark).
    fn dark_button_primary<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().dark_button_primary()
    }

    /// Returns a button with highlight button styling (orb_dark).
    fn highlight_button<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().highlight_button()
    }

    /// Returns a button with top bar styling.
    fn top_bar_button<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().top_bar_button()
    }

    /// Returns a button with primary button styling that stretches vertical and horizontal (morph).
    fn primary_stretch_button<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().primary_stretch_button()
    }

    /// Returns a button with secondary button styling that stretches vertical and horizontal (morph).
    fn secondary_stretch_button<S: 'static>(&self) -> Box<Button<S, D, R>> {
        self.theme().secondary_stretch_button()
    }

    /// Returns a container with default background styling (morph).
    fn background<S: 'static>(&self) -> Box<Container<S, D>>
    where
        D: DrawTarget<Color = Rgb888>,
    {
        self.theme().background()
    }

    /// Returns a container with default content styling (morph).
    fn content_container<S: 'static>(&self) -> Box<Container<S, D>>
    where
        D: DrawTarget<Color = Rgb888>,
    {
        self.theme().content_container()
    }

    /// Returns a container with default side bar styling (morph).
    fn side_bar<S: 'static>(&self) -> Box<Container<S, D>>
    where
        D: DrawTarget<Color = Rgb888>,
    {
        self.theme().side_bar()
    }

    /// Returns a label with body label styling (orb_dark).
    fn label<S: 'static>(&self) -> Box<Label<S, D, R>>
    where
        D: DrawTarget<Color = Rgb888>,
    {
        self.theme().label()
    }

    /// Returns a label with header label styling (orb_dark).
    fn header_label<S: 'static>(&self) -> Box<Label<S, D, R>>
    where
        D: DrawTarget<Color = Rgb888>,
    {
        self.theme().header_label()
    }

    /// Returns a tab widget with default tab widget styling (orb_dark).
    fn tab_widget<S: 'static>(&self) -> Box<TabWidget<S, D, R>> {
        self.theme().tab_widget()
    }

    /// Returns a switch with default tab widget styling (orb_dark).
    fn switch<S: 'static>(&self) -> Box<Switch<S, D>> {
        self.theme().switch()
    }

    /// Returns a text field.
    fn text_field<S: 'static>(&self) -> Box<TextField<S, D, R>> {
        self.theme().text_field()
    }

    /// Returns a text_field that stretches horizontal.
    fn stretch_text_field<S: 'static>(&self) -> Box<TextField<S, D, R>> {
        self.theme().stretch_text_field()
    }

    /// Returns a scroll area with no border and background.
    fn no_border_scroll_area<S: 'static>(&self) -> Box<ScrollArea<S, D>> {
        self.theme().no_border_scroll_area()
    }

    /// Returns a scroll area with default styling.
    fn scroll_area<S: 'static>(&self) -> Box<ScrollArea<S, D>> {
        self.theme().scroll_area()
    }

    /// Returns a default styles segmented widget.
    fn segmented<S: 'static>(&self) -> Box<Segmented<S, D, R>> {
        self.theme().segmented()
    }

    /// Returns a default styled item.
    fn item<S: 'static>(&self) -> Box<Item<S, D>> {
        self.theme().item()
    }

    /// Returns a default styled list.
    fn list<I: Clone + 'static, S: 'static>(&self) -> Box<List<I, S, D, R>> {
        self.theme().list()
    }

    /// Returns a list with no border.
    fn no_border_list<I: Clone + 'static, S: 'static>(&self) -> Box<List<I, S, D, R>> {
        self.theme().no_border_list()
    }

    /// Return a qr code widget with default style.
    fn qr_code<S: 'static>(&self) -> Box<QrCodeWidget<S, D>> {
        self.theme().qr_code()
    }

    /// Return a [`Container`] without background style.
    fn padding<S: 'static>(&self) -> Box<Container<S, D>> {
        Container::new()
    }

    /// Returns a keyboard with styling (orb_dark).
    fn keyboard<S: 'static>(&self, layout: KeyboardLayout) -> Box<Keyboard<S, D, R>> {
        self.theme().keyboard(layout)
    }

    /// Returns a new scope.
    fn scope<S: 'static, I: 'static>(&self, state: I) -> Box<Scope<S, D, I>> {
        Scope::new(state)
    }

    /// Returns a [`Frame`] with default settings and no `break_point`.
    fn frame<S: 'static>(&self) -> Box<Frame<S, D>> {
        Frame::new().navigation_width(240)
    }

    /// Returns a [`TopBar`] with default settings.
    fn top_bar<S: 'static>(&self) -> Box<TopBar<S, D>> {
        self.theme().top_bar().padding(8)
    }
}
