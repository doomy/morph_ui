use crate::{
    eg::{extensions::*, geometry::*, pixelcolor::*, primitives::*},
    font::{FontTextStyle, FontTextStyleBuilder},
    stdlib::boxed::Box,
    themes::ThemeHelper,
    utils::*,
    widgets::*,
    DrawTarget,
};

pub mod material_icons;

use crate::font::FontLoader;

// font sizes
pub const FONT_SIZE_14: u32 = 14;
pub const FONT_SIZE_16: u32 = 16;
pub const FONT_SIZE_20: u32 = 20;

// fonts
pub const FONT_ROBOTO_REGULAR: &str = "Roboto-Regular";
pub const FONT_ROBOTO_MEDIUM: &str = "Roboto-Medium";
pub const FONT_MATERIAL_ICONS: &str = "Material-Icons";

pub type TextStyle = FontTextStyle<Rgb888>;

/// Default widget orb theme of morph_ui with Rgb888 colors.
#[derive(Clone, Debug, PartialEq)]
pub struct MorphTheme {
    // colors
    pub background_color: Rgb888,
    pub top_color: Rgb888,
    pub widget_background_color: Rgb888,
    pub widget_background_dark_color: Rgb888,
    pub widget_background_hover_color: Rgb888,
    pub widget_background_active_color: Rgb888,
    pub widget_background_disabled_color: Rgb888,
    pub widget_background_highlight_color: Rgb888,
    pub widget_background_highlight_hover_color: Rgb888,
    pub widget_background_highlight_active_color: Rgb888,
    pub widget_foreground_color: Rgb888,
    pub widget_foreground_active_color: Rgb888,
    pub widget_foreground_hover_color: Rgb888,
    pub widget_foreground_disabled_color: Rgb888,
    pub widget_placeholder_color: Rgb888,
    pub widget_stroke_color: Rgb888,
    pub accent_color: Rgb888,
    pub font_loader: FontLoader,
}

impl Default for MorphTheme {
    fn default() -> Self {
        let mut font_loader = FontLoader::new();

        font_loader
            .load_font_from_bytes(
                FONT_ROBOTO_REGULAR,
                include_bytes!("../../../../assets/Roboto-Regular.ttf"),
            )
            .unwrap();

        font_loader
            .load_font_from_bytes(
                FONT_ROBOTO_MEDIUM,
                include_bytes!("../../../../assets/Roboto-Medium.ttf"),
            )
            .unwrap();
        font_loader
            .load_font_from_bytes(
                FONT_MATERIAL_ICONS,
                include_bytes!("../../../../assets/MaterialIcons.ttf"),
            )
            .unwrap();

        MorphTheme {
            background_color: Rgb888::new(27, 34, 36),
            top_color: Rgb888::new(20, 26, 27),
            widget_background_color: Rgb888::new(38, 48, 52),
            widget_background_dark_color: Rgb888::new(15, 19, 21),
            widget_background_hover_color: Rgb888::new(56, 70, 75),
            widget_background_active_color: Rgb888::new(22, 160, 133),
            widget_background_disabled_color: Rgb888::new(36, 46, 45),
            widget_foreground_color: Rgb888::new(255, 255, 255),
            widget_background_highlight_color: Rgb888::new(219, 91, 91),
            widget_background_highlight_hover_color: Rgb888::new(233, 111, 111),
            widget_background_highlight_active_color: Rgb888::new(228, 132, 132),
            widget_foreground_active_color: Rgb888::new(255, 255, 255),
            widget_foreground_hover_color: Rgb888::new(255, 255, 255),
            widget_foreground_disabled_color: Rgb888::new(111, 120, 115),
            widget_placeholder_color: Rgb888::new(168, 168, 168),
            widget_stroke_color: Rgb888::new(14, 17, 18),
            accent_color: Rgb888::new(50, 178, 99),
            font_loader,
        }
    }
}

impl MorphTheme {
    /// Creates a new MorphTheme with dark settings.
    pub fn new() -> Self {
        Self::default()
    }

    // todo light theme
}

impl<D> ThemeHelper<D, FontTextStyle<Rgb888>, MorphTheme> for MorphTheme
where
    D: DrawTarget<Color = Rgb888> + 'static,
{
    /// Returns a button with primary button styling (orb_dark).
    fn primary_button<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        Button::new()
            .focusable(true)
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .constraints(Constraints::create().height(32))
            .corners(CornerRadii::new(Size::new(1, 1)))
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.accent_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_disabled_color)
                    .build(),
            )
            .style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_active_color)
                    .background_color(self.accent_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_hover_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Disabled,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_disabled_color)
                    .background_color(self.widget_background_disabled_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Checked,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    fn button<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        self.primary_button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
    }

    fn dark_button<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        self.button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_dark_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_dark_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_hover_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Disabled,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_disabled_color)
                    .background_color(self.widget_background_disabled_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Checked,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    /// Returns a button with primary button styling (orb_dark).
    fn dark_button_primary<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        self.primary_button()
            .text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_active_color)
                    .background_color(self.accent_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_hover_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Disabled,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_disabled_color)
                    .background_color(self.widget_background_disabled_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Checked,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    fn highlight_button<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        self.primary_button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_highlight_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_highlight_color)
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_highlight_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_highlight_hover_color)
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_highlight_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_highlight_active_color)
                    .build(),
            )
    }

    fn top_bar_button<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        self.primary_button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.top_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.top_color)
                    .build(),
            )
    }

    fn primary_stretch_button<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        self.primary_button()
            .constraints(Constraints::default())
            .corners(CornerRadii::default())
    }

    fn secondary_stretch_button<S: 'static>(&self) -> Box<Button<S, D, FontTextStyle<Rgb888>>> {
        self.button()
            .constraints(Constraints::default())
            .corners(CornerRadii::default())
    }

    fn background<S: 'static>(&self) -> Box<Container<S, D>> {
        Container::new().style(
            PrimitiveStyleBuilder::new()
                .fill_color(self.background_color)
                .build(),
        )
    }

    fn content_container<S: 'static>(&self) -> Box<Container<S, D>> {
        Container::new().padding(8).style(
            PrimitiveStyleBuilder::new()
                .fill_color(self.background_color)
                .build(),
        )
    }

    fn side_bar<S: 'static>(&self) -> Box<Container<S, D>> {
        Container::new().padding(8).style(
            PrimitiveStyleBuilder::new()
                .stroke_width(1)
                .stroke_color(self.widget_stroke_color)
                .fill_color(self.background_color)
                .build(),
        )
    }

    fn label<S: 'static>(&self) -> Box<Label<S, D, FontTextStyle<Rgb888>>> {
        Label::new().style(
            FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                .font_size(FONT_SIZE_20)
                .text_color(self.widget_foreground_active_color)
                .build(),
        )
    }

    fn header_label<S: 'static>(&self) -> Box<Label<S, D, FontTextStyle<Rgb888>>> {
        Label::new().style(
            FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                .font_size(FONT_SIZE_20)
                .text_color(self.widget_foreground_active_color)
                .build(),
        )
    }

    fn tab_widget<S: 'static>(&self) -> Box<TabWidget<S, D, FontTextStyle<Rgb888>>> {
        TabWidget::new()
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_active_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Pressed,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Hover,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Checked,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_20)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    fn switch<S: 'static>(&self) -> Box<Switch<S, D>> {
        Switch::new()
            .focusable(true)
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .padding(2)
            .constraints(Constraints::create().width(40).height(20))
            .corners(CornerRadii::new(Size::new(2, 2)))
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_width(1)
                    .stroke_color(Rgb888::new(203, 203, 203))
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_width(1)
                    .stroke_color(Rgb888::new(203, 203, 203))
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_foreground_color)
                    .build(),
            )
            .style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.accent_color)
                    .build(),
            )
            .style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_background_disabled_color)
                    .build(),
            )
            .toggle_style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_disabled_color)
                    .build(),
            )
            .toggle_style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_foreground_color)
                    .build(),
            )
    }

    fn text_field<S: 'static>(&self) -> Box<TextField<S, D, FontTextStyle<Rgb888>>> {
        TextField::new()
            .focusable(true)
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .constraints(Constraints::create().height(32).width(120))
            .corners(CornerRadii::new(Size::new(1, 1)))
            .style(
                TextFieldVisual::Default,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                TextFieldVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_foreground_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                TextFieldVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_background_active_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                TextFieldVisual::Focused,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .cursor_style(
                TextFieldVisual::Focused,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.accent_color)
                    .build(),
            )
            .text_style(
                TextFieldVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_color)
                    // .background_color(self.widget_background_color)
                    .build(),
            )
            .text_style(
                TextFieldVisual::Focused,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_color)
                    .build(),
            )
            .placeholder_style(
                FontTextStyleBuilder::new(self.font_loader.font(FONT_ROBOTO_REGULAR).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_placeholder_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
    }

    fn stretch_text_field<S: 'static>(&self) -> Box<TextField<S, D, FontTextStyle<Rgb888>>> {
        self.text_field()
            .constraints(Constraints::create().height(32).min_width(120))
    }

    fn no_border_scroll_area<S: 'static>(&self) -> Box<ScrollArea<S, D>> {
        ScrollArea::new()
            .constraints(ConstraintsBuilder::new().min_width(100).min_height(100))
            .scroll_bar_size(8)
            .scroll_bar_margin(2)
            .scroll_bar_corners(CornerRadii::new(Size::new(4, 4)))
            .scroll_bar_style(
                ScrollBarVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_foreground_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
    }

    fn scroll_area<S: 'static>(&self) -> Box<ScrollArea<S, D>> {
        self.no_border_scroll_area()
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_color(self.widget_stroke_color)
                    .stroke_width(1)
                    .build(),
            )
            .corners(CornerRadii::new(Size::new(2, 2)))
    }

    fn segmented<S: 'static>(&self) -> Box<Segmented<S, D, FontTextStyle<Rgb888>>> {
        Segmented::new()
            .padding(4)
            .height(32)
            .corners(CornerRadii::new(Size::new(2, 2)))
            .segment_corners(CornerRadii::new(Size::new(2, 2)))
            .style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
            .segment_text_style(
                ButtonVisual::Default,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_active_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
            .segment_text_style(
                ButtonVisual::Pressed,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .segment_text_style(
                ButtonVisual::Hover,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .segment_text_style(
                ButtonVisual::Checked,
                FontTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
                    .font_size(FONT_SIZE_16)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    fn item<S: 'static>(&self) -> Box<Item<S, D>> {
        Item::new()
            .focusable(true)
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .padding(8)
            .constraints(Constraints::create().min_width(32).min_height(32))
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_disabled_color)
                    .build(),
            )
            .style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
    }

    fn list<I: Clone + 'static, S: 'static>(&self) -> Box<List<I, S, D, FontTextStyle<Rgb888>>> {
        self.no_border_list()
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_color(self.widget_stroke_color)
                    .stroke_width(1)
                    .build(),
            )
            .corners(CornerRadii::new(Size::new(2, 2)))
    }

    fn no_border_list<I: Clone + 'static, S: 'static>(
        &self,
    ) -> Box<List<I, S, D, FontTextStyle<Rgb888>>> {
        List::new()
            .padding(2)
            .style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(0)
                    .stroke_color(Rgb888::GREEN)
                    .fill_color(self.background_color)
                    .build(),
            )
            .constraints(ConstraintsBuilder::new().min_width(100).min_height(100))
            .scroll_bar_size(8)
            .scroll_bar_margin(2)
            .scroll_bar_corners(CornerRadii::new(Size::new(4, 4)))
            .scroll_bar_style(
                ScrollBarVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_foreground_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
    }

    fn qr_code<S: 'static>(&self) -> Box<QrCodeWidget<S, D>> {
        QrCodeWidget::new().style(QrCodeStyle {
            fill_color: Some(Rgb888::WHITE),
            color: Some(Rgb888::BLACK),
        })
    }

    /// Returns a keyboard with styling (orb_dark).
    fn keyboard<S: 'static>(
        &self,
        layout: KeyboardLayout,
    ) -> Box<Keyboard<S, D, FontTextStyle<Rgb888>>> {
        let theme = self.clone();

        let mut key_board = Keyboard::new()
            .layout(layout)
            .padding(8)
            .spacing(4)
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .build(),
            )
            .key_builder(move |key_model, caps_lock| {
                theme
                    .button()
                    .text(key_model.value(caps_lock).to_string().as_str())
                    .on_tap(move |state: &mut KeyboardState| state.key_model_action(key_model))
            });

        {
            let mut layout = key_board.get_layout_mut();
            layout.caps_lock_text = String::from(material_icons::MD_ARROW_UPWARD);
            layout.back_space_text = String::from(material_icons::MD_KEYBOARD_BACKSPACE);
            layout.space_text = String::from(material_icons::MD_SPACE_BAR);
            layout.enter_text = String::from(material_icons::MD_KEYBOARD_RETURN);
            layout.arrow_right_text = String::from(material_icons::MD_KEYBOARD_ARROW_RIGHT);
            layout.arrow_left_text = String::from(material_icons::MD_KEYBOARD_ARROW_LEFT);
            layout.toggle_type_text = String::from(material_icons::MD_KEYBOARD);
        }

        let theme = self.clone();

        key_board = key_board.op_key_builder(move |text, primary| {
            if primary {
                theme.dark_button_primary().text(text)
            } else {
                theme.dark_button().text(text)
            }
        });

        let theme = self.clone();

        key_board.text_field_builder(move |text| theme.text_field().text(text))
    }

    fn top_bar<S: 'static>(&self) -> Box<TopBar<S, D>> {
        TopBar::new().style(
            PrimitiveStyleBuilder::new()
                .fill_color(self.top_color)
                .build(),
        )
    }

    fn theme(&self) -> &MorphTheme {
        self
    }
}
