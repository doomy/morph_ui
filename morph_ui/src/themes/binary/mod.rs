use embedded_graphics::mono_font::MonoTextStyleBuilder;

#[cfg(not(feature = "std"))]
use crate::stdlib::*;

use crate::{
    eg::{
        extensions::*,
        geometry::*,
        mono_font::{ascii::*, MonoTextStyle},
        pixelcolor::*,
        primitives::*,
    },
    themes::ThemeHelper,
    utils::*,
    widgets::{button::*, scroll_area::*, text_field::*, *},
    DrawTarget,
};

/// Default widget orb theme of morph_ui with BinaryColor colors.
#[derive(Debug, PartialEq, Clone)]
pub struct MorphTheme {
    // colors
    pub background_color: BinaryColor,
    pub top_color: BinaryColor,
    pub widget_background_color: BinaryColor,
    pub widget_background_dark_color: BinaryColor,
    pub widget_background_hover_color: BinaryColor,
    pub widget_background_active_color: BinaryColor,
    pub widget_background_disabled_color: BinaryColor,
    pub widget_background_highlight_color: BinaryColor,
    pub widget_background_highlight_hover_color: BinaryColor,
    pub widget_background_highlight_active_color: BinaryColor,
    pub widget_foreground_color: BinaryColor,
    pub widget_foreground_active_color: BinaryColor,
    pub widget_foreground_hover_color: BinaryColor,
    pub widget_foreground_disabled_color: BinaryColor,
    pub widget_placeholder_color: BinaryColor,
    pub widget_stroke_color: BinaryColor,
    pub accent_color: BinaryColor,
}

impl Default for MorphTheme {
    fn default() -> Self {
        MorphTheme {
            background_color: BinaryColor::On,
            top_color: BinaryColor::On,
            widget_background_color: BinaryColor::On,
            widget_background_dark_color: BinaryColor::On,
            widget_background_hover_color: BinaryColor::On,
            widget_background_disabled_color: BinaryColor::On,
            widget_foreground_color: BinaryColor::Off,
            widget_background_highlight_color: BinaryColor::On,
            widget_background_highlight_hover_color: BinaryColor::On,
            widget_background_highlight_active_color: BinaryColor::On,
            widget_foreground_active_color: BinaryColor::Off,
            widget_foreground_hover_color: BinaryColor::Off,
            widget_foreground_disabled_color: BinaryColor::Off,
            widget_placeholder_color: BinaryColor::Off,
            widget_stroke_color: BinaryColor::Off,
            accent_color: BinaryColor::Off,
            widget_background_active_color: BinaryColor::On,
        }
    }
}

impl MorphTheme {
    /// Creates a new MorphTheme with binary settings.

    pub fn new() -> Self {
        Self::default()
    }

    // todo light theme
}

impl<D> ThemeHelper<D, MonoTextStyle<'static, BinaryColor>, MorphTheme> for MorphTheme
where
    D: DrawTarget<Color = BinaryColor> + 'static,
{
    /// Returns a button with primary button styling (orb_dark).
    fn primary_button<S: 'static>(&self) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        Button::new()
            .focusable(true)
            .constraints(Constraints::create().height(32))
            .corners(CornerRadii::new(Size::new(1, 1)))
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.accent_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_disabled_color)
                    .build(),
            )
            .style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_active_color)
                    .background_color(self.accent_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_hover_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Disabled,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_disabled_color)
                    .background_color(self.widget_background_disabled_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Checked,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    fn button<S: 'static>(&self) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.primary_button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
    }

    /// Returns a button with dark button styling (orb_dark).
    fn dark_button<S: 'static>(&self) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_dark_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_dark_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_hover_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Disabled,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_disabled_color)
                    .background_color(self.widget_background_disabled_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Checked,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    /// Returns a button with dark button styling (orb_dark).
    fn dark_button_primary<S: 'static>(
        &self,
    ) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.primary_button()
            .text_style(
                ButtonVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_active_color)
                    .background_color(self.accent_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_hover_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Disabled,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_disabled_color)
                    .background_color(self.widget_background_disabled_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Checked,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    fn highlight_button<S: 'static>(
        &self,
    ) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.primary_button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_highlight_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_highlight_color)
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_highlight_hover_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Hover,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_highlight_hover_color)
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_highlight_active_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Pressed,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_highlight_active_color)
                    .build(),
            )
    }

    fn top_bar_button<S: 'static>(&self) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.primary_button()
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.top_color)
                    .build(),
            )
            .text_style(
                ButtonVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.top_color)
                    .build(),
            )
    }

    fn primary_stretch_button<S: 'static>(
        &self,
    ) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.primary_button()
            .constraints(Constraints::default())
            .corners(CornerRadii::default())
    }

    fn secondary_stretch_button<S: 'static>(
        &self,
    ) -> Box<Button<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.button()
            .constraints(Constraints::default())
            .corners(CornerRadii::default())
    }

    fn background<S: 'static>(&self) -> Box<Container<S, D>> {
        Container::new().style(
            PrimitiveStyleBuilder::new()
                .fill_color(self.background_color)
                .build(),
        )
    }

    fn content_container<S: 'static>(&self) -> Box<Container<S, D>> {
        Container::new().padding(8).style(
            PrimitiveStyleBuilder::new()
                .fill_color(self.background_color)
                .build(),
        )
    }

    fn side_bar<S: 'static>(&self) -> Box<Container<S, D>> {
        Container::new().padding(8).style(
            PrimitiveStyleBuilder::new()
                .stroke_width(1)
                .stroke_color(self.widget_stroke_color)
                .fill_color(self.background_color)
                .build(),
        )
    }

    fn label<S: 'static>(&self) -> Box<Label<S, D, MonoTextStyle<'static, BinaryColor>>> {
        Label::new().style(
            MonoTextStyleBuilder::new()
                .font(&FONT_7X13)
                .text_color(self.widget_foreground_active_color)
                .build(),
        )
    }

    fn header_label<S: 'static>(&self) -> Box<Label<S, D, MonoTextStyle<'static, BinaryColor>>> {
        Label::new().style(
            MonoTextStyleBuilder::new()
                .font(&FONT_7X13)
                .text_color(self.widget_foreground_active_color)
                .build(),
        )
    }

    fn tab_widget<S: 'static>(&self) -> Box<TabWidget<S, D, MonoTextStyle<'static, BinaryColor>>> {
        TabWidget::new()
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .build(),
            )
            .tab_style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_active_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Pressed,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_active_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Hover,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_hover_color)
                    .build(),
            )
            .tab_text_style(
                ButtonVisual::Checked,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .background_color(self.widget_background_active_color)
                    .text_color(self.widget_foreground_active_color)
                    .build(),
            )
    }

    fn switch<S: 'static>(&self) -> Box<Switch<S, D>> {
        Switch::new()
            .focusable(true)
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .padding(2)
            .constraints(Constraints::create().width(40).height(20))
            .corners(CornerRadii::new(Size::new(2, 2)))
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_foreground_color)
                    .build(),
            )
            .style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.accent_color)
                    .build(),
            )
            .style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_background_disabled_color)
                    .build(),
            )
            .toggle_style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_disabled_color)
                    .build(),
            )
            .toggle_style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_foreground_color)
                    .build(),
            )
    }

    fn text_field<S: 'static>(&self) -> Box<TextField<S, D, MonoTextStyle<'static, BinaryColor>>> {
        TextField::new()
            .focusable(true)
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .constraints(Constraints::create().height(32).width(120))
            .corners(CornerRadii::new(Size::new(1, 1)))
            .style(
                TextFieldVisual::Default,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                TextFieldVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_foreground_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                TextFieldVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_background_active_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                TextFieldVisual::Focused,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .cursor_style(
                TextFieldVisual::Focused,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.accent_color)
                    .build(),
            )
            .text_style(
                TextFieldVisual::Default,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X14)
                    .text_color(self.widget_foreground_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
            .text_style(
                TextFieldVisual::Focused,
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X14)
                    .text_color(self.widget_foreground_color)
                    .build(),
            )
            .placeholder_style(
                MonoTextStyleBuilder::new()
                    .font(&FONT_7X14)
                    .text_color(self.widget_placeholder_color)
                    .background_color(self.widget_background_color)
                    .build(),
            )
    }

    fn stretch_text_field<S: 'static>(
        &self,
    ) -> Box<TextField<S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.text_field()
            .constraints(Constraints::create().height(32).min_width(120))
    }

    fn no_border_scroll_area<S: 'static>(&self) -> Box<ScrollArea<S, D>> {
        ScrollArea::new()
            .constraints(ConstraintsBuilder::new().min_width(100).min_height(100))
            .scroll_bar_size(8)
            .scroll_bar_margin(2)
            .scroll_bar_corners(CornerRadii::new(Size::new(4, 4)))
            .scroll_bar_style(
                ScrollBarVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_foreground_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
    }

    fn scroll_area<S: 'static>(&self) -> Box<ScrollArea<S, D>> {
        self.no_border_scroll_area()
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_color(self.widget_stroke_color)
                    .stroke_width(1)
                    .build(),
            )
            .corners(CornerRadii::new(Size::new(2, 2)))
    }

    fn segmented<S: 'static>(&self) -> Box<Segmented<S, D, MonoTextStyle<'static, BinaryColor>>> {
        Segmented::new()
            .padding(4)
            .height(32)
            .corners(CornerRadii::new(Size::new(2, 2)))
            .segment_corners(CornerRadii::new(Size::new(2, 2)))
            .style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .build(),
            )
            .segment_style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
        // .segment_text_style(
        //     ButtonVisual::Default,
        //     MonoTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
        //         .font(&FONT_7X14)
        //         .text_color(self.widget_foreground_active_color)
        //         .background_color(self.widget_background_color)
        //         .build(),
        // )
        // .segment_text_style(
        //     ButtonVisual::Pressed,
        //     MonoTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
        //         .font(&FONT_7X14)
        //         .text_color(self.widget_foreground_color)
        //         .background_color(self.widget_background_active_color)
        //         .build(),
        // )
        // .segment_text_style(
        //     ButtonVisual::Hover,
        //     MonoTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
        //         .font(&FONT_7X14)
        //         .text_color(self.widget_foreground_color)
        //         .background_color(self.widget_background_hover_color)
        //         .build(),
        // )
        // .segment_text_style(
        //     ButtonVisual::Checked,
        //     MonoTextStyleBuilder::new(self.font_loader.font(FONT_MATERIAL_ICONS).unwrap())
        //         .font(&FONT_7X14)
        //         .background_color(self.widget_background_active_color)
        //         .text_color(self.widget_foreground_active_color)
        //         .build(),
        // )
    }

    fn item<S: 'static>(&self) -> Box<Item<S, D>> {
        Item::new()
            .focusable(true)
            .focus_style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(1)
                    .stroke_color(self.accent_color)
                    .build(),
            )
            .padding(8)
            .constraints(Constraints::create().min_width(32).min_height(32))
            .style(
                ButtonVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .style(
                ButtonVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_hover_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
            .style(
                ButtonVisual::Disabled,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_disabled_color)
                    .build(),
            )
            .style(
                ButtonVisual::Checked,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .stroke_width(1)
                    .stroke_color(self.widget_stroke_color)
                    .build(),
            )
    }

    fn list<I: Clone + 'static, S: 'static>(
        &self,
    ) -> Box<List<I, S, D, MonoTextStyle<'static, BinaryColor>>> {
        self.no_border_list()
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .stroke_color(self.widget_stroke_color)
                    .stroke_width(1)
                    .build(),
            )
            .corners(CornerRadii::new(Size::new(2, 2)))
    }

    fn no_border_list<I: Clone + 'static, S: 'static>(
        &self,
    ) -> Box<List<I, S, D, MonoTextStyle<'static, BinaryColor>>> {
        List::new()
            .padding(2)
            .style(
                PrimitiveStyleBuilder::new()
                    .stroke_width(0)
                    .stroke_color(BinaryColor::On)
                    .fill_color(self.background_color)
                    .build(),
            )
            .constraints(ConstraintsBuilder::new().min_width(100).min_height(100))
            .scroll_bar_size(8)
            .scroll_bar_margin(2)
            .scroll_bar_corners(CornerRadii::new(Size::new(4, 4)))
            .scroll_bar_style(
                ScrollBarVisual::Default,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Hover,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_foreground_color)
                    .build(),
            )
            .scroll_bar_style(
                ScrollBarVisual::Pressed,
                PrimitiveStyleBuilder::new()
                    .fill_color(self.widget_background_active_color)
                    .build(),
            )
    }

    fn qr_code<S: 'static>(&self) -> Box<QrCodeWidget<S, D>> {
        QrCodeWidget::new().style(QrCodeStyle {
            fill_color: Some(BinaryColor::Off),
            color: Some(BinaryColor::On),
        })
    }

    /// Returns a keyboard with styling (orb_dark).
    fn keyboard<S: 'static>(
        &self,
        layout: KeyboardLayout,
    ) -> Box<Keyboard<S, D, MonoTextStyle<'static, BinaryColor>>> {
        let theme = self.clone();

        let mut key_board = Keyboard::new()
            .layout(layout)
            .padding(8)
            .spacing(4)
            .style(
                PrimitiveStyleBuilder::new()
                    .fill_color(self.background_color)
                    .build(),
            )
            .key_builder(move |key_model, caps_lock| {
                theme
                    .button()
                    .text(String::from(key_model.value(caps_lock)).as_str())
                    .on_tap(move |state: &mut KeyboardState| state.key_model_action(key_model))
            });

        let theme = self.clone();

        key_board = key_board.op_key_builder(move |text, primary| {
            if primary {
                theme.dark_button_primary().text(text)
            } else {
                theme.dark_button().text(text)
            }
        });

        let theme = self.clone();

        key_board.text_field_builder(move |text| theme.text_field().text(text))
    }

    fn top_bar<S: 'static>(&self) -> Box<TopBar<S, D>> {
        TopBar::new().style(
            PrimitiveStyleBuilder::new()
                .fill_color(self.top_color)
                .build(),
        )
    }

    fn theme(&self) -> &MorphTheme {
        self
    }
}
