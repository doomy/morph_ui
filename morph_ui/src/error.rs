/// Represents orbtk OrbClient specific errors.
#[derive(Clone, Debug)]
pub enum Error {
    /// Describes an error while running the shell.
    ShellRunError,
    /// Cannot load a font.
    CannotLoadFont,
    /// Cannot create qr code.
    QrCodeError,
}
