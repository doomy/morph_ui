use rusttype::Font;

use crate::{error::Error, stdlib::collections::BTreeMap};

#[cfg(not(feature = "std"))]
use crate::stdlib::*;

/// `FontLoader` is used to load, store and get fonts.
#[derive(Debug, Default, Clone)]
pub struct FontLoader {
    fonts: BTreeMap<String, Font<'static>>,
}

impl PartialEq for FontLoader {
    fn eq(&self, _: &Self) -> bool {
        true
    }
}

impl FontLoader {
    /// Creates a new font loader.
    pub fn new() -> Self {
        Self {
            fonts: BTreeMap::new(),
        }
    }

    /// Loads a font from the given byte array.
    pub fn load_font_from_bytes(
        &mut self,
        name: impl Into<String>,
        bytes: &'static [u8],
    ) -> Result<(), Error> {
        if let Some(font) = Font::try_from_bytes(bytes) {
            self.fonts.insert(name.into(), font);
            return Ok(());
        }

        Err(Error::CannotLoadFont)
    }

    /// Inserts a new font.
    pub fn insert(&mut self, name: impl Into<String>, font: Font<'static>) {
        self.fonts.insert(name.into(), font);
    }

    /// Gets a font for the given font family name.
    pub fn font(&self, family: &str) -> Option<Font<'static>> {
        self.fonts.get(family).cloned()
    }
}
