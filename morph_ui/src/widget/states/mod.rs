//! This module contains generic states that can be used by different widgets.

mod selection_state;

pub use selection_state::*;
