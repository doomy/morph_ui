use crate::stdlib::{vec::*, *};

/// This enum is used to define the selection mode of a widget (None | Single | Multiple).
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum SelectionMode {
    /// Nothing can be selected.
    None,

    /// Only one item can be selected.
    Single,

    /// Multiple items can be deselected.
    Multiple,
}

impl Default for SelectionMode {
    fn default() -> Self {
        SelectionMode::Single
    }
}

impl From<&str> for SelectionMode {
    fn from(s: &str) -> Self {
        match s {
            "none" | "None" => SelectionMode::None,
            "multiple" | "Multiple" => SelectionMode::Multiple,
            _ => SelectionMode::Single,
        }
    }
}

impl From<String> for SelectionMode {
    fn from(s: String) -> Self {
        SelectionMode::from(s.as_str())
    }
}

/// Used to handle the selection of a widget.
#[derive(Default, Clone, Debug, PartialEq)]
pub struct SelectionState {
    mode: SelectionMode,
    selected_indexes: Vec<usize>,
    old_selected_indexes: Vec<usize>,
}

impl SelectionState {
    /// Creates a new selection state with the given mode.
    pub fn new(mode: SelectionMode) -> Self {
        SelectionState {
            mode,
            selected_indexes: vec![],
            old_selected_indexes: vec![],
        }
    }

    /// Creates a new selection an initial add 0 to selection.
    pub fn from_first_selection(mode: SelectionMode) -> Self {
        SelectionState {
            mode,
            selected_indexes: vec![0],
            old_selected_indexes: vec![],
        }
    }

    pub fn get_selection_mode(&self) -> SelectionMode {
        self.mode
    }

    pub fn set_selection_mode(&mut self, mode: impl Into<SelectionMode>) {
        self.mode = mode.into();
    }

    /// Returns `true`if the given index is selected otherwise `false`.
    pub fn is_selected(&self, index: usize) -> bool {
        self.selected_indexes.contains(&index)
    }

    /// Select the given index if selection mode is `SelectionMode::Single` or `SelectionMode::Multiple`.
    pub fn select(&mut self, index: impl Into<Option<usize>>) {
        if let Some(index) = index.into() {
            match self.mode {
                SelectionMode::None => return,
                SelectionMode::Single => {
                    if self.selected_indexes.contains(&index) {
                        return;
                    }
                    self.old_selected_indexes.append(&mut self.selected_indexes);
                }
                _ => (),
            }

            self.selected_indexes.push(index);
            return;
        }

        self.selected_indexes.clear();
    }

    /// Returns the first selected index.
    pub fn get_selected_index(&self) -> Option<usize> {
        self.selected_indexes.get(0).copied()
    }

    // fn selected_indexes(&self) ->

    /// Remote all old selected indexes and return them.
    pub fn remove_old_selected_indexes(&mut self) -> Vec<usize> {
        let mut vec = vec![];
        vec.append(&mut self.old_selected_indexes);
        vec
    }

    /// Clears all selected and old selected indexes
    pub fn clear(&mut self) {
        self.selected_indexes.clear();
        self.old_selected_indexes.clear();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_str() {
        let none = SelectionMode::from("none");
        assert_eq!(none, SelectionMode::None);

        let none = SelectionMode::from("None");
        assert_eq!(none, SelectionMode::None);

        let multiple = SelectionMode::from("multiple");
        assert_eq!(multiple, SelectionMode::Multiple);

        let multiple = SelectionMode::from("Multiple");
        assert_eq!(multiple, SelectionMode::Multiple);

        let single = SelectionMode::from("single");
        assert_eq!(single, SelectionMode::Single);

        let single = SelectionMode::from("Single");
        assert_eq!(single, SelectionMode::Single);

        let single = SelectionMode::from("abc");
        assert_eq!(single, SelectionMode::Single);
    }

    #[test]
    fn test_select() {
        let mut state = SelectionState::new(SelectionMode::None);
        state.select(Some(0));
        assert!(!state.is_selected(0));

        let mut state = SelectionState::new(SelectionMode::Single);
        state.select(Some(0));
        assert!(state.is_selected(0));
        state.select(Some(1));
        assert!(state.remove_old_selected_indexes().contains(&0));
        assert!(!state.is_selected(0));
        assert!(state.is_selected(1));

        let mut state = SelectionState::new(SelectionMode::Multiple);
        state.select(Some(0));
        state.select(Some(1));
        assert!(state.is_selected(0));
        assert!(state.is_selected(1));
    }
}
