use crate::eg::primitives::Rectangle;

/// Represents the base collection of widget properties.
///
/// This struct is internal used by the [`Widget`] trait.
#[derive(Copy, Clone, Default, Debug, PartialEq)]
pub struct WidgetBase {
    pub(crate) id: u32,
    pub bounding_box: Rectangle,
    pub is_focusable: bool,
    pub has_focus: bool,
    pub stretch: u32,
}

impl WidgetBase {
    /// Creates a new widget base with default settings.
    pub fn new() -> Self {
        WidgetBase::default()
    }

    /// Returns the id of the widget.
    pub fn get_id(&self) -> u32 {
        self.id
    }
}
