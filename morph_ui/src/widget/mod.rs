use crate::{
    downcast::Any,
    eg::{geometry::*, primitives::Rectangle},
    stdlib::{vec::*, *},
    *,
};

mod helpers;
pub mod states;
mod widget_base;

pub use helpers::*;
pub use widget_base::*;

use downcast::downcast;

/// This trait is needed to implement a widget.
pub trait Widget<S, D>: Any
where
    S: 'static,
    D: DrawTarget + 'static,
{
    /// Returns a reference of the widget base.
    fn base(&self) -> &WidgetBase;

    /// Returns a mutable reference of the widget base.
    fn base_mut(&mut self) -> &mut WidgetBase;

    /// Returns the id of the widget.
    fn id(&self) -> u32 {
        self.base().id
    }

    /// If the id of the widget is not set a new will be generated and set.
    fn init_id(&mut self, itx: &mut InitContext<S>) {
        if self.base().id == 0 {
            self.base_mut().id = itx.get_id();
        }
    }

    /// Returns the bounding box of the widget.
    fn bounding_box(&self) -> Rectangle {
        self.base().bounding_box
    }

    /// Returns a mutable reference of the bounding box of the widget.
    fn bounding_box_mut(&mut self) -> &mut Rectangle {
        &mut self.base_mut().bounding_box
    }

    /// Returns the stretch value of the widget. The stretch value is used by a `Stretch` parent layout.
    fn stretch(&self) -> u32 {
        self.base().stretch
    }

    /// Updates the position of the widget and draws it.
    fn update_and_draw(
        &mut self,
        parent_position: Point,
        dtx: &mut DrawContext<D, S>,
    ) -> Result<(), D::Error> {
        if !self.base().has_focus && dtx.ktx().has_focus(self.id()) {
            self.base_mut().has_focus = true;
            dtx.stx_mut().trigger_render_update(Some(self.id()));
        }

        if self.base().has_focus && dtx.ktx_mut().lost_focus(self.id()) {
            self.base_mut().has_focus = false;
            dtx.stx_mut().trigger_render_update(Some(self.id()));
        }

        if dtx.stx().layout_update() {
            self.bounding_box_mut().top_left += parent_position;
        }

        // if the bounding box is outside of the clip box the widget will not be drawn.
        if let Some(clip_box) = dtx.clip_box() {
            if self.bounding_box().intersection(&clip_box) == Rectangle::zero() {
                return Ok(());
            }
        }

        self.draw(dtx)?;

        self.draw_debug(dtx)
    }

    #[cfg(debug_assertions)]
    fn draw_debug(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let dtx = dtx.all();
        dtx.3.draw_widget_raster::<D, S>(self.bounding_box(), dtx.0)
    }

    #[cfg(not(debug_assertions))]
    fn draw_debug(&mut self, _: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        Ok(())
    }

    /// Draws the widget.
    fn draw(&mut self, _dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        Ok(())
    }

    /// Calculates the layout of the widget and returns its desired size.
    fn layout(&mut self, ltx: LayoutContext) -> Size;

    /// Implement this to process events on the widget.
    fn event(&mut self, _event: &dyn Any, _etx: &mut EventContext<S>) {}

    /// Used to initializes the widget from its origin (former instance).
    fn init(&mut self, _origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        self.init_id(itx);
    }

    /// Transfer data from old instance to the given new instance.
    fn transfer(&self, _new: &mut dyn Any, _itx: &mut InitContext<S>) {}

    fn init_and_update(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        // prevent to use the wrong origin (check by type)
        let origin = if let Some(origin) = origin {
            if origin.type_id() == self.type_id() {
                Some(origin)
            } else {
                None
            }
        } else {
            None
        };

        self.init(origin, itx);

        if self.base().is_focusable
            && !itx.ktx().focusable_widgets.contains(&self.id())
            && !self.is_disabled()
        {
            itx.ktx_mut().focusable_widgets.push(self.id());
        }

        if self.base().has_focus {
            itx.ktx_mut()
                .send_focus_request(FocusRequest::Focus(self.id()));
        }

        self.init_id(itx);
    }

    /// Returns the top left position of the widget.
    #[inline]
    fn top_left(&self) -> Point {
        self.bounding_box().top_left
    }

    /// Sets the top left position of the widget.
    #[inline]
    fn set_top_left(&mut self, top_left: Point) {
        self.bounding_box_mut().top_left = top_left;
    }

    /// Returns x of the top left position of the widget.
    #[inline]
    fn x(&self) -> i32 {
        self.top_left().x
    }

    /// Sets x of the top left position of the widget.
    #[inline]
    fn set_x(&mut self, x: i32) {
        self.bounding_box_mut().top_left.x = x;
    }

    /// Returns y of the top left position.
    #[inline]
    fn y(&self) -> i32 {
        self.top_left().y
    }

    /// Sets y of the top left position of the widget.
    #[inline]
    fn set_y(&mut self, y: i32) {
        self.bounding_box_mut().top_left.y = y;
    }

    /// Returns the size of teh widget.
    #[inline]
    fn size(&self) -> Size {
        self.bounding_box().size
    }

    /// Sets the size of the widget.
    #[inline]
    fn set_size(&mut self, size: Size) {
        self.bounding_box_mut().size = size
    }

    /// Returns the width of the widget.
    #[inline]
    fn width(&self) -> u32 {
        self.size().width
    }

    /// Sets the width of the widget.
    #[inline]
    fn set_width(&mut self, width: u32) {
        self.bounding_box_mut().size.width = width;
    }

    /// Returns the height of the widget.
    #[inline]
    fn height(&self) -> u32 {
        self.size().height
    }

    /// Sets the width of the widget.
    #[inline]
    fn set_height(&mut self, height: u32) {
        self.bounding_box_mut().size.height = height;
    }

    /// Return whether the widget contains a given point.
    #[inline]
    fn contains_point(&self, point: Point) -> bool {
        self.bounding_box().contains(point)
    }

    /// Returns information about the given widget object as string. This method is handy for debug purposes.
    /// Do not print more content which one line can print.‚
    fn widget_string(&self) -> String;

    /// Returns a reference to the list of children of the widget.
    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        Vec::new()
    }

    /// Returns a mutable reference to the list of children of the widget.
    fn get_children_mut(&mut self) -> Vec<&mut dyn Widget<S, D>> {
        Vec::new()
    }

    /// Returns the number of children.
    fn children_len(&self) -> usize {
        self.get_children().len()
    }

    /// Returns `true` if the children are empty.
    fn is_children_empty(&self) -> bool {
        self.get_children().is_empty()
    }

    /// Returns if the widget is disabled or enabled.
    fn is_disabled(&self) -> bool {
        false
    }

    /// Sets is_focusable of a widget.
    fn set_is_focusable(&mut self, focusable: bool) {
        self.base_mut().is_focusable = focusable;
    }

    /// Returns `true` if the widget is focusable otherwise `false`.
    fn get_is_focusable(&self) -> bool {
        self.base().is_focusable
    }

    /// Returns `true` if the widget has current focus otherwise `false`.
    fn has_focus(&self) -> bool {
        self.base().has_focus
    }
}

downcast!(<S, D> dyn Widget<S, D> where D: DrawTarget);

#[cfg(test)]
mod tests {
    use super::*;

    struct DummyState {}

    struct DummyDrawTarget {}

    impl DrawTarget for DummyDrawTarget {
        type Color = eg::pixelcolor::Rgb888;

        type Error = ();

        fn draw_iter<I>(&mut self, _: I) -> Result<(), Self::Error>
        where
            I: IntoIterator<Item = embedded_graphics::Pixel<Self::Color>>,
        {
            todo!()
        }
    }

    impl Dimensions for DummyDrawTarget {
        fn bounding_box(&self) -> Rectangle {
            todo!()
        }
    }

    struct DummyWidget {
        base: WidgetBase,
    }

    impl Widget<DummyState, DummyDrawTarget> for DummyWidget {
        fn base(&self) -> &WidgetBase {
            &self.base
        }

        fn base_mut(&mut self) -> &mut WidgetBase {
            &mut self.base
        }

        fn draw(&mut self, _: &mut DrawContext<DummyDrawTarget, DummyState>) -> Result<(), ()> {
            todo!()
        }

        fn layout(&mut self, _: LayoutContext) -> Size {
            todo!()
        }

        fn widget_string(&self) -> String {
            todo!()
        }
    }

    #[test]
    fn test_x() {
        let mut widget = DummyWidget {
            base: WidgetBase::new(),
        };

        assert_eq!(0, widget.x());

        widget.set_x(2);
        assert_eq!(
            Rectangle::new(Point::new(2, 0), Size::zero()),
            widget.bounding_box()
        );

        assert_eq!(2, widget.x());
    }

    #[test]
    fn test_y() {
        let mut widget = DummyWidget {
            base: WidgetBase::new(),
        };

        assert_eq!(0, widget.y());

        widget.set_y(2);
        assert_eq!(
            Rectangle::new(Point::new(0, 2), Size::zero()),
            widget.bounding_box()
        );

        assert_eq!(2, widget.y());
    }

    #[test]
    fn test_top_left() {
        let mut widget = DummyWidget {
            base: WidgetBase::new(),
        };

        assert_eq!(Point::zero(), widget.top_left());

        widget.set_top_left(Point::new(2, 2));
        assert_eq!(
            Rectangle::new(Point::new(2, 2), Size::zero()),
            widget.bounding_box()
        );

        assert_eq!(Point::new(2, 2), widget.top_left());
    }

    #[test]
    fn test_width() {
        let mut widget = DummyWidget {
            base: WidgetBase::new(),
        };

        assert_eq!(0, widget.width());

        widget.set_width(2);
        assert_eq!(
            Rectangle::new(Point::zero(), Size::new(2, 0)),
            widget.bounding_box()
        );

        assert_eq!(2, widget.width());
    }

    #[test]
    fn test_height() {
        let mut widget = DummyWidget {
            base: WidgetBase::new(),
        };

        assert_eq!(0, widget.height());

        widget.set_height(2);
        assert_eq!(
            Rectangle::new(Point::zero(), Size::new(0, 2)),
            widget.bounding_box()
        );

        assert_eq!(2, widget.height());
    }

    #[test]
    fn test_size() {
        let mut widget = DummyWidget {
            base: WidgetBase::new(),
        };

        assert_eq!(Size::zero(), widget.size());

        widget.set_size(Size::new(2, 2));
        assert_eq!(
            Rectangle::new(Point::zero(), Size::new(2, 2)),
            widget.bounding_box()
        );

        assert_eq!(Size::new(2, 2), widget.size());
    }
}
