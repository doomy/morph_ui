use crate::{eg::draw_target::DrawTarget, widget::Widget};

/// Returns the first child of the parent with the given type
pub fn get_widget_child_mut<S, D, W>(parent: &mut dyn Widget<S, D>) -> Option<&mut W>
where
    S: 'static,
    D: DrawTarget + 'static,
    W: Widget<S, D> + 'static,
{
    for child in parent.get_children_mut() {
        if let Ok(child) = child.downcast_mut::<W>() {
            return Some(child);
        }
    }

    None
}

pub fn type_name<T: ?Sized>(_val: &T) -> &'static str {
    crate::stdlib::any::type_name::<T>()
}

/// Returns the first child of the parent with the given type
pub fn get_widget_child<S, D, W>(parent: &dyn Widget<S, D>) -> Option<&W>
where
    S: 'static,
    D: DrawTarget + 'static,
    W: Widget<S, D> + 'static,
{
    for child in parent.get_children() {
        if let Ok(child) = child.downcast_ref::<W>() {
            return Some(child);
        }
    }

    None
}

/// Returns available_size if stretch is set to `true` otherwise it returns the minimum of reference_size + offset and available_size.
///
/// This function can be used to calculate the size of a `Widget`.
pub fn get_size(stretch: bool, available_size: u32, reference_size: u32, offset: u32) -> u32 {
    if stretch {
        available_size
    } else {
        (reference_size + offset).min(available_size)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn get_sizes() {
        assert_eq!(get_size(true, 99, 100, 20), 99);
        assert_eq!(get_size(false, 99, 10, 20), 30);
        assert_eq!(get_size(false, 99, 10, 200), 99);
    }
}
