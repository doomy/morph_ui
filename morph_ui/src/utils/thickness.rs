use crate::eg::{
    geometry::{Point, Size},
    primitives::Rectangle,
};

/// Defines the inner space of a widget, that surrounds its child.
///
/// # Example
///
/// ```
/// use morph_ui::{eg::geometry::Size, utils::Thickness};
///
/// assert_eq!(Size::new(6, 4), Thickness::new(1, 2, 3, 4).inner_size(Size::new(10, 10)));   
/// assert_eq!(Size::new(14, 16), Thickness::new(1, 2, 3, 4).outer_size(Size::new(10, 10)));        
/// ```
#[derive(Debug, Default, Copy, Clone, PartialEq)]
pub struct Thickness {
    pub left: u32,
    pub top: u32,
    pub right: u32,
    pub bottom: u32,
}

impl Thickness {
    /// Creates a new thickness with left, top, right and bottom set to 0.
    pub fn zero() -> Self {
        Thickness {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
        }
    }
    /// Creates a new thickness with the given settings..
    pub fn new(left: u32, top: u32, right: u32, bottom: u32) -> Self {
        Thickness {
            left,
            top,
            right,
            bottom,
        }
    }

    /// Returns the inner width by the given available width and padding.
    pub fn inner_width(&self, width: u32) -> u32 {
        (width as i32 - self.left as i32 - self.right as i32).max(0) as u32
    }

    /// Returns the inner height by the given available height and padding.
    pub fn inner_height(&self, height: u32) -> u32 {
        (height as i32 - self.top as i32 - self.bottom as i32).max(0) as u32
    }

    /// Returns the inner size by the given available size and padding.
    pub fn inner_size(&self, available_size: Size) -> Size {
        Size::new(
            self.inner_width(available_size.width),
            self.inner_height(available_size.height),
        )
    }

    /// Returns a inner bounding box for the given thickness and outer bounding box.
    pub fn inner_bounding_box(&self, bounding_box: Rectangle) -> Rectangle {
        let size = self.inner_size(bounding_box.size);
        Rectangle::new(
            Point::new(
                bounding_box.top_left.x + self.left as i32,
                bounding_box.top_left.y + self.top as i32,
            ),
            size,
        )
    }

    /// Returns the outer width by the given available width and padding.
    pub fn outer_width(&self, width: u32) -> u32 {
        width + self.left + self.right
    }

    /// Returns the outer height by the given available height and padding.
    pub fn outer_height(&self, height: u32) -> u32 {
        height + self.top + self.bottom
    }

    /// Returns the outer size by the given inner size and padding.
    pub fn outer_size(&self, inner_size: Size) -> Size {
        Size::new(
            self.outer_width(inner_size.width),
            self.outer_height(inner_size.height),
        )
    }
}

impl From<u32> for Thickness {
    fn from(t: u32) -> Self {
        Thickness {
            left: t,
            top: t,
            right: t,
            bottom: t,
        }
    }
}

impl From<(u32, u32)> for Thickness {
    fn from(t: (u32, u32)) -> Self {
        Thickness {
            left: t.0,
            top: t.1,
            right: t.0,
            bottom: t.1,
        }
    }
}

impl From<(u32, u32, u32, u32)> for Thickness {
    fn from(t: (u32, u32, u32, u32)) -> Self {
        Thickness {
            left: t.0,
            top: t.1,
            right: t.2,
            bottom: t.3,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_inner_size() {
        let padding = Thickness {
            left: 5,
            top: 6,
            right: 7,
            bottom: 8,
        };

        let size = Size::new(20, 20);

        assert_eq!(Size::new(8, 6), padding.inner_size(size));

        let size = Size::new(2, 2);

        assert_eq!(Size::new(0, 0), padding.inner_size(size));
    }

    #[test]
    fn test_outer_size() {
        let padding = Thickness {
            left: 5,
            top: 6,
            right: 7,
            bottom: 8,
        };

        let size = Size::new(8, 6);

        assert_eq!(Size::new(20, 20), padding.outer_size(size));
    }

    #[test]
    fn test_from() {
        assert_eq!(
            Thickness {
                left: 8,
                top: 8,
                right: 8,
                bottom: 8
            },
            Thickness::from(8)
        );

        assert_eq!(
            Thickness {
                left: 8,
                top: 9,
                right: 8,
                bottom: 9
            },
            Thickness::from((8, 9))
        );

        assert_eq!(
            Thickness {
                left: 5,
                top: 6,
                right: 7,
                bottom: 8
            },
            Thickness::from((5, 6, 7, 8))
        )
    }

    #[test]
    fn test_inner_bounding_b0x() {
        assert_eq!(
            Rectangle::new(Point::new(0, 0), Size::new(8, 8)),
            Thickness {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
            }
            .inner_bounding_box(Rectangle::new(Point::new(0, 0), Size::new(8, 8)))
        );

        assert_eq!(
            Rectangle::new(Point::new(2, 2), Size::new(4, 4)),
            Thickness {
                left: 2,
                top: 2,
                right: 2,
                bottom: 2
            }
            .inner_bounding_box(Rectangle::new(Point::new(0, 0), Size::new(8, 8)))
        );

        assert_eq!(
            Rectangle::new(Point::new(0, 2), Size::new(6, 6)),
            Thickness {
                left: 0,
                top: 2,
                right: 2,
                bottom: 0
            }
            .inner_bounding_box(Rectangle::new(Point::new(0, 0), Size::new(8, 8)))
        );

        assert_eq!(
            Rectangle::new(Point::new(2, 0), Size::new(6, 6)),
            Thickness {
                left: 2,
                top: 0,
                right: 0,
                bottom: 2
            }
            .inner_bounding_box(Rectangle::new(Point::new(0, 0), Size::new(8, 8)))
        );
    }
}
