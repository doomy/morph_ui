/// Defines a cursor with a start and an end.
#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord, Copy, Default)]
pub struct Cursor {
    pub start: usize,
    pub end: usize,
}

impl Cursor {
    /// Creates a new cursor with start and end.
    pub fn new(start: usize, end: usize) -> Self {
        Cursor { start, end }
    }

    /// Returns the len of the cursor selection.
    pub fn len(&self) -> usize {
        i32::abs(self.start as i32 - self.end as i32) as usize
    }

    /// If the len of the cursor selection is 0 it returns `false` otherwise `true`.
    pub fn is_empty(&self) -> bool {
        self.start == self.end
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_len() {
        let cursor = Cursor::new(0, 0);
        assert_eq!(0, cursor.len());

        let cursor = Cursor::new(2, 5);
        assert_eq!(3, cursor.len());

        let cursor = Cursor::new(5, 2);
        assert_eq!(3, cursor.len());
    }

    #[test]
    fn test_is_empty() {
        let cursor = Cursor::new(0, 0);
        assert!(cursor.is_empty());

        let cursor = Cursor::new(5, 5);
        assert!(cursor.is_empty());

        let cursor = Cursor::new(2, 3);
        assert!(!cursor.is_empty());

        let cursor = Cursor::new(3, 2);
        assert!(!cursor.is_empty());
    }
}
