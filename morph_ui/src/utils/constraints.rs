use crate::eg::geometry::Size;

/// Defines size constraints.
///
/// # Example
///
/// ```
/// use morph_ui::{eg::geometry::Size, utils::Constraints};
///
/// assert_eq!(Size::new(5, 5), Constraints::create().width(5).build().constrain_size(Size::new(0, 5)));        
/// assert_eq!(Size::new(5, 5), Constraints::create().min_width(5).build().constrain_size(Size::new(0, 5)));     
/// assert_eq!(Size::new(5, 5), Constraints::create().max_width(5).build().constrain_size(Size::new(6, 5)));     
/// ```
#[derive(Debug, Default, Copy, Clone, PartialEq)]
pub struct Constraints {
    pub width: Option<u32>,
    pub height: Option<u32>,
    pub min_width: Option<u32>,
    pub min_height: Option<u32>,
    pub max_width: Option<u32>,
    pub max_height: Option<u32>,
}

impl Constraints {
    /// Creates a new constraints builder.
    pub fn create() -> ConstraintsBuilder {
        ConstraintsBuilder::new()
    }

    /// Constraints the given width. Highest priority has minimum, then maximum and last width.
    fn constrain_width(&self, mut requested_width: u32) -> u32 {
        if let Some(width) = self.width {
            if requested_width != width {
                requested_width = width;
            }
        }

        if let Some(max_width) = self.max_width {
            if requested_width > max_width {
                requested_width = max_width;
            }
        }

        if let Some(min_width) = self.min_width {
            if requested_width < min_width {
                requested_width = min_width;
            }
        }

        requested_width
    }

    /// Constraints the given height. Highest priority has minimum, then maximum and last height.
    fn constrain_height(&self, mut requested_height: u32) -> u32 {
        if let Some(height) = self.height {
            if requested_height != height {
                requested_height = height;
            }
        }

        if let Some(max_height) = self.max_height {
            if requested_height > max_height {
                requested_height = max_height;
            }
        }

        if let Some(min_height) = self.min_height {
            if requested_height < min_height {
                requested_height = min_height;
            }
        }

        requested_height
    }

    /// Constraints the given size. Highest priority has minimum, then maximum and last width and height.
    pub fn constrain_size(&self, size: Size) -> Size {
        Size::new(
            self.constrain_width(size.width),
            self.constrain_height(size.height),
        )
    }
}

/// Is used to build a `Constraints` object.
#[derive(Default)]
pub struct ConstraintsBuilder {
    constraints: Constraints,
}

impl ConstraintsBuilder {
    /// Creates a new constraints builder.
    pub fn new() -> Self {
        ConstraintsBuilder {
            constraints: Constraints::default(),
        }
    }

    /// Builder method that is used to injects a width.
    #[must_use]
    pub fn width(mut self, width: u32) -> Self {
        self.constraints.width = Some(width);
        self
    }

    /// Builder method that is used to injects a height.
    #[must_use]
    pub fn height(mut self, height: u32) -> Self {
        self.constraints.height = Some(height);
        self
    }

    /// Builder method that is used to injects a minimum width.
    #[must_use]
    pub fn min_width(mut self, min_width: u32) -> Self {
        self.constraints.min_width = Some(min_width);
        self
    }

    /// Builder method that is used to injects a minimum height.
    #[must_use]
    pub fn min_height(mut self, min_height: u32) -> Self {
        self.constraints.min_height = Some(min_height);
        self
    }

    /// Builder method that is used to injects a maximum width.
    #[must_use]
    pub fn max_width(mut self, max_width: u32) -> Self {
        self.constraints.max_width = Some(max_width);
        self
    }

    /// Builder method that is used to injects a maximum height.
    #[must_use]
    pub fn max_height(mut self, max_height: u32) -> Self {
        self.constraints.max_height = Some(max_height);
        self
    }

    /// Creates the constraints with given settings.
    pub fn build(self) -> Constraints {
        self.constraints
    }
}

impl From<ConstraintsBuilder> for Constraints {
    fn from(b: ConstraintsBuilder) -> Self {
        b.build()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_constrain_size() {
        assert_eq!(
            Size::new(5, 4),
            Constraints::create()
                .width(5)
                .height(4)
                .build()
                .constrain_size(Size::default())
        );

        assert_eq!(
            Size::new(4, 3),
            Constraints::create()
                .min_width(4)
                .min_height(3)
                .build()
                .constrain_size(Size::new(3, 2))
        );

        assert_eq!(
            Size::new(10, 9),
            Constraints::create()
                .max_width(10)
                .max_height(9)
                .build()
                .constrain_size(Size::new(11, 10))
        );

        assert_eq!(
            Size::new(4, 3),
            Constraints::create()
                .width(2)
                .height(1)
                .min_width(4)
                .min_height(3)
                .build()
                .constrain_size(Size::new(2, 1))
        );

        assert_eq!(
            Size::new(4, 3),
            Constraints::create()
                .width(5)
                .height(3)
                .max_width(4)
                .max_height(3)
                .build()
                .constrain_size(Size::new(5, 4))
        );
    }
}
