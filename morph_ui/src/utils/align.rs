/// Defines the alignment (horizontal / vertical) of a widget.
///
/// # Example
///
/// ```
/// use morph_ui::utils::Align;
///
/// assert_eq!(0, Align::Start.align(0, 10, 6));        
/// assert_eq!(2, Align::Center.align(0, 10, 6));        
/// assert_eq!(0, Align::Stretch.align(0, 10, 6));        
/// assert_eq!(4, Align::End.align(0, 10, 6));        
/// ```
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Align {
    Start,
    Center,
    Stretch,
    End,
}

impl Default for Align {
    fn default() -> Self {
        Align::Start
    }
}

impl Align {
    /// Aligns the given position (x or y) by given outer size (width or height) value and the size (width or height) value.
    pub fn align(&self, position: i32, available: u32, size: u32) -> i32 {
        match self {
            Align::Start => position,
            Align::Center => position + (available as i32 - size as i32) / 2,
            Align::Stretch => position,
            Align::End => position + available as i32 - size as i32,
        }
    }
}

impl From<&str> for Align {
    fn from(s: &str) -> Self {
        match s {
            "center" | "Center" => Align::Center,
            "end" | "End" => Align::End,
            "stretch" | "Stretch" => Align::Stretch,
            _ => Align::Start,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_align() {
        let position = 2;
        let available = 100;
        let size = 20;

        assert_eq!(2, Align::Start.align(position, available, size));
        assert_eq!(42, Align::Center.align(position, available, size));
        assert_eq!(2, Align::Stretch.align(position, available, size));
        assert_eq!(82, Align::End.align(position, available, size));
    }

    #[test]
    fn test_from_str() {
        assert_eq!(Align::Start, Align::from("Start"));
        assert_eq!(Align::Start, Align::from("start"));
        assert_eq!(Align::Center, Align::from("center"));
        assert_eq!(Align::Center, Align::from("Center"));
        assert_eq!(Align::Stretch, Align::from("stretch"));
        assert_eq!(Align::Stretch, Align::from("Stretch"));
        assert_eq!(Align::End, Align::from("end"));
        assert_eq!(Align::End, Align::from("End"));
    }
}
