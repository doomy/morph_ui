use crate::{eg::draw_target::DrawTarget, stdlib::boxed::Box, widget::Widget, widgets::Wrap};

/// `WrapBuilder` is used to define builder methods to construct a [`Wrap`].
///
/// There is a default implementation of this trait for `Box<Wrap<S, D>>`.
pub trait WrapBuilder {
    type State;
    type DrawTarget;

    /// Builder method that is used to set the vertical spacing between the children of the `Wrap`.
    #[must_use]
    fn ver_spacing(self, ver_spacing: u32) -> Self;

    /// Builder method that is used to set the horizontal spacing between the children of the `Wrap`.
    #[must_use]
    fn hor_spacing(self, hor_spacing: u32) -> Self;

    /// Builder method that is used to push a child to the `Wrap` layout.
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;
}

impl<S, D> WrapBuilder for Box<Wrap<S, D>>
where
    D: DrawTarget,
{
    type State = S;

    type DrawTarget = D;

    fn ver_spacing(mut self, ver_spacing: u32) -> Self {
        self.set_ver_spacing(ver_spacing);
        self
    }

    fn hor_spacing(mut self, hor_spacing: u32) -> Self {
        self.set_hor_spacing(hor_spacing);
        self
    }

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.push(child);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        eg::{mock_display::MockDisplay, pixelcolor::Rgb565},
        stdlib::*,
    };

    type TestWrap = Wrap<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_ver_spacing() {
        assert_eq!(TestWrap::new().ver_spacing(10).get_ver_spacing(), 10);
    }

    #[test]
    fn builder_hor_spacing() {
        assert_eq!(TestWrap::new().hor_spacing(10).get_hor_spacing(), 10);
    }

    #[test]
    fn builder_child() {
        let test_wrap = TestWrap::new()
            .child(TestWrap::new())
            .child(TestWrap::new());

        assert_eq!(test_wrap.len(), 2);
        assert!(!test_wrap.is_empty());
        assert_eq!(test_wrap.get_children().len(), 2);
    }
}
