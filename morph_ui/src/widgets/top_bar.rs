use crate::{
    downcast::Any,
    eg::{geometry::*, primitives::*},
    stdlib::{vec::*, *},
    *,
};

mod top_bar_builder;

pub use top_bar_builder::*;

/// `TopBar` represents a container with front, title and tail child.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn top_bar<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<TopBar<S, D>> {
///    TopBar::new()
///         .title(Label::new()
///                 .text("Title")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }       
/// ```
pub struct TopBar<S, D>
where
    D: DrawTarget,
{
    container: Box<Container<S, D>>,
}

impl<S, D> Default for TopBar<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn default() -> Self {
        TopBar {
            container: Self::build(),
        }
    }
}

impl<S, D> TopBar<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    /// Constructs a new `TopBar` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(TopBar::default())
    }

    /// Gets the (start) front widget  of `TopBar`.
    pub fn get_front(&self) -> Option<&dyn Widget<S, D>> {
        self.front_container().get_child()
    }

    /// Gets a mutable reference of the (start) front widget  of `TopBar`.
    pub fn get_front_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.front_container_mut().get_child_mut()
    }

    /// Sets the (start) front widget of `TopBar`.
    pub fn set_front(&mut self, front: Box<dyn Widget<S, D>>) {
        self.front_container_mut().set_child(front);
    }

    /// Gets the (center) title widget  of `TopBar`.
    pub fn get_title(&self) -> Option<&dyn Widget<S, D>> {
        self.title_stretch().get_child()
    }

    /// Gets a mutable reference of the (center) title widget  of `TopBar`.
    pub fn get_title_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.title_stretch_mut().get_child_mut()
    }

    /// Sets the (end) title widget  of `TopBar`.
    pub fn set_title(&mut self, title: Box<dyn Widget<S, D>>) {
        self.title_stretch_mut().set_child(title);
    }

    /// Gets the (end) tail widget  of `TopBar`.
    pub fn get_tail(&self) -> Option<&dyn Widget<S, D>> {
        self.tail_container().get_child()
    }

    /// Gets a mutable reference of the (center) tail widget  of `TopBar`.
    pub fn get_tail_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.tail_container_mut().get_child_mut()
    }

    /// Sets the (center) tail widget  of `TopBar`.
    pub fn set_tail(&mut self, tail: Box<dyn Widget<S, D>>) {
        self.tail_container_mut().set_child(tail);
    }

    /// Gets the padding of the `TopBar`.
    pub fn get_padding(&self) -> Thickness {
        self.container.get_padding()
    }

    /// Sets the padding of the `TopBar`.
    pub fn set_padding(&mut self, padding: Thickness) {
        self.container.set_padding(padding);
    }

    /// Gets the background style of the `TopBar`.
    pub fn get_style(&self) -> Option<PrimitiveStyle<D::Color>> {
        self.container.get_style()
    }

    /// Sets the padding of the `TopBar`.
    pub fn set_style(&mut self, style: PrimitiveStyle<D::Color>) {
        self.container.set_style(style);
    }

    /// Gets the horizontal spacing between the children of the `TopBar`.
    pub fn get_spacing(&self) -> u32 {
        self.row().get_spacing()
    }

    /// Sets the horizontal spacing between the children of the `TopBar`.
    pub fn set_spacing(&mut self, spacing: u32) {
        self.row_mut().set_spacing(spacing);
    }

    // -- helpers

    fn row(&self) -> &Row<S, D> {
        get_widget_child(self.container.as_ref()).unwrap()
    }

    fn row_mut(&mut self) -> &mut Row<S, D> {
        get_widget_child_mut(self.container.as_mut()).unwrap()
    }

    fn front_container(&self) -> &Container<S, D> {
        self.row()
            .get_children()
            .get(0)
            .unwrap()
            .downcast_ref()
            .unwrap()
    }

    fn front_container_mut(&mut self) -> &mut Container<S, D> {
        self.row_mut()
            .children_mut()
            .get_mut(0)
            .unwrap()
            .1
            .downcast_mut()
            .unwrap()
    }

    fn title_stretch_mut(&mut self) -> &mut Stretch<S, D> {
        self.row_mut()
            .children_mut()
            .get_mut(1)
            .unwrap()
            .1
            .downcast_mut()
            .unwrap()
    }

    fn title_stretch(&self) -> &Stretch<S, D> {
        self.row()
            .get_children()
            .get(1)
            .unwrap()
            .downcast_ref()
            .unwrap()
    }

    fn tail_container_mut(&mut self) -> &mut Container<S, D> {
        self.row_mut()
            .children_mut()
            .get_mut(2)
            .unwrap()
            .1
            .downcast_mut()
            .unwrap()
    }

    fn tail_container(&self) -> &Container<S, D> {
        self.row()
            .get_children()
            .get(2)
            .unwrap()
            .downcast_ref()
            .unwrap()
    }

    // Builds the base structure of the top bar.
    fn build() -> Box<Container<S, D>> {
        Container::new().child(
            Row::new()
                // front
                .child(Container::new())
                // title
                .child(Stretch::new())
                // tail
                .child(Container::new()),
        )
    }
}

impl<S, D> Widget<S, D> for TopBar<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        self.container.base()
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        self.container.base_mut()
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        self.container.draw(dtx)
    }

    fn stretch(&self) -> u32 {
        self.container.stretch()
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        self.container.layout(ltx)
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        self.container.event(event, etx);
    }

    fn widget_string(&self) -> String {
        format!(
            "TopBar {{ stretch: {}, bounds: {:?} }}",
            self.stretch(),
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            self.container.init_and_update(None, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<TopBar<S, D>>() {
            new.container
                .init_and_update(Some(self.container.as_ref()), itx);
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        self.container.get_children()
    }
}
