use crate::{
    downcast::Any,
    eg::{geometry::*, primitives::*},
    stdlib::{collections::BTreeMap, ops::DerefMut, *},
    utils::real::Real,
    *,
};

mod helpers;
mod scroll_area_builder;
mod scroll_area_state;
mod scroll_bar_visual;

pub use helpers::*;
pub use scroll_area_builder::*;
pub use scroll_area_state::*;
pub use scroll_bar_visual::*;

/// `ScrollArea` is a single child layout widget that can be used to scroll its content vertical and horizontal.
///
/// The ScrollArea stretches always vertical and horizontal in its given constraints.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, utils::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<ScrollArea<S, D>> {
///    ScrollArea::new()  
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }       
/// ```
pub struct ScrollArea<S, D>
where
    D: DrawTarget + 'static,
{
    child: Option<Box<dyn Widget<S, D>>>,
    base: WidgetBase,
    state: ScrollAreaState,
    constraints: Constraints,
    is_ver_scroll: bool,
    is_hor_scroll: bool,
    style: Option<PrimitiveStyle<D::Color>>,
    scroll_bar_styles: BTreeMap<ScrollBarVisual, PrimitiveStyle<D::Color>>,
    scroll_bar_size: u32,
    scroll_bar_margin: u32,
    scroll_bar_corners: CornerRadii,
    ver_scroll_bar_bounding_box: Rectangle,
    hor_scroll_bar_bounding_box: Rectangle,
    corners: CornerRadii,
    contains_mouse: bool,
    content_size: Size,
}

impl<S, D> Default for ScrollArea<S, D>
where
    S: 'static,

    D: DrawTarget + 'static,
{
    fn default() -> Self {
        ScrollArea {
            child: None,
            base: WidgetBase::new(),
            state: ScrollAreaState::default(),
            constraints: Constraints::default(),
            is_ver_scroll: true,
            is_hor_scroll: true,
            style: None,
            scroll_bar_styles: BTreeMap::new(),
            scroll_bar_size: 8,
            scroll_bar_margin: 2,
            scroll_bar_corners: CornerRadii::default(),
            ver_scroll_bar_bounding_box: Rectangle::zero(),
            hor_scroll_bar_bounding_box: Rectangle::zero(),
            corners: CornerRadii::default(),
            contains_mouse: false,
            content_size: Size::zero(),
        }
    }
}

impl<S, D> ScrollArea<S, D>
where
    S: 'static,

    D: DrawTarget + 'static,
{
    /// Constructs a new `ScrollArea` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(ScrollArea::default())
    }

    /// Gets the box constraints of the button.
    pub fn get_constraints(&self) -> Constraints {
        self.constraints
    }

    /// Sets the box constraints of the button.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.constraints = constraints.into();
    }

    /// Gets the value that indicates if vertical scrolling is enabled.
    pub fn get_is_ver_scroll(&self) -> bool {
        self.is_ver_scroll
    }

    /// Sets is_ver_scroll flag. If set to `true` vertical scrolling is enabled otherwise not. Default is `true`.
    pub fn set_is_ver_scroll(&mut self, is_ver_scroll: bool) {
        self.is_ver_scroll = is_ver_scroll;
    }

    /// Gets the value that indicates if horizontal scrolling is enabled.
    pub fn get_is_hor_scroll(&self) -> bool {
        self.is_hor_scroll
    }

    /// Sets is_hor_scroll flag. If set to `true` horizontal scrolling is enabled otherwise not. Default is `true`.
    pub fn set_is_hor_scroll(&mut self, is_hor_scroll: bool) {
        self.is_hor_scroll = is_hor_scroll;
    }

    /// Gets the background style of the scroll area.
    pub fn get_style(&self) -> Option<PrimitiveStyle<D::Color>> {
        self.style
    }

    /// Sets the background style of the scroll area.
    pub fn set_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.style = Some(style.into());
    }

    /// Gets a reference of the  scroll bar styles.
    pub fn get_scroll_bar_styles(&self) -> &BTreeMap<ScrollBarVisual, PrimitiveStyle<D::Color>> {
        &self.scroll_bar_styles
    }

    /// Sets the scroll bar styles.
    pub fn set_scroll_bar_styles(
        &mut self,
        styles: BTreeMap<ScrollBarVisual, PrimitiveStyle<D::Color>>,
    ) {
        self.scroll_bar_styles = styles;
    }

    /// Inserts the style of the scroll bars.
    pub fn insert_scroll_bar_style(
        &mut self,
        visual: ScrollBarVisual,
        scroll_bar_style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.scroll_bar_styles
            .insert(visual, scroll_bar_style.into());
    }

    /// Gets the size (width for vertical | height for horizontal) of the scroll bars.
    pub fn get_scroll_bar_size(&self) -> u32 {
        self.scroll_bar_size
    }

    /// Sets the size (width for vertical | height for horizontal) of the scroll bars.
    pub fn set_scroll_bar_size(&mut self, size: u32) {
        self.scroll_bar_size = size;
    }

    /// Gets the margin (width for vertical | height for horizontal) of the scroll bars.
    pub fn get_scroll_bar_margin(&self) -> u32 {
        self.scroll_bar_margin
    }

    /// Sets the margin (width for vertical | height for horizontal) of the scroll bars.
    pub fn set_scroll_bar_margin(&mut self, margin: u32) {
        self.scroll_bar_margin = margin;
    }

    /// Gets he corner radius of the scroll bars.
    pub fn get_scroll_bar_corners(&self) -> CornerRadii {
        self.scroll_bar_corners
    }

    /// Sets the corner radius of the scroll bars.
    pub fn set_scroll_bar_corners(&mut self, scroll_bar_corners: CornerRadii) {
        self.scroll_bar_corners = scroll_bar_corners;
    }

    /// Returns the corner radius of the scroll area.
    pub fn get_corners(&self) -> CornerRadii {
        self.corners
    }

    /// Sets the corner radius of the scroll area.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.corners = corners;
    }

    /// Returns the child of the scroll area.
    pub fn get_child(&self) -> Option<&dyn Widget<S, D>> {
        self.child.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the child of the container.
    pub fn get_child_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.child.as_mut().map(|c| c.as_mut())
    }

    /// Sets the child of the scroll area.
    pub fn set_child(&mut self, child: Box<dyn Widget<S, D>>) {
        self.child = Some(child);
    }

    // helpers

    // Draw the given scroll bar.
    fn draw_scroll_bar(
        &mut self,
        dtx: &mut DrawContext<D, S>,
        visual: ScrollBarVisual,
        bounding_box: Rectangle,
    ) -> Result<(), D::Error> {
        if let Some(style) = self.scroll_bar_styles.get(&visual) {
            if self.scroll_bar_corners == CornerRadii::default() {
                bounding_box.into_styled(*style).draw(&mut dtx.display())?;
            } else {
                RoundedRectangle::new(bounding_box, self.scroll_bar_corners)
                    .into_styled(*style)
                    .draw(&mut dtx.display())?;
            }
        }

        Ok(())
    }

    // Returns `true` if the vertical scroll bar is visible.
    fn is_ver_scroll_bar_visible(&self) -> bool {
        self.is_ver_scroll && self.height() < self.content_size.height
    }

    // Returns `true` if the horizontal scroll bar is visible.
    fn is_hor_scroll_bar_visible(&self) -> bool {
        self.is_hor_scroll && self.width() < self.content_size.width
    }

    // Returns the view port of the scroll area.
    fn view_port(&self) -> Size {
        let mut view_port = self.size();

        if self.is_ver_scroll_bar_visible() {
            view_port -= Size::new(2 * self.scroll_bar_margin + self.scroll_bar_size, 0);
        }

        if self.is_hor_scroll_bar_visible() {
            view_port -= Size::new(0, 2 * self.scroll_bar_margin + self.scroll_bar_size);
        }

        view_port
    }
}

impl<S, D> Widget<S, D> for ScrollArea<S, D>
where
    S: 'static,

    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let layout_update = dtx.stx().layout_update();
        let render_update = dtx.has_render_update(Some(self.id()));

        if render_update {
            if let Some(style) = self.style {
                // draw background
                if self.corners == CornerRadii::default() {
                    self.bounding_box()
                        .into_styled(style)
                        .draw(&mut dtx.display())?;
                } else {
                    RoundedRectangle::new(self.bounding_box(), self.corners)
                        .into_styled(style)
                        .draw(&mut dtx.display())?;
                }
            }
        }

        let top_left = self.bounding_box().top_left;
        let view_port = self.view_port();
        if let Some(child) = &mut self.child {
            child.update_and_draw(
                top_left,
                &mut DrawContext::from_draw_context(
                    dtx.all(),
                    Some(Rectangle::new(top_left, view_port)),
                    layout_update || render_update,
                ),
            )?;
        }

        // draw scroll bars
        if self.is_ver_scroll_bar_visible() {
            if dtx.stx().layout_update() {
                self.ver_scroll_bar_bounding_box.top_left += self.bounding_box().top_left;
            }

            self.draw_scroll_bar(
                dtx,
                self.state.ver_scroll_bar_visual,
                self.ver_scroll_bar_bounding_box,
            )?;
        }

        if self.is_hor_scroll_bar_visible() {
            if dtx.stx().layout_update() {
                self.hor_scroll_bar_bounding_box.top_left += self.bounding_box().top_left;
            }

            self.draw_scroll_bar(
                dtx,
                self.state.hor_scroll_bar_visual,
                self.hor_scroll_bar_bounding_box,
            )?;
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        self.set_size(self.constraints.constrain_size(Size::new(
            get_size(ltx.h_stretch(), ltx.available_width(), 0, 0),
            get_size(ltx.v_stretch(), ltx.available_height(), 0, 0),
        )));

        let mut view_port = self.view_port();

        if let Some(child) = &mut self.child {
            let available_size = get_max_bounds(self.is_hor_scroll, self.is_ver_scroll, view_port);
            self.content_size = child.layout(LayoutContext::new(available_size, false, false));
        }

        // correct layout if viewport has changes after initial layout calculation
        let new_view_port = self.view_port();
        if view_port != self.view_port() {
            if let Some(child) = &mut self.child {
                let available_size =
                    get_max_bounds(self.is_hor_scroll, self.is_ver_scroll, new_view_port);
                self.content_size = child.layout(LayoutContext::new(available_size, false, false));
            }

            view_port = new_view_port;
        }

        let is_ver_scroll_bar = self.is_ver_scroll_bar_visible();
        let is_hor_scroll_bar = self.is_hor_scroll_bar_visible();

        if let Some(child) = &mut self.child {
            if is_hor_scroll_bar {
                child.set_x(self.state.offset.x);
            } else {
                child.set_x(0);
            }

            if is_ver_scroll_bar {
                child.set_y(self.state.offset.y);
            } else {
                child.set_y(0);
            }

            // vertical scroll bar
            if is_ver_scroll_bar {
                let vertical_ratio =
                    Real::from(view_port.height) / Real::from(self.content_size.height);
                let vertical_height: u32 = (Real::from(view_port.height) * vertical_ratio).into();
                let vertical_offset: i32 =
                    (-Real::from(self.state.offset.y) * vertical_ratio).into();

                self.ver_scroll_bar_bounding_box = Rectangle::new(
                    Point::new(
                        view_port.width as i32 + self.scroll_bar_margin as i32,
                        self.scroll_bar_margin as i32 + vertical_offset,
                    ),
                    Size::new(self.scroll_bar_size, vertical_height),
                );
            }

            // horizontal scroll bar
            if is_hor_scroll_bar {
                let horizontal_ratio =
                    Real::from(view_port.width) / Real::from(self.content_size.width);
                let horizontal_width: u32 = (Real::from(view_port.width) * horizontal_ratio)
                    .round()
                    .into();
                let horizontal_offset: i32 = (Real::from(-self.state.offset.x) * horizontal_ratio)
                    .round()
                    .into();

                self.hor_scroll_bar_bounding_box = Rectangle::new(
                    Point::new(
                        self.scroll_bar_margin as i32 + horizontal_offset,
                        view_port.height as i32 + self.scroll_bar_margin as i32,
                    ),
                    Size::new(horizontal_width, self.scroll_bar_size),
                );
            }
        }

        self.size()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Ok(move_event) = event.downcast_ref::<MoveEvent>() {
            self.contains_mouse = self.contains_point(move_event.position());
        }

        let old_state = self.state;

        if self.child.is_some() {
            self.state.update(
                self.id(),
                event,
                etx,
                (self.is_ver_scroll, self.is_hor_scroll),
                (self.view_port(), self.content_size),
                (
                    self.ver_scroll_bar_bounding_box,
                    self.hor_scroll_bar_bounding_box,
                ),
                self.contains_mouse,
            );
        }

        if let Some(child) = &mut self.child {
            child.event(event, etx);
        }

        if !self.contains_mouse {
            if self.state.ver_scroll_bar_visual == ScrollBarVisual::Hover {
                self.state.ver_scroll_bar_visual = ScrollBarVisual::Default;
            }

            if self.state.hor_scroll_bar_visual == ScrollBarVisual::Hover {
                self.state.hor_scroll_bar_visual = ScrollBarVisual::Default;
            }
        }

        if self.state.has_changes(&old_state) {
            etx.stx_mut().trigger_render_update(Some(self.id()));
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "ScrollArea {{ is_ver_scroll: {}, is_hor_scroll: {}, bounds {:?} }}",
            self.is_ver_scroll,
            self.is_hor_scroll,
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else if let Some(child) = &mut self.child {
            child.init_and_update(None, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<ScrollArea<S, D>>() {
            if let Some(new_child) = &mut new.child {
                new.state = self.state;
                new_child.init_and_update(self.child.as_deref(), itx);
            }

            new.base.id = self.base.id;
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        if let Some(child) = &self.child {
            return vec![child.as_ref()];
        }

        vec![]
    }

    fn get_children_mut(&mut self) -> Vec<&mut dyn Widget<S, D>> {
        if let Some(child) = &mut self.child {
            return vec![child.deref_mut()];
        }

        vec![]
    }
}
