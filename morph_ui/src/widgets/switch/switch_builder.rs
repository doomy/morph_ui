use crate::{eg::prelude::*, stdlib::boxed::Box, widgets::switch::*};

/// `SwitchBuilder` is used to define builder methods to construct a [`Switch`].
///
/// There is a default implementation of this trait for `Box<Switch<S, C, D, R>>`.
pub trait SwitchBuilder {
    type Style;
    type State;

    /// Builder method that is used to insert a new background style for the given visual.
    #[must_use]
    fn style(self, visual: ButtonVisual, style: Self::Style) -> Self;

    /// Builder method that is used to insert a new toggle style for the given visual.
    #[must_use]
    fn toggle_style(self, visual: ButtonVisual, style: Self::Style) -> Self;

    /// Builder method that is used to set the focus style.
    #[must_use]
    fn focus_style(self, style: Self::Style) -> Self;

    // Builder method that is used to set the padding of the switch. It is the space around the text of the switch.
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the constraints of the switch.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;

    /// Builder method that is used to set start visual of the switch.
    #[must_use]
    fn visual(self, visual: impl Into<ButtonVisual>) -> Self;

    /// Builder method that is used to set the value that indicates if the switch is currently checked.
    #[must_use]
    fn checked(self, is_checked: bool) -> Self;

    /// Builder method that is used to set the corner radius of the switch background.
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the corner radius of the switch toggle.
    #[must_use]
    fn toggle_corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the value that indicates if the switch is enabled.
    #[must_use]
    fn enabled(self, is_enabled: bool) -> Self;

    /// Builder method that is used to set the callback that is called after the switch is tapped.
    #[must_use]
    fn on_tap<F: Fn(&mut Self::State) + 'static>(self, on_tap: F) -> Self;

    /// Builder method that is used to set the callback that is called after `is_checked` of switch is changed.
    #[must_use]
    fn on_is_checked_changed<F: Fn(&mut Self::State, bool) + 'static>(
        self,
        on_is_checked_changed: F,
    ) -> Self;

    /// Builder method that is used to set is_focusable of the switch.
    #[must_use]
    fn focusable(self, is_focusable: bool) -> Self;
}

impl<S, D> SwitchBuilder for Box<Switch<S, D>>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    type Style = PrimitiveStyle<D::Color>;
    type State = S;

    fn style(mut self, visual: ButtonVisual, style: Self::Style) -> Self {
        self.insert_style(visual, style);
        self
    }

    fn focus_style(mut self, style: Self::Style) -> Self {
        self.set_focus_style(style);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }

    fn visual(mut self, visual: impl Into<ButtonVisual>) -> Self {
        self.set_visual(visual);
        self
    }

    fn checked(mut self, is_checked: bool) -> Self {
        self.set_is_checked(is_checked);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }

    fn enabled(mut self, is_enabled: bool) -> Self {
        self.set_is_enabled(is_enabled);
        self
    }

    fn on_tap<F: Fn(&mut Self::State) + 'static>(mut self, on_tap: F) -> Self {
        self.set_on_tap(on_tap);
        self
    }

    fn on_is_checked_changed<F: Fn(&mut Self::State, bool) + 'static>(
        mut self,
        on_is_checked_changed: F,
    ) -> Self {
        self.set_on_is_checked_changed(on_is_checked_changed);
        self
    }

    fn focusable(mut self, is_focusable: bool) -> Self {
        self.set_is_focusable(is_focusable);
        self
    }

    fn toggle_style(mut self, visual: ButtonVisual, style: Self::Style) -> Self {
        self.insert_toggle_style(visual, style);
        self
    }

    fn toggle_corners(mut self, corners: CornerRadii) -> Self {
        self.set_toggle_corners(corners);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestSwitch = Switch<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_style() {
        assert_eq!(
            TestSwitch::new()
                .style(
                    ButtonVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_styles()
                .get(&ButtonVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_toggle_style() {
        assert_eq!(
            TestSwitch::new()
                .toggle_style(
                    ButtonVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_toggle_styles()
                .get(&ButtonVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_focus_style() {
        assert_eq!(
            TestSwitch::new()
                .focus_style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_focus_style(),
            PrimitiveStyleBuilder::new()
                .fill_color(Rgb565::BLACK)
                .build()
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestSwitch::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestSwitch::new()
                .constraints(Constraints::create().width(100).height(70))
                .get_constraints(),
            Constraints::create().width(100).height(70).build()
        );
    }

    #[test]
    fn builder_is_checked() {
        assert!(!TestSwitch::new().get_is_checked());
        assert!(TestSwitch::new().checked(true).get_is_checked());
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestSwitch::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_toggle_corners() {
        assert_eq!(
            TestSwitch::new()
                .toggle_corners(CornerRadii::new(Size::new(10, 0)))
                .get_toggle_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_is_enabled() {
        assert!(TestSwitch::new().get_is_enabled());
        assert!(!TestSwitch::new().enabled(false).get_is_enabled());
    }
}
