use crate::{downcast::Any, eg::geometry::*, stdlib::*, *};

mod wrap_builder;

pub use wrap_builder::*;

/// `Wrap` orders its children horizontal until there is not enough horizontal space left. The next children will placed in a next row.
///
/// To construct a `Label` check the [`WrapBuilder`] trait. It is implemented for `Box<Wrap<S, D>>`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn wrap<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Wrap<S, D>> {
///    Wrap::new()
///         .ver_spacing(4)
///         .hor_spacing(4)
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
///         .child(Label::new()
///                 .text("morph")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }       
/// ```
pub struct Wrap<S, D: DrawTarget> {
    ver_spacing: u32,
    hor_spacing: u32,
    children: Vec<Box<dyn Widget<S, D>>>,
    base: WidgetBase,
}

impl<S, D: DrawTarget> Default for Wrap<S, D> {
    fn default() -> Self {
        Wrap {
            ver_spacing: 0,
            hor_spacing: 0,
            children: Vec::new(),
            base: WidgetBase::new(),
        }
    }
}

impl<S, D: DrawTarget> Wrap<S, D> {
    /// Constructs a new `Wrap` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Wrap::default())
    }

    /// Gets the vertical spacing between the children of the `Wrap`.
    pub fn get_ver_spacing(&self) -> u32 {
        self.ver_spacing
    }

    /// Sets the vertical spacing between the children of the `Wrap`.
    pub fn set_ver_spacing(&mut self, ver_spacing: u32) {
        self.ver_spacing = ver_spacing;
    }

    /// Gets the horizontal spacing between the children of the `Wrap`.
    pub fn get_hor_spacing(&self) -> u32 {
        self.hor_spacing
    }

    /// Sets the horizontal spacing between the children of the `Wrap`.
    pub fn set_hor_spacing(&mut self, hor_spacing: u32) {
        self.hor_spacing = hor_spacing;
    }

    /// Appends an child to the back of the children collection.
    pub fn push(&mut self, child: Box<dyn Widget<S, D>>) {
        self.children.push(child);
    }

    /// Returns the count of the children.
    pub fn len(&self) -> usize {
        self.children.len()
    }

    /// Returns `true` if the column dos not contains children.
    pub fn is_empty(&self) -> bool {
        self.children.is_empty()
    }
}

impl<S: 'static, D: DrawTarget + 'static> Widget<S, D> for Wrap<S, D> {
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let top_left = self.top_left();

        for child in &mut self.children {
            child.update_and_draw(top_left, dtx)?;
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let mut size = Size::zero();
        let mut width_count = 0;
        let mut height_count = 0;
        let children_len = self.children.len();

        for i in 0..self.children.len() {
            if let Some(child) = self.children.get_mut(i) {
                let child_size =
                    child.layout(LayoutContext::new(ltx.available_size(), false, false));

                if width_count + child_size.width > ltx.available_width() {
                    width_count = 0;
                    size.height += height_count + self.ver_spacing;
                    height_count = 0;
                    size.height += self.ver_spacing;
                }

                child.bounding_box_mut().top_left =
                    Point::new(width_count as i32, size.height as i32);

                width_count += child_size.width;
                size.width = size.width.max(width_count);
                width_count += self.hor_spacing;
                height_count = height_count.max(child_size.height);

                if i == children_len - 1 {
                    size.height += height_count;
                }
            }
        }

        self.set_size(size);
        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        for child in &mut self.children {
            child.event(event, etx);
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Wrap {{ vertical_spacing: {}, horizontal_spacing: {}, bounds: {:?} }}",
            self.ver_spacing,
            self.hor_spacing,
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            for i in 0..self.children.len() {
                self.children[i].init_and_update(None, itx);
            }
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Wrap<S, D>>() {
            for i in 0..new.children.len() {
                new.children[i].init_and_update(self.children.get(i).map(|c| c.as_ref()), itx);
            }

            new.base.id = self.base.id;
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        self.children.iter().map(|c| c.as_ref()).collect()
    }

    fn get_children_mut(&mut self) -> Vec<&mut dyn Widget<S, D>> {
        self.children.iter_mut().map(|c| c.as_mut()).collect()
    }
}
