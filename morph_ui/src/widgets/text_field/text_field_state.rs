use crate::{
    downcast::Any,
    eg::{geometry::*, primitives::Rectangle},
    stdlib::string::*,
    widgets::text_field::TextFieldVisual,
    *,
};

#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord, Default)]
pub struct TextFieldState {
    pub(crate) visual: TextFieldVisual,

    pub(crate) cursor: Cursor,
    pub(crate) text_offset: i32,
    pub(crate) text: String,
    is_enabled: bool,
    pub(crate) is_password_box: bool,
}

impl TextFieldState {
    pub fn new() -> Self {
        Self {
            visual: TextFieldVisual::Default,
            cursor: Cursor::default(),
            text_offset: 0,
            text: String::default(),
            is_enabled: true,
            is_password_box: false,
        }
    }

    pub fn get_visual(&self) -> TextFieldVisual {
        self.visual
    }

    /// Gets the value that indicates if the button is enabled.
    pub fn get_is_enabled(&self) -> bool {
        self.is_enabled
    }

    /// Sets the value that indicates if the button is enabled.
    pub fn set_is_enabled(&mut self, is_enabled: bool) {
        self.is_enabled = is_enabled;
    }

    pub fn update<S>(
        &mut self,
        id: u32,
        event: &dyn Any,
        etx: &mut EventContext<S>,
        position: Point,
        size: Size,
    ) {
        if let Ok(mouse_event) = event.downcast_ref::<MouseEvent>() {
            let contains_mouse = Rectangle::new(position, size).contains(mouse_event.position());

            match self.visual {
                TextFieldVisual::Default | TextFieldVisual::Hover => {
                    if contains_mouse
                        && mouse_event.button() == MouseButton::Left
                        && mouse_event.pressed()
                    {
                        self.visual = TextFieldVisual::Pressed;

                        etx.ktx_mut().send_focus_request(FocusRequest::Focus(id));
                        etx.ktx_mut().send_keyboard_request(KeyboardRequest::Open(
                            self.text.clone(),
                            id,
                            self.cursor,
                            self.is_password_box,
                        ));

                        self.visual = TextFieldVisual::Default;
                    }
                }

                TextFieldVisual::Pressed => {
                    if mouse_event.button() == MouseButton::Left && !mouse_event.pressed() {
                        if !contains_mouse {
                            self.visual = TextFieldVisual::Default;
                            return;
                        }

                        self.visual = TextFieldVisual::Hover;
                    }
                }
                _ => {}
            }
        }

        if let Ok(move_event) = event.downcast_ref::<MoveEvent>() {
            let contains_mouse = Rectangle::new(position, size).contains(move_event.position());

            match self.visual {
                TextFieldVisual::Default => {
                    if contains_mouse {
                        self.visual = TextFieldVisual::Hover;
                    }
                }
                TextFieldVisual::Hover => {
                    if !contains_mouse {
                        self.visual = TextFieldVisual::Default;
                    }
                }
                _ => {}
            }
        }
    }
}
