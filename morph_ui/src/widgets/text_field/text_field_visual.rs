/// Defines the state of `TextField`. The state depends from the event input.
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Eq, Ord)]
pub enum TextFieldVisual {
    Default,
    Hover,
    Pressed,
    Disabled,
    Focused,
}

impl Default for TextFieldVisual {
    fn default() -> Self {
        Self::Default
    }
}
