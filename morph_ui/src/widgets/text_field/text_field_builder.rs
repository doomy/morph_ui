use crate::{
    eg::{prelude::*, text::renderer::TextRenderer},
    stdlib::boxed::Box,
    widgets::text_field::*,
};

/// `TextFieldBuilder` is used to define builder methods to construct a [`TextField`].
///
/// There is a default implementation of this trait for `Box<TextField<S, C, D, R>>`.
pub trait TextFieldBuilder {
    type TextStyle;
    type Style;
    type State;

    /// Builder method that is used ot set the text of the button.
    #[must_use]
    fn text(self, text: &str) -> Self;

    /// Builder method that is used to insert a new background style for the given visual.
    #[must_use]
    fn style(self, visual: TextFieldVisual, style: Self::Style) -> Self;

    /// Builder method that is used to insert a new text style for the given visual.
    #[must_use]
    fn text_style(self, visual: TextFieldVisual, style: Self::TextStyle) -> Self;

    /// Builder method that is used to set the focus style.
    #[must_use]
    fn focus_style(self, style: Self::Style) -> Self;

    /// Builder method that is used to set the style of the placeholder label.
    #[must_use]
    fn placeholder_style(self, style: Self::TextStyle) -> Self;

    /// Builder method that is used to insert a new the styles for the cursor.
    #[must_use]
    fn cursor_style(self, visual: TextFieldVisual, style: Self::Style) -> Self;

    // Builder method that is used to set the padding of the button. It is the space around the text of the button.
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the constraints of the button.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;

    /// Builder method that is used to set the corner radius of the buttons background.
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to add a new text input filter.
    #[must_use]
    fn filter(self, filter: &[char]) -> Self;

    /// Builder method that is used to set the flag that used to configure `TextField` as password box. Default is `false`.
    #[must_use]
    fn password_box(self, is_password_box: bool) -> Self;

    /// Builder method that is used to set the value that is used to that the character that masks each character of the `TextField` as long as `is_password_box is set to `true`.
    /// Default is `*`.
    #[must_use]
    fn password_mask(self, mask: char) -> Self;

    /// Builder method that is used to set the text that is displayed when the text of the `TextField` is empty.
    #[must_use]
    fn placeholder(self, placeholder: &str) -> Self;

    /// Builder method that is used to set the flat that is used to force focus on the `TextField`. If this is set to `true` it is possible that multiple widgets on the same time are focused.
    #[must_use]
    fn force_active_focus(self, force_active_focus: bool) -> Self;

    /// Builder method that is used to set the text cursor.
    #[must_use]
    fn cursor(self, cursor: Cursor) -> Self;

    /// Builder method that is used to set the callback that is called after the text changed.
    #[must_use]
    fn on_text_changed<F: Fn(&mut Self::State, &str) + 'static>(self, on_text_changed: F) -> Self;

    /// Builder method that is used to set the callback that is called after the enter key is registered on a focused `TextField`.
    #[must_use]
    fn on_enter<F: Fn(&mut Self::State) + 'static>(self, on_enter: F) -> Self;

    /// Builder method that is used to set the value that indicates if the button is enabled.
    #[must_use]
    fn enabled(self, is_enabled: bool) -> Self;

    /// Builder method that is used to set is_focusable of the button.
    #[must_use]
    fn focusable(self, is_focusable: bool) -> Self;
}

impl<S, D, R> TextFieldBuilder for Box<TextField<S, D, R>>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
{
    type TextStyle = R;
    type Style = PrimitiveStyle<D::Color>;
    type State = S;

    fn text(mut self, text: &str) -> Self {
        self.set_text(text);
        self
    }

    fn style(mut self, visual: TextFieldVisual, style: Self::Style) -> Self {
        self.insert_style(visual, style);
        self
    }

    fn text_style(mut self, visual: TextFieldVisual, style: Self::TextStyle) -> Self {
        self.insert_text_style(visual, style);
        self
    }

    fn focus_style(mut self, style: Self::Style) -> Self {
        self.set_focus_style(style);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }

    fn enabled(mut self, is_enabled: bool) -> Self {
        self.set_is_enabled(is_enabled);
        self
    }

    fn focusable(mut self, is_focusable: bool) -> Self {
        self.set_is_focusable(is_focusable);
        self
    }

    fn placeholder_style(mut self, style: Self::TextStyle) -> Self {
        self.set_placeholder_style(style);
        self
    }

    fn cursor_style(mut self, visual: TextFieldVisual, style: Self::Style) -> Self {
        self.insert_cursor_style(visual, style);
        self
    }

    fn filter(mut self, filter: &[char]) -> Self {
        self.add_filter(filter);
        self
    }

    fn password_box(mut self, is_password_box: bool) -> Self {
        self.set_is_password_box(is_password_box);
        self
    }

    fn password_mask(mut self, mask: char) -> Self {
        self.set_password_mask(mask);
        self
    }

    fn placeholder(mut self, placeholder: &str) -> Self {
        self.set_placeholder(placeholder);
        self
    }

    fn force_active_focus(mut self, force_active_focus: bool) -> Self {
        self.set_force_active_focus(force_active_focus);
        self
    }

    fn cursor(mut self, cursor: Cursor) -> Self {
        self.set_cursor(cursor);
        self
    }

    fn on_text_changed<F: Fn(&mut Self::State, &str) + 'static>(
        mut self,
        on_text_changed: F,
    ) -> Self {
        self.set_on_text_changed(on_text_changed);
        self
    }

    fn on_enter<F: Fn(&mut Self::State) + 'static>(mut self, on_enter: F) -> Self {
        self.set_on_enter(on_enter);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{
        mock_display::MockDisplay,
        mono_font::{ascii::*, *},
        pixelcolor::Rgb565,
    };

    type TestTextField = TextField<u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn builder_text() {
        assert_eq!(
            TestTextField::new().text("Hello morph").get_text(),
            "Hello morph"
        );
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestTextField::new()
                .style(
                    TextFieldVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_styles()
                .get(&TextFieldVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_text_style() {
        assert_eq!(
            TestTextField::new()
                .text_style(
                    TextFieldVisual::Default,
                    MonoTextStyleBuilder::new()
                        .font(&FONT_7X13)
                        .text_color(Rgb565::BLACK)
                        .build()
                )
                .get_text_styles()
                .get(&TextFieldVisual::Default),
            Some(
                &MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_focus_style() {
        assert_eq!(
            TestTextField::new()
                .focus_style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_focus_style(),
            PrimitiveStyleBuilder::new()
                .fill_color(Rgb565::BLACK)
                .build()
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestTextField::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestTextField::new()
                .constraints(Constraints::create().width(100).height(70))
                .get_constraints(),
            Constraints::create().width(100).height(70).build()
        );
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestTextField::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_is_password_box() {
        assert!(TestTextField::new()
            .password_box(true)
            .get_is_password_box());
    }

    #[test]
    fn builder_password_mask() {
        assert_eq!(
            TestTextField::new().password_mask('x').get_password_mask(),
            'x'
        );
    }

    #[test]
    fn builder_force_active_focus() {
        assert!(TestTextField::new()
            .force_active_focus(true)
            .get_force_active_focus());
    }

    #[test]
    fn builder_cursor() {
        assert_eq!(
            TestTextField::new().cursor(Cursor::new(5, 10)).get_cursor(),
            Cursor::new(5, 10)
        );
    }

    #[test]
    fn builder_is_enabled() {
        assert!(TestTextField::new().get_is_enabled());
        assert!(!TestTextField::new().enabled(false).get_is_enabled());
    }
}
