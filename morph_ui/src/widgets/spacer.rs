use crate::{
    eg::geometry::*,
    stdlib::{marker::PhantomData, *},
    *,
};

mod spacer_builder;

pub use spacer_builder::*;

/// `Spacer` is use to create empty space in a layout widget like `Row` or `Column`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, widget::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<dyn Widget<S, D>> {
///    Column::new()
///         .spacing(4)
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
///         .child(Spacer::new())
///         .align(Label::new()
///                 .text("morph")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///                 , "center"
///         )
/// }       
/// ```
pub struct Spacer<S, D> {
    base: WidgetBase,
    _state: PhantomData<S>,
    _dt: PhantomData<D>,
}

impl<S, D> Default for Spacer<S, D> {
    fn default() -> Self {
        Spacer {
            _state: PhantomData::default(),
            _dt: PhantomData::default(),
            base: WidgetBase {
                stretch: 1,
                ..Default::default()
            },
        }
    }
}

impl<S, D> Spacer<S, D> {
    /// Constructs a new `Spacer` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Spacer::default())
    }

    /// Gets the stretch factor of the spacer. The stretch axis is depending on the parent layout.
    pub fn get_stretch(&self) -> u32 {
        self.base.stretch
    }

    /// Sets the stretch factor of the spacer. The stretch axis is depending on the parent layout.
    pub fn set_stretch(&mut self, stretch: u32) {
        self.base.stretch = stretch;
    }
}

impl<S, D> Widget<S, D> for Spacer<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let width = if ltx.h_stretch() {
            ltx.available_width()
        } else {
            1
        };

        let height = if ltx.v_stretch() {
            ltx.available_height()
        } else {
            1
        };

        self.set_size(Size::new(width, height));

        self.size()
    }

    fn widget_string(&self) -> String {
        format!(
            "Spacer {{ stretch: {}, bounds {:?}  }}",
            self.stretch(),
            self.bounding_box()
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestSpacer = Spacer<u32, MockDisplay<Rgb565>>;

    #[test]
    fn layout() {
        assert_eq!(
            TestSpacer::new().layout(LayoutContext::new(Size::new(50, 50), true, false)),
            Size::new(1, 50)
        );

        assert_eq!(
            TestSpacer::new().layout(LayoutContext::new(Size::new(50, 50), false, true)),
            Size::new(50, 1)
        );
    }
}
