use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{vec::*, *},
    *,
};

mod frame_builder;

pub use frame_builder::*;

/// `Frame` is a responsive area with a two column layout. First column is reserved for a `navigation` widget e.g. a [`List`]
/// and the second column is intended for the `content` widget.
/// It is possible to define a `break_point`. If the width of `Frame` is less the given break point it will switch to a one
/// column layout. Then it is possible to switch between the view of the `navigation` and the `content` widget by using the `show_content`
/// flag.
///
/// # Layout details
///
/// ## Two column layout
///
/// * Active if no `break_point is set` or width of `Frame` is greater then `break_point`
/// * `navigation` widget is stretched vertical and the width will be constrained to the given `navigation_width`
/// * `content` widget uses remaining width (stretches) and is stretched vertical
///
/// ## One column layout (mobile layout)
///
/// * Active if `break_point is set` and width of `Frame` is less then `break_point`
/// * If `show_content` is set to `false` the `navigation` is shown otherwise the `content` widget
/// * The shown widget stretches vertical and horizontal
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Frame<i32, D>> {
///     Frame::new()
///         .navigation(Label::<i32, D, MonoTextStyle<'static, Rgb565>>::new().text("Navigation"))
///         .content(Label::<i32, D, MonoTextStyle<'static, Rgb565>>::new().text("Content"))
///         .break_point(600)
/// }
/// ```
pub struct Frame<S, D>
where
    D: DrawTarget,
{
    navigation: Option<Box<dyn Widget<S, D>>>,
    content: Option<Box<dyn Widget<S, D>>>,
    break_point: Option<u32>,
    show_content: bool,
    navigation_width: u32,
    is_mobile_layout: bool,
    on_layout_break: Option<Box<dyn Fn(&mut S, bool) + 'static>>,
    layout_break: bool,
    base: WidgetBase,
}

impl<S, D> Default for Frame<S, D>
where
    D: DrawTarget,
{
    fn default() -> Self {
        Frame {
            navigation: None,
            content: None,
            break_point: None,
            show_content: false,
            navigation_width: 240,
            is_mobile_layout: false,
            on_layout_break: None,
            layout_break: false,
            base: WidgetBase {
                stretch: 1,
                ..Default::default()
            },
        }
    }
}

impl<S, D> Frame<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    /// Constructs a new `Frame` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Frame::default())
    }

    /// Gets the navigation widget of `Frame`.
    pub fn get_navigation(&self) -> Option<&dyn Widget<S, D>> {
        self.navigation.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the navigation of the container.
    pub fn get_navigation_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.navigation.as_mut().map(|c| c.as_mut())
    }

    /// Sets the navigation widget of `Frame`.
    pub fn set_navigation(&mut self, navigation: Box<dyn Widget<S, D>>) {
        self.navigation = Some(navigation);
    }

    /// Gets the content of `Frame`.
    pub fn get_content(&self) -> Option<&dyn Widget<S, D>> {
        self.content.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the content of the container.
    pub fn get_content_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.content.as_mut().map(|c| c.as_mut())
    }

    /// Sets the content of `Frame`.
    pub fn set_content(&mut self, content: Box<dyn Widget<S, D>>) {
        self.content = Some(content);
    }

    /// Gets the break point that is used to define the wrap between two column and single column layout.
    pub fn get_break_point(&self) -> Option<u32> {
        self.break_point
    }

    /// Sets the break point that is used to define the wrap between two column and single column layout.
    pub fn set_break_point(&mut self, break_point: u32) {
        self.break_point = Some(break_point);
    }

    /// Gets the flag that indicates if the content is displayed if the `Frame` breaks to single column layout.
    pub fn get_show_content(&self) -> bool {
        self.show_content
    }

    /// If set to `true` if the navigation break to single column layout only the content will shown otherwise
    /// the navigation. If navigation does not break to single column layout both content and navigation will
    /// shown and `show_content` will be ignored.
    pub fn set_show_content(&mut self, show_content: bool) {
        self.show_content = show_content;
    }

    /// Gets the constrained width of the `Frame` area on two column layout.
    pub fn get_navigation_width(&self) -> u32 {
        self.navigation_width
    }

    /// Sets the constrained width of the `Frame` area on two column layout.
    pub fn set_navigation_width(&mut self, navigation_width: u32) {
        self.navigation_width = navigation_width;
    }

    /// Sets the callback that is called after a layout break.
    pub fn set_on_layout_break<F: Fn(&mut S, bool) + 'static>(&mut self, on_layout_break: F) {
        self.on_layout_break = Some(Box::new(on_layout_break));
    }

    // -- helpers
    fn check_break(&mut self, size: Size) {
        let is_mobile_layout = self.is_mobile_layout;

        self.is_mobile_layout = is_mobile(self.break_point, size);

        if is_mobile_layout != self.is_mobile_layout && self.size() != Size::zero() {
            self.layout_break = true;
        }
    }
}

impl<S, D> Widget<S, D> for Frame<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        // raises callback after layout break
        if self.layout_break {
            if let Some(on_layout_break) = &self.on_layout_break {
                on_layout_break(dtx.stx_mut().state_mut(), self.is_mobile_layout);
                dtx.stx_mut().trigger_rebuild();
            }

            self.layout_break = false;
        }

        let top_left = self.top_left();

        if !self.is_mobile_layout || !self.show_content {
            if let Some(navigation) = &mut self.navigation {
                navigation.update_and_draw(top_left, dtx)?;
            }
        }

        if !self.is_mobile_layout || self.show_content {
            if let Some(content) = &mut self.content {
                content.update_and_draw(top_left, dtx)?;
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        self.set_size(ltx.available_size());

        let size = self.size();

        self.check_break(size);

        if !self.is_mobile_layout || !self.show_content {
            if let Some(navigation) = &mut self.navigation {
                navigation.set_top_left(Point::zero());

                navigation.layout(LayoutContext::new(
                    navigation_size(self.is_mobile_layout, size, self.navigation_width),
                    true,
                    true,
                ));
            }
        }

        if !self.is_mobile_layout || self.show_content {
            if let Some(content) = &mut self.content {
                content.set_top_left(content_top_left(
                    self.is_mobile_layout,
                    self.navigation_width,
                ));
                content.layout(content_ltx(
                    self.is_mobile_layout,
                    self.navigation_width,
                    size,
                ));
            }
        }

        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Some(navigation) = &mut self.navigation {
            navigation.event(event, etx);
        }

        if let Some(content) = &mut self.content {
            content.event(event, etx);
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Navigation {{ stretch: {}, bounds: {:?} }}",
            self.stretch(),
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            if let Some(navigation) = &mut self.navigation {
                navigation.init_and_update(None, itx);
            }

            if let Some(content) = &mut self.content {
                content.init_and_update(None, itx);
            }
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Frame<S, D>>() {
            if let Some(new_navigation) = &mut new.navigation {
                new_navigation.init_and_update(self.navigation.as_deref(), itx);
            }
            if let Some(new_content) = &mut new.content {
                new_content.init_and_update(self.content.as_deref(), itx);
            }

            new.layout_break = self.layout_break;
            new.is_mobile_layout = self.is_mobile_layout;
            new.base.id = self.base.id;
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        let mut vec = Vec::new();
        if let Some(navigation) = &self.navigation {
            vec.push(navigation.as_ref());
        }

        if let Some(content) = &self.content {
            vec.push(content.as_ref());
        }

        vec
    }
}

// -- helpers

fn is_mobile(break_point: Option<u32>, size: Size) -> bool {
    if let Some(break_point) = break_point {
        return size.width < break_point;
    }

    false
}

fn content_top_left(is_mobile: bool, navigation_width: u32) -> Point {
    if is_mobile {
        Point::zero()
    } else {
        Point::new(navigation_width as i32, 0)
    }
}

fn navigation_size(is_mobile: bool, available_size: Size, navigation_width: u32) -> Size {
    if is_mobile {
        return available_size;
    }

    Size::new(navigation_width, available_size.height)
}

fn content_ltx(is_mobile: bool, navigation_width: u32, size: Size) -> LayoutContext {
    if is_mobile {
        LayoutContext::new(size, true, true)
    } else {
        LayoutContext::new(size - Size::new(navigation_width, 0), true, true)
    }
}

#[cfg(test)]
mod tests {
    use crate::LayoutContext;

    use super::{Point, Size};

    #[test]
    fn is_mobile() {
        assert!(!super::is_mobile(None, Size::default()));
        assert!(!super::is_mobile(Some(600), Size::new(1000, 600)));
        assert!(super::is_mobile(Some(600), Size::new(599, 600)));
    }

    #[test]
    fn content_top_left() {
        assert_eq!(super::content_top_left(false, 100), Point::new(100, 0));
        assert_eq!(super::content_top_left(true, 100), Point::zero());
    }

    #[test]
    fn navigation_size() {
        assert_eq!(
            super::navigation_size(false, Size::new(100, 100), 50),
            Size::new(50, 100)
        );
        assert_eq!(
            super::navigation_size(true, Size::new(100, 100), 50),
            Size::new(100, 100)
        );
    }

    #[test]
    fn content_ltx() {
        assert_eq!(
            super::content_ltx(false, 50, Size::new(100, 100)),
            LayoutContext::new(Size::new(50, 100), true, true)
        );
        assert_eq!(
            super::content_ltx(true, 50, Size::new(100, 100)),
            LayoutContext::new(Size::new(100, 100), true, true)
        );
    }
}
