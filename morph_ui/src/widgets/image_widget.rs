use crate::{
    eg::{geometry::*, image::*, pixelcolor::PixelColor},
    stdlib::{marker::PhantomData, string::String, *},
    *,
};

mod image_widget_builder;

pub use image_widget_builder::*;

/// `ImageWidget` is use to draw an image.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, widget::*};
/// use morph_ui::eg::{pixelcolor::{raw::BigEndian, *}, draw_target::DrawTarget, image::ImageRaw,};
///
/// const DATA: &[u8] = &[0x55; 8 * 8 * 3];
///
/// fn view<S: 'static, D: DrawTarget<Color = Rgb888> + 'static>() -> Box<dyn Widget<S, D>> {
///    ImageWidget::new()
///         .image(ImageRaw::<Rgb888, BigEndian>::new(DATA, 8))
/// }       
/// ```
pub struct ImageWidget<S, D, T: ImageDrawable + 'static> {
    base: WidgetBase,
    image: Option<T>,
    _state: PhantomData<S>,
    _dt: PhantomData<D>,
}

impl<S, D, T: ImageDrawable + 'static> Default for ImageWidget<S, D, T> {
    fn default() -> Self {
        ImageWidget {
            base: WidgetBase::new(),
            image: None,
            _state: PhantomData::default(),
            _dt: PhantomData::default(),
        }
    }
}

impl<S, D, T: ImageDrawable + 'static> ImageWidget<S, D, T> {
    /// Constructs a new `ImageWidget` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(ImageWidget::default())
    }

    /// Returns a reference of the image of the widget.
    pub fn get_image(&self) -> Option<&T> {
        self.image.as_ref()
    }

    /// Sets the image of the widget.
    pub fn set_image(&mut self, image: T) {
        self.image = Some(image);
    }
}

impl<C, T, S, D> Widget<S, D> for ImageWidget<S, D, T>
where
    C: PixelColor,
    T: ImageDrawable<Color = C> + 'static,
    S: 'static,
    D: DrawTarget<Color = C> + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        if !dtx.has_render_update(None) {
            return Ok(());
        }

        if let Some(image) = &self.image {
            Image::new(image, self.top_left()).draw(&mut dtx.display())?;
        }

        Ok(())
    }

    fn layout(&mut self, _: LayoutContext) -> Size {
        let size = if let Some(image) = &self.image {
            image.bounding_box().size
        } else {
            Size::zero()
        };

        self.set_size(size);

        size
    }

    fn widget_string(&self) -> String {
        format!("ImageWidget {{ bounds {:?} }}", self.bounding_box())
    }
}
