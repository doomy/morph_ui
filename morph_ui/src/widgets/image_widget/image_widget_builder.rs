use crate::{eg::prelude::*, stdlib::boxed::Box, widgets::ImageWidget};

/// `ImageWidgetBuilder` is used to define builder methods to construct a [`ImageWidget`].
///
/// There is a default implementation of this trait for `Box<ImageWidget<S, D>>`.
pub trait ImageWidgetBuilder {
    type Image;

    /// Builder method that is used to set the image of the widget.
    #[must_use]
    fn image(self, image: Self::Image) -> Self;
}

impl<S, D, T> ImageWidgetBuilder for Box<ImageWidget<S, D, T>>
where
    D: DrawTarget,
    T: ImageDrawable,
{
    type Image = T;

    fn image(mut self, image: Self::Image) -> Self {
        self.set_image(image);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{
        image::ImageRaw,
        mock_display::MockDisplay,
        pixelcolor::{raw::BigEndian, Rgb888},
    };

    const DATA: &[u8] = &[0x55; 8 * 8 * 3];
    type TestImageWidget = ImageWidget<u32, MockDisplay<Rgb888>, ImageRaw<'static, Rgb888>>;

    #[test]
    fn builder_stretch() {
        assert!(TestImageWidget::new()
            .image(ImageRaw::<Rgb888, BigEndian>::new(DATA, 8))
            .get_image()
            .is_some());
    }
}
