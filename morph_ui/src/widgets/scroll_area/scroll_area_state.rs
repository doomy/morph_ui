use crate::{
    context::EventContext,
    downcast::Any,
    eg::{
        geometry::{Point, Size},
        primitives::Rectangle,
    },
    events::{MouseButton, MouseEvent, MoveEvent, ScrollEvent},
    widgets::scroll_area::{helpers::*, ScrollBarVisual},
};

const SCROLL_SPEED: i32 = 2;

/// Contains the permanent fields of `ScrollArea`.
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Default)]
pub struct ScrollAreaState {
    /// Represents the current scroll offset.
    /// x => horizontal offset
    /// y => vertical offset
    pub offset: Point,

    /// Defines the visual state of the vertical scroll bar.
    pub ver_scroll_bar_visual: ScrollBarVisual,

    /// Defines the visual state of the horizontal scroll bar.
    pub hor_scroll_bar_visual: ScrollBarVisual,

    pub mouse_pos: Point,
}

impl ScrollAreaState {
    /// Updates the state by the given event. If this state cannot handle the given event, nothing on the state will changed.
    #[allow(clippy::too_many_arguments)]
    pub fn update<S>(
        &mut self,
        id: u32,
        event: &dyn Any,
        etx: &mut EventContext<S>,
        scroll: (bool, bool),
        sizes: (Size, Size),
        bounding_boxes: (Rectangle, Rectangle),
        contains_mouse: bool,
    ) {
        if let Ok(move_event) = event.downcast_ref::<MoveEvent>() {
            // vertical scroll bar pressed handling
            if scroll.0 && sizes.1.height > sizes.0.height {
                if self.ver_scroll_bar_visual == ScrollBarVisual::Pressed {
                    if self.mouse_pos != move_event.position() {
                        self.offset.y = get_offset(
                            self.offset.y,
                            self.mouse_pos.y - move_event.position().y,
                            1,
                            sizes.0.height,
                            sizes.1.height,
                        );
                        etx.stx_mut().trigger_update(Some(id));
                    }
                } else {
                    let contains_mouse = bounding_boxes.0.contains(move_event.position());

                    match self.ver_scroll_bar_visual {
                        ScrollBarVisual::Default => {
                            if contains_mouse {
                                self.ver_scroll_bar_visual = ScrollBarVisual::Hover;
                            }
                        }
                        ScrollBarVisual::Hover => {
                            if !contains_mouse {
                                self.ver_scroll_bar_visual = ScrollBarVisual::Default;
                            }
                        }
                        _ => {}
                    }
                }
            }

            // horizontal scroll bar pressed handling
            if scroll.1 && sizes.1.width > sizes.0.width {
                if self.hor_scroll_bar_visual == ScrollBarVisual::Pressed {
                    if self.mouse_pos != move_event.position() {
                        self.offset.x = get_offset(
                            self.offset.x,
                            self.mouse_pos.x - move_event.position().x,
                            1,
                            sizes.0.width,
                            sizes.1.width,
                        );
                        etx.stx_mut().trigger_update(Some(id));
                    }
                } else {
                    let contains_mouse = bounding_boxes.1.contains(move_event.position());

                    match self.hor_scroll_bar_visual {
                        ScrollBarVisual::Default => {
                            if contains_mouse {
                                self.hor_scroll_bar_visual = ScrollBarVisual::Hover;
                            }
                        }
                        ScrollBarVisual::Hover => {
                            if !contains_mouse {
                                self.hor_scroll_bar_visual = ScrollBarVisual::Default;
                            }
                        }
                        _ => {}
                    }
                }
            }

            self.mouse_pos = move_event.position();
        }

        if let Ok(mouse_event) = event.downcast_ref::<MouseEvent>() {
            // vertical scroll bar hover handling
            if scroll.0 && sizes.1.height > sizes.0.height {
                let contains_mouse = bounding_boxes.0.contains(mouse_event.position());

                match self.ver_scroll_bar_visual {
                    ScrollBarVisual::Default | ScrollBarVisual::Hover => {
                        if contains_mouse
                            && mouse_event.button() == MouseButton::Left
                            && mouse_event.pressed()
                        {
                            self.ver_scroll_bar_visual = ScrollBarVisual::Pressed;
                        }
                    }

                    ScrollBarVisual::Pressed => {
                        if mouse_event.button() == MouseButton::Left && !mouse_event.pressed() {
                            if !contains_mouse {
                                self.ver_scroll_bar_visual = ScrollBarVisual::Default;
                            } else {
                                // workaround that will be replaced by real touch events
                                if cfg!(target_os = "ios") || cfg!(target_os = "android") {
                                    self.ver_scroll_bar_visual = ScrollBarVisual::Default;
                                } else {
                                    self.ver_scroll_bar_visual = ScrollBarVisual::Hover;
                                }
                            }
                        }
                    }
                }
            }

            // horizontal scroll bar hover handling
            if scroll.1 && sizes.1.width > sizes.0.width {
                let contains_mouse = bounding_boxes.1.contains(mouse_event.position());

                match self.hor_scroll_bar_visual {
                    ScrollBarVisual::Default | ScrollBarVisual::Hover => {
                        if contains_mouse
                            && mouse_event.button() == MouseButton::Left
                            && mouse_event.pressed()
                        {
                            self.hor_scroll_bar_visual = ScrollBarVisual::Pressed;
                        }
                    }

                    ScrollBarVisual::Pressed => {
                        if mouse_event.button() == MouseButton::Left && !mouse_event.pressed() {
                            if !contains_mouse {
                                self.hor_scroll_bar_visual = ScrollBarVisual::Default;
                            } else {
                                // workaround that will be replaced by real touch events
                                if cfg!(target_os = "ios") || cfg!(target_os = "android") {
                                    self.hor_scroll_bar_visual = ScrollBarVisual::Default;
                                } else {
                                    self.hor_scroll_bar_visual = ScrollBarVisual::Hover;
                                }
                            }
                        }
                    }
                }
            }
        }

        if !contains_mouse {
            return;
        }

        if let Ok(scroll_event) = event.downcast_ref::<ScrollEvent>() {
            if scroll.1 && sizes.1.width > sizes.0.width {
                self.offset.x = get_offset(
                    self.offset.x,
                    scroll_event.offset().x,
                    SCROLL_SPEED,
                    sizes.0.width,
                    sizes.1.width,
                );
                etx.stx_mut().trigger_update(Some(id));
            }

            if scroll.0 && sizes.1.height > sizes.0.height {
                self.offset.y = get_offset(
                    self.offset.y,
                    scroll_event.offset().y,
                    SCROLL_SPEED,
                    sizes.0.height,
                    sizes.1.height,
                );
                etx.stx_mut().trigger_update(Some(id));
            }
        }
    }

    pub fn has_changes(&self, other: &ScrollAreaState) -> bool {
        self.offset != other.offset
            || self.ver_scroll_bar_visual != other.ver_scroll_bar_visual
            || self.hor_scroll_bar_visual != other.hor_scroll_bar_visual
    }
}
