use crate::eg::geometry::Size;

// Returns the max bounds depending on the given scroll settings.
pub fn get_max_bounds(is_horizontal_scroll: bool, is_vertical_scroll: bool, size: Size) -> Size {
    Size::new(
        get_max_size(is_horizontal_scroll, size.width),
        get_max_size(is_vertical_scroll, size.height),
    )
}

// Returns max of u32 if scroll is enabled otherwise the given size.
pub fn get_max_size(scroll_enabled: bool, size: u32) -> u32 {
    if scroll_enabled {
        // it needs also to match the i32 type therefore use i32::MAX instead of u32::MAX
        i32::MAX as u32
    } else {
        size
    }
}

// Calculates a new offset from old offset, scroll offset, speed, view port size and content size.
pub fn get_offset(
    offset: i32,
    scroll_offset: i32,
    speed: i32,
    view_port: u32,
    content: u32,
) -> i32 {
    i32::max(
        i32::min(0, offset + scroll_offset * speed),
        view_port as i32 - content as i32,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_max_size() {
        assert_eq!(32, get_max_size(false, 32));
        assert_eq!(i32::MAX as u32, get_max_size(true, 32));
    }

    #[test]
    fn test_get_max_bounds() {
        assert_eq!(
            Size::new(32, 64),
            get_max_bounds(false, false, Size::new(32, 64))
        );
        assert_eq!(
            Size::new(i32::MAX as u32, 64),
            get_max_bounds(true, false, Size::new(32, 64))
        );
        assert_eq!(
            Size::new(32, i32::MAX as u32),
            get_max_bounds(false, true, Size::new(32, 64))
        );
        assert_eq!(
            Size::new(i32::MAX as u32, i32::MAX as u32),
            get_max_bounds(true, true, Size::new(32, 64))
        );
    }

    #[test]
    fn test_get_offset() {
        assert_eq!(-10, get_offset(0, -20, 1, 90, 100));
        assert_eq!(-20, get_offset(0, -20, 1, 80, 100));
        assert_eq!(0, get_offset(0, 50, 1, 80, 100));
    }
}
