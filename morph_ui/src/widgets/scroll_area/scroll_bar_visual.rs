/// Defines the visual state of a scroll bar that is bar of the [`ScrollArea`].
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Eq, Ord)]
pub enum ScrollBarVisual {
    /// Default state.
    Default,

    /// ScrollBar is hovered.
    Hover,

    /// ScrollBar is pressed.
    Pressed,
}

impl Default for ScrollBarVisual {
    fn default() -> Self {
        ScrollBarVisual::Default
    }
}
