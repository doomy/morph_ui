use crate::{
    eg::{prelude::*, primitives::*},
    stdlib::boxed::Box,
    widgets::ScrollArea,
    *,
};

/// `ScrollAreaBuilder` is used to define builder methods to construct a [`ScrollArea`].
///
/// There is a default implementation of this trait for `Box<ScrollArea<S, D>>`.
pub trait ScrollAreaBuilder {
    type State;
    type DrawTarget;
    type Style;

    /// Builder method that is used to set the child of the scroll area.
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the box constraints of the scroll area.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;

    /// Builder method that is used to enable or disable vertical scrolling.
    #[must_use]
    fn ver_scroll(self, is_ver_scroll: bool) -> Self;

    /// Builder method that is used to enable or disable horizontal scrolling.
    #[must_use]
    fn hor_scroll(self, is_hor_scroll: bool) -> Self;

    /// Builder method that is used to set the background style of the scroll area.
    #[must_use]
    fn style(self, style: Self::Style) -> Self;

    /// Builder method that is used to set the style of the scroll bars.
    #[must_use]
    fn scroll_bar_style(self, visual: ScrollBarVisual, style: Self::Style) -> Self;

    /// Builder method that is used to set the size of the scroll bars.
    #[must_use]
    fn scroll_bar_size(self, size: u32) -> Self;

    /// Builder method that is used to set the margin of the scroll bars.
    #[must_use]
    fn scroll_bar_margin(self, margin: u32) -> Self;

    /// Builder method that is used to set the corner radius of the scroll bars.
    #[must_use]
    fn scroll_bar_corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the corner radius of the scroll area.
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;
}

impl<S, D> ScrollAreaBuilder for Box<ScrollArea<S, D>>
where
    S: 'static,
    D: DrawTarget,
{
    type State = S;
    type DrawTarget = D;
    type Style = PrimitiveStyle<D::Color>;

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_child(child);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }

    fn ver_scroll(mut self, is_ver_scroll: bool) -> Self {
        self.set_is_ver_scroll(is_ver_scroll);
        self
    }

    fn hor_scroll(mut self, is_hor_scroll: bool) -> Self {
        self.set_is_hor_scroll(is_hor_scroll);
        self
    }

    fn style(mut self, style: Self::Style) -> Self {
        self.set_style(style);
        self
    }

    fn scroll_bar_style(mut self, visual: ScrollBarVisual, style: Self::Style) -> Self {
        self.insert_scroll_bar_style(visual, style);
        self
    }

    fn scroll_bar_size(mut self, size: u32) -> Self {
        self.set_scroll_bar_size(size);
        self
    }

    fn scroll_bar_margin(mut self, margin: u32) -> Self {
        self.set_scroll_bar_margin(margin);
        self
    }

    fn scroll_bar_corners(mut self, corners: CornerRadii) -> Self {
        self.set_scroll_bar_corners(corners);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestScrollArea = ScrollArea<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_child() {
        let scroll_area = TestScrollArea::new().child(TestScrollArea::new());
        assert!(scroll_area.get_child().is_some());
    }

    #[test]
    fn builder_is_ver_scroll() {
        assert!(TestScrollArea::new().ver_scroll(true).get_is_ver_scroll());
    }

    #[test]
    fn builder_is_hor_scroll() {
        assert!(TestScrollArea::new().hor_scroll(true).get_is_hor_scroll());
    }

    #[test]
    fn builder_scroll_bar_size() {
        assert_eq!(
            TestScrollArea::new()
                .scroll_bar_size(10)
                .get_scroll_bar_size(),
            10
        );
    }

    #[test]
    fn builder_scroll_bar_margin() {
        assert_eq!(
            TestScrollArea::new()
                .scroll_bar_margin(10)
                .get_scroll_bar_margin(),
            10
        );
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestScrollArea::new()
                .style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_style(),
            Some(
                PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_scroll_bar_style() {
        assert_eq!(
            TestScrollArea::new()
                .scroll_bar_style(
                    ScrollBarVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_scroll_bar_styles()
                .get(&ScrollBarVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestScrollArea::new()
                .constraints(Constraints::create().width(100).height(70))
                .get_constraints(),
            Constraints::create().width(100).height(70).build()
        );
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestScrollArea::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_scroll_bar_corners() {
        assert_eq!(
            TestScrollArea::new()
                .scroll_bar_corners(CornerRadii::new(Size::new(10, 0)))
                .get_scroll_bar_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }
}
