use crate::{
    downcast::Any,
    eg::{
        geometry::*,
        primitives::*,
        text::{
            renderer::{CharacterStyle, TextRenderer},
            Baseline,
        },
    },
    stdlib::{cmp, collections::BTreeMap, marker::PhantomData, string::*, vec, vec::*, *},
    *,
};

static DEFAULT_PASSWORD_MASK: char = '*';

mod text_field_builder;
mod text_field_state;
mod text_field_visual;

pub use text_field_builder::*;
pub use text_field_state::*;
pub use text_field_visual::*;

/// `TextField` is a widget that can displays text and handles text input from keyboard.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::text_field::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<TextField<String, D, MonoTextStyle<'static, Rgb565>>> {
///     TextField::new()
///         .text("Click me")
///         .on_text_changed(|state: &mut String, text: &str| *state = text.into())
/// }
/// ```
pub struct TextField<S, D, R>
where
    D: DrawTarget,
{
    text_styles: BTreeMap<TextFieldVisual, R>,
    focus_style: PrimitiveStyle<D::Color>,
    styles: BTreeMap<TextFieldVisual, PrimitiveStyle<D::Color>>,
    cursor_styles: BTreeMap<TextFieldVisual, PrimitiveStyle<D::Color>>,
    label: Box<Label<S, D, R>>,
    state: TextFieldState,
    padding: Thickness,
    base: WidgetBase,
    cursor_box: Rectangle,
    _text_align: Align,
    constraints: Constraints,
    corners: CornerRadii,
    filter: Vec<char>,
    password_mask: char,
    placeholder_label: Label<S, D, R>,
    placeholder_style: Option<R>,
    on_text_changed: Option<Box<dyn Fn(&mut S, &str) + 'static>>,
    on_enter: Option<Box<dyn Fn(&mut S) + 'static>>,
    force_active_focus: bool,
    _state: PhantomData<S>,
    _dt: PhantomData<D>,
}

impl<S, D, R> Default for TextField<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    fn default() -> Self {
        TextField {
            styles: BTreeMap::new(),
            focus_style: PrimitiveStyle::new(),
            text_styles: BTreeMap::new(),
            cursor_styles: BTreeMap::new(),
            label: Label::new(),
            state: TextFieldState::new(),
            padding: Thickness::from(8),
            base: WidgetBase::new(),
            cursor_box: Rectangle::default(),
            _text_align: Align::default(),
            constraints: Constraints::default(),
            corners: CornerRadii::default(),
            filter: vec![],
            password_mask: DEFAULT_PASSWORD_MASK,
            placeholder_label: Label::default(),
            placeholder_style: None,
            on_text_changed: None,
            on_enter: None,
            force_active_focus: false,
            _state: PhantomData::default(),
            _dt: PhantomData::default(),
        }
    }
}

impl<S, D, R> TextField<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    /// Constructs a new `TextField` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(TextField::default())
    }

    /// Returns the text of the `TextField` as &str.
    pub fn get_text(&self) -> &str {
        self.state.text.as_str()
    }

    /// Sets the text of the `TextField`.
    pub fn set_text(&mut self, text: &str) {
        self.state.text = text.into();
        self.label.set_text(self.state.text.as_str());
    }

    /// Returns the background style map of the text_field.
    pub fn get_styles(&self) -> &BTreeMap<TextFieldVisual, PrimitiveStyle<D::Color>> {
        &self.styles
    }

    /// Sets the styles of the `TextField` background.
    pub fn set_styles(&mut self, styles: BTreeMap<TextFieldVisual, PrimitiveStyle<D::Color>>) {
        self.styles = styles;
    }

    // Inserts a new background style for the given visual.
    pub fn insert_style(
        &mut self,
        visual: TextFieldVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.styles.insert(visual, style.into());
    }

    /// Gets the text styles map of the `TextField`.
    pub fn get_text_styles(&self) -> &BTreeMap<TextFieldVisual, R> {
        &self.text_styles
    }

    /// Sets the text styles map of the `TextField`.
    pub fn set_text_styles(&mut self, text_styles: BTreeMap<TextFieldVisual, R>) {
        self.text_styles = text_styles;
    }

    /// Inserts a new text style to the text styles map of the `Button`.
    pub fn insert_text_style(&mut self, visual: TextFieldVisual, style: impl Into<R>) {
        self.text_styles.insert(visual, style.into());
    }

    /// Gets the focus style of the `TextField`.
    pub fn get_focus_style(&self) -> PrimitiveStyle<D::Color> {
        self.focus_style
    }

    /// Sets the focus style of the `TextField`.
    pub fn set_focus_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.focus_style = style.into();
    }

    /// Gets the style of the placeholder label.
    pub fn get_placeholder_style(&self) -> Option<&R> {
        self.placeholder_style.as_ref()
    }

    /// Sets the style of the placeholder label.
    pub fn set_placeholder_style(&mut self, placeholder_style: R) {
        self.placeholder_style = Some(placeholder_style);
    }

    /// Returns the background style map of the `TextField`.
    pub fn get_cursor_styles(&self) -> &BTreeMap<TextFieldVisual, PrimitiveStyle<D::Color>> {
        &self.cursor_styles
    }

    /// Sets the styles of the cursor.
    pub fn set_cursor_styles(
        &mut self,
        cursor_styles: BTreeMap<TextFieldVisual, PrimitiveStyle<D::Color>>,
    ) {
        self.cursor_styles = cursor_styles;
    }

    // Inserts a new the styles for the cursor.
    pub fn insert_cursor_style(
        &mut self,
        visual: TextFieldVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.cursor_styles.insert(visual, style.into());
    }

    /// Gets the padding of the `TextField`. It is the space around the text of the `TextField`.
    pub fn get_padding(&self) -> Thickness {
        self.padding
    }

    /// Sets the padding of the `TextField`. It is the space around the text of the `TextField`.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.padding = padding.into();
    }

    /// Gets the box constraints of the `TextField`.
    pub fn get_constraints(&self) -> Constraints {
        self.constraints
    }

    /// Sets the box constraints of the `TextField`.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.constraints = constraints.into();
    }

    /// Gets the corner radius of the `TextField`s background.
    pub fn get_corners(&self) -> CornerRadii {
        self.corners
    }

    /// Sets the corner radius of the `TextField`s background.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.corners = corners;
    }

    /// Gets the text input filter.
    pub fn get_filter(&self) -> &[char] {
        &self.filter
    }

    /// Adds a text input filter.
    pub fn add_filter(&mut self, filter: &[char]) {
        self.filter.append(&mut filter.to_vec());
    }

    /// Gets the flag that used to configure `TextField` as password box.
    pub fn get_is_password_box(&self) -> bool {
        self.state.is_password_box
    }

    /// Sets the flag that used to configure `TextField` as password box. Default is `false`.
    pub fn set_is_password_box(&mut self, is_password_box: bool) {
        self.state.is_password_box = is_password_box;
    }

    /// Gets the value that is used to that the character that masks each character of the `TextField` as long as `is_password_box is set to `true`.
    pub fn get_password_mask(&self) -> char {
        self.password_mask
    }

    /// Sets the value that is used to that the character that masks each character of the `TextField` as long as `is_password_box is set to `true`.
    /// Default is `*`.
    pub fn set_password_mask(&mut self, password_mask: char) {
        self.password_mask = password_mask;
    }

    /// gets the text that is displayed when the text of the `TextField` is empty.
    pub fn get_placeholder(&self) -> &str {
        self.placeholder_label.get_text()
    }

    /// Sets the text that is displayed when the text of the `TextField` is empty.
    pub fn set_placeholder(&mut self, placeholder: &str) {
        self.placeholder_label.set_text(placeholder);
    }

    /// Gets the flat that is used to force focus on the `TextField`.
    /// If this is set to `true` it is possible that multiple widgets on the same time are focused.
    pub fn get_force_active_focus(&self) -> bool {
        self.force_active_focus
    }

    /// This is used to force focus on the `TextField`. If this is set to `true` it is possible that multiple widgets on the same time are focused.
    pub fn set_force_active_focus(&mut self, force_active_focus: bool) {
        self.force_active_focus = force_active_focus;
    }

    /// Gets the current text cursor.
    pub fn get_cursor(&self) -> Cursor {
        self.state.cursor
    }

    /// Sets the current text cursor.
    pub fn set_cursor(&mut self, cursor: Cursor) {
        self.state.cursor = cursor;
    }

    /// Gets the value that indicates if the `TextField` is enabled.
    pub fn get_is_enabled(&self) -> bool {
        self.state.get_is_enabled()
    }

    /// Sets the value that indicates if the `TextField` is enabled.
    pub fn set_is_enabled(&mut self, is_enabled: bool) {
        self.state.set_is_enabled(is_enabled);
    }

    /// Sets the callback that is called after the text changed.
    pub fn set_on_text_changed<F: Fn(&mut S, &str) + 'static>(&mut self, on_text_changed: F) {
        self.on_text_changed = Some(Box::new(on_text_changed));
    }

    /// Sets the callback that is called after the enter key is registered on a focused `TextField`.
    pub fn set_on_enter<F: Fn(&mut S) + 'static>(&mut self, on_enter: F) {
        self.on_enter = Some(Box::new(on_enter));
    }

    // -- Helpers --

    fn text_visual(&self) -> TextFieldVisual {
        if self.text_styles.contains_key(&self.state.get_visual()) {
            self.state.get_visual()
        } else {
            TextFieldVisual::Default
        }
    }

    fn style_visual(&self) -> TextFieldVisual {
        if self.base.has_focus || self.force_active_focus {
            return TextFieldVisual::Focused;
        }

        if self.styles.contains_key(&self.state.get_visual()) {
            self.state.get_visual()
        } else {
            TextFieldVisual::Default
        }
    }

    /// Builder method that is used is used to set the alignment of the text.
    #[must_use]
    pub fn _text_align(mut self, _text_align: impl Into<Align>) -> Self {
        self._text_align = _text_align.into();
        self
    }

    // Sets the text of the label depending on `is_password_box`.
    fn update_label_text(&mut self) {
        if !self.get_is_password_box() {
            self.label.set_text(self.state.text.as_str());
        } else {
            // masks the text with password mask
            self.label.set_text(
                vec![self.password_mask; self.state.text.encode_utf16().count()]
                    .iter()
                    .collect::<String>()
                    .as_str(),
            );
        }
    }

    // Returns the width of a text part of the label.
    fn text_part_width(&self, start: usize, end: usize) -> u32 {
        let s = cmp::min(start, end);
        let e = cmp::max(start, end);

        let len = self.label.get_text().len();

        if s < len && end <= len {
            let utf_16: Vec<u16> = self.label.get_text().encode_utf16().collect();
            let utf_16_text_part: Vec<u16> = utf_16[s..e].to_vec();

            let text_part = String::from_utf16_lossy(&utf_16_text_part);
            if let Some(style) = self.label.get_style() {
                return style
                    .measure_string(text_part.as_str(), Point::default(), Baseline::Top)
                    .bounding_box
                    .size
                    .width;
            }
        }

        0
    }

    // Removes the text from the current cursor selection
    fn remove_selection(&mut self, before_cursor: bool) {
        let mut text: Vec<u16> = self.state.text.encode_utf16().collect();

        if self.state.cursor.is_empty() {
            // remove single character

            if before_cursor && self.state.cursor.start > 0 {
                text.remove(self.state.cursor.start as usize - 1);

                self.state.cursor.start -= 1;
            } else if !before_cursor
                && self.state.cursor.start < String::from(self.label.get_text()).len()
            {
                text.remove(self.state.cursor.start as usize);
            }
        } else {
            let index = cmp::min(self.state.cursor.start, self.state.cursor.end);
            // remove multiple characters
            for _ in 0..self.state.cursor.len() {
                text.remove(index);
            }
            self.state.cursor.start = index;
        }

        self.state.cursor.end = self.state.cursor.start;
        self.state.text = String::from_utf16_lossy(&text);
        self.update_label_text();
    }

    // Inserts text.
    fn insert_text(&mut self, input_text: &str) {
        // check if the text input matches the filter

        if !self.filter.is_empty() && !input_text.contains(self.filter.as_slice()) {
            return;
        }

        // remove text to replace
        if !self.state.cursor.is_empty() {
            self.remove_selection(true);
        }

        // handle utf16 text input
        let mut text: Vec<u16> = self.state.text.encode_utf16().collect();
        let input_utf_16: Vec<u16> = input_text.encode_utf16().collect();
        let input_len = input_utf_16.len();

        let mut start = self.state.cursor.start;
        for u in input_utf_16 {
            text.insert(start, u);
            start += 1;
        }
        self.state.text = String::from_utf16_lossy(&text);
        self.update_label_text();

        self.state.cursor.start += input_len;
        self.state.cursor.end = self.state.cursor.start;
    }
}

impl<S, D, R> Widget<S, D> for TextField<S, D, R>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + Clone + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        // `TextField` always need to be redrawn after focus is changed.
        let redraw = dtx.has_render_update(Some(self.id()));

        if redraw {
            // draw background
            if let Some(style) = self.styles.get(&self.style_visual()) {
                if self.corners == CornerRadii::default() {
                    self.bounding_box()
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                } else {
                    RoundedRectangle::new(self.bounding_box(), self.corners)
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                }
            }
        }

        if dtx.stx().layout_update() {
            self.cursor_box.top_left += self.top_left() + Point::new(self.state.text_offset, 0);
        }

        if redraw && (self.has_focus() || self.force_active_focus) {
            // draw cursor
            if let Some(cursor_style) = self.cursor_styles.get(&self.style_visual()) {
                self.cursor_box
                    .into_styled(*cursor_style)
                    .draw(&mut dtx.display())?;
            }
        }

        if !self.label.get_text().is_empty() {
            // draw text
            if let Some(text_style) = self.text_styles.get(&self.text_visual()) {
                self.label
                    .set_clip_box(self.padding.inner_bounding_box(self.bounding_box()));
                self.label.set_style(text_style.clone());
            }

            let dtx = dtx.all();
            self.label.update_and_draw(
                self.top_left() + Point::new(self.state.text_offset, 0),
                &mut DrawContext::from_draw_context(dtx, None, redraw),
            )?;
        } else {
            // draw placeholder
            if let Some(style) = &self.placeholder_style {
                self.placeholder_label
                    .set_clip_box(self.padding.inner_bounding_box(self.bounding_box()));
                self.placeholder_label.set_style(style.clone());
            }

            let dtx = dtx.all();
            self.placeholder_label.update_and_draw(
                self.top_left() + Point::new(1, 0),
                &mut DrawContext::from_draw_context(dtx, None, redraw),
            )?;
        }

        if redraw && (self.has_focus() || self.force_active_focus) {
            if self.corners == CornerRadii::default() {
                self.bounding_box()
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            } else {
                RoundedRectangle::new(self.bounding_box(), self.corners)
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        if let Some(text_style) = self.text_styles.get(&self.text_visual()) {
            self.label.set_style(text_style.clone());
            let text_size = self.label.layout(LayoutContext::new(
                self.constraints
                    .constrain_size(self.padding.inner_size(ltx.available_size())),
                false,
                false,
            ));

            // background layout
            self.set_size(self.constraints.constrain_size(Size::new(
                get_size(
                    ltx.h_stretch(),
                    ltx.available_width(),
                    text_size.width,
                    self.padding.left + self.padding.right,
                ),
                get_size(
                    ltx.v_stretch(),
                    ltx.available_height(),
                    text_size.height,
                    self.padding.top + self.padding.bottom,
                ),
            )));

            // cursor layout

            // get minimum value of cursor as start
            let cursor_start =
                self.text_part_width(0, cmp::min(self.state.cursor.start, self.state.cursor.end));

            // cursor needs to be minimum 1 pixel width, use cmp::min to clip the cursor
            let cursor_width = cmp::min(
                self.padding
                    .inner_bounding_box(self.bounding_box())
                    .size
                    .width,
                cmp::max(
                    1,
                    self.text_part_width(self.state.cursor.start, self.state.cursor.end),
                ),
            );
            self.cursor_box.top_left = Point::new(
                (self.padding.left + cursor_start) as i32,
                self.padding.top as i32,
            );
            self.cursor_box.size =
                Size::new(cursor_width, self.padding.inner_height(self.height()));

            // check if cursor is outside of its bounds and correct with offset

            let cursor_x = self.cursor_box.top_left.x + self.state.text_offset;
            if cursor_x < self.padding.left as i32 {
                self.state.text_offset += self.padding.left as i32 - (cursor_x);
            }

            let end_x = self.width() as i32 - self.padding.right as i32;
            if cursor_x > end_x {
                self.state.text_offset += end_x - cursor_x;
            }

            if !self.label.get_text().is_empty() {
                // label layout
                self.label.set_top_left(Point::new(
                    Align::Start.align(self.padding.left as i32, self.width(), text_size.width),
                    Align::Center.align(0, self.height(), text_size.height),
                ));
            } else {
                // placeholder layout
                self.placeholder_label.set_top_left(Point::new(
                    Align::Start.align(self.padding.left as i32, self.width(), text_size.width),
                    Align::Center.align(0, self.height(), text_size.height),
                ));
            }
        } else {
            self.set_size(Size::zero())
        }

        self.size()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Ok(keyboard_event) = event.downcast_ref::<KeyboardEvent>() {
            if self.id() == keyboard_event.id() {
                self.set_text(keyboard_event.text());
                self.set_cursor(keyboard_event.cursor());
                etx.stx_mut().trigger_update(Some(self.id()));
                etx.ktx_mut()
                    .send_focus_request(FocusRequest::Focus(self.id()));
            }
            return;
        }
        if !event.is::<MouseEvent>()
            && !event.is::<MoveEvent>()
            && !event.is::<TextInputEvent>()
            && !event.is::<KeyEvent>()
            && !event.is::<DropEvent>()
            || self.state.get_visual() == TextFieldVisual::Disabled
        {
            return;
        }

        let old_visual = self.state.get_visual();

        self.state
            .update(self.id(), event, etx, self.top_left(), self.size());

        if old_visual != self.state.get_visual() {
            etx.stx_mut().trigger_render_update(Some(self.id()));
        }

        let text = self.state.text.clone();
        let cursor = self.state.cursor;

        // drop event
        if let Ok(e) = event.downcast_ref::<DropEvent>() {
            if self.bounding_box().contains(e.position()) {
                self.insert_text(e.content().as_str());
            }
        }

        if self.has_focus() || self.force_active_focus {
            let text_label = String::from(self.label.get_text());
            let text_label_utf_16: Vec<u16> = text_label.encode_utf16().collect();

            let inner_bounding_box = self.padding.inner_bounding_box(self.bounding_box());

            // mouse event
            if let Ok(e) = event.downcast_ref::<MouseEvent>() {
                // set cursor position by mouse down
                if e.button() == MouseButton::Left
                    && e.pressed()
                    && inner_bounding_box.contains(e.position())
                {
                    // check the position of the mouse inside of the text
                    for i in 0..text_label_utf_16.len() {
                        let utf_16_text_part = text_label_utf_16[0..i].to_vec();

                        let text_part = String::from_utf16_lossy(&utf_16_text_part);
                        if let Some(style) = self.label.get_style() {
                            let text_measure = style.measure_string(
                                text_part.as_str(),
                                Point::default(),
                                Baseline::Top,
                            );

                            if inner_bounding_box.top_left.x
                                + text_measure.bounding_box.size.width as i32
                                + self.state.text_offset
                                > e.position().x
                            {
                                break;
                            }

                            self.state.cursor.start = i;
                            self.state.cursor.end = i;
                        }
                    }
                }
            }

            // key event
            if let Ok(e) = event.downcast_ref::<KeyEvent>() {
                if e.pressed() {
                    match e.key() {
                        // select all
                        Key::A => {
                            if etx.ktx().is_os_ctrl_pressed() {
                                // select all
                                self.state.cursor.start = 0;
                                self.state.cursor.end = self.state.text.encode_utf16().count();
                            }
                        }
                        // copy
                        Key::C => {
                            if etx.ktx().is_os_ctrl_pressed() {
                                let start = if self.state.cursor.start < self.state.cursor.end {
                                    self.state.cursor.start
                                } else {
                                    self.state.cursor.end
                                };

                                let end = if self.state.cursor.start > self.state.cursor.end {
                                    self.state.cursor.start
                                } else {
                                    self.state.cursor.end
                                };

                                etx.ctx_mut().write(&self.state.text[start..end]);
                            }
                        }
                        // paste
                        Key::V => {
                            if etx.ktx().is_os_ctrl_pressed() {
                                self.insert_text(etx.ctx().content.as_str());
                            }
                        }
                        // cut
                        Key::X => {
                            if etx.ktx().is_os_ctrl_pressed() {
                                let start = if self.state.cursor.start < self.state.cursor.end {
                                    self.state.cursor.start
                                } else {
                                    self.state.cursor.end
                                };

                                let end = if self.state.cursor.start > self.state.cursor.end {
                                    self.state.cursor.start
                                } else {
                                    self.state.cursor.end
                                };

                                etx.ctx_mut().write(&self.state.text[start..end]);
                                self.insert_text("");
                            }
                        }
                        // remove char with bks
                        Key::Backspace => self.remove_selection(true),
                        // remove char with delete
                        Key::Delete => self.remove_selection(false),
                        // navigate left
                        Key::ArrowLeft => {
                            if !etx.ktx().is_shift_pressed() {
                                // move cursor left
                                if self.state.cursor.start > self.state.cursor.end {
                                    self.state.cursor.start = self.state.cursor.end;
                                } else {
                                    if self.state.cursor.start > 0 {
                                        self.state.cursor.start -= 1;
                                    }
                                    self.state.cursor.end = self.state.cursor.start;
                                }
                            } else {
                                // move cursor end
                                if self.state.cursor.end > 0 {
                                    self.state.cursor.end -= 1;
                                }
                            }
                        }
                        // navigate right
                        Key::ArrowRight => {
                            if !etx.ktx().is_shift_pressed() {
                                // move cursor right
                                if self.state.cursor.start < self.state.cursor.end {
                                    self.state.cursor.start = self.state.cursor.end;
                                } else if self.state.cursor.start < text_label_utf_16.len() {
                                    self.state.cursor.start += 1;
                                    self.state.cursor.end = self.state.cursor.start;
                                }
                            } else {
                                // move cursor end
                                if self.state.cursor.end < text_label_utf_16.len() {
                                    self.state.cursor.end += 1;
                                }
                            }
                        }
                        Key::Enter => {
                            if let Some(on_enter) = &self.on_enter {
                                on_enter(etx.stx_mut().state_mut());
                            }
                        }
                        _ => {}
                    }
                }
            }

            // text input
            if !etx.ktx().is_ctrl_pressed() {
                if let Ok(e) = event.downcast_ref::<TextInputEvent>() {
                    self.insert_text(String::from(e.input()).as_str());
                }
            }
        }

        if text != self.state.text {
            if let Some(on_text_changed) = &self.on_text_changed {
                on_text_changed(etx.stx_mut().state_mut(), self.state.text.as_str());
            }
        }

        // trigger render and layout update when text or cursor position is changed
        if text.as_str() != self.state.text || cursor != self.state.cursor {
            etx.stx_mut().trigger_update(Some(self.id()));
        }
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        }

        if self.style_visual() == TextFieldVisual::Disabled {
            self.base.is_focusable = false;
        }

        self.init_id(itx);
        self.update_label_text();
    }

    fn transfer(&self, new: &mut dyn Any, _itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<TextField<S, D, R>>() {
            new.state = self.state.clone();
            new.base.id = self.base.id;
            new.base.has_focus = self.base.has_focus;
            new.force_active_focus = self.force_active_focus;
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "TextField {{ text: \"{}\", is_password_box: {}, bounds: {:?} }}",
            self.state.text,
            self.get_is_password_box(),
            self.bounding_box()
        )
    }

    fn is_disabled(&self) -> bool {
        self.state.get_visual() == TextFieldVisual::Disabled
    }
}
