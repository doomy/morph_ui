use crate::{
    downcast::Any,
    eg::{geometry::*, primitives::*},
    stdlib::{collections::BTreeMap, marker::PhantomData, string::*, *},
    widgets::*,
    *,
};

mod switch_builder;

pub use switch_builder::*;

/// `Switch` is a widget that can be tapped and performs an action (on_tap) and can be toggled between checked and unchecked.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Switch<i32, D>> {
///     Switch::new()
///         .is_checked(true)
/// }
/// ```

pub struct Switch<S, D: DrawTarget> {
    styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>,
    focus_style: PrimitiveStyle<D::Color>,
    toggle_styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>,
    state: ButtonState,
    base: WidgetBase,
    toggle_bounding_box: Rectangle,
    corners: CornerRadii,
    toggle_corners: CornerRadii,
    padding: Thickness,
    constraints: Constraints,
    on_tap: Option<Box<dyn Fn(&mut S) + 'static>>,
    on_is_checked_changed: Option<Box<dyn Fn(&mut S, bool) + 'static>>,
    _dt: PhantomData<D>,
}

impl<S, D: DrawTarget + 'static> Default for Switch<S, D> {
    fn default() -> Self {
        let mut state = ButtonState::new();
        state.set_is_checkable(true);

        Switch {
            styles: BTreeMap::new(),
            focus_style: PrimitiveStyle::new(),
            toggle_styles: BTreeMap::new(),
            state,
            base: WidgetBase::new(),
            toggle_bounding_box: Rectangle::default(),
            padding: Thickness::from(1),
            corners: CornerRadii::default(),
            toggle_corners: CornerRadii::default(),
            constraints: Constraints::default(),
            on_tap: None,
            on_is_checked_changed: None,
            _dt: PhantomData::default(),
        }
    }
}

impl<S, D> Switch<S, D>
where
    D: DrawTarget + 'static,
{
    /// Constructs a new `Button` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Switch::default())
    }

    /// Returns the background style map of the switch.
    pub fn get_styles(&self) -> &BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>> {
        &self.styles
    }

    /// Sets the background styles map of the switch.
    pub fn set_styles(&mut self, styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>) {
        self.styles = styles;
    }

    /// Inserts a new background style for the given visual.
    pub fn insert_style(
        &mut self,
        visual: ButtonVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.styles.insert(visual, style.into());
    }

    /// Returns the styles for the switch toggle.
    pub fn get_toggle_styles(&self) -> &BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>> {
        &self.toggle_styles
    }
    /// Sets the styles of the switch toggle.
    pub fn set_toggle_styles(&mut self, styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>) {
        self.toggle_styles = styles;
    }

    /// Inserts a new toggle style for the given visual.
    pub fn insert_toggle_style(
        &mut self,
        visual: ButtonVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.toggle_styles.insert(visual, style.into());
    }

    /// Gets the focus style of the switch.
    pub fn get_focus_style(&self) -> PrimitiveStyle<D::Color> {
        self.focus_style
    }

    /// Sets the focus style of the switch.
    pub fn set_focus_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.focus_style = style.into();
    }

    /// Gets the padding of the switch. It is the space around the text of the switch.
    pub fn get_padding(&self) -> Thickness {
        self.padding
    }

    /// Sets the padding of the switch. It is the space around the text of the switch.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.padding = padding.into();
    }

    /// Gets the box constraints of the switch.
    pub fn get_constraints(&self) -> Constraints {
        self.constraints
    }

    /// Sets the box constraints of the switch.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.constraints = constraints.into();
    }

    /// Gets the current visual state of the switch.
    pub fn get_visual(&self) -> ButtonVisual {
        self.state.get_visual()
    }

    /// Sets the current visual state of the switch.
    pub fn set_visual(&mut self, visual: impl Into<ButtonVisual>) {
        self.state.set_visual(visual);
    }

    /// Gets the value that indicates if the switch is currently checked.
    pub fn get_is_checked(&self) -> bool {
        self.state.get_is_checked()
    }

    ///  Sets the value that indicates if the switch is currently checked.
    pub fn set_is_checked(&mut self, is_checked: bool) {
        self.state.set_is_checked(is_checked);
    }

    /// Gets the corner radius of the switch background.
    pub fn get_corners(&self) -> CornerRadii {
        self.corners
    }

    /// Sets the corner radius of the switch background.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.corners = corners;
    }

    /// Gets the value that indicates if the switch is enabled.
    pub fn get_is_enabled(&self) -> bool {
        self.state.get_is_enabled()
    }

    /// Sets the value that indicates if the switch is enabled.
    pub fn set_is_enabled(&mut self, is_enabled: bool) {
        self.state.set_is_enabled(is_enabled);
    }

    //// Gets the corner radius of the switch toggle.
    pub fn get_toggle_corners(&self) -> CornerRadii {
        self.toggle_corners
    }

    /// Sets the toggle corner radius.
    pub fn set_toggle_corners(&mut self, toggle_corners: CornerRadii) {
        self.toggle_corners = toggle_corners;
    }

    /// Sets the callback that is called after the button is tapped.
    pub fn set_on_tap<F: Fn(&mut S) + 'static>(&mut self, on_tap: F) {
        self.on_tap = Some(Box::new(on_tap));
    }

    /// Sets the callback that is called after `is_checked` of button is changed.
    pub fn set_on_is_checked_changed<F: Fn(&mut S, bool) + 'static>(
        &mut self,
        on_checked_changed: F,
    ) {
        self.on_is_checked_changed = Some(Box::new(on_checked_changed));
    }

    // -- Helpers --

    fn style_visual(&self) -> ButtonVisual {
        if self.styles.contains_key(&self.state.get_visual()) {
            self.state.get_visual()
        } else {
            ButtonVisual::Default
        }
    }

    fn toggle_style_visual(&self) -> ButtonVisual {
        if self.toggle_styles.contains_key(&self.state.get_visual()) {
            self.state.get_visual()
        } else {
            ButtonVisual::Default
        }
    }
}

impl<S, D> Widget<S, D> for Switch<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let redraw = dtx.has_render_update(Some(self.id()));

        if redraw {
            // Draws the background.
            if let Some(style) = self.styles.get(&self.style_visual()) {
                if self.corners == CornerRadii::default() {
                    self.bounding_box()
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                } else {
                    RoundedRectangle::new(self.bounding_box(), self.corners)
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                }
            }
        }

        // Draws the toggle
        if let Some(toggle_style) = self.toggle_styles.get(&self.toggle_style_visual()) {
            if dtx.stx().layout_update() {
                self.toggle_bounding_box.top_left += self.top_left();
            }

            if redraw {
                if self.toggle_corners != CornerRadii::default() {
                    self.toggle_bounding_box
                        .into_styled(*toggle_style)
                        .draw(&mut dtx.display())?;
                } else {
                    RoundedRectangle::new(self.toggle_bounding_box, self.toggle_corners)
                        .into_styled(*toggle_style)
                        .draw(&mut dtx.display())?;
                }
            }
        }

        if redraw && self.base.has_focus {
            if self.corners == CornerRadii::default() {
                self.bounding_box()
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            } else {
                RoundedRectangle::new(self.bounding_box(), self.corners)
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let width = if ltx.h_stretch() {
            ltx.available_width()
        } else {
            0
        };

        let height = if ltx.v_stretch() {
            ltx.available_height()
        } else {
            0
        };

        self.set_size(self.constraints.constrain_size(Size::new(width, height)));
        self.toggle_bounding_box.size = toggle_size(self.padding, self.size());
        self.toggle_bounding_box.top_left = toggle_position(
            self.padding,
            self.size(),
            self.toggle_bounding_box.size,
            self.state.get_is_checked(),
        );

        self.size()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        let mut is_on_tap = false;
        let mut is_checked_changed = false;

        self.state.update(
            event,
            etx,
            self.base,
            &mut is_on_tap,
            &mut is_checked_changed,
        );

        if is_on_tap {
            if let Some(on_tap) = &self.on_tap {
                on_tap(etx.stx_mut().state_mut());
            }
        }

        if is_checked_changed {
            etx.stx_mut().trigger_layout_update();
            if let Some(on_is_checked_changed) = &self.on_is_checked_changed {
                on_is_checked_changed(etx.stx_mut().state_mut(), self.get_is_checked());
            }
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Switch {{ checked: {}, bounds: {:?} }}",
            self.get_is_checked(),
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, _itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Switch<S, D>>() {
            new.state = self.state;
            new.base.id = self.base.id;
            new.base.has_focus = self.base.has_focus;
        }
    }

    fn is_disabled(&self) -> bool {
        !self.get_is_enabled()
    }
}

// Calculates the toggle size by the given padding and size.
fn toggle_size(padding: Thickness, size: Size) -> Size {
    let toggle_width = padding.inner_height(size.height);
    Size::new(toggle_width, toggle_width)
}

// Calculates the toggle size by the given padding and size, toggle_size and is_checked,
fn toggle_position(padding: Thickness, size: Size, toggle_size: Size, is_checked: bool) -> Point {
    if is_checked {
        return Point::new(
            size.width as i32 - padding.right as i32 - toggle_size.width as i32,
            padding.top as i32,
        );
    }

    Point::new(padding.right as i32, padding.top as i32)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_toggle_size() {
        assert_eq!(
            Size::new(4, 4),
            toggle_size(Thickness::from(2), Size::new(8, 8))
        );

        assert_eq!(
            Size::new(2, 2),
            toggle_size(Thickness::new(1, 2, 3, 4), Size::new(10, 8))
        );
    }

    #[test]
    fn test_toggle_position() {
        assert_eq!(
            Point::new(2, 2),
            toggle_position(
                Thickness::from(2),
                Size::new(10, 10),
                Size::new(4, 4),
                false
            )
        );

        assert_eq!(
            Point::new(4, 2),
            toggle_position(Thickness::from(2), Size::new(10, 10), Size::new(4, 4), true)
        );
    }
}
