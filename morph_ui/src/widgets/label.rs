use crate::{
    eg::{
        draw_target::DrawTargetExt,
        geometry::*,
        pixelcolor::*,
        primitives::Rectangle,
        text::{
            renderer::{CharacterStyle, TextRenderer},
            Text,
        },
    },
    stdlib::{marker::PhantomData, *},
    widget::get_size,
    *,
};

mod label_builder;

pub use label_builder::*;

/// `Label` is a widget to display styled text.
///
/// To construct a `Label` check the [`LabelBuilder`] trait. It is implemented for `Box<Label<S, D, C>>`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::label::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn label<S, D: DrawTarget>() -> Box<Label<S, D, MonoTextStyle<'static, Rgb565>>> {
///     Label::new()
///         .text("Hello morph")
///         .style(
///             MonoTextStyleBuilder::new()
///                 .font(&FONT_7X13)
///                 .text_color(Rgb565::BLACK)
///             .build(),
///         )
/// }        
/// ```
#[derive(Debug, Clone, PartialEq)]
pub struct Label<S, D, R> {
    style: Option<R>,
    text: String,
    base: WidgetBase,
    clip_box: Option<Rectangle>,
    text_position: Point,
    align: Align,
    constraints: Constraints,
    _state: PhantomData<S>,
    _dt: PhantomData<D>,
}

impl<S, D, R> Default for Label<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    fn default() -> Self {
        Label {
            style: None,
            text: String::default(),
            base: WidgetBase::new(),
            clip_box: None,
            text_position: Point::default(),
            align: Align::default(),
            constraints: Constraints::default(),
            _state: PhantomData::default(),
            _dt: PhantomData::default(),
        }
    }
}

impl<S, D, R> Label<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    /// Constructs a new `Label` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Label::default())
    }

    /// Gets the text as &str.
    pub fn get_text(&self) -> &str {
        self.text.as_str()
    }

    /// Sets the text of the label.
    pub fn set_text(&mut self, text: &str) {
        self.text = text.into();
    }

    /// Gets the text alignment of the label.
    pub fn get_align(&self) -> Align {
        self.align
    }

    /// Sets the text alignment of the label.
    pub fn set_align(&mut self, align: impl Into<Align>) {
        self.align = align.into();
    }

    /// Returns a reference of the text style.
    pub fn get_style(&self) -> Option<&R> {
        self.style.as_ref()
    }

    /// Sets the text style of the label.
    pub fn set_style(&mut self, text_style: impl Into<R>) {
        self.style = Some(text_style.into());
    }

    /// Gets the constraints of the label.
    pub fn get_constraints(&self) -> Constraints {
        self.constraints
    }

    /// Sets the constraints of the label.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.constraints = constraints.into();
    }

    /// Sets the clip box of the label.
    pub fn set_clip_box(&mut self, clip_box: Rectangle) {
        self.clip_box = Some(clip_box);
    }
}

impl<S, C, D, R> Widget<S, D> for Label<S, D, R>
where
    S: 'static,
    C: PixelColor,
    D: DrawTarget<Color = C> + 'static,
    R: TextRenderer<Color = C> + CharacterStyle + Clone + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        // draw text
        if let Some(text_style) = &self.style {
            let styled_text = Text::with_baseline(
                self.text.as_str(),
                self.top_left() + self.text_position,
                text_style.clone(),
                eg::text::Baseline::Top,
            );

            if let Some(clip_box) = self.clip_box {
                styled_text.draw(&mut dtx.display().clipped(&clip_box))?;
            } else {
                styled_text.draw(&mut dtx.display())?;
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        if let Some(text_style) = &self.style {
            let text_size = text_style
                .measure_string(
                    self.text.as_str(),
                    Point::default(),
                    crate::eg::text::Baseline::Top,
                )
                .bounding_box
                .size;
            self.set_size(self.constraints.constrain_size(Size::new(
                get_size(ltx.h_stretch(), ltx.available_width(), text_size.width, 0),
                get_size(ltx.v_stretch(), ltx.available_height(), text_size.height, 0),
            )));

            let x = self.align.align(0, self.width(), text_size.width);
            let y = Align::Center.align(0, self.height(), text_size.height);
            self.text_position = Point::new(x, y);
        } else {
            self.text_position = Point::default();
            self.set_size(self.constraints.constrain_size(Size::new(
                get_size(ltx.h_stretch(), ltx.available_width(), 0, 0),
                get_size(ltx.v_stretch(), ltx.available_height(), 0, 0),
            )));
        }

        self.size()
    }

    fn widget_string(&self) -> String {
        format!(
            "Label {{ text: \"{}\", bounds: {:?} }}",
            self.text,
            self.bounding_box()
        )
    }
}
