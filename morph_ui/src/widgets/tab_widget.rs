use crate::{
    downcast::Any,
    eg::{
        geometry::*,
        prelude::Primitive,
        primitives::PrimitiveStyle,
        text::renderer::{CharacterStyle, TextRenderer},
    },
    stdlib::{collections::BTreeMap, vec, *},
    widget::states::*,
    widgets::button::*,
    *,
};

mod tab;
mod tab_widget_builder;

pub use tab::*;
pub use tab_widget_builder::*;

/// `TabWidget` is used to register a tab with a header and a content. The content of the current selected
/// tab will be displayed by the `TabWidget`.
///
/// Only the content of the current selected tab will be layout and drawn. The `TabWidget` stretches the
/// current visible content in both directions.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<TabWidget<i32, D, MonoTextStyle<'static, Rgb565>>> {
///     TabWidget::new()
///         .tab("Tab 1", Label::new()
///                         .style(
///                             MonoTextStyleBuilder::new()
///                                 .font(&FONT_7X13)
///                                 .text_color(Rgb565::BLACK)
///                                 .build(),
///                         )
///                         .text("Tab 1 content"))
///         .tab("Tab 2", Label::new()
///                         .style(
///                             MonoTextStyleBuilder::new()
///                                 .font(&FONT_7X13)
///                                 .text_color(Rgb565::BLACK)
///                                 .build(),
///                         )
///                         .text("Tab 2 content"))
/// }
/// ```
pub struct TabWidget<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    tabs: Vec<Tab<S, D, R>>,
    base: WidgetBase,
    stx: StateContext<SelectionState>,
    header_height: u32,
    header_spacing: u32,
    tab_text_styles: BTreeMap<ButtonVisual, R>,
    tab_styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>,
    background_style: PrimitiveStyle<D::Color>,
}

impl<S, D, R> Default for TabWidget<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    fn default() -> Self {
        TabWidget {
            tabs: Vec::new(),
            base: WidgetBase::new(),
            stx: StateContext::new(SelectionState::from_first_selection(SelectionMode::Single)),
            header_height: 32,
            header_spacing: 1,
            tab_styles: BTreeMap::new(),
            tab_text_styles: BTreeMap::new(),
            background_style: PrimitiveStyle::new(),
        }
    }
}

impl<S, D: DrawTarget, R: TextRenderer> TabWidget<S, D, R>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + Clone + 'static,
{
    /// Constructs a new `TabWidget` wrapped in a [`TabWidget`].
    pub fn new() -> Box<Self> {
        Box::new(TabWidget::default())
    }

    /// Returns the list of tabs.
    pub fn get_tabs(&self) -> &[Tab<S, D, R>] {
        &self.tabs
    }

    /// Pushes a new tab with a header label and a content widget.
    pub fn push(&mut self, header: &str, content: Box<dyn Widget<S, D>>) {
        let index = self.tabs.len();

        if index == 0 {
            self.stx.state_mut().select(index);
        }

        self.tabs.push(Tab {
            header: Button::new()
                .text(header)
                .checkable(true)
                .on_tap(move |s: &mut SelectionState| s.select(index)),
            content,
        });
    }

    /// Returns the background style.
    pub fn get_style(&self) -> PrimitiveStyle<D::Color> {
        self.background_style
    }

    /// Sets the background style of the `TabWidget`.
    pub fn set_style(&mut self, background_style: impl Into<PrimitiveStyle<D::Color>>) {
        self.background_style = background_style.into();
    }

    /// Returns the map of tab styles.
    pub fn get_tab_styles(&self) -> &BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>> {
        &self.tab_styles
    }

    /// Inserts a new the style (background and border) that is used by the tab buttons.
    pub fn insert_tab_style(
        &mut self,
        state: ButtonVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.tab_styles.insert(state, style.into());
    }

    /// Returns the map of tab text styles.
    pub fn get_tab_text_styles(&self) -> &BTreeMap<ButtonVisual, R> {
        &self.tab_text_styles
    }

    /// Inserts a new the text style (background and border) that is used by the tab buttons.
    pub fn insert_tab_text_style(&mut self, state: ButtonVisual, text_style: impl Into<R>) {
        self.tab_text_styles.insert(state, text_style.into());
    }

    /// Returns the height of the header area.
    pub fn get_header_height(&self) -> u32 {
        self.header_height
    }

    /// Sets the height of the header area and its children.
    pub fn set_header_height(&mut self, header_height: u32) {
        self.header_height = header_height;
    }

    /// Gets the space between the header tabs.
    pub fn get_header_spacing(&self) -> u32 {
        self.header_spacing
    }

    /// Sets the space between the header tabs.
    pub fn set_header_spacing(&mut self, header_spacing: u32) {
        self.header_spacing = header_spacing;
    }

    /// Returns the number of tabs.
    pub fn len(&self) -> usize {
        self.tabs.len()
    }

    /// Returns true if the `TabWidget` contains no tabs.
    pub fn is_empty(&self) -> bool {
        self.tabs.is_empty()
    }
}

impl<S, D, R> Widget<S, D> for TabWidget<S, D, R>
where
    S: 'static,

    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + Clone + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        // transfer updates from global state context to internal state context
        let redraw = dtx.has_render_update(Some(self.id()));

        if redraw {
            self.stx.trigger_render_update(Some(self.id()));

            // draw the background
            self.bounding_box()
                .into_styled(self.background_style)
                .draw(&mut dtx.display())?;
        }

        if dtx.stx().layout_update() {
            self.stx.trigger_layout_update();
        }

        let top_left = self.top_left();
        let selected_index = self.stx.state().get_selected_index().unwrap();

        for i in 0..self.tabs.len() {
            let tab = &mut self.tabs[i];
            {
                let (display, _, ktx, dbx) = dtx.all();
                tab.header.update_and_draw(
                    top_left,
                    &mut DrawContext::new(display, &mut self.stx, ktx, redraw, dbx),
                )?;
            }

            // draw only selected tab content
            if i == selected_index {
                let (display, stx, ktx, dbx) = dtx.all();
                tab.content.update_and_draw(
                    top_left,
                    &mut DrawContext::new(display, stx, ktx, redraw, dbx),
                )?;
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        self.set_size(ltx.available_size());

        let header_available_size = Size::new(self.width(), self.header_height);
        let mut header_width_count = 0;

        let content_available_size = Size::new(
            self.width(),
            (self.height() as i32 - self.header_height as i32).max(0) as u32,
        );
        for i in 0..self.tabs.len() {
            // the tab widget will try to stretch the tab headers vertical and shrink them horizontal
            let header_size =
                self.tabs[i]
                    .header
                    .layout(LayoutContext::new(header_available_size, true, false));

            self.tabs[i]
                .header
                .set_top_left(Point::new(header_width_count, 0));

            header_width_count += header_size.width as i32 + self.header_spacing as i32;

            // layout only selected tab content
            if self.stx.state().is_selected(i) {
                // the tab widget will try to stretch the child in both direction if possible
                self.tabs[i]
                    .content
                    .layout(LayoutContext::new(content_available_size, true, true));
                self.tabs[i]
                    .content
                    .set_top_left(Point::new(0, self.header_height as i32));
            }
        }

        self.size()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        // propagate events to tab headers and tab contents
        for i in 0..self.tabs.len() {
            {
                let (_, ktx, ctx) = etx.all();
                self.tabs[i]
                    .header
                    .event(event, &mut EventContext::new(&mut self.stx, ktx, ctx));
            }

            // propagate events only to selected tab content
            if self.stx.state().is_selected(i) {
                self.tabs[i].content.event(event, etx);
            }
        }

        let old_selected_tabs = self.stx.state_mut().remove_old_selected_indexes();
        let selected_tab = self.stx.state().get_selected_index().unwrap();

        self.tabs[selected_tab].header.set_is_checked(true);

        for i in old_selected_tabs {
            self.tabs[i].header.set_is_checked(false);
        }

        // transfer updates from internal states context to global state context
        if self.stx.render_update() {
            etx.stx_mut().trigger_render_update(Some(self.id()));
        }

        if self.stx.layout_update() || !self.stx.state().is_selected(selected_tab) {
            etx.stx_mut().trigger_layout_update();
        }

        self.stx.reset_update();
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        }

        for i in 0..self.tabs.len() {
            let tab = &mut self.tabs[i];
            if self.stx.state().is_selected(i) {
                tab.header.set_is_checked(true);
            }

            // adjust styling.
            tab.header.set_styles(self.tab_styles.clone());
            tab.header.set_text_styles(self.tab_text_styles.clone());

            tab.header
                .init(None, &mut InitContext::from_stx(itx, &mut self.stx));
            tab.content.init(None, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, _itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Self>() {
            new.stx = self.stx.clone();
        }
    }

    fn widget_string(&self) -> String {
        let mut tabs = String::from("[ ");
        self.tabs.iter().map(|t| t.header.get_text()).for_each(|t| {
            tabs.push('\"');
            tabs.push_str(t);
            tabs.push_str("\", ");
        });
        if tabs != *"[ ]" {
            tabs.remove(tabs.len() - 2);
            tabs.remove(tabs.len() - 1);
        }
        tabs.push_str(" ]");

        format!(
            "TabWidget {{ tabs: {}, bounds: {:?} }}",
            tabs,
            self.bounding_box()
        )
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        if let Some(selected_tab) = self.stx.state().get_selected_index() {
            if let Some(tab) = self.tabs.get(selected_tab) {
                return vec![tab.content.as_ref()];
            }
        }

        vec![]
    }
}
