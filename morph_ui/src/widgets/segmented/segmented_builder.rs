use crate::{
    eg::{prelude::*, text::renderer::TextRenderer},
    stdlib::boxed::Box,
    widgets::segmented::*,
};

/// `SegmentedBuilder` is used to define builder methods to construct a [`Segmented`].
///
/// There is a default implementation of this trait for `Box<Segmented<S, C, D, R>>`.
pub trait SegmentedBuilder {
    type TextStyle;
    type Style;
    type State;

    /// Builder method that is used to injects a new segment with a header label and a content widget.
    #[must_use]
    fn segment(self, content: &str) -> Self;

    /// Builder method that is used to insert a new background style.
    #[must_use]
    fn style(self, style: Self::Style) -> Self;

    /// Builder method that is used to insert a new background style for the given visual.
    #[must_use]
    fn segment_style(self, visual: ButtonVisual, style: Self::Style) -> Self;

    /// Builder method that is used to insert a new text style for the given visual.
    #[must_use]
    fn segment_text_style(self, visual: ButtonVisual, style: Self::TextStyle) -> Self;

    // Builder method that is used to set the padding of the `Segmented`. It is the space around the text of the `Segmented`.
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the corner radius of the `Segmented`s background.
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the selected index of `Segmented`.
    #[must_use]
    fn selected_index(self, index: usize) -> Self;

    /// Builder method that is used to set the segment items corner radius of the `Segmented`s background.
    #[must_use]
    fn segment_corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the height.
    #[must_use]
    fn height(self, height: u32) -> Self;

    /// Builder method that is used to set the checked changed callback. This will be called only
    /// if `is_toggle` is set to `true`.
    #[must_use]
    fn on_selection_changed<F: Fn(&mut Self::State, usize) + 'static>(
        self,
        on_selection_changed: F,
    ) -> Self;
}

impl<S, D, R> SegmentedBuilder for Box<Segmented<S, D, R>>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
{
    type TextStyle = R;
    type Style = PrimitiveStyle<D::Color>;
    type State = S;

    fn segment(mut self, content: &str) -> Self {
        self.push_segment(content);
        self
    }

    fn segment_style(mut self, visual: ButtonVisual, style: Self::Style) -> Self {
        self.insert_segment_style(visual, style);
        self
    }

    fn segment_text_style(mut self, visual: ButtonVisual, style: Self::TextStyle) -> Self {
        self.insert_segment_text_style(visual, style);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }

    fn selected_index(mut self, index: usize) -> Self {
        self.set_selected_index(index);
        self
    }

    fn segment_corners(mut self, corners: CornerRadii) -> Self {
        self.set_segment_corners(corners);
        self
    }

    fn height(mut self, height: u32) -> Self {
        self.set_height(height);
        self
    }

    fn on_selection_changed<F: Fn(&mut Self::State, usize) + 'static>(
        mut self,
        on_selection_changed: F,
    ) -> Self {
        self.set_on_selection_changed(on_selection_changed);
        self
    }

    fn style(mut self, style: Self::Style) -> Self {
        self.set_style(style);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{
        mock_display::MockDisplay,
        mono_font::{ascii::*, *},
        pixelcolor::Rgb565,
        primitives::*,
    };

    type TestSegmented = Segmented<u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn builder_style() {
        assert_eq!(
            TestSegmented::new()
                .style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_style(),
            Some(
                PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_segment_style() {
        assert_eq!(
            TestSegmented::new()
                .segment_style(
                    ButtonVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_segment_styles()
                .get(&ButtonVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_segment_text_style() {
        assert_eq!(
            TestSegmented::new()
                .segment_text_style(
                    ButtonVisual::Default,
                    MonoTextStyleBuilder::new()
                        .font(&FONT_7X13)
                        .text_color(Rgb565::BLACK)
                        .build()
                )
                .get_text_styles()
                .get(&ButtonVisual::Default),
            Some(
                &MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestSegmented::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_selected_index() {
        assert_eq!(
            TestSegmented::new().selected_index(2).get_selected_index(),
            Some(2)
        );
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestSegmented::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_segment_corners() {
        assert_eq!(
            TestSegmented::new()
                .segment_corners(CornerRadii::new(Size::new(10, 0)))
                .get_segment_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_height() {
        assert_eq!(TestSegmented::new().height(5).get_height(), 5);
    }
}
