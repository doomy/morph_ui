use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{collections::VecDeque, *},
    widgets::column::Flex,
    *,
};

pub type AlignChildren<S, D> = VecDeque<(Align, Box<dyn Widget<S, D>>)>;

mod row_builder;

pub use row_builder::*;

/// `Row` is a layout widget that orders its children horizontal.
///
/// To construct a `Label` check the [`RowBuilder`] trait. It is implemented for `Box<Row<S, D>>`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn row<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Row<S, D>> {
///    Row::new()
///         .spacing(4)
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
///         .align(Label::new()
///                 .text("morph")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///                 , "center"
///         )
/// }       
/// ```
pub struct Row<S, D: DrawTarget> {
    flex: Flex<S, D>,
}

impl<S, D: DrawTarget> Default for Row<S, D> {
    fn default() -> Self {
        Row {
            flex: Flex::new(false),
        }
    }
}

impl<S, D: DrawTarget> Row<S, D> {
    /// Constructs a new `Row` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Row::default())
    }

    /// Gets the horizontal spacing between the children of the `Row`.
    pub fn get_spacing(&self) -> u32 {
        self.flex.spacing
    }

    /// Sets the horizontal spacing between the children of the `Row`.
    pub fn set_spacing(&mut self, spacing: u32) {
        self.flex.spacing = spacing;
    }

    /// Push a child to the `Row` layout and set the vertical child alignment to [`Align::Start`].
    pub fn push(&mut self, child: Box<dyn Widget<S, D>>) {
        self.flex.children.push_back((Align::Start, child));
    }

    /// Push a child to the `Row` layout and set the vertical alignment of the child.
    pub fn push_align(&mut self, child: Box<dyn Widget<S, D>>, align: impl Into<Align>) {
        self.flex.children.push_back((align.into(), child));
    }

    /// Returns the count of the children.
    pub fn len(&self) -> usize {
        self.flex.children.len()
    }

    /// Returns `true` if the row dos not contains children.
    pub fn is_empty(&self) -> bool {
        self.flex.children.is_empty()
    }

    /// Returns a mutable reference of the children.
    pub fn children_mut(&mut self) -> &mut AlignChildren<S, D> {
        &mut self.flex.children
    }
}

impl<S: 'static, D: DrawTarget + 'static> Widget<S, D> for Row<S, D> {
    fn base(&self) -> &WidgetBase {
        &self.flex.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.flex.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        self.flex.draw(dtx)
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        self.flex.layout(ltx)
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        self.flex.event(event, etx);
    }

    fn widget_string(&self) -> String {
        format!(
            "Row {{ spacing: {}, bounds: {:?} }}",
            self.flex.spacing,
            self.bounding_box()
        )
    }
    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            self.flex.init_and_update(None, itx);
        }
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = &mut new.downcast_mut::<Row<S, D>>() {
            new.flex.init(Some(&self.flex), itx);
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        self.flex.get_children()
    }

    fn get_children_mut(&mut self) -> Vec<&mut dyn Widget<S, D>> {
        self.flex.get_children_mut()
    }
}
