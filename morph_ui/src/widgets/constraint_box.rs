use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{boxed::Box, *},
    *,
};

mod constraint_box_builder;

pub use constraint_box_builder::*;

/// `ConstraintBox` is a layout widget that constraints its child.
///
/// To construct a `ConstraintBox` check the [`ConstraintBoxBuilder`] trait. It is implemented for `Box<ConstraintBox<S, D>>`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, utils::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn constraints_box<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<ConstraintBox<S, D>> {
///    ConstraintBox::new()
///         .constraints(Constraints::create().max_width(100).max_height(20))
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }       
/// ```
pub struct ConstraintBox<S, D>
where
    D: DrawTarget,
{
    child: Option<Box<dyn Widget<S, D>>>,
    base: WidgetBase,
    constraints: Constraints,
}

impl<S, D> Default for ConstraintBox<S, D>
where
    D: DrawTarget,
{
    fn default() -> Self {
        ConstraintBox {
            child: None,
            base: WidgetBase {
                stretch: 1,
                ..Default::default()
            },
            constraints: Constraints::default(),
        }
    }
}

impl<S, D> ConstraintBox<S, D>
where
    D: DrawTarget,
{
    /// Constructs a new `ConstraintBox` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(ConstraintBox::default())
    }

    /// Returns the child of the box.
    pub fn get_child(&self) -> Option<&dyn Widget<S, D>> {
        self.child.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the child of the container.
    pub fn get_child_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.child.as_mut().map(|c| c.as_mut())
    }

    /// Sets the child of the box.
    pub fn set_child(&mut self, child: Box<dyn Widget<S, D>>) {
        self.child = Some(child);
    }

    /// Gets the constraints of the box.
    pub fn get_constraints(&self) -> Constraints {
        self.constraints
    }

    /// Sets the constraints of the box.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.constraints = constraints.into();
    }
}

impl<S, D> Widget<S, D> for ConstraintBox<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let top_left = self.top_left();
        if let Some(child) = &mut self.child {
            child.update_and_draw(top_left, dtx)?;
        }

        Ok(())
    }

    fn stretch(&self) -> u32 {
        if let Some(child) = &self.child {
            return child.stretch();
        }

        self.base.stretch
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let mut size = Size::zero();
        if let Some(child) = &mut self.child {
            child.set_top_left(Point::zero());

            size = child.layout(LayoutContext::new(
                self.constraints.constrain_size(ltx.available_size()),
                ltx.v_stretch(),
                ltx.h_stretch(),
            ));
        }

        self.set_size(size);

        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Some(child) = &mut self.child {
            child.event(event, etx);
        }
    }

    fn widget_string(&self) -> String {
        format!("ConstraintBox {{  bounds: {:?} }}", self.bounding_box())
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else if let Some(child) = &mut self.child {
            child.init_and_update(None, itx);
        }
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<ConstraintBox<S, D>>() {
            if let Some(new_child) = &mut new.child {
                new_child.init_and_update(self.child.as_ref().map(|c| c.as_ref()), itx);
            }
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        if let Some(child) = &self.child {
            return vec![child.as_ref()];
        }

        vec![]
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        eg::{mock_display::MockDisplay, mono_font::MonoTextStyle, pixelcolor::Rgb565},
        widgets::Label,
    };

    type TestConstraintBox = ConstraintBox<u32, MockDisplay<Rgb565>>;
    type TestLabel = Label<u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn layout_no_child() {
        let mut test_box = TestConstraintBox::new()
            .constraints(ConstraintsBuilder::new().min_width(100).min_height(70));

        assert_eq!(
            test_box.layout(LayoutContext::new(Size::new(300, 200), false, false)),
            Size::new(0, 0)
        );
    }

    #[test]
    fn layout_child() {
        let mut test_box = TestConstraintBox::new()
            .constraints(ConstraintsBuilder::new().max_width(100).max_height(70))
            .child(TestLabel::new());

        assert_eq!(
            test_box.layout(LayoutContext::new(Size::new(300, 200), true, true)),
            Size::new(100, 70)
        );
    }
}
