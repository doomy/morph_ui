use crate::{
    downcast::Any,
    eg::{geometry::*, primitives::*, Drawable},
    stdlib::*,
    *,
};

mod container_builder;

pub use container_builder::*;

/// `Container` is a single child that can draws a bounding_box and background around its child and can surround it with a padding.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn container<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Container<S, D>> {
///    Container::new()
///         .padding(4)
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }       
/// ```
pub struct Container<S, D>
where
    D: DrawTarget,
{
    child: Option<Box<dyn Widget<S, D>>>,
    base: WidgetBase,
    padding: Thickness,
    style: Option<PrimitiveStyle<D::Color>>,
    corners: CornerRadii,
}

impl<S, D> Default for Container<S, D>
where
    D: DrawTarget,
{
    fn default() -> Self {
        Container {
            child: None,
            base: WidgetBase::new(),
            padding: Thickness::zero(),
            style: None,
            corners: CornerRadii::default(),
        }
    }
}

impl<S, D> Container<S, D>
where
    D: DrawTarget,
{
    /// Constructs a new `Container` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Container::default())
    }

    /// Returns the style of the container.
    pub fn get_style(&self) -> Option<PrimitiveStyle<D::Color>> {
        self.style
    }

    /// Sets the style of the container.
    pub fn set_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.style = Some(style.into());
    }

    /// Returns the child of the container.
    pub fn get_child(&self) -> Option<&dyn Widget<S, D>> {
        self.child.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the child of the container.
    pub fn get_child_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.child.as_mut().map(|c| c.as_mut())
    }

    /// Sets the child of the container.
    pub fn set_child(&mut self, child: Box<dyn Widget<S, D>>) {
        self.child = Some(child);
    }

    /// Removes the child from the container.
    pub fn remove_child(&mut self) {
        self.child = None;
    }

    /// Gets the padding of the container.
    pub fn get_padding(&self) -> Thickness {
        self.padding
    }

    /// Sets the padding of the container.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.padding = padding.into();
    }

    /// Returns the corner radius of the container.
    pub fn get_corners(&self) -> CornerRadii {
        self.corners
    }

    /// Sets the corner radius.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.corners = corners;
    }
}

impl<S, D> Widget<S, D> for Container<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        if dtx.has_render_update(None) {
            // draws background and bounding_box
            if let Some(style) = self.style {
                if self.corners == CornerRadii::default() {
                    self.bounding_box()
                        .into_styled(style)
                        .draw(&mut dtx.display())?;
                } else {
                    RoundedRectangle::new(self.bounding_box(), self.corners)
                        .into_styled(style)
                        .draw(&mut dtx.display())?;
                }
            }
        }

        let top_left = self.top_left();
        if let Some(child) = &mut self.child {
            child.update_and_draw(top_left, dtx)?;
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let mut size = Size::zero();

        if let Some(child) = &mut self.child {
            let available_size = self.padding.inner_size(ltx.available_size());
            let child_size = child.layout(LayoutContext::new(
                available_size,
                ltx.v_stretch(),
                ltx.h_stretch(),
            ));
            size = self.padding.outer_size(child_size);
            child.set_top_left(Point::new(
                self.padding.left as i32,
                self.padding.top as i32,
            ));
        }

        self.set_size(size);

        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Some(child) = &mut self.child {
            child.event(event, etx);
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Container {{ padding: \"{:?}\", bounds: {:?} }}",
            self.padding,
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else if let Some(child) = &mut self.child {
            child.init_and_update(None, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Container<S, D>>() {
            if let Some(new_child) = &mut new.child {
                new_child.init_and_update(self.child.as_deref(), itx);
            }

            new.base.id = self.base.id;
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        if let Some(child) = &self.child {
            return vec![child.as_ref()];
        }

        vec![]
    }

    fn get_children_mut(&mut self) -> Vec<&mut dyn Widget<S, D>> {
        if let Some(child) = self.child.as_deref_mut() {
            return vec![child];
        }

        vec![]
    }
}
