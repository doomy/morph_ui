use crate::{
    eg::{prelude::*, text::renderer::TextRenderer},
    stdlib::boxed::Box,
    widgets::Label,
    *,
};

/// `LabelBuilder` is used to define builder methods to construct a [`Label`].
///
/// There is a default implementation of this trait for `Box<Label<S, D, C>>`.
pub trait LabelBuilder {
    type TextStyle;

    /// Builder method that is used ot set the text of the label.
    #[must_use]
    fn text(self, text: &str) -> Self;

    /// Builder method that is used to set the text alignment of the label.
    #[must_use]
    fn align(self, align: impl Into<Align>) -> Self;

    /// Builder method that is used to set the text style of the label.
    #[must_use]
    fn style(self, style: Self::TextStyle) -> Self;

    /// Builder method that is used to set the constraints of the label.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;
}

impl<S, D, R> LabelBuilder for Box<Label<S, D, R>>
where
    D: DrawTarget,
    R: TextRenderer,
{
    type TextStyle = R;

    fn text(mut self, text: &str) -> Self {
        self.set_text(text);
        self
    }

    fn align(mut self, align: impl Into<Align>) -> Self {
        self.set_align(align);
        self
    }

    fn style(mut self, style: Self::TextStyle) -> Self {
        self.set_style(style);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        eg::{
            mock_display::MockDisplay,
            mono_font::{ascii::*, *},
            pixelcolor::Rgb565,
        },
        utils::*,
    };

    type TestLabel = Label<u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn builder_text() {
        assert_eq!(
            TestLabel::new().text("Hello morph").get_text(),
            "Hello morph"
        );
    }

    #[test]
    fn builder_align() {
        assert_eq!(TestLabel::new().align("start").get_align(), Align::Start);
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestLabel::new()
                .style(
                    MonoTextStyleBuilder::new()
                        .font(&FONT_7X13)
                        .text_color(Rgb565::BLACK)
                        .build()
                )
                .get_style(),
            Some(
                &MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestLabel::new()
                .constraints(ConstraintsBuilder::new().width(10).height(20))
                .get_constraints(),
            ConstraintsBuilder::new().width(10).height(20).build()
        );
    }
}
