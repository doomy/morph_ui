use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{vec::*, *},
    *,
};

mod stretch_builder;

pub use stretch_builder::*;

/// `Stretch` is a single child layout widget that stretches its children in the main axis of its parent.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, utils::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Stretch<S, D>> {
///    Stretch::new()  
///         .stretch(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 ),
///                 5
///         )
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }       
/// ```
pub struct Stretch<S, D>
where
    D: DrawTarget,
{
    child: Option<Box<dyn Widget<S, D>>>,
    base: WidgetBase,
}

impl<S, D> Default for Stretch<S, D>
where
    D: DrawTarget,
{
    fn default() -> Self {
        Stretch {
            child: None,
            base: WidgetBase {
                stretch: 1,
                ..Default::default()
            },
        }
    }
}

impl<S, D> Stretch<S, D>
where
    D: DrawTarget,
{
    /// Constructs a new `Stretch` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Stretch::default())
    }

    /// Gets the child of `Stretch`.
    pub fn get_child(&self) -> Option<&dyn Widget<S, D>> {
        self.child.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the child of the container.
    pub fn get_child_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.child.as_mut().map(|c| c.as_mut())
    }

    /// Sets the child of stretch and set the stretch factor of the child to 1.
    pub fn set_child(&mut self, child: Box<dyn Widget<S, D>>) {
        self.set_stretch_child(child, 1);
    }

    /// Sets the child of stretch and sets the stretch factor of the child.
    pub fn set_stretch_child(&mut self, child: Box<dyn Widget<S, D>>, stretch: u32) {
        self.child = Some(child);
        self.base.stretch = stretch;
    }

    /// Gets the current stretch factor.
    pub fn get_stretch(&self) -> u32 {
        self.base.stretch
    }
}

impl<S, D> Widget<S, D> for Stretch<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let top_left = self.top_left();
        if let Some(child) = &mut self.child {
            child.update_and_draw(top_left, dtx)?;
        }

        Ok(())
    }

    fn stretch(&self) -> u32 {
        self.base.stretch
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let size = if let Some(child) = &mut self.child {
            child.set_top_left(Point::zero());

            child.layout(ltx)
        } else {
            let width = if ltx.h_stretch() {
                ltx.available_width()
            } else {
                1
            };

            let height = if ltx.v_stretch() {
                ltx.available_height()
            } else {
                1
            };

            Size::new(width, height)
        };

        self.set_size(size);

        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Some(child) = &mut self.child {
            child.event(event, etx);
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Stretch {{ stretch: {}, bounds: {:?} }}",
            self.stretch(),
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else if let Some(child) = &mut self.child {
            child.init_and_update(None, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Stretch<S, D>>() {
            if let Some(new_child) = &mut new.child {
                new_child.init_and_update(self.child.as_deref(), itx);
            }

            new.base.id = self.base.id;
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        if let Some(child) = &self.child {
            return vec![child.as_ref()];
        }

        vec![]
    }
}
