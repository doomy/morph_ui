use crate::{
    downcast::Any,
    eg::{
        geometry::*,
        primitives::{CornerRadii, PrimitiveStyle},
        text::renderer::{CharacterStyle, TextRenderer},
    },
    stdlib::{collections::BTreeMap, ops::Deref, *},
    widget::states::*,
    widgets::button::*,
    *,
};

mod segmented_builder;

pub use segmented_builder::*;

/// `Segmented` provides the possibility to select between different segments. Multiple segments can be added to `Segmented`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::segmented::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Segmented<i32, D, MonoTextStyle<'static, Rgb565>>> {
///     Segmented::new()
///         .segment("A")
///         .segment("B")
/// }
/// ```
pub struct Segmented<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer + CharacterStyle + Clone + 'static,
{
    base: WidgetBase,
    stx: StateContext<SelectionState>,
    segment_text_styles: BTreeMap<ButtonVisual, R>,
    segment_styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>,
    content_container: Box<Container<SelectionState, D>>,
    on_selection_changed: Option<Box<dyn Fn(&mut S, usize) + 'static>>,
    segment_corners: CornerRadii,
    height: u32,
}

impl<S, D, R> Default for Segmented<S, D, R>
where
    D: DrawTarget + 'static,
    R: TextRenderer + CharacterStyle + Clone + 'static,
{
    fn default() -> Self {
        Segmented {
            base: WidgetBase::new(),
            stx: StateContext::new(SelectionState::from_first_selection(SelectionMode::Single)),
            segment_styles: BTreeMap::new(),
            segment_text_styles: BTreeMap::new(),
            content_container: Container::new().child(Row::new()),
            on_selection_changed: None,
            segment_corners: CornerRadii::default(),
            height: 32,
        }
    }
}

impl<S, D, R> Segmented<S, D, R>
where
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + Clone + 'static,
{
    /// Constructs a new `Segmented` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Segmented::default())
    }

    /// Injects a new segment with a header label and a content widget.
    pub fn push_segment(&mut self, content: &str) {
        let selected_index = self.stx.state().get_selected_index().unwrap();

        let row = self.row_mut();
        let index = row.len();
        row.push(
            Button::<SelectionState, D, R>::new()
                .checked(index == selected_index)
                .checkable(false)
                .text(content)
                .on_tap(move |s: &mut SelectionState| s.select(index)),
        );
    }

    /// Returns the style of the background.
    pub fn get_style(&self) -> Option<PrimitiveStyle<D::Color>> {
        self.content_container.get_style()
    }

    /// Sets the style of the background.
    pub fn set_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.content_container.set_style(style);
    }

    /// Gets the selected index of `Segmented`.
    pub fn get_selected_index(&self) -> Option<usize> {
        self.stx.state().get_selected_index()
    }

    /// Builder method that is used to set the selected index.
    pub fn set_selected_index(&mut self, index: usize) {
        self.stx.state_mut().select(index);
    }

    /// Gets the corner radius of the `Segmented`s background.
    pub fn get_corners(&self) -> CornerRadii {
        self.content_container.get_corners()
    }

    /// Sets the corner radius.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.content_container.set_corners(corners);
    }

    /// Gets the corner radius of the segmented items.
    pub fn get_segment_corners(&self) -> CornerRadii {
        self.segment_corners
    }

    /// Sets the segment corner radius.
    pub fn set_segment_corners(&mut self, segment_corners: CornerRadii) {
        self.segment_corners = segment_corners;
    }

    /// Returns the style of the segmented items.
    pub fn get_segment_styles(&self) -> &BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>> {
        &self.segment_styles
    }

    /// Inserts style (background and border) of the segment buttons.
    pub fn insert_segment_style(
        &mut self,
        state: ButtonVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.segment_styles.insert(state, style.into());
    }

    /// Gets the text styles of the segmented items.
    pub fn get_text_styles(&self) -> &BTreeMap<ButtonVisual, R> {
        &self.segment_text_styles
    }

    /// Inserts text style of the segment buttons.
    pub fn insert_segment_text_style(&mut self, state: ButtonVisual, text_style: impl Into<R>) {
        self.segment_text_styles.insert(state, text_style.into());
    }

    /// Gets the padding of the `Segmented`. It is the space around the text of the `List`.
    pub fn get_padding(&self) -> Thickness {
        self.content_container.get_padding()
    }

    /// Sets the padding of the segmented widget.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.content_container.set_padding(padding);
    }

    /// Gets the height of the segmented widget.
    pub fn get_height(&self) -> u32 {
        self.height
    }

    /// Sets the hight of the segmented widget.
    pub fn set_height(&mut self, height: u32) {
        self.height = height;
    }

    /// Sets the checked changed callback. This will be called only
    /// if `is_toggle` is set to `true`.
    pub fn set_on_selection_changed<F: Fn(&mut S, usize) + 'static>(
        &mut self,
        on_selection_changed: F,
    ) {
        self.on_selection_changed = Some(Box::new(on_selection_changed));
    }

    // -- helper

    // Used to update the button selection.
    fn update_selection(&mut self) {
        let selected_index = self.stx.state().get_selected_index().unwrap();
        let removed_selection = self.stx.state_mut().remove_old_selected_indexes();

        self.update_child_selection(selected_index, true);
        for i in removed_selection {
            self.update_child_selection(i, false);
        }
    }

    fn update_child_selection(&mut self, index: usize, selected: bool) {
        if let Some(button) = self.row_mut().children_mut().get_mut(index) {
            if let Ok(button) = button.1.downcast_mut::<Button<SelectionState, D, R>>() {
                button.set_is_checked(selected);
            }
        }
    }

    fn row_mut(&mut self) -> &mut Row<SelectionState, D> {
        get_widget_child_mut::<SelectionState, D, Row<SelectionState, D>>(
            self.content_container.as_mut(),
        )
        .unwrap()
    }
}

impl<S, D, R> Widget<S, D> for Segmented<S, D, R>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + Clone + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let redraw = dtx.has_render_update(Some(self.id()));

        // transfer updates from global state context to internal state context
        if dtx.stx().render_update() {
            self.stx.trigger_render_update(Some(self.id()));
        }

        if dtx.stx().layout_update() {
            self.stx.trigger_layout_update();
        }

        let (display, _, ktx, dtx) = dtx.all();
        self.content_container.set_top_left(self.top_left());
        self.content_container.draw(&mut DrawContext::new(
            display,
            &mut self.stx,
            ktx,
            redraw,
            dtx,
        ))?;

        self.stx.reset_update();

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let size = self.content_container.layout(LayoutContext::new(
            Size::new(ltx.available_width(), self.height),
            ltx.v_stretch(),
            ltx.h_stretch(),
        ));

        self.set_size(size);

        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        let selected_index = self.stx.state().get_selected_index();

        let (_, ktx, ctx) = etx.all();

        self.content_container
            .event(event, &mut EventContext::new(&mut self.stx, ktx, ctx));

        if let Some(selected_index) = selected_index {
            // raises selection changed callback.
            if !self.stx.state().is_selected(selected_index) {
                if let Some(on_selection_changed) = &self.on_selection_changed {
                    on_selection_changed(
                        etx.stx_mut().state_mut(),
                        self.stx.state().get_selected_index().unwrap(),
                    );
                }

                self.update_selection();
            }

            // transfer updates from internal states context to global state context
            if self.stx.render_update() {
                etx.stx_mut().trigger_render_update(Some(self.id()));
            }

            if self.stx.layout_update() || !self.stx.state().is_selected(selected_index) {
                etx.stx_mut().trigger_layout_update();
            }
        }
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            self.content_container
                .init(None, &mut InitContext::from_stx(itx, &mut self.stx));
        }

        self.init_id(itx);

        // Add styles to buttons
        if let Some(child) = self.content_container.get_child_mut() {
            if let Ok(row) = child.downcast_mut::<Row<SelectionState, D>>() {
                for (_, child) in row.children_mut() {
                    if let Ok(button) = child.downcast_mut::<Button<SelectionState, D, R>>() {
                        button.set_text_styles(self.segment_text_styles.clone());
                        button.set_styles(self.segment_styles.clone());
                        button.set_corners(self.segment_corners);
                    }
                }
            }
        }

        self.update_selection();
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Segmented<S, D, R>>() {
            new.stx = self.stx.clone();
            new.content_container.init(
                Some(self.content_container.deref()),
                &mut InitContext::from_stx(itx, &mut new.stx),
            );
        }
    }

    fn widget_string(&self) -> String {
        format!("Segmented {{ bounds: {:?} }}", self.bounding_box())
    }
}
