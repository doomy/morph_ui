use crate::{
    eg::draw_target::DrawTarget, stdlib::boxed::Box, utils::*, widget::Widget,
    widgets::ConstraintBox,
};

/// `ConstraintBoxBuilder` is used to define builder methods to construct a [`ConstraintBox`].
///
/// There is a default implementation of this trait for `Box<ConstraintBox<S, D>>`.
pub trait ConstraintBoxBuilder {
    type State;
    type DrawTarget;

    /// Builder method that is used to set the child of the `ConstraintBox`. The child is constraints to the given constraintss of the box.
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the constraints of the box.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;
}

impl<S, D> ConstraintBoxBuilder for Box<ConstraintBox<S, D>>
where
    D: DrawTarget,
{
    type State = S;
    type DrawTarget = D;

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_child(child);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        eg::{mock_display::MockDisplay, pixelcolor::Rgb565},
        stdlib::*,
        widgets::Column,
    };

    type TestConstraintBox = ConstraintBox<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_child() {
        assert!(TestConstraintBox::new()
            .child(Column::new())
            .get_child()
            .is_some());
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestConstraintBox::new()
                .constraints(ConstraintsBuilder::new().width(10).height(20))
                .get_constraints(),
            ConstraintsBuilder::new().width(10).height(20).build()
        );
    }
}
