use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{vec::*, *},
    *,
};

mod event_adapter_builder;

pub use event_adapter_builder::*;

type EventHandler<S> = dyn Fn(&mut EventContext<S>, &dyn Any);

/// `EventAdapter` is used to add an separate event handler to your ui.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, widget::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<dyn Widget<S, D>> {
///    EventAdapter::new()
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
///         .attach(|_etx, _event|{})
/// }       
pub struct EventAdapter<S, D>
where
    D: DrawTarget,
{
    child: Option<Box<dyn Widget<S, D>>>,
    base: WidgetBase,
    handler: Option<Box<EventHandler<S>>>,
}

impl<S, D> Default for EventAdapter<S, D>
where
    D: DrawTarget,
{
    fn default() -> Self {
        EventAdapter {
            child: None,
            base: WidgetBase::new(),
            handler: None,
        }
    }
}

impl<S, D> EventAdapter<S, D>
where
    D: DrawTarget,
{
    /// Constructs a new `EventAdapter` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(EventAdapter::default())
    }

    /// Gets the child of `EventAdapter`.
    pub fn get_child(&self) -> Option<&dyn Widget<S, D>> {
        self.child.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the child of the container.
    pub fn get_child_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.child.as_mut().map(|c| c.as_mut())
    }

    /// Sets the child of `EventAdapter`.
    pub fn set_child(&mut self, child: Box<dyn Widget<S, D>>) {
        self.child = Some(child);
    }

    /// Gets a reference of the handler.
    pub fn get_handler(&self) -> Option<&EventHandler<S>> {
        self.handler.as_deref()
    }

    /// Builder method that is used to attach an event handler to the adapter.
    pub fn set_handler<F: Fn(&mut EventContext<S>, &dyn Any) + 'static>(&mut self, handler: F) {
        self.handler = Some(Box::new(handler));
    }
}

impl<S, D> Widget<S, D> for EventAdapter<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let top_left = self.top_left();
        if let Some(child) = &mut self.child {
            child.update_and_draw(top_left, dtx)?;
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        if let Some(child) = &mut self.child {
            return child.layout(ltx);
        }

        Size::default()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Some(child) = &mut self.child {
            child.event(event, etx);
        }

        if let Some(handler) = &self.handler {
            handler(etx, event);
        }
    }

    fn widget_string(&self) -> String {
        format!("EventAdapter {{ bounds: {:?} }}", self.bounding_box())
    }
    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else if let Some(child) = &mut self.child {
            child.init_and_update(None, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<EventAdapter<S, D>>() {
            if let Some(new_child) = &mut new.child {
                new_child.init_and_update(self.child.as_deref(), itx);
            }

            new.base.id = self.base.id;
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        if let Some(child) = &self.child {
            return vec![child.as_ref()];
        }

        vec![]
    }
}
