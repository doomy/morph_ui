use crate::{
    eg::{prelude::*, text::renderer::TextRenderer},
    stdlib::boxed::Box,
    widgets::tab_widget::*,
};

/// `TabWidgetBuilder` is used to define builder methods to construct a [`TabWidget`].
///
/// There is a default implementation of this trait for `Box<TabWidget<S, C, D, R>>`.
pub trait TabWidgetBuilder {
    type TextStyle;
    type Style;
    type State;
    type DrawTarget: DrawTarget;

    /// Builder method that is used to insert a new tab with a header label and a content widget.
    #[must_use]
    fn tab(self, header: &str, content: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the background style of the `TabWidget`.
    #[must_use]
    fn style(self, background_style: Self::Style) -> Self;

    /// Builder method that is used to insert a new the style (background and border) that is used by the tab buttons.
    #[must_use]
    fn tab_style(self, state: ButtonVisual, style: Self::Style) -> Self;

    /// Builder method that is used to insert a new the text style (background and border) that is used by the tab buttons.
    #[must_use]
    fn tab_text_style(self, state: ButtonVisual, style: Self::TextStyle) -> Self;

    /// Builder method that is used to set the header height.
    #[must_use]
    fn header_height(self, height: u32) -> Self;

    /// Builder method that is used ot set the spacing between header tabs.
    #[must_use]
    fn header_spacing(self, spacing: u32) -> Self;
}

impl<S, D, R> TabWidgetBuilder for Box<TabWidget<S, D, R>>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
{
    type TextStyle = R;
    type Style = PrimitiveStyle<D::Color>;
    type State = S;
    type DrawTarget = D;

    fn tab(
        mut self,
        header: &str,
        content: Box<dyn Widget<Self::State, Self::DrawTarget>>,
    ) -> Self {
        self.push(header, content);
        self
    }

    fn style(mut self, background_style: Self::Style) -> Self {
        self.set_style(background_style);
        self
    }

    fn tab_style(mut self, state: ButtonVisual, style: Self::Style) -> Self {
        self.insert_tab_style(state, style);
        self
    }

    fn tab_text_style(mut self, state: ButtonVisual, style: Self::TextStyle) -> Self {
        self.insert_tab_text_style(state, style);
        self
    }

    fn header_height(mut self, height: u32) -> Self {
        self.set_header_height(height);
        self
    }

    fn header_spacing(mut self, spacing: u32) -> Self {
        self.set_header_spacing(spacing);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{
        mock_display::MockDisplay,
        mono_font::{ascii::*, *},
        pixelcolor::Rgb565,
        primitives::PrimitiveStyleBuilder,
    };

    type TestTabWidget = TabWidget<u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn builder_tab() {
        let test_tab_widget = TestTabWidget::new().tab("Hello morph", TestTabWidget::new());

        assert_eq!(test_tab_widget.len(), 1);
        assert!(!test_tab_widget.is_empty());
        assert_eq!(
            test_tab_widget.get_tabs()[0].header.get_text(),
            "Hello morph"
        );
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestTabWidget::new()
                .style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_style(),
            PrimitiveStyleBuilder::new()
                .fill_color(Rgb565::BLACK)
                .build()
        );
    }

    #[test]
    fn builder_tab_style() {
        assert_eq!(
            TestTabWidget::new()
                .tab_style(
                    ButtonVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_tab_styles()
                .get(&ButtonVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_tab_text_style() {
        assert_eq!(
            TestTabWidget::new()
                .tab_text_style(
                    ButtonVisual::Default,
                    MonoTextStyleBuilder::new()
                        .font(&FONT_7X13)
                        .text_color(Rgb565::BLACK)
                        .build()
                )
                .get_tab_text_styles()
                .get(&ButtonVisual::Default),
            Some(
                &MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_header_height() {
        assert_eq!(TestTabWidget::new().header_height(5).get_header_height(), 5);
    }

    #[test]
    fn builder_header_spacing() {
        assert_eq!(
            TestTabWidget::new().header_spacing(5).get_header_spacing(),
            5
        );
    }
}
