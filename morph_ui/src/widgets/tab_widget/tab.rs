#[cfg(not(feature = "std"))]
use crate::stdlib::*;
use crate::{eg::text::renderer::TextRenderer, widget::states::*, widgets::button::*, *};

// Wrapper struct for a tab with header and content
pub struct Tab<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    pub(crate) header: Box<Button<SelectionState, D, R>>,
    pub(crate) content: Box<dyn Widget<S, D>>,
}
