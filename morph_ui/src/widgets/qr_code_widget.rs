use crate::{
    eg::{
        extensions::*,
        geometry::{Point, Size},
    },
    error::Error,
    stdlib::{marker::PhantomData, *},
    *,
};

mod qr_code_widget_builder;

pub use qr_code_widget_builder::*;

/// `QrCodeWidget` is used to display a qr code.
pub struct QrCodeWidget<S, D>
where
    D: DrawTarget,
{
    _state: PhantomData<S>,
    _dt: PhantomData<D>,
    base: WidgetBase,
    qr_code: QrCode<D::Color>,
}

impl<S, D> Default for QrCodeWidget<S, D>
where
    D: DrawTarget,
{
    fn default() -> Self {
        QrCodeWidget {
            _state: PhantomData::default(),
            _dt: PhantomData::default(),
            base: WidgetBase::default(),
            qr_code: QrCode::new("", Point::zero()).unwrap(),
        }
    }
}

impl<S, D> QrCodeWidget<S, D>
where
    D: DrawTarget,
{
    /// Constructs a new `QrCodeWidget` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(QrCodeWidget::default())
    }

    /// Gets the qr code text.
    pub fn get_text(&self) -> &str {
        self.qr_code.get_text()
    }

    /// Gets the qr code text.
    ///
    /// Returns an `Error` if there is an error on creation of the qr code.
    pub fn set_text(&mut self, text: &str) -> Result<(), Error> {
        let style = self.qr_code.get_style();
        self.qr_code = QrCode::with_style(text, Point::zero(), style)?;

        Ok(())
    }

    /// Returns the style of the container.
    pub fn get_style(&self) -> QrCodeStyle<D::Color> {
        self.qr_code.get_style()
    }

    /// Sets the style of the qr code.
    pub fn set_style(&mut self, style: QrCodeStyle<D::Color>) {
        self.qr_code.set_style(style)
    }

    /// Gets the scale of the qr code.
    pub fn get_scale(&self) -> u32 {
        self.qr_code.scale
    }

    /// Sets the scale of the qr code (default is 4).
    pub fn set_scale(&mut self, scale: u32) {
        self.qr_code.scale = scale;
    }
}

impl<D, S> Widget<S, D> for QrCodeWidget<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        self.qr_code.top_left = self.bounding_box().top_left;
        self.qr_code.draw(dtx.draw_target_mut())
    }

    fn layout(&mut self, _: LayoutContext) -> Size {
        self.bounding_box_mut().size = self.qr_code.size();

        self.size()
    }

    fn widget_string(&self) -> String {
        format!(
            "QrCodeWidget {{ text: {}, stretch: {}, bounds: {:?} }}",
            self.get_text(),
            self.stretch(),
            self.bounding_box()
        )
    }
}
