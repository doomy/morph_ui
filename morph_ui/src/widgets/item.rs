use crate::{
    downcast::Any,
    eg::{geometry::*, primitives::*},
    stdlib::{collections::BTreeMap, vec::*, *},
    utils::*,
    *,
};

mod item_builder;

pub use item_builder::*;

/// `Item` represents a [`Button`] like widget that can contains a child widget.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Item<i32, D>> {
///     Item::new()
///         .on_tap(|count: &mut i32| *count += 1)
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }
/// ```
pub struct Item<S, D>
where
    D: DrawTarget,
{
    child: Option<Box<dyn Widget<S, D>>>,
    styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>,
    focus_style: PrimitiveStyle<D::Color>,
    state: ButtonState,
    padding: Thickness,
    constraints: Constraints,
    base: WidgetBase,
    corners: CornerRadii,
    on_tap: Option<Box<dyn Fn(&mut S) + 'static>>,
    on_is_checked_changed: Option<Box<dyn Fn(&mut S, bool) + 'static>>,
}

impl<S, D> Default for Item<S, D>
where
    D: DrawTarget,
{
    fn default() -> Self {
        Item {
            child: None,
            styles: BTreeMap::new(),
            focus_style: PrimitiveStyle::new(),
            state: ButtonState::new(),
            padding: Thickness::from(8),
            constraints: Constraints::default(),
            base: WidgetBase::new(),
            corners: CornerRadii::default(),
            on_tap: None,
            on_is_checked_changed: None,
        }
    }
}

impl<S, D> Item<S, D>
where
    D: DrawTarget,
{
    /// Constructs a new `Item` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Item::default())
    }

    /// Gets the child of `Item`.
    pub fn get_child(&self) -> Option<&dyn Widget<S, D>> {
        self.child.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the child of the container.
    pub fn get_child_mut(&mut self) -> Option<&mut dyn Widget<S, D>> {
        self.child.as_mut().map(|c| c.as_mut())
    }

    /// Sets the child of item
    pub fn set_child(&mut self, child: Box<dyn Widget<S, D>>) {
        self.child = Some(child);
    }

    /// Returns the background style map of the item.
    pub fn get_styles(&self) -> &BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>> {
        &self.styles
    }

    /// Sets the background styles map of the item.
    pub fn set_styles(&mut self, styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>) {
        self.styles = styles;
    }

    /// Inserts a new background style for the given visual.
    pub fn insert_style(
        &mut self,
        visual: ButtonVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.styles.insert(visual, style.into());
    }

    /// Gets the focus style of the item.
    pub fn get_focus_style(&self) -> PrimitiveStyle<D::Color> {
        self.focus_style
    }

    /// Sets the focus style of the item.
    pub fn set_focus_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.focus_style = style.into();
    }

    /// Gets the padding of the item. It is the space around the text of the item.
    pub fn get_padding(&self) -> Thickness {
        self.padding
    }

    /// Sets the padding of the item. It is the space around the text of the item.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.padding = padding.into();
    }

    /// Gets the box constraints of the item.
    pub fn get_constraints(&self) -> Constraints {
        self.constraints
    }

    /// Sets the box constraints of the item.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.constraints = constraints.into();
    }

    /// Gets the current visual state of the item.
    pub fn get_visual(&self) -> ButtonVisual {
        self.state.get_visual()
    }

    /// Sets the current visual state of the item.
    pub fn set_visual(&mut self, visual: impl Into<ButtonVisual>) {
        self.state.set_visual(visual);
    }

    /// Gets if the item is checkable or not.
    pub fn get_is_checkable(&self) -> bool {
        self.state.get_is_checkable()
    }

    /// Sets if the item is checkable or not.
    pub fn set_is_checkable(&mut self, is_checkable: bool) {
        self.state.set_is_checkable(is_checkable);
    }

    /// Gets the value that indicates if the item is currently checked.
    pub fn get_is_checked(&self) -> bool {
        self.state.get_is_checked()
    }

    ///  Sets the value that indicates if the item is currently checked.
    pub fn set_is_checked(&mut self, is_checked: bool) {
        self.state.set_is_checked(is_checked);
    }

    /// Gets the corner radius of the buttons background.
    pub fn get_corners(&self) -> CornerRadii {
        self.corners
    }

    /// Sets the corner radius of the buttons background.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.corners = corners;
    }

    /// Gets the value that indicates if the item is enabled.
    pub fn get_is_enabled(&self) -> bool {
        self.state.get_is_enabled()
    }

    /// Sets the value that indicates if the item is enabled.
    pub fn set_is_enabled(&mut self, is_enabled: bool) {
        self.state.set_is_enabled(is_enabled);
    }

    /// Sets the callback that is called after the item is tapped.
    pub fn set_on_tap<F: Fn(&mut S) + 'static>(&mut self, on_tap: F) {
        self.on_tap = Some(Box::new(on_tap));
    }

    /// Sets the callback that is called after `is_checked` of item is changed.
    pub fn set_on_is_checked_changed<F: Fn(&mut S, bool) + 'static>(
        &mut self,
        on_checked_changed: F,
    ) {
        self.on_is_checked_changed = Some(Box::new(on_checked_changed));
    }

    fn style_visual(&self) -> ButtonVisual {
        if self.styles.contains_key(&self.state.get_visual()) {
            self.state.get_visual()
        } else {
            ButtonVisual::Default
        }
    }
}

impl<S, D> Widget<S, D> for Item<S, D>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let redraw = dtx.has_render_update(Some(self.id()));

        if redraw {
            // draw background
            if let Some(style) = self.styles.get(&self.style_visual()) {
                if self.corners == CornerRadii::default() {
                    self.bounding_box()
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                } else {
                    RoundedRectangle::new(self.bounding_box(), self.corners)
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                }
            }
        }

        {
            let dtx = dtx.all();

            let position = self.bounding_box().top_left;
            if let Some(child) = &mut self.child {
                child.update_and_draw(
                    position,
                    &mut DrawContext::from_draw_context(dtx, None, redraw),
                )?;
            }
        }

        if redraw && self.base.has_focus {
            if self.corners == CornerRadii::default() {
                self.bounding_box()
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            } else {
                RoundedRectangle::new(self.bounding_box(), self.corners)
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        if let Some(child) = &mut self.child {
            let child_size = child.layout(LayoutContext::new(
                self.constraints
                    .constrain_size(self.padding.inner_size(ltx.available_size())),
                false,
                false,
            ));

            child.set_top_left(Point::new(
                self.padding.left as i32,
                self.padding.top as i32,
            ));

            self.set_size(self.constraints.constrain_size(Size::new(
                get_size(
                    ltx.h_stretch(),
                    ltx.available_width(),
                    child_size.width,
                    self.padding.left + self.padding.right,
                ),
                get_size(
                    ltx.v_stretch(),
                    ltx.available_height(),
                    child_size.height,
                    self.padding.top + self.padding.bottom,
                ),
            )));
        }

        self.size()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        let mut is_on_tap = false;
        let mut is_checked_changed = false;

        self.state.update(
            event,
            etx,
            self.base,
            &mut is_on_tap,
            &mut is_checked_changed,
        );

        if is_on_tap {
            if let Some(on_tap) = &self.on_tap {
                on_tap(etx.stx_mut().state_mut());
            }
        }

        if is_checked_changed {
            if let Some(on_is_checked_changed) = &self.on_is_checked_changed {
                on_is_checked_changed(etx.stx_mut().state_mut(), self.get_is_checked());
            }
        }
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, _itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Item<S, D>>() {
            new.state = self.state;
            new.base.id = self.base.id;
            new.base.has_focus = self.base.has_focus;
        }
    }

    fn widget_string(&self) -> String {
        format!("Item {{ bounds: {:?} }}", self.bounding_box())
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        if let Some(child) = &self.child {
            return vec![child.as_ref()];
        }

        vec![]
    }

    fn is_disabled(&self) -> bool {
        self.state.get_visual() == ButtonVisual::Disabled
    }
}
