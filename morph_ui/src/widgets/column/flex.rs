use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{collections::VecDeque, ops::*, *},
    utils::real::Real,
    *,
};

pub type AlignChildren<S, D> = VecDeque<(Align, Box<dyn Widget<S, D>>)>;

/// `Flex` is the internal base for [`Column`] and [`Row`].
pub(crate) struct Flex<S, D: DrawTarget> {
    pub spacing: u32,
    pub children: AlignChildren<S, D>,
    pub base: WidgetBase,
    pub is_vertical: bool,
}

impl<S, D: DrawTarget> Flex<S, D> {
    /// Constructs a new `Label` wrapped in a [`Box`].
    pub fn new(is_vertical: bool) -> Self {
        Flex {
            spacing: 0,
            children: VecDeque::new(),
            base: WidgetBase::new(),
            is_vertical,
        }
    }
}

impl<S: 'static, D: DrawTarget + 'static> Widget<S, D> for Flex<S, D> {
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let mut position = 0;

        let top_left = self.top_left();
        let width = self.width();
        let height = self.height();

        for (align, child) in &mut self.children {
            if dtx.stx().layout_update() {
                // align children
                if self.is_vertical {
                    child.set_x(align.align(0, width, child.width()));
                    child.set_y(position);
                } else {
                    child.set_x(position);
                    child.set_y(align.align(0, height, child.height()));
                }
            }

            child.update_and_draw(top_left, dtx)?;

            if dtx.stx().layout_update() {
                if self.is_vertical {
                    position += child.height() as i32 + self.spacing as i32;
                } else {
                    position += child.width() as i32 + self.spacing as i32;
                }
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let mut counter_axis_av_size = if self.is_vertical {
            ltx.available_height() as i32
        } else {
            ltx.available_width() as i32
        };

        let mut main_axis_count = 0;

        let mut stretch = Vec::new();
        let mut stretch_sum = 0;

        for i in 0..self.children.len() {
            if let Some((align, child)) = self.children.get_mut(i) {
                // if child is stretch its size will calculated later.
                let stretch_factor = child.stretch();
                if stretch_factor > 0 {
                    stretch.push((i, stretch_factor));
                    stretch_sum += stretch_factor;
                    continue;
                }

                if self.is_vertical {
                    // calculate size of no stretch children
                    let child_size = child.layout(LayoutContext::new(
                        Size::new(ltx.available_width(), counter_axis_av_size as u32),
                        false,
                        *align == Align::Stretch,
                    ));

                    main_axis_count = main_axis_count.max(child_size.width);

                    counter_axis_av_size -= child_size.height as i32 + self.spacing as i32;
                } else {
                    // calculate size of no stretch children
                    let child_size = child.layout(LayoutContext::new(
                        Size::new(counter_axis_av_size as u32, ltx.available_height()),
                        *align == Align::Stretch,
                        false,
                    ));

                    main_axis_count = main_axis_count.max(child_size.height);

                    counter_axis_av_size -= child_size.width as i32 + self.spacing as i32;
                }
            }
        }

        // remove spaces from available height
        let counter_axis_stretch_size = (counter_axis_av_size as i32
            - self.spacing as i32 * (stretch.len() as i32 - 1).max(0) as i32)
            .max(0);

        // calculate size of stretch children
        for i in 0..stretch.len() {
            if let Some((j, stretch_value)) = stretch.get(i) {
                if let Some((align, child)) = self.children.get_mut(*j) {
                    let counter_axis_stretch = Real::from(counter_axis_stretch_size)
                        * (Real::from(*stretch_value) / Real::from(stretch_sum));

                    let counter_axis_stretch_size = if i < stretch.len() - 1 {
                        counter_axis_stretch.floor().into()
                    } else {
                        counter_axis_stretch.ceil().into()
                    };

                    if self.is_vertical {
                        let child_size = child.layout(LayoutContext::new(
                            Size::new(ltx.available_width(), counter_axis_stretch_size),
                            true,
                            *align == Align::Stretch,
                        ));

                        main_axis_count = main_axis_count.max(child_size.width);

                        counter_axis_av_size -= child_size.height as i32 + self.spacing as i32;
                    } else {
                        let child_size = child.layout(LayoutContext::new(
                            Size::new(counter_axis_stretch_size, ltx.available_height()),
                            *align == Align::Stretch,
                            true,
                        ));

                        main_axis_count = main_axis_count.max(child_size.height);

                        counter_axis_av_size -= child_size.width as i32 + self.spacing as i32;
                    }
                }
            }
        }

        if self.is_vertical {
            self.set_size(Size::new(
                main_axis_count,
                (ltx.available_height() as i32 - counter_axis_av_size).max(0) as u32,
            ));
        } else {
            self.set_size(Size::new(
                (ltx.available_width() as i32 - counter_axis_av_size).max(0) as u32,
                main_axis_count,
            ));
        }

        self.size()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        for (_, child) in &mut self.children {
            child.event(event, etx);
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Column {{ spacing: {}, bounds: {:?} }}",
            self.spacing,
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            for i in 0..self.children.len() {
                self.children[i].1.init_and_update(None, itx);
            }
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Flex<S, D>>() {
            for i in 0..new.children.len() {
                new.children[i]
                    .1
                    .init_and_update(self.children.get(i).map(|c| c.1.as_ref()), itx);
            }

            new.base.id = self.base.id;
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        self.children.iter().map(|c| c.1.as_ref()).collect()
    }

    fn get_children_mut(&mut self) -> Vec<&mut dyn Widget<S, D>> {
        self.children.iter_mut().map(|c| c.1.deref_mut()).collect()
    }
}
