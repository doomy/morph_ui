use crate::{eg::prelude::*, stdlib::boxed::Box, widgets::Spacer};

/// `SpacerBuilder` is used to define builder methods to construct a [`Spacer`].
///
/// There is a default implementation of this trait for `Box<Spacer<S, D>>`.
pub trait SpacerBuilder {
    /// Builder method that is used to set the stretch factor of the spacer. The stretch axis is depending on the parent layout.
    #[must_use]
    fn stretch(self, stretch: u32) -> Self;
}

impl<S, D> SpacerBuilder for Box<Spacer<S, D>>
where
    D: DrawTarget,
{
    fn stretch(mut self, stretch: u32) -> Self {
        self.set_stretch(stretch);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestSpacer = Spacer<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_stretch() {
        assert_eq!(TestSpacer::new().stretch(5).get_stretch(), 5);
    }
}
