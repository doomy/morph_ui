use crate::{eg::prelude::*, stdlib::boxed::Box, widgets::Scope, *};

/// `ScopeBuilder` is used to define builder methods to construct a [`Scope`].
///
/// There is a default implementation of this trait for `Box<Scope<S, D, I>>`.
pub trait ScopeBuilder {
    type State;
    type DrawTarget;

    /// Builder method that is used to set the child of scope.
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;
}

impl<S, D, I> ScopeBuilder for Box<Scope<S, D, I>>
where
    D: DrawTarget,
{
    type State = I;
    type DrawTarget = D;

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_child(child);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestScope = Scope<u32, MockDisplay<Rgb565>, u32>;

    #[test]
    fn builder_child() {
        let stretch = TestScope::new(0).child(TestScope::new(1));
        assert!(stretch.get_child().is_some());
    }
}
