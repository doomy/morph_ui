use crate::{
    eg::{extensions::*, prelude::*},
    error::Error,
    stdlib::boxed::Box,
    widgets::QrCodeWidget,
};

/// `QrCodeWidgetBuilder` is used to define builder methods to construct a [`QrCodeWidget`].
///
/// There is a default implementation of this trait for `Box<QrCodeWidget<S, D>>`.
pub trait QrCodeWidgetBuilder: Sized {
    type Style;

    /// Builder method that is used to set the text of the qr code.
    ///
    /// Returns an `Error` if there is an error on creation of the qr code.

    fn text(self, text: &str) -> Result<Self, Error>;

    /// Builder method that is used to set the style of the qr code.
    #[must_use]
    fn style(self, style: Self::Style) -> Self;

    /// Builder method that is used to set the scale of the qr code.
    #[must_use]
    fn scale(self, scale: u32) -> Self;
}

impl<S, D> QrCodeWidgetBuilder for Box<QrCodeWidget<S, D>>
where
    D: DrawTarget,
{
    type Style = QrCodeStyle<D::Color>;

    fn text(mut self, text: &str) -> Result<Self, Error> {
        self.set_text(text)?;
        Ok(self)
    }

    fn style(mut self, style: Self::Style) -> Self {
        self.set_style(style);
        self
    }

    fn scale(mut self, scale: u32) -> Self {
        self.set_scale(scale);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestQrCodeWidget = QrCodeWidget<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_text() {
        assert_eq!(
            TestQrCodeWidget::new().text("test").unwrap().get_text(),
            "test"
        );
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestQrCodeWidget::new().text("test").unwrap().get_text(),
            "test"
        );
    }
}
