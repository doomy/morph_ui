use crate::stdlib::fmt::Debug;

/// Defines the state of the `Button`. The state depends from the event input
/// e.g. mouse hover, mouse down, mouse up. Used for styling the button.
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Eq, Ord)]
pub enum ButtonVisual {
    /// Default visual state of the button.
    Default,

    /// Button is visual hovered by a pointer.
    Hover,

    /// Button is visual pressed.
    Pressed,

    /// Button is visual disabled.
    Disabled,

    /// Button is visual checked.
    Checked,
}

impl Default for ButtonVisual {
    fn default() -> Self {
        ButtonVisual::Default
    }
}

impl From<&str> for ButtonVisual {
    fn from(s: &str) -> Self {
        match s {
            "hover" | "Hover" => ButtonVisual::Hover,
            "pressed" | "Pressed" => ButtonVisual::Pressed,
            "disabled" | "Disabled" => ButtonVisual::Disabled,
            "checked" | "Checked" => ButtonVisual::Checked,
            _ => ButtonVisual::Default,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from() {
        assert_eq!(ButtonVisual::Default, ButtonVisual::from("default"));
        assert_eq!(ButtonVisual::Default, ButtonVisual::from("Default"));
        assert_eq!(ButtonVisual::Default, ButtonVisual::from(""));
        assert_eq!(ButtonVisual::Hover, ButtonVisual::from("hover"));
        assert_eq!(ButtonVisual::Hover, ButtonVisual::from("Hover"));
        assert_eq!(ButtonVisual::Disabled, ButtonVisual::from("disabled"));
        assert_eq!(ButtonVisual::Disabled, ButtonVisual::from("Disabled"));
        assert_eq!(ButtonVisual::Checked, ButtonVisual::from("checked"));
        assert_eq!(ButtonVisual::Checked, ButtonVisual::from("Checked"));
    }
}
