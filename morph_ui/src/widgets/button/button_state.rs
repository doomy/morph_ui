use crate::{stdlib::fmt::Debug, widgets::button::*};

/// `ButtonState` is used to handle the interactions of a button and button like widgets.
///
/// The state contains all properties of the button that can be manipulated by interaction and its callbacks.
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Default)]
pub struct ButtonState {
    visual: ButtonVisual,
    is_checkable: bool,
    is_checked: bool,
    is_enabled: bool,
}

impl ButtonState {
    pub fn new() -> Self {
        Self {
            visual: ButtonVisual::Default,
            is_checkable: false,
            is_checked: false,
            is_enabled: true,
        }
    }

    /// Gets if the button is checkable or not.
    pub fn get_is_checkable(&self) -> bool {
        self.is_checkable
    }

    /// Sets if the button is checkable or not.
    pub fn set_is_checkable(&mut self, is_checkable: bool) {
        self.is_checkable = is_checkable;
    }

    /// Gets the value that indicates if the button is currently checked.
    pub fn get_is_checked(&self) -> bool {
        self.is_checked
    }

    /// Sets the value that indicates if the button is currently checked.
    pub fn set_is_checked(&mut self, is_checked: bool) {
        self.is_checked = is_checked;
    }

    /// Gets the value that indicates if the button is enabled.
    pub fn get_is_enabled(&self) -> bool {
        self.is_enabled
    }

    /// Sets the value that indicates if the button is enabled.
    pub fn set_is_enabled(&mut self, is_enabled: bool) {
        self.is_enabled = is_enabled;
    }

    /// Gets the current visual state of the button.
    pub fn get_visual(&self) -> ButtonVisual {
        if !self.is_enabled {
            return ButtonVisual::Disabled;
        }

        if self.is_checked {
            return ButtonVisual::Checked;
        }

        self.visual
    }

    /// Sets the current visual state of the button.
    pub fn set_visual(&mut self, visual: impl Into<ButtonVisual>) {
        self.visual = visual.into();
    }

    /// Updates the current state of the button by an event.
    pub fn update<S>(
        &mut self,
        event: &dyn Any,
        etx: &mut EventContext<S>,
        button_base: WidgetBase,
        is_on_tap: &mut bool,
        is_checked_changed: &mut bool,
    ) {
        if !event.is::<MouseEvent>()
            && !event.is::<MoveEvent>()
            && !event.is::<KeyEvent>()
            && !event.is::<ControllerButtonEvent>()
            || !self.is_enabled
        {
            return;
        }

        let old_visual = self.visual;

        if let Ok(mouse_event) = event.downcast_ref::<MouseEvent>() {
            let contains_mouse = button_base.bounding_box.contains(mouse_event.position());

            match self.visual {
                ButtonVisual::Default | ButtonVisual::Hover => {
                    if contains_mouse
                        && mouse_event.button() == MouseButton::Left
                        && mouse_event.pressed()
                    {
                        self.visual = ButtonVisual::Pressed;
                    }
                }

                ButtonVisual::Pressed => {
                    if mouse_event.button() == MouseButton::Left && !mouse_event.pressed() {
                        if !contains_mouse {
                            self.visual = ButtonVisual::Default;
                        } else {
                            // workaround that will be replaced by real touch events
                            if cfg!(target_os = "ios") || cfg!(target_os = "android") {
                                self.visual = ButtonVisual::Default;
                            } else {
                                self.visual = ButtonVisual::Hover;
                            }

                            *is_on_tap = true;

                            if self.is_checkable {
                                self.set_is_checked(!self.get_is_checked());
                                *is_checked_changed = true;
                            }
                        }
                    }
                }
                _ => {}
            }
        }

        if let Ok(move_event) = event.downcast_ref::<MoveEvent>() {
            let contains_mouse = button_base.bounding_box.contains(move_event.position());

            match self.visual {
                ButtonVisual::Default => {
                    if contains_mouse {
                        self.visual = ButtonVisual::Hover;
                    }
                }
                ButtonVisual::Hover => {
                    if !contains_mouse {
                        self.visual = ButtonVisual::Default;
                    }
                }
                _ => {}
            }
        }

        // button action of focus
        if button_base.has_focus {
            if let Ok(key_event) = event.downcast_ref::<KeyEvent>() {
                if key_event.key() == Key::Enter {
                    if key_event.pressed() && self.visual == ButtonVisual::Default {
                        self.visual = ButtonVisual::Pressed;
                    }
                    if !key_event.pressed() && self.visual == ButtonVisual::Pressed {
                        self.visual = ButtonVisual::Default;
                        *is_on_tap = true;

                        if self.is_checkable {
                            self.set_is_checked(!self.get_is_checked());
                            *is_checked_changed = true;
                        }
                    }
                }
            }

            if let Ok(controller_button_event) = event.downcast_ref::<ControllerButtonEvent>() {
                if controller_button_event.button() == ControllerButton::B {
                    if controller_button_event.pressed() && self.visual == ButtonVisual::Default {
                        self.visual = ButtonVisual::Pressed;
                    }

                    if !controller_button_event.pressed() && self.visual == ButtonVisual::Pressed {
                        self.visual = ButtonVisual::Default;
                        *is_on_tap = true;

                        if self.is_checkable {
                            self.set_is_checked(!self.get_is_checked());
                            *is_checked_changed = true;
                        }
                    }
                }
            }
        }

        if old_visual != self.visual || *is_checked_changed {
            etx.stx_mut()
                .trigger_render_update(Some(button_base.get_id()));
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::context::*;

    fn update_state(
        event: &dyn Any,
        state: &mut ButtonState,
        base: WidgetBase,
        is_on_tap: &mut bool,
        is_checked_changed: &mut bool,
    ) {
        let mut stx = StateContext::new(0);
        let mut ktx = KeyboardContext::new();
        let mut cbx = ClipboardContext::new();

        let mut etx = EventContext::new(&mut stx, &mut ktx, &mut cbx);

        state.update(event, &mut etx, base, is_on_tap, is_checked_changed);
    }

    #[test]
    fn mouse_over() {
        let mut button = WidgetBase::new();
        button.bounding_box = Rectangle::new(Point::new(0, 0), Size::new(20, 20));

        let mut state = ButtonState::new();

        assert_eq!(state.get_visual(), ButtonVisual::Default);

        update_state(
            &MoveEvent::new(Point::new(1, 1)),
            &mut state,
            button,
            &mut false,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Hover);

        update_state(
            &MoveEvent::new(Point::new(30, 30)),
            &mut state,
            button,
            &mut false,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Default);
    }

    #[test]
    fn mouse_button() {
        let mut button = WidgetBase::new();
        button.bounding_box = Rectangle::new(Point::new(0, 0), Size::new(20, 20));

        let mut state = ButtonState::new();

        let mut on_is_tap = false;

        update_state(
            &MouseEvent::new(Point::new(30, 30), MouseButton::Left, true),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Default);
        assert!(!on_is_tap);

        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, true),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Pressed);
        assert!(!on_is_tap);

        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, false),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Hover);
        assert!(on_is_tap);

        // outside release
        on_is_tap = false;
        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, true),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        update_state(
            &MouseEvent::new(Point::new(100, 100), MouseButton::Left, false),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Default);
        assert!(!on_is_tap);
    }

    #[test]
    fn is_checked() {
        let mut button = WidgetBase::new();
        button.bounding_box = Rectangle::new(Point::new(0, 0), Size::new(20, 20));

        let mut state = ButtonState::new();

        let mut on_is_checked_changed = false;

        update_state(
            &MouseEvent::new(Point::new(30, 30), MouseButton::Left, true),
            &mut state,
            button,
            &mut false,
            &mut on_is_checked_changed,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Default);
        assert!(!on_is_checked_changed);
        assert!(!state.get_is_checked());

        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, true),
            &mut state,
            button,
            &mut false,
            &mut on_is_checked_changed,
        );

        assert!(!on_is_checked_changed);
        assert!(!state.get_is_checked());

        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, false),
            &mut state,
            button,
            &mut false,
            &mut on_is_checked_changed,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Hover);
        assert!(!on_is_checked_changed);
        assert!(!state.get_is_checked());

        // checkable
        state.set_is_checkable(true);

        update_state(
            &MouseEvent::new(Point::new(30, 30), MouseButton::Left, true),
            &mut state,
            button,
            &mut false,
            &mut on_is_checked_changed,
        );

        assert!(!on_is_checked_changed);

        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, true),
            &mut state,
            button,
            &mut false,
            &mut on_is_checked_changed,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Pressed);
        assert!(!on_is_checked_changed);

        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, false),
            &mut state,
            button,
            &mut false,
            &mut on_is_checked_changed,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Checked);
        assert!(on_is_checked_changed);
        assert!(state.get_is_checked());
    }

    #[test]
    fn focus() {
        let mut button = WidgetBase::new();
        button.bounding_box = Rectangle::new(Point::new(0, 0), Size::new(20, 20));

        let mut state = ButtonState::new();

        let mut on_is_tap = false;

        update_state(
            &KeyEvent::new(KEY_ENTER, true),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Default);
        assert!(!on_is_tap);

        update_state(
            &KeyEvent::new(KEY_ENTER, false),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Default);
        assert!(!on_is_tap);

        // with focus
        button.has_focus = true;
        update_state(
            &KeyEvent::new(KEY_ENTER, true),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Pressed);
        assert!(!on_is_tap);

        update_state(
            &KeyEvent::new(KEY_ENTER, false),
            &mut state,
            button,
            &mut on_is_tap,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Default);
        assert!(on_is_tap);
    }

    #[test]
    fn disabled() {
        let mut button = WidgetBase::new();
        button.bounding_box = Rectangle::new(Point::new(0, 0), Size::new(20, 20));

        let mut state = ButtonState::new();
        state.set_is_enabled(false);

        update_state(
            &MoveEvent::new(Point::new(30, 30)),
            &mut state,
            button,
            &mut false,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Disabled);

        update_state(
            &MouseEvent::new(Point::new(10, 10), MouseButton::Left, true),
            &mut state,
            button,
            &mut false,
            &mut false,
        );

        assert_eq!(state.get_visual(), ButtonVisual::Disabled);
    }
}
