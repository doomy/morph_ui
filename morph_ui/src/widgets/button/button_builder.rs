use crate::{
    eg::{prelude::*, text::renderer::TextRenderer},
    stdlib::boxed::Box,
    widgets::button::*,
};

/// `ButtonBuilder` is used to define builder methods to construct a [`Button`].
///
/// There is a default implementation of this trait for `Box<Button<S, D, R>>`.
pub trait ButtonBuilder {
    type TextStyle;
    type Style;
    type State;

    /// Builder method that is used ot set the text of the button.
    #[must_use]
    fn text(self, text: &str) -> Self;

    /// Builder method that is used to insert a new background style for the given visual.
    #[must_use]
    fn style(self, visual: ButtonVisual, style: Self::Style) -> Self;

    /// Builder method that is used to insert a new text style for the given visual.
    #[must_use]
    fn text_style(self, visual: ButtonVisual, style: Self::TextStyle) -> Self;

    /// Builder method that is used to set the focus style.
    #[must_use]
    fn focus_style(self, style: Self::Style) -> Self;

    // Builder method that is used to set the padding of the button. It is the space around the text of the button.
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the constraints of the button.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;

    /// Builder method that is used to set start visual of the button.
    #[must_use]
    fn visual(self, visual: impl Into<ButtonVisual>) -> Self;

    /// Builder method that is used to set if the button is checkable or not.
    #[must_use]
    fn checkable(self, is_checkable: bool) -> Self;

    /// Builder method that is used to set the value that indicates if the button is currently checked.
    #[must_use]
    fn checked(self, is_checked: bool) -> Self;

    /// Builder method that is used to set the corner radius of the buttons background.
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the value that indicates if the button is enabled.
    #[must_use]
    fn enabled(self, is_enabled: bool) -> Self;

    /// Builder method that is used to set the callback that is called after the button is tapped.
    #[must_use]
    fn on_tap<F: Fn(&mut Self::State) + 'static>(self, on_tap: F) -> Self;

    /// Builder method that is used to set the callback that is called after `is_checked` of button is changed.
    #[must_use]
    fn on_is_checked_changed<F: Fn(&mut Self::State, bool) + 'static>(
        self,
        on_is_checked_changed: F,
    ) -> Self;

    /// Builder method that is used to set is_focusable of the button.
    #[must_use]
    fn focusable(self, is_focusable: bool) -> Self;
}

impl<S, D, R> ButtonBuilder for Box<Button<S, D, R>>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
{
    type TextStyle = R;
    type Style = PrimitiveStyle<D::Color>;
    type State = S;

    fn text(mut self, text: &str) -> Self {
        self.set_text(text);
        self
    }

    fn style(mut self, visual: ButtonVisual, style: Self::Style) -> Self {
        self.insert_style(visual, style);
        self
    }

    fn text_style(mut self, visual: ButtonVisual, style: Self::TextStyle) -> Self {
        self.insert_text_style(visual, style);
        self
    }

    fn focus_style(mut self, style: Self::Style) -> Self {
        self.set_focus_style(style);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }

    fn visual(mut self, visual: impl Into<ButtonVisual>) -> Self {
        self.set_visual(visual);
        self
    }

    fn checkable(mut self, is_checkable: bool) -> Self {
        self.set_is_checkable(is_checkable);
        self
    }

    fn checked(mut self, is_checked: bool) -> Self {
        self.set_is_checked(is_checked);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }

    fn enabled(mut self, is_enabled: bool) -> Self {
        self.set_is_enabled(is_enabled);
        self
    }

    fn on_tap<F: Fn(&mut Self::State) + 'static>(mut self, on_tap: F) -> Self {
        self.set_on_tap(on_tap);
        self
    }

    fn on_is_checked_changed<F: Fn(&mut Self::State, bool) + 'static>(
        mut self,
        on_is_checked_changed: F,
    ) -> Self {
        self.set_on_is_checked_changed(on_is_checked_changed);
        self
    }

    fn focusable(mut self, is_focusable: bool) -> Self {
        self.set_is_focusable(is_focusable);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{
        mock_display::MockDisplay,
        mono_font::{ascii::*, *},
        pixelcolor::Rgb565,
    };

    type TestButton = Button<u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn builder_text() {
        assert_eq!(
            TestButton::new().text("Hello morph").get_text(),
            "Hello morph"
        );
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestButton::new()
                .style(
                    ButtonVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_styles()
                .get(&ButtonVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_text_style() {
        assert_eq!(
            TestButton::new()
                .text_style(
                    ButtonVisual::Default,
                    MonoTextStyleBuilder::new()
                        .font(&FONT_7X13)
                        .text_color(Rgb565::BLACK)
                        .build()
                )
                .get_text_styles()
                .get(&ButtonVisual::Default),
            Some(
                &MonoTextStyleBuilder::new()
                    .font(&FONT_7X13)
                    .text_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_focus_style() {
        assert_eq!(
            TestButton::new()
                .focus_style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_focus_style(),
            PrimitiveStyleBuilder::new()
                .fill_color(Rgb565::BLACK)
                .build()
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestButton::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestButton::new()
                .constraints(Constraints::create().width(100).height(70))
                .get_constraints(),
            Constraints::create().width(100).height(70).build()
        );
    }

    #[test]
    fn builder_is_checkable() {
        assert!(!TestButton::new().get_is_checkable());
        assert!(TestButton::new().checkable(true).get_is_checkable());
    }

    #[test]
    fn builder_is_checked() {
        assert!(!TestButton::new().get_is_checked());
        assert!(TestButton::new().checked(true).get_is_checked());
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestButton::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_is_enabled() {
        assert!(TestButton::new().get_is_enabled());
        assert!(!TestButton::new().enabled(false).get_is_enabled());
    }
}
