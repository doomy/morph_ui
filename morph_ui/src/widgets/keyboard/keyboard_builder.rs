use crate::{
    eg::{prelude::*, primitives::*, text::renderer::*},
    stdlib::boxed::Box,
    *,
};

/// `KeyboardBuilder` is used to define builder methods to construct a [`Keyboard`].
///
/// There is a default implementation of this trait for `Box<Keyboard<S, D, R>>`.
pub trait KeyboardBuilder {
    type TextStyle;
    type Style;
    type State;
    type DrawTarget: DrawTarget;

    /// Builder method that is used to set the style of the [`Keyboard`].
    #[must_use]
    fn style(self, style: Self::Style) -> Self;

    /// Builder method that is used to set the padding of the [`Keyboard`].
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the spacing between the button of the [`Keyboard`].
    #[must_use]
    fn spacing(self, spacing: u32) -> Self;

    /// Builder method that is used to set the `Keyboard` layout.
    #[must_use]
    fn layout(self, layout: KeyboardLayout) -> Self;

    /// Builder method that is used to set builder for the key buttons.
    ///
    /// First parameter is the key model of the button, second defines if caps lock is active.
    #[must_use]
    fn key_builder<
        F: Fn(KeyModel, bool) -> Box<Button<KeyboardState, Self::DrawTarget, Self::TextStyle>>
            + 'static,
    >(
        self,
        key_builder: F,
    ) -> Self;

    /// Builder method that is used to set builder for the operation key buttons.
    ///
    /// First parameter is the text of the button, second defines if the button is primary.
    #[must_use]
    fn op_key_builder<
        F: Fn(&str, bool) -> Box<Button<KeyboardState, Self::DrawTarget, Self::TextStyle>> + 'static,
    >(
        self,
        op_key_builder: F,
    ) -> Self;

    /// Builder method that is used to set the builder for the text_field buttons.
    #[must_use]
    fn text_field_builder<
        F: Fn(&str) -> Box<TextField<KeyboardState, Self::DrawTarget, Self::TextStyle>> + 'static,
    >(
        self,
        text_field_builder: F,
    ) -> Self;
}

impl<S, D, R> KeyboardBuilder for Box<Keyboard<S, D, R>>
where
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
{
    type TextStyle = R;
    type Style = PrimitiveStyle<D::Color>;
    type State = S;
    type DrawTarget = D;

    fn style(mut self, style: Self::Style) -> Self {
        self.set_style(style);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn spacing(mut self, spacing: u32) -> Self {
        self.set_spacing(spacing);
        self
    }

    fn layout(mut self, layout: KeyboardLayout) -> Self {
        self.set_layout(layout);
        self
    }

    fn key_builder<
        F: Fn(KeyModel, bool) -> Box<Button<KeyboardState, Self::DrawTarget, Self::TextStyle>>
            + 'static,
    >(
        mut self,
        key_builder: F,
    ) -> Self {
        self.set_key_builder(key_builder);
        self
    }

    fn op_key_builder<
        F: Fn(&str, bool) -> Box<Button<KeyboardState, Self::DrawTarget, Self::TextStyle>> + 'static,
    >(
        mut self,
        op_key_builder: F,
    ) -> Self {
        self.set_op_key_builder(op_key_builder);
        self
    }

    fn text_field_builder<
        F: Fn(&str) -> Box<TextField<KeyboardState, Self::DrawTarget, Self::TextStyle>> + 'static,
    >(
        mut self,
        text_field_builder: F,
    ) -> Self {
        self.set_text_field_builder(text_field_builder);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, mono_font::*, pixelcolor::Rgb565};

    type TestKeyboard = Keyboard<u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn builder_style() {
        assert_eq!(
            TestKeyboard::new()
                .style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_style(),
            Some(
                PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestKeyboard::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_spacing() {
        assert_eq!(TestKeyboard::new().spacing(10).get_spacing(), 10);
    }
}
