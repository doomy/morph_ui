use crate::{
    events::*,
    stdlib::{boxed::Box, string::String},
    widgets::keyboard::*,
};

/// This `Keyboard` state is used to handle actions on the `Keyboard`. It's available on the keyboard buttons.
#[derive(Default)]
pub struct KeyboardState {
    is_caps_lock: bool,
    events: EventQueue,
    keyboard_type: String,
    close: bool,
}

impl KeyboardState {
    /// Creates a new keyboard state.
    pub fn new() -> Self {
        KeyboardState {
            keyboard_type: String::from(ALPHA_KEYBOARD),
            ..Default::default()
        }
    }

    /// Toggles the keyboard type from alpha to numeric and from numeric to alpha.
    pub fn toggle_keyboard_type(&mut self) {
        if self.keyboard_type.as_str() == ALPHA_KEYBOARD {
            self.keyboard_type = String::from(NUMERIC_KEYBOARD);
        } else {
            self.keyboard_type = String::from(ALPHA_KEYBOARD);
        }
    }

    /// Toggles caps lock.
    pub fn toggle_caps_lock(&mut self) {
        self.is_caps_lock = !self.is_caps_lock;
    }

    /// Triggers a action from the given key_model.
    pub fn key_model_action(&mut self, key_model: KeyModel) {
        self.events
            .enqueue(TextInputEvent::new(key_model.value(self.is_caps_lock)));
    }

    /// Execute a back space event on the keyboard state.
    pub fn back_space(&mut self) {
        self.events.enqueue(KeyEvent::new(KEY_BACKSPACE, true));
    }

    /// Executes a space event on the keyboard state.
    pub fn space(&mut self) {
        self.events.enqueue(TextInputEvent::new(' '));
    }

    /// Executes arrow left event.
    pub fn arrow_left(&mut self) {
        self.events.enqueue(KeyEvent::new(KEY_ARROW_LEFT, true));
    }

    /// Executes arrow right event.
    pub fn arrow_right(&mut self) {
        self.events.enqueue(KeyEvent::new(KEY_ARROW_RIGHT, true));
    }

    /// Pops an event from the event stack.
    pub fn dequeue_event(&mut self) -> Option<Box<dyn Any>> {
        self.events.dequeue()
    }

    /// Returns `true` if caps lock is active, otherwise `None`.
    pub fn is_caps_lock(&self) -> bool {
        self.is_caps_lock
    }

    /// Returns the current keyboard type.
    pub fn keyboard_type(&self) -> String {
        self.keyboard_type.clone()
    }

    /// Trigger to close the keyboard.
    pub fn close_keyboard(&mut self) {
        self.close = true;
    }

    /// Returns `true` if the keyboard should closed. Also resets the close flag of the state back to `false`.
    pub fn close(&mut self) -> bool {
        let close = self.close;
        self.close = false;
        close
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn keyboard_state() {
        let mut state = KeyboardState::new();

        assert!(state.events.is_empty());
        assert_eq!(state.events.len(), 0);
        assert_eq!(state.keyboard_type, ALPHA_KEYBOARD);

        state.toggle_keyboard_type();
        assert_eq!(state.keyboard_type, NUMERIC_KEYBOARD);

        assert!(!state.is_caps_lock);
        state.toggle_caps_lock();
        assert!(state.is_caps_lock());

        state.key_model_action(KeyModel::new('a', 'A'));
        assert_eq!(
            state
                .events
                .dequeue()
                .unwrap()
                .downcast_ref::<TextInputEvent>()
                .unwrap(),
            &TextInputEvent::new('A')
        );

        state.toggle_caps_lock();

        state.key_model_action(KeyModel::new('a', 'A'));
        assert_eq!(
            state
                .events
                .dequeue()
                .unwrap()
                .downcast_ref::<TextInputEvent>()
                .unwrap(),
            &TextInputEvent::new('a')
        );

        state.back_space();
        assert_eq!(
            state
                .events
                .dequeue()
                .unwrap()
                .downcast_ref::<KeyEvent>()
                .unwrap(),
            &KeyEvent::new(KEY_BACKSPACE, true)
        );

        state.space();
        assert_eq!(
            state
                .events
                .dequeue()
                .unwrap()
                .downcast_ref::<TextInputEvent>()
                .unwrap(),
            &TextInputEvent::new(' ')
        );

        state.arrow_left();
        assert_eq!(
            state
                .events
                .dequeue()
                .unwrap()
                .downcast_ref::<KeyEvent>()
                .unwrap(),
            &KeyEvent::new(KEY_ARROW_LEFT, true)
        );

        state.arrow_right();
        assert_eq!(
            state
                .events
                .dequeue()
                .unwrap()
                .downcast_ref::<KeyEvent>()
                .unwrap(),
            &KeyEvent::new(KEY_ARROW_RIGHT, true)
        );

        assert!(!state.close);
        state.close_keyboard();
        assert!(state.close);
        assert!(state.close());
        assert!(!state.close);
    }
}
