/// Defines an keyboard key entry on the on screen keyboard.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct KeyModel {
    value: char,
    shift_value: char,
}

impl KeyModel {
    /// Creates a new key model.
    pub fn new(value: char, shift_value: char) -> Self {
        KeyModel { value, shift_value }
    }

    /// Returns the value of the model depending on if shift is active.
    pub fn value(&self, shift: bool) -> char {
        if !shift {
            self.value
        } else {
            self.shift_value
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_key_model_value() {
        let key_model = KeyModel::new('a', 'A');
        assert_eq!(key_model.value(false), 'a');
        assert_eq!(key_model.value(true), 'A');
    }
}
