use crate::{
    stdlib::{collections::BTreeMap, string::String, vec::Vec},
    widgets::keyboard::KeyModel,
};

/// Identifier of the alphabetic keyboard.
pub const ALPHA_KEYBOARD: &str = "abc";

/// Identifier of the numeric keyboard.
pub const NUMERIC_KEYBOARD: &str = "123";

/// Used to define a on screen keyboard layout for a specific language.
#[derive(Debug, Default, Clone, PartialEq)]
pub struct KeyboardLayout {
    pub space_text: String,
    pub caps_lock_text: String,
    pub enter_text: String,
    pub back_space_text: String,
    pub arrow_left_text: String,
    pub arrow_right_text: String,
    pub toggle_type_text: String,
    pub rows: BTreeMap<String, Vec<Vec<KeyModel>>>,
}

pub fn insert_row(rows: &mut Vec<Vec<KeyModel>>, key_models: &[KeyModel]) {
    let mut row = Vec::new();

    for model in key_models {
        row.push(*model);
    }

    rows.push(row);
}
