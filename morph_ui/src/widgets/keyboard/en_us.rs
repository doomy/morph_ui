use crate::{
    stdlib::{collections::BTreeMap, vec::Vec},
    widgets::keyboard::*,
};

/// Returns an en_US keyboard layout.
pub fn en_us() -> KeyboardLayout {
    let mut layout = BTreeMap::new();

    // characters
    let mut character_rows = Vec::new();

    insert_row(
        &mut character_rows,
        &[
            KeyModel::new('q', 'Q'),
            KeyModel::new('w', 'W'),
            KeyModel::new('e', 'E'),
            KeyModel::new('r', 'R'),
            KeyModel::new('t', 'T'),
            KeyModel::new('y', 'Y'),
            KeyModel::new('u', 'U'),
            KeyModel::new('i', 'I'),
            KeyModel::new('o', 'O'),
            KeyModel::new('p', 'P'),
        ],
    );

    insert_row(
        &mut character_rows,
        &[
            KeyModel::new('a', 'A'),
            KeyModel::new('s', 'S'),
            KeyModel::new('d', 'D'),
            KeyModel::new('f', 'F'),
            KeyModel::new('g', 'G'),
            KeyModel::new('h', 'H'),
            KeyModel::new('j', 'J'),
            KeyModel::new('k', 'K'),
            KeyModel::new('l', 'L'),
        ],
    );

    insert_row(
        &mut character_rows,
        &[
            KeyModel::new('z', 'Z'),
            KeyModel::new('x', 'X'),
            KeyModel::new('c', 'C'),
            KeyModel::new('v', 'V'),
            KeyModel::new('b', 'B'),
            KeyModel::new('n', 'N'),
            KeyModel::new('m', 'M'),
        ],
    );

    layout.insert(String::from(ALPHA_KEYBOARD), character_rows);

    // numbers
    let mut number_rows = Vec::new();

    insert_row(
        &mut number_rows,
        &[
            KeyModel::new('1', '['),
            KeyModel::new('2', ']'),
            KeyModel::new('3', '{'),
            KeyModel::new('4', '}'),
            KeyModel::new('5', '#'),
            KeyModel::new('6', '%'),
            KeyModel::new('7', '^'),
            KeyModel::new('8', '*'),
            KeyModel::new('9', '+'),
            KeyModel::new('0', '='),
        ],
    );

    insert_row(
        &mut number_rows,
        &[
            KeyModel::new('-', '_'),
            KeyModel::new('/', '\\'),
            KeyModel::new(':', '|'),
            KeyModel::new(';', '~'),
            KeyModel::new('(', '<'),
            KeyModel::new(')', '>'),
            KeyModel::new('$', '€'),
            KeyModel::new('&', '£'),
            KeyModel::new('@', '¥'),
            KeyModel::new('"', '.'),
        ],
    );

    // todo
    insert_row(
        &mut number_rows,
        &[
            KeyModel::new('.', '.'),
            KeyModel::new(',', ','),
            KeyModel::new('?', '?'),
            KeyModel::new('!', '°'),
            KeyModel::new('\'', '\''),
        ],
    );

    layout.insert(String::from(NUMERIC_KEYBOARD), number_rows);

    KeyboardLayout {
        space_text: String::from("space"),
        caps_lock_text: String::from("Sh"),
        enter_text: String::from("En"),
        back_space_text: String::from("Bks"),
        arrow_left_text: String::from("Lef"),
        arrow_right_text: String::from("Rig"),
        toggle_type_text: String::from("KT"),
        rows: layout,
    }
}
