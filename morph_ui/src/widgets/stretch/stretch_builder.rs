use crate::{eg::prelude::*, stdlib::boxed::Box, widgets::Stretch, *};

/// `StretchBuilder` is used to define builder methods to construct a [`Stretch`].
///
/// There is a default implementation of this trait for `Box<Stretch<S, D>>`.
pub trait StretchBuilder {
    type State;
    type DrawTarget;

    /// Builder method that is used to set the child of stretch and its stretch factor to 1.
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the child of stretch and sets the stretch factor of the child.
    #[must_use]
    fn stretch(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>, stretch: u32) -> Self;
}

impl<S, D> StretchBuilder for Box<Stretch<S, D>>
where
    D: DrawTarget,
{
    type State = S;
    type DrawTarget = D;

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_child(child);
        self
    }

    fn stretch(
        mut self,
        child: Box<dyn Widget<Self::State, Self::DrawTarget>>,
        stretch: u32,
    ) -> Self {
        self.set_stretch_child(child, stretch);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestStretch = Stretch<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_stretch() {
        let stretch = TestStretch::new().stretch(TestStretch::new(), 5);
        assert_eq!(stretch.get_stretch(), 5);
        assert!(stretch.get_child().is_some());
    }

    #[test]
    fn builder_child() {
        let stretch = TestStretch::new().child(TestStretch::new());
        assert_eq!(stretch.get_stretch(), 1);
        assert!(stretch.get_child().is_some());
    }
}
