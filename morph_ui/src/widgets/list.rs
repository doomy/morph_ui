use downcast::TypeMismatch;

use crate::{
    downcast::Any,
    eg::{
        geometry::*,
        primitives::*,
        text::renderer::{CharacterStyle, TextRenderer},
    },
    stdlib::{collections::BTreeMap, vec::*, *},
    widget::states::*,
    widgets::scroll_area::*,
    *,
};

mod list_build_context;
mod list_builder;

pub use list_build_context::*;
pub use list_builder::*;

pub type SelectionChanged<S> = Box<dyn Fn(&mut S, Option<usize>) + 'static>;
pub type SelectionBuilder<T, S, D, R> =
    dyn Fn(&ListBuildContext<'_, T, S>) -> Box<Button<SelectionState, D, R>>;

/// `List` widget is used to display a list of items. The items can also be selected.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, widget::states::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<List<u32, i32, D, MonoTextStyle<'static, Rgb565>>> {
///     List::new()
///      .items(&[1, 2, 3, 4, 5, 6, 7]).builder(
///         |btx: &ListBuildContext<u32, i32>| {
///             let index = btx.index;
///             Button::new().text(format!("{}", index).as_str()).on_tap(
///                 move |state: &mut SelectionState| {
///                     state.select(index);
///                 },
///             )
///         },
///      )
/// }
/// ```
pub struct List<T, S, D, R>
where
    R: TextRenderer + 'static,
    D: DrawTarget + 'static,
{
    scroll_area: Box<ScrollArea<SelectionState, D>>,
    stx: StateContext<SelectionState>,
    items: Vec<T>,
    builder: Option<Box<SelectionBuilder<T, S, D, R>>>,
    on_selection_changed: Option<SelectionChanged<S>>,
    selection_set: bool,
}

impl<T, S, D, R> Default for List<T, S, D, R>
where
    T: Clone + 'static,
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer + 'static,
{
    fn default() -> Self {
        List {
            scroll_area: ScrollArea::new()
                .hor_scroll(false)
                .child(Container::new().child(Column::new())),
            stx: StateContext::new(SelectionState::default()),
            items: Vec::new(),
            builder: None,
            on_selection_changed: None,
            selection_set: false,
        }
    }
}

impl<T, S, D, R> List<T, S, D, R>
where
    R: TextRenderer + 'static,
    T: Clone + 'static,
    S: 'static,
    D: DrawTarget + 'static,
{
    /// Constructs a new `List` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Self::default())
    }

    /// Gets the style of the `List`.
    pub fn get_style(&self) -> Option<PrimitiveStyle<D::Color>> {
        self.scroll_area.get_style()
    }

    /// Sets the style of the `List`.
    pub fn set_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.scroll_area.set_style(style);
    }

    /// Gets a reference of the  scroll bar styles.
    pub fn get_scroll_bar_styles(&self) -> &BTreeMap<ScrollBarVisual, PrimitiveStyle<D::Color>> {
        self.scroll_area.get_scroll_bar_styles()
    }

    /// Gets the size (width for vertical | height for horizontal) of the scroll bars.
    pub fn get_scroll_bar_size(&self) -> u32 {
        self.scroll_area.get_scroll_bar_size()
    }

    /// Sets the size (width for vertical | height for horizontal) of the scroll bars.
    pub fn set_scroll_bar_size(&mut self, size: u32) {
        self.scroll_area.set_scroll_bar_size(size);
    }

    /// Sets the style of the scroll bars.
    pub fn insert_scroll_bar_style(
        &mut self,
        visual: ScrollBarVisual,
        scroll_bar_style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.scroll_area
            .insert_scroll_bar_style(visual, scroll_bar_style);
    }

    /// Gets the padding of the `List`. It is the space around the text of the `List`.
    pub fn get_padding(&self) -> Thickness {
        self.container().get_padding()
    }

    /// Sets the padding of the `List`.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.container_mut().set_padding(padding);
    }

    /// Gets the box constraints of the `List`.
    pub fn get_constraints(&self) -> Constraints {
        self.scroll_area.get_constraints()
    }

    /// Sets the box constraints of the `List`.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.scroll_area.set_constraints(constraints);
    }

    /// Gets the margin (width for vertical | height for horizontal) of the scroll bars.
    pub fn get_scroll_bar_margin(&self) -> u32 {
        self.scroll_area.get_scroll_bar_margin()
    }

    /// Sets the margin (width for vertical | height for horizontal) of the scroll bars.
    pub fn set_scroll_bar_margin(&mut self, margin: u32) {
        self.scroll_area.set_scroll_bar_margin(margin);
    }

    /// Gets the scroll bar corner radius.
    pub fn get_scroll_bar_corners(&self) -> CornerRadii {
        self.scroll_area.get_scroll_bar_corners()
    }

    /// Sets the corner radius of the scroll bars.
    pub fn set_scroll_bar_corners(&mut self, scroll_bar_corners: CornerRadii) {
        self.scroll_area.set_scroll_bar_corners(scroll_bar_corners);
    }

    /// Gets the corner radius of the `List`s background.
    pub fn get_corners(&self) -> CornerRadii {
        self.scroll_area.get_corners()
    }

    /// Sets the corner radius.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.scroll_area.set_corners(corners);
    }

    /// Gets the selection mode of the `List`.
    pub fn get_selection_mode(&self) -> SelectionMode {
        self.stx.state().get_selection_mode()
    }

    /// Sets the selection mode of the `List`.
    pub fn set_selection_mode(&mut self, mode: impl Into<SelectionMode>) {
        self.stx.state_mut().set_selection_mode(mode);
    }

    /// Gets the items of the `List`.
    pub fn get_items(&self) -> &[T] {
        &self.items
    }

    /// Sets the items of the `List`.
    pub fn set_items(&mut self, items: &[T]) {
        self.items = items.to_vec();
    }

    /// Gets the selected index of `List`.
    pub fn get_selected_index(&self) -> Option<usize> {
        self.stx.state().get_selected_index()
    }

    /// Sets the selected index of `List`.
    pub fn set_selected_index(&mut self, index: Option<usize>) {
        self.stx.state_mut().select(index);
        self.selection_set = true;
    }

    /// Gets the value that indicates if vertical scrolling is enabled.
    pub fn get_is_ver_scroll(&self) -> bool {
        self.scroll_area.get_is_ver_scroll()
    }

    /// Sets is_ver_scroll flag. If set to `true` vertical scrolling is enabled otherwise not. Default is `true`.
    pub fn set_is_ver_scroll(&mut self, is_ver_scroll: bool) {
        self.scroll_area.set_is_ver_scroll(is_ver_scroll);
    }

    /// Sets the function to build the visual representation of an item.
    pub fn set_builder<
        F: Fn(&ListBuildContext<'_, T, S>) -> Box<Button<SelectionState, D, R>> + 'static,
    >(
        &mut self,
        builder: F,
    ) {
        self.builder = Some(Box::new(builder));
    }

    /// Sets selection changed callback.
    pub fn set_on_selection_changed<F: Fn(&mut S, Option<usize>) + 'static>(
        &mut self,
        on_selection_changed: F,
    ) {
        self.on_selection_changed = Some(Box::new(on_selection_changed));
    }

    // -- helpers

    fn container(&self) -> &Container<SelectionState, D> {
        get_widget_child::<SelectionState, D, Container<SelectionState, D>>(
            self.scroll_area.as_ref(),
        )
        .unwrap()
    }

    fn container_mut(&mut self) -> &mut Container<SelectionState, D> {
        get_widget_child_mut::<SelectionState, D, Container<SelectionState, D>>(
            self.scroll_area.as_mut(),
        )
        .unwrap()
    }

    fn column_mut(&mut self) -> &mut Column<SelectionState, D> {
        get_widget_child_mut::<SelectionState, D, Column<SelectionState, D>>(self.container_mut())
            .unwrap()
    }

    fn update_selection(
        item: Result<&mut Button<SelectionState, D, R>, TypeMismatch>,
        selected: bool,
    ) {
        if let Ok(item) = item {
            item.set_is_checked(selected);
        }
    }
}

impl<T, S, D, R> Widget<S, D> for List<T, S, D, R>
where
    R: TextRenderer<Color = D::Color> + CharacterStyle + Clone + 'static,
    T: Clone + 'static,
    S: 'static,

    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        self.scroll_area.base()
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        self.scroll_area.base_mut()
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let redraw = dtx.has_render_update(Some(self.id()));

        if dtx.stx().layout_update() {
            self.stx.trigger_layout_update();
        }

        if dtx.stx().render_update() {
            self.stx.trigger_render_update(Some(self.id()));
        }

        let (dt, _, ktx, dbx) = dtx.all();

        self.scroll_area
            .draw(&mut DrawContext::new(dt, &mut self.stx, ktx, redraw, dbx))?;

        self.stx.reset_update();

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        self.scroll_area.layout(ltx)
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if self.column_mut().get_children().is_empty() {
            return;
        }

        let selected_index = self.stx.state().get_selected_index();
        let (_, ktx, ctx) = etx.all();
        self.scroll_area
            .event(event, &mut EventContext::new(&mut self.stx, ktx, ctx));

        let old_selected_items = self.stx.state_mut().remove_old_selected_indexes();
        if let Some(selected_item) = self.stx.state().get_selected_index() {
            let selected_item = selected_item.min(self.items.len() - 1);

            Self::update_selection(
                self.column_mut().get_children_mut()[selected_item]
                    .downcast_mut::<Button<SelectionState, D, R>>(),
                true,
            );
        }

        for i in old_selected_items {
            Self::update_selection(
                self.column_mut().get_children_mut()[i]
                    .downcast_mut::<Button<SelectionState, D, R>>(),
                false,
            );
        }

        if self.stx.layout_update() {
            etx.stx_mut().trigger_layout_update();
        }

        if self.stx.render_update() {
            etx.stx_mut().trigger_render_update(Some(self.id()));
        }

        self.stx.reset_update();

        if selected_index != self.stx.state().get_selected_index() {
            if let Some(on_selection_changed) = &self.on_selection_changed {
                on_selection_changed(
                    etx.stx_mut().state_mut(),
                    self.stx.state().get_selected_index(),
                );
            }
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "List {{ len: \"{:?}\", bounds: {:?} }}",
            self.items.len(),
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        for i in 0..self.items.len() {
            let item = &self.items[i];
            let is_selected = self.stx.state().is_selected(i);

            if let Some(builder) = &self.builder {
                let item_widget = builder(&ListBuildContext {
                    item,
                    index: i,
                    is_selected,
                    app: itx.stx_mut().state_mut(),
                    state: self.stx.state_mut(),
                });

                self.column_mut().push_align(item_widget, "stretch");
            }
        }

        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            self.scroll_area
                .init(None, &mut InitContext::from_stx(itx, &mut self.stx));
        }

        self.init_id(itx);

        for i in 0..self.column_mut().children_len() {
            let selected = self.stx.state().is_selected(i);

            Self::update_selection(
                self.column_mut().get_children_mut()[i]
                    .downcast_mut::<Button<SelectionState, D, R>>(),
                selected,
            );
        }
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<List<T, S, D, R>>() {
            if !new.selection_set {
                new.stx = self.stx.clone();
            }
            new.selection_set = false;
            new.scroll_area.init(
                Some(self.scroll_area.as_ref()),
                &mut InitContext::from_stx(itx, &mut new.stx),
            );
        }
    }
}
