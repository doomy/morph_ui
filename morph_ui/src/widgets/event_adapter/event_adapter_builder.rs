use crate::{downcast::Any, eg::prelude::*, stdlib::boxed::Box, widgets::EventAdapter, *};

/// `EventAdapterBuilder` is used to define builder methods to construct a [`EventAdapter`].
///
/// There is a default implementation of this trait for `Box<EventAdapter<S, D>>`.
pub trait EventAdapterBuilder {
    type State;
    type DrawTarget;

    /// Builder method that is used to set the child of stretch and its stretch factor to 1.
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to attach a handler to the event adapter.
    #[must_use]
    fn attach<F: Fn(&mut EventContext<Self::State>, &dyn Any) + 'static>(self, handler: F) -> Self;
}

impl<S, D> EventAdapterBuilder for Box<EventAdapter<S, D>>
where
    D: DrawTarget,
{
    type State = S;
    type DrawTarget = D;

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_child(child);
        self
    }

    fn attach<F: Fn(&mut EventContext<Self::State>, &dyn Any) + 'static>(
        mut self,
        handler: F,
    ) -> Self {
        self.set_handler(handler);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestEventAdapter = EventAdapter<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_child() {
        let event_adapter = TestEventAdapter::new().child(TestEventAdapter::new());
        assert!(event_adapter.get_child().is_some());
    }

    #[test]
    fn builder_attach() {
        assert!(TestEventAdapter::new()
            .attach(|_, _| {})
            .get_handler()
            .is_some());
    }
}
