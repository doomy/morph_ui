use crate::{
    eg::{prelude::*, primitives::*},
    stdlib::boxed::Box,
    widgets::Container,
    *,
};

/// `ContainerBuilder` is used to define builder methods to construct a [`Container`].
///
/// There is a default implementation of this trait for `Box<Container<S, D>>`.
pub trait ContainerBuilder {
    type DrawTarget;
    type Style;
    type State;

    /// Builder method that is used to set the style of the [`Container`].
    #[must_use]
    fn style(self, style: Self::Style) -> Self;

    /// Builder method that is used to set the child of the [`Container`].
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the padding of the [`Container`].
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the corner radius of the [`Container`].
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;
}

impl<S, D> ContainerBuilder for Box<Container<S, D>>
where
    D: DrawTarget,
{
    type DrawTarget = D;
    type State = S;
    type Style = PrimitiveStyle<D::Color>;

    fn style(mut self, style: Self::Style) -> Self {
        self.set_style(style);
        self
    }

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_child(child);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        eg::{mock_display::MockDisplay, pixelcolor::Rgb565},
        utils::*,
    };

    type TestContainer = Container<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_style() {
        assert_eq!(
            TestContainer::new()
                .style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_style(),
            Some(
                PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_child() {
        let container = TestContainer::new().child(TestContainer::new());
        assert!(container.get_child().is_some());
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestContainer::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestContainer::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }
}
