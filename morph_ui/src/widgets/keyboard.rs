mod de_de;
mod en_us;
mod key_model;
mod keyboard_builder;
mod keyboard_layout;
mod keyboard_state;

pub use de_de::*;
pub use en_us::*;
pub use key_model::*;
pub use keyboard_builder::*;
pub use keyboard_layout::*;
pub use keyboard_state::*;

use crate::{
    downcast::Any,
    eg::{
        geometry::*,
        primitives::PrimitiveStyle,
        text::renderer::{CharacterStyle, TextRenderer},
    },
    stdlib::{marker::PhantomData, ops::Deref, string::String, *},
    *,
};

pub type KeyBuilder<D, R> = dyn Fn(KeyModel, bool) -> Box<Button<KeyboardState, D, R>>;
pub type OpKeyBuilder<D, R> = dyn Fn(&str, bool) -> Box<Button<KeyboardState, D, R>>;
pub type KeyboardTextFieldBuilder<D, R> = dyn Fn(&str) -> Box<TextField<KeyboardState, D, R>>;

/// `Keyboard` represents an on screen `Keyboard` that can be to insert text on devices without a physical `Keyboard`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::keyboard::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Keyboard<i32, D, MonoTextStyle<'static, Rgb565>>> {
///     Keyboard::new()
///         .layout(en_us())
/// }
/// ```
pub struct Keyboard<S, D, R>
where
    D: DrawTarget + 'static,
{
    base: WidgetBase,
    layout: KeyboardLayout,
    key_builder: Option<Box<KeyBuilder<D, R>>>,
    op_key_builder: Option<Box<OpKeyBuilder<D, R>>>,
    text_field_builder: Option<Box<KeyboardTextFieldBuilder<D, R>>>,
    spacing: u32,
    container: Box<Container<KeyboardState, D>>,
    stx: StateContext<KeyboardState>,
    target_id: Option<u32>,
    _state: PhantomData<S>,
}

impl<S, D, R> Default for Keyboard<S, D, R>
where
    S: 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,

    D: DrawTarget + 'static,
{
    fn default() -> Self {
        Keyboard {
            layout: KeyboardLayout::default(),
            key_builder: None,
            op_key_builder: None,
            text_field_builder: None,
            spacing: 0,
            base: WidgetBase::new(),
            container: Container::new(),
            stx: StateContext::new(KeyboardState::new()),
            target_id: None,
            _state: PhantomData::default(),
        }
    }
}

impl<S, D, R> Keyboard<S, D, R>
where
    S: 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
    D: DrawTarget + 'static,
{
    /// Constructs a new `Keyboard` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Keyboard::default())
    }

    /// Returns the background style.
    pub fn get_style(&self) -> Option<PrimitiveStyle<D::Color>> {
        self.container.get_style()
    }

    /// Sets the style of the background.
    pub fn set_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.container.set_style(style);
    }

    /// Gets the padding of the `Keyboard`.
    pub fn get_padding(&self) -> Thickness {
        self.container.get_padding()
    }

    /// Sets the padding of the `Keyboard`.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.container.set_padding(padding);
    }

    /// Gets the spacing between the key buttons.
    pub fn get_spacing(&self) -> u32 {
        self.spacing
    }

    /// Sets the spacing between the key buttons.
    pub fn set_spacing(&mut self, spacing: u32) {
        self.spacing = spacing;
    }

    /// Returns a mutable reference to the `Keyboard` layout.
    pub fn get_layout_mut(&mut self) -> &mut KeyboardLayout {
        &mut self.layout
    }

    /// Gets the `Keyboard` layout.
    pub fn get_layout(&self) -> &KeyboardLayout {
        &self.layout
    }

    /// Sets the `Keyboard` layout.
    pub fn set_layout(&mut self, layout: KeyboardLayout) {
        self.layout = layout;
    }

    /// Sets builder for the key buttons.
    ///
    /// First parameter is the key model of the button, second defines if caps lock is active.
    pub fn set_key_builder<F: Fn(KeyModel, bool) -> Box<Button<KeyboardState, D, R>> + 'static>(
        &mut self,
        key_builder: F,
    ) {
        self.key_builder = Some(Box::new(key_builder));
    }

    /// Sets the builder for the the operation key buttons.
    ///
    /// First parameter is the text of the button, second defines if the button is primary.
    pub fn set_op_key_builder<F: Fn(&str, bool) -> Box<Button<KeyboardState, D, R>> + 'static>(
        &mut self,
        op_key_builder: F,
    ) {
        self.op_key_builder = Some(Box::new(op_key_builder));
    }

    /// Sets builder for the text_field buttons.
    pub fn set_text_field_builder<F: Fn(&str) -> Box<TextField<KeyboardState, D, R>> + 'static>(
        &mut self,
        text_field_builder: F,
    ) {
        self.text_field_builder = Some(Box::new(text_field_builder));
    }

    // helpers

    fn add_caps_lock(&self, row: &mut Row<KeyboardState, D>, caps_lock: bool) {
        if let Some(builder) = &self.op_key_builder {
            row.push(
                builder(self.layout.caps_lock_text.as_str(), false)
                    .checkable(true)
                    .checked(caps_lock)
                    .on_tap(move |state: &mut KeyboardState| state.toggle_caps_lock()),
            );
        }
    }

    fn add_backspace(&self, row: &mut Row<KeyboardState, D>) {
        if let Some(builder) = &self.op_key_builder {
            row.push(
                builder(self.layout.back_space_text.as_str(), false)
                    .on_tap(move |state: &mut KeyboardState| state.back_space()),
            );
        }
    }

    fn add_space(&self, row: &mut Row<KeyboardState, D>) {
        if let Some(builder) = &self.op_key_builder {
            row.push(
                Stretch::new().child(
                    builder(self.layout.space_text.as_str(), false)
                        .constraints(Constraints::create().max_width(120))
                        .on_tap(move |state: &mut KeyboardState| state.space()),
                ),
            );
        }
    }

    fn add_enter(&self, row: &mut Row<KeyboardState, D>) {
        if let Some(builder) = &self.op_key_builder {
            row.push(
                builder(self.layout.enter_text.as_str(), true)
                    .on_tap(move |state: &mut KeyboardState| state.close_keyboard()),
            );
        }
    }

    fn add_arrow_left(&self, row: &mut Row<KeyboardState, D>) {
        if let Some(builder) = &self.op_key_builder {
            row.push(
                builder(self.layout.arrow_left_text.as_str(), false)
                    .on_tap(move |state: &mut KeyboardState| state.arrow_left()),
            );
        }
    }

    fn add_arrow_right(&self, row: &mut Row<KeyboardState, D>) {
        if let Some(builder) = &self.op_key_builder {
            row.push(
                builder(self.layout.arrow_right_text.as_str(), false)
                    .on_tap(move |state: &mut KeyboardState| state.arrow_right()),
            );
        }
    }

    fn add_toggle_type(&self, row: &mut Row<KeyboardState, D>) {
        if let Some(builder) = &self.op_key_builder {
            row.push(
                builder(self.layout.toggle_type_text.as_str(), false)
                    .on_tap(move |state: &mut KeyboardState| state.toggle_keyboard_type()),
            );
        }
    }

    // builds the `Keyboard` layout
    fn build(
        &mut self,
        keyboard_type: &str,
        text: &str,
        cursor: Option<Cursor>,
        is_password: bool,
    ) {
        let caps_lock = self.stx.state().is_caps_lock();

        let mut column = Column::new().spacing(self.spacing);
        column.push(Spacer::new());

        // copy text_field from first column to second column
        if let Some(col) = get_widget_child_mut::<KeyboardState, D, Column<KeyboardState, D>>(
            self.container.as_mut(),
        ) {
            // pop spacer
            col.children_mut().pop_front();

            if let Some(child) = col.children_mut().pop_front() {
                column.children_mut().push_back(child);
            }
        } else if let Some(builder) = &self.text_field_builder {
            let mut text_field = builder(text)
                .force_active_focus(true)
                .password_box(is_password);

            if let Some(cursor) = cursor {
                text_field.set_cursor(cursor);
            }
            column.push_align(text_field, "center");
        }

        let mut row_count = 0;

        // init keys
        if let Some(layout) = self.layout.rows.get(keyboard_type) {
            for key_row in layout {
                let mut row = Row::<KeyboardState, D>::new().spacing(4);

                if row_count == 2 {
                    self.add_caps_lock(&mut row, caps_lock);
                }

                if let Some(builder) = &self.key_builder {
                    for key in key_row {
                        row.push(builder(*key, caps_lock));
                    }
                }

                if row_count == 2 {
                    self.add_backspace(&mut row);
                }

                row_count += 1;
                column.push_align(row, "center");
            }

            let mut row = Row::<KeyboardState, D>::new().spacing(self.spacing);

            row.push(Spacer::new());
            self.add_toggle_type(&mut row);
            self.add_arrow_left(&mut row);
            self.add_space(&mut row);
            self.add_arrow_right(&mut row);
            self.add_enter(&mut row);
            row.push(Spacer::new());

            column.push_align(row, "center");
        }

        self.container.set_child(column);
    }
}

impl<S, D, R> Widget<S, D> for Keyboard<S, D, R>
where
    S: 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let redraw = dtx.has_render_update(Some(self.id()));

        if dtx.stx().layout_update() {
            self.stx.trigger_layout_update();
        }

        if dtx.stx().render_update() {
            self.stx.trigger_render_update(Some(self.id()));
        }

        let (dt, _, ktx, dbx) = dtx.all();
        self.container.set_top_left(self.top_left());
        self.container
            .draw(&mut DrawContext::new(dt, &mut self.stx, ktx, redraw, dbx))
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let size = self.container.layout(ltx);
        self.set_size(size);
        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        let (_, ktx, ctx) = etx.all();

        let caps_lock_old = self.stx.state().is_caps_lock();
        let keyboard_type_old = self.stx.state().keyboard_type();

        self.container
            .event(event, &mut EventContext::new(&mut self.stx, ktx, ctx));

        while let Some(event) = self.stx.state_mut().dequeue_event() {
            self.container.event(
                event.deref(),
                &mut EventContext::new(&mut self.stx, ktx, ctx),
            );
        }

        if caps_lock_old != self.stx.state().is_caps_lock()
            || keyboard_type_old != self.stx.state().keyboard_type()
        {
            self.build(self.stx.state().keyboard_type().as_str(), "", None, false);
        }

        if self.stx.layout_update() {
            etx.stx_mut().trigger_layout_update();
        }

        if self.stx.render_update() {
            etx.stx_mut().trigger_render_update(Some(self.id()));
        }

        self.stx.reset_update();

        if self.stx.state_mut().close() {
            if let Some(col) = get_widget_child_mut::<KeyboardState, D, Column<KeyboardState, D>>(
                self.container.as_mut(),
            ) {
                // pop spacer
                col.children_mut().pop_front();

                if let Some(mut child) = col.children_mut().pop_front() {
                    if let Ok(text_field) =
                        &mut child.1.downcast_mut::<TextField<KeyboardState, D, R>>()
                    {
                        etx.ktx_mut().send_keyboard_request(KeyboardRequest::Close(
                            String::from(text_field.get_text()),
                            self.target_id.unwrap(),
                            text_field.get_cursor(),
                        ));
                    }
                }
            }

            self.container.remove_child();
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Keyboard {{ stretch: {}, bounds {:?}  }}",
            self.stretch(),
            self.bounding_box()
        )
    }

    fn init(&mut self, _: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let KeyboardRequest::Open(text, id, cursor, is_password) = itx.ktx().keyboard_request() {
            self.target_id = Some(*id);

            self.build(ALPHA_KEYBOARD, text, Some(*cursor), *is_password);
        }
        self.container
            .init(None, &mut InitContext::from_stx(itx, &mut self.stx));

        self.init_id(itx);
    }
}
