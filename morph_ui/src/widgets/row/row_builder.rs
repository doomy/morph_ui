use crate::{
    eg::draw_target::DrawTarget, stdlib::boxed::Box, utils::*, widget::Widget, widgets::Row,
};

/// `RowBuilder` is used to define builder methods to construct a [`Row`].
///
/// There is a default implementation of this trait for `Box<Row<S, D>>`.
pub trait RowBuilder {
    type State;
    type DrawTarget;

    /// Builder method that is used to set the horizontal spacing between the children of the `Row`.
    #[must_use]
    fn spacing(self, spacing: u32) -> Self;

    /// Builder method that is used to push a child to the `Row` layout and set the vertical child alignment to [`Align::Start`].
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to push a child to the `Row` layout and set the vertical alignment of the child.
    #[must_use]
    fn align(
        self,
        child: Box<dyn Widget<Self::State, Self::DrawTarget>>,
        align: impl Into<Align>,
    ) -> Self;
}

impl<S, D> RowBuilder for Box<Row<S, D>>
where
    D: DrawTarget,
{
    type State = S;
    type DrawTarget = D;

    fn spacing(mut self, spacing: u32) -> Self {
        self.set_spacing(spacing);
        self
    }

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.push(child);
        self
    }

    fn align(
        mut self,
        child: Box<dyn Widget<Self::State, Self::DrawTarget>>,
        align: impl Into<Align>,
    ) -> Self {
        self.push_align(child, align);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        eg::{mock_display::MockDisplay, pixelcolor::Rgb565},
        stdlib::*,
    };

    type TestRow = Row<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_spacing() {
        assert_eq!(TestRow::new().spacing(10).get_spacing(), 10);
    }

    #[test]
    fn builder_child() {
        let mut test_row = TestRow::new().child(TestRow::new());

        assert_eq!(test_row.len(), 1);
        assert!(!test_row.is_empty());
        assert_eq!(test_row.children_mut()[0].0, Align::Start);
    }

    #[test]
    fn builder_align() {
        let mut test_row = TestRow::new().align(TestRow::new(), "center");

        assert_eq!(test_row.len(), 1);
        assert!(!test_row.is_empty());
        assert_eq!(test_row.children_mut()[0].0, Align::Center);
    }
}
