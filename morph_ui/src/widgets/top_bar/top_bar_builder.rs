use crate::{
    eg::{prelude::*, primitives::*},
    stdlib::boxed::Box,
    widgets::TopBar,
    *,
};

/// `TopBarBuilder` is used to define builder methods to construct a [`TopBar`].
///
/// There is a default implementation of this trait for `Box<TopBar<S, D>>`.
pub trait TopBarBuilder {
    type DrawTarget;
    type Style;
    type State;

    /// Builder method that is used to set the (start) front widget o of the [`TopBar`].
    #[must_use]
    fn front(self, front: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the (center) title widget o of the [`TopBar`].
    #[must_use]
    fn title(self, title: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the (end) tail widget o of the [`TopBar`].
    #[must_use]
    fn tail(self, tail: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the style of the [`TopBar`].
    #[must_use]
    fn style(self, style: Self::Style) -> Self;

    /// Builder method that is used to set the padding of the [`TopBar`].
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the horizontal spacing between the children of the  [`TopBar`].
    #[must_use]
    fn spacing(self, spacing: u32) -> Self;
}

impl<S, D> TopBarBuilder for Box<TopBar<S, D>>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    type DrawTarget = D;
    type State = S;
    type Style = PrimitiveStyle<D::Color>;

    fn front(mut self, front: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_front(front);
        self
    }

    fn title(mut self, title: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_title(title);
        self
    }

    fn tail(mut self, tail: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_tail(tail);
        self
    }

    fn style(mut self, style: Self::Style) -> Self {
        self.set_style(style);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding.into());
        self
    }

    fn spacing(mut self, spacing: u32) -> Self {
        self.set_spacing(spacing);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        eg::{mock_display::MockDisplay, pixelcolor::Rgb565},
        utils::*,
    };

    type TestTopBar = TopBar<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_front() {
        assert!(TestTopBar::new()
            .front(TestTopBar::new())
            .get_front()
            .is_some());
    }

    #[test]
    fn builder_title() {
        assert!(TestTopBar::new()
            .title(TestTopBar::new())
            .get_title()
            .is_some());
    }

    #[test]
    fn builder_tail() {
        assert!(TestTopBar::new()
            .tail(TestTopBar::new())
            .get_tail()
            .is_some());
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestTopBar::new()
                .style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_style(),
            Some(
                PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestTopBar::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_spacing() {
        assert_eq!(TestTopBar::new().spacing(10).get_spacing(), 10);
    }
}
