use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{marker::PhantomData, *},
    *,
};

mod scope_builder;

pub use scope_builder::*;

/// `Scope` is a parent widget that is used to introduce a new state for its children.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*, utils::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Scope<S, D, u32>> {
///    Scope::new(0)  
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
/// }       
/// ```
pub struct Scope<S, D, I>
where
    D: DrawTarget,
{
    child: Option<Box<dyn Widget<I, D>>>,
    stx: StateContext<I>,
    base: WidgetBase,
    _state: PhantomData<S>,
}

impl<S, D, I> Scope<S, D, I>
where
    D: DrawTarget,
{
    /// Constructs a new `Scope` wrapped in a [`Box`].
    pub fn new(state: I) -> Box<Self> {
        Box::new(Scope {
            stx: StateContext::new(state),
            child: None,
            base: WidgetBase::default(),
            _state: PhantomData::default(),
        })
    }

    /// Gets the child of `Scope`.
    pub fn get_child(&self) -> Option<&dyn Widget<I, D>> {
        self.child.as_ref().map(|c| c.as_ref())
    }

    /// Returns a mutable reference of the child of the container.
    pub fn get_child_mut(&mut self) -> Option<&mut dyn Widget<I, D>> {
        self.child.as_mut().map(|c| c.as_mut())
    }

    /// Sets the child of stretch and set the stretch factor of the child to 1.
    pub fn set_child(&mut self, child: Box<dyn Widget<I, D>>) {
        self.child = Some(child);
    }
}

impl<S, D, I> Widget<S, D> for Scope<S, D, I>
where
    I: 'static + Clone,
    S: 'static,
    D: DrawTarget + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let top_left = self.top_left();
        if let Some(child) = &mut self.child {
            child.update_and_draw(top_left, &mut DrawContext::from_stx(dtx, &mut self.stx))?;
        }

        Ok(())
    }

    fn stretch(&self) -> u32 {
        self.base.stretch
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        let mut size = Size::zero();
        if let Some(child) = &mut self.child {
            child.set_top_left(Point::zero());

            size = child.layout(ltx);
        }

        self.set_size(size);

        size
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        if let Some(child) = &mut self.child {
            child.event(event, &mut EventContext::from_stx(etx, &mut self.stx));
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Scope {{ stretch: {}, bounds: {:?} }}",
            self.stretch(),
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else if let Some(child) = &mut self.child {
            child.init_and_update(None, &mut InitContext::from_stx(itx, &mut self.stx));
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Scope<S, D, I>>() {
            if let Some(new_child) = &mut new.child {
                new_child.init_and_update(
                    self.child.as_deref(),
                    &mut InitContext::from_stx(itx, &mut new.stx),
                );
            }

            new.base.id = self.base.id;
            new.stx = self.stx.clone();
        }
    }
}
