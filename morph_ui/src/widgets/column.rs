use crate::{
    downcast::Any,
    eg::geometry::*,
    stdlib::{collections::VecDeque, *},
    *,
};

pub type AlignChildren<S, D> = VecDeque<(Align, Box<dyn Widget<S, D>>)>;

mod column_builder;
mod flex;

pub use column_builder::*;
pub use flex::*;

/// `Column` is a layout widget that orders its children vertical.
///
/// To construct a `Label` check the [`ColumnBuilder`] trait. It is implemented for `Box<Column<S, D>>`.
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn column<S: 'static, D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Column<S, D>> {
///    Column::new()
///         .spacing(4)
///         .child(Label::new()
///                 .text("Hello")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///         )
///         .align(Label::new()
///                 .text("morph")
///                 .style(
///                     MonoTextStyleBuilder::new()
///                         .font(&FONT_7X13)
///                         .text_color(Rgb565::BLACK)
///                     .build(),
///                 )
///                 , "center"
///         )
/// }       
/// ```
pub struct Column<S, D: DrawTarget> {
    flex: Flex<S, D>,
}

impl<S, D: DrawTarget> Default for Column<S, D> {
    fn default() -> Self {
        Column {
            flex: Flex::new(true),
        }
    }
}

impl<S, D: DrawTarget> Column<S, D> {
    /// Constructs a new `Column` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Column::default())
    }

    /// Gets the vertical spacing between the children of the `Column`.
    pub fn get_spacing(&self) -> u32 {
        self.flex.spacing
    }

    /// Sets the vertical spacing between the children of the `Column`.
    pub fn set_spacing(&mut self, spacing: u32) {
        self.flex.spacing = spacing;
    }

    /// Push a child to the `Column` layout and set the horizontal child alignment to [`Align::Start`].
    pub fn push(&mut self, child: Box<dyn Widget<S, D>>) {
        self.flex.children.push_back((Align::Start, child));
    }

    /// Push a child to the `Column` layout and set the horizontal alignment of the child.
    pub fn push_align(&mut self, child: Box<dyn Widget<S, D>>, align: impl Into<Align>) {
        self.flex.children.push_back((align.into(), child));
    }

    /// Returns the count of the children.
    pub fn len(&self) -> usize {
        self.flex.children.len()
    }

    /// Returns `true` if the column dos not contains children.
    pub fn is_empty(&self) -> bool {
        self.flex.children.is_empty()
    }

    /// Returns a mutable reference of the children.
    pub fn children_mut(&mut self) -> &mut AlignChildren<S, D> {
        &mut self.flex.children
    }
}

impl<S: 'static, D: DrawTarget + 'static> Widget<S, D> for Column<S, D> {
    fn base(&self) -> &WidgetBase {
        &self.flex.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.flex.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        self.flex.draw(dtx)
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        self.flex.layout(ltx)
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        self.flex.event(event, etx);
    }

    fn widget_string(&self) -> String {
        format!(
            "Column {{ spacing: {}, bounds: {:?} }}",
            self.flex.spacing,
            self.bounding_box()
        )
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        } else {
            self.flex.init_and_update(None, itx);
        }
    }

    fn transfer(&self, new: &mut dyn Any, itx: &mut InitContext<S>) {
        if let Ok(new) = &mut new.downcast_mut::<Column<S, D>>() {
            new.flex.init(Some(&self.flex), itx);
        }
    }

    fn get_children(&self) -> Vec<&dyn Widget<S, D>> {
        self.flex.get_children()
    }

    fn get_children_mut(&mut self) -> Vec<&mut dyn Widget<S, D>> {
        self.flex.get_children_mut()
    }
}
