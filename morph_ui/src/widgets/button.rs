use crate::{
    downcast::Any,
    eg::{
        geometry::*,
        primitives::*,
        text::renderer::{CharacterStyle, TextRenderer},
    },
    events::*,
    stdlib::{collections::BTreeMap, *},
    utils::*,
    widgets::Label,
    *,
};

mod button_builder;
mod button_state;
mod button_visual;

pub use button_builder::*;
pub use button_state::*;
pub use button_visual::*;

/// `Button` is a widget that can be tapped and performs an action (on_tap).
///
/// # Example
///
/// ```
/// use morph_ui::{widgets::button::*};
/// use morph_ui::eg::{pixelcolor::*, draw_target::DrawTarget, mono_font::{ascii::*, *}};
///
/// fn view<D: DrawTarget<Color = Rgb565> + 'static>() -> Box<Button<i32, D, MonoTextStyle<'static, Rgb565>>> {
///     Button::new()
///         .text("Click me")
///         .on_tap(|count: &mut i32| *count += 1)
/// }
/// ```
pub struct Button<S, D, R>
where
    D: DrawTarget,
{
    text_styles: BTreeMap<ButtonVisual, R>,
    styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>,
    focus_style: PrimitiveStyle<D::Color>,
    label: Box<Label<S, D, R>>,
    state: ButtonState,
    padding: Thickness,
    constraints: Constraints,
    base: WidgetBase,
    corners: CornerRadii,
    on_tap: Option<Box<dyn Fn(&mut S) + 'static>>,
    on_is_checked_changed: Option<Box<dyn Fn(&mut S, bool) + 'static>>,
}

impl<S, D, R> Default for Button<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    fn default() -> Self {
        Button {
            styles: BTreeMap::new(),
            focus_style: PrimitiveStyle::new(),
            text_styles: BTreeMap::new(),
            label: Label::new(),
            state: ButtonState::new(),
            padding: Thickness::from(8),
            constraints: Constraints::default(),
            base: WidgetBase::new(),
            corners: CornerRadii::default(),
            on_tap: None,
            on_is_checked_changed: None,
        }
    }
}

impl<S, D, R> Button<S, D, R>
where
    D: DrawTarget,
    R: TextRenderer,
{
    /// Constructs a new `Button` wrapped in a [`Box`].
    pub fn new() -> Box<Self> {
        Box::new(Button::default())
    }

    /// Gets the text of the `Button`.
    pub fn get_text(&self) -> &str {
        self.label.get_text()
    }

    /// Sets the text of the `Button`.
    pub fn set_text(&mut self, text: &str) {
        self.label.set_text(text);
    }

    /// Returns the background style map of the `Button`.
    pub fn get_styles(&self) -> &BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>> {
        &self.styles
    }

    /// Sets the background styles map of the `Button`.
    pub fn set_styles(&mut self, styles: BTreeMap<ButtonVisual, PrimitiveStyle<D::Color>>) {
        self.styles = styles;
    }

    /// Inserts a new background style for the given visual.
    pub fn insert_style(
        &mut self,
        visual: ButtonVisual,
        style: impl Into<PrimitiveStyle<D::Color>>,
    ) {
        self.styles.insert(visual, style.into());
    }

    /// Gets the text styles map of the `Button`.
    pub fn get_text_styles(&self) -> &BTreeMap<ButtonVisual, R> {
        &self.text_styles
    }

    /// Sets the text styles map of the `Button`.
    pub fn set_text_styles(&mut self, text_styles: BTreeMap<ButtonVisual, R>) {
        self.text_styles = text_styles;
    }

    /// Inserts a new text style to the text styles map of the `Button`.
    pub fn insert_text_style(&mut self, visual: ButtonVisual, style: impl Into<R>) {
        self.text_styles.insert(visual, style.into());
    }

    /// Gets the focus style of the `Button`.
    pub fn get_focus_style(&self) -> PrimitiveStyle<D::Color> {
        self.focus_style
    }

    /// Sets the focus style of the `Button`.
    pub fn set_focus_style(&mut self, style: impl Into<PrimitiveStyle<D::Color>>) {
        self.focus_style = style.into();
    }

    /// Gets the padding of the `Button`. It is the space around the text of the `Button`.
    pub fn get_padding(&self) -> Thickness {
        self.padding
    }

    /// Sets the padding of the `Button`. It is the space around the text of the `Button`.
    pub fn set_padding(&mut self, padding: impl Into<Thickness>) {
        self.padding = padding.into();
    }

    /// Gets the box constraints of the `Button`.
    pub fn get_constraints(&self) -> Constraints {
        self.constraints
    }

    /// Sets the box constraints of the `Button`.
    pub fn set_constraints(&mut self, constraints: impl Into<Constraints>) {
        self.constraints = constraints.into();
    }

    /// Gets the current visual state of the `Button`.
    pub fn get_visual(&self) -> ButtonVisual {
        self.state.get_visual()
    }

    /// Sets the current visual state of the `Button`.
    pub fn set_visual(&mut self, visual: impl Into<ButtonVisual>) {
        self.state.set_visual(visual);
    }

    /// Gets if the `Button` is checkable or not.
    pub fn get_is_checkable(&self) -> bool {
        self.state.get_is_checkable()
    }

    /// Sets if the `Button` is checkable or not.
    pub fn set_is_checkable(&mut self, is_checkable: bool) {
        self.state.set_is_checkable(is_checkable);
    }

    /// Gets the value that indicates if the `Button` is currently checked.
    pub fn get_is_checked(&self) -> bool {
        self.state.get_is_checked()
    }

    ///  Sets the value that indicates if the `Button` is currently checked.
    pub fn set_is_checked(&mut self, is_checked: bool) {
        self.state.set_is_checked(is_checked);
    }

    /// Gets the corner radius of the `Button`s background.
    pub fn get_corners(&self) -> CornerRadii {
        self.corners
    }

    /// Sets the corner radius of the `Button`s background.
    pub fn set_corners(&mut self, corners: CornerRadii) {
        self.corners = corners;
    }

    /// Gets the value that indicates if the `Button` is enabled.
    pub fn get_is_enabled(&self) -> bool {
        self.state.get_is_enabled()
    }

    /// Sets the value that indicates if the `Button` is enabled.
    pub fn set_is_enabled(&mut self, is_enabled: bool) {
        self.state.set_is_enabled(is_enabled);
    }

    /// Sets the callback that is called after the `Button` is tapped.
    pub fn set_on_tap<F: Fn(&mut S) + 'static>(&mut self, on_tap: F) {
        self.on_tap = Some(Box::new(on_tap));
    }

    /// Sets the callback that is called after `is_checked` of `Button` is changed.
    pub fn set_on_is_checked_changed<F: Fn(&mut S, bool) + 'static>(
        &mut self,
        on_checked_changed: F,
    ) {
        self.on_is_checked_changed = Some(Box::new(on_checked_changed));
    }

    // -- Helpers --

    fn text_visual(&self) -> ButtonVisual {
        if self.text_styles.contains_key(&self.state.get_visual()) {
            self.state.get_visual()
        } else {
            ButtonVisual::Default
        }
    }

    fn style_visual(&self) -> ButtonVisual {
        if self.styles.contains_key(&self.state.get_visual()) {
            self.state.get_visual()
        } else {
            ButtonVisual::Default
        }
    }
}

impl<S, D, R> Widget<S, D> for Button<S, D, R>
where
    S: 'static,

    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + Clone + 'static,
{
    fn base(&self) -> &WidgetBase {
        &self.base
    }

    fn base_mut(&mut self) -> &mut WidgetBase {
        &mut self.base
    }

    fn draw(&mut self, dtx: &mut DrawContext<D, S>) -> Result<(), D::Error> {
        let redraw = dtx.has_render_update(Some(self.base.id));

        if redraw {
            // draw background
            if let Some(style) = self.styles.get(&self.style_visual()) {
                if self.corners == CornerRadii::default() {
                    self.bounding_box()
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                } else {
                    RoundedRectangle::new(self.bounding_box(), self.corners)
                        .into_styled(*style)
                        .draw(&mut dtx.display())?;
                }
            }

            if let Some(text_style) = self.text_styles.get(&self.text_visual()) {
                self.label.set_style(text_style.clone());
            }
        }

        {
            let clip_box = dtx.clip_box();
            let dtx = dtx.all();
            self.label.update_and_draw(
                self.top_left(),
                &mut DrawContext::from_draw_context(dtx, clip_box, redraw),
            )?;
        }

        if redraw && self.has_focus() {
            if self.corners == CornerRadii::default() {
                self.bounding_box()
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            } else {
                RoundedRectangle::new(self.bounding_box(), self.corners)
                    .into_styled(self.focus_style)
                    .draw(&mut dtx.display())?;
            }
        }

        Ok(())
    }

    fn layout(&mut self, ltx: LayoutContext) -> Size {
        if let Some(text_style) = self.text_styles.get(&self.text_visual()) {
            self.label.set_style(text_style.clone());
            let text_size = self.label.layout(LayoutContext::new(
                self.constraints
                    .constrain_size(self.padding.inner_size(ltx.available_size())),
                false,
                false,
            ));

            self.set_size(self.constraints.constrain_size(Size::new(
                get_size(
                    ltx.h_stretch(),
                    ltx.available_width(),
                    text_size.width,
                    self.padding.left + self.padding.right,
                ),
                get_size(
                    ltx.v_stretch(),
                    ltx.available_height(),
                    text_size.height,
                    self.padding.top + self.padding.bottom,
                ),
            )));

            self.label.set_top_left(Point::new(
                Align::Center.align(0, self.width(), text_size.width),
                Align::Center.align(0, self.height(), text_size.height),
            ));
        } else {
            self.set_size(Size::default());
        }

        self.size()
    }

    fn event(&mut self, event: &dyn Any, etx: &mut EventContext<S>) {
        let mut is_on_tap = false;
        let mut is_checked_changed = false;

        self.state.update(
            event,
            etx,
            self.base,
            &mut is_on_tap,
            &mut is_checked_changed,
        );

        if is_on_tap {
            if let Some(on_tap) = &self.on_tap {
                on_tap(etx.stx_mut().state_mut());
            }
        }

        if is_checked_changed {
            if let Some(on_is_checked_changed) = &self.on_is_checked_changed {
                on_is_checked_changed(etx.stx_mut().state_mut(), self.get_is_checked());
            }
        }
    }

    fn init(&mut self, origin: Option<&dyn Widget<S, D>>, itx: &mut InitContext<S>) {
        if let Some(origin) = origin {
            origin.transfer(self, itx);
        }

        self.init_id(itx);
    }

    fn transfer(&self, new: &mut dyn Any, _itx: &mut InitContext<S>) {
        if let Ok(new) = new.downcast_mut::<Button<S, D, R>>() {
            new.state = self.state;
            new.base.id = self.base.id;
            new.base.has_focus = self.base.has_focus;
        }
    }

    fn widget_string(&self) -> String {
        format!(
            "Button {{ text: \"{}\", is_checked {}, bounds: {:?} }}",
            self.label.get_text(),
            self.state.get_is_checked(),
            self.bounding_box()
        )
    }

    fn is_disabled(&self) -> bool {
        !self.get_is_enabled()
    }
}
