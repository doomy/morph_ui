use crate::{
    eg::{draw_target::DrawTarget, primitives::*},
    stdlib::boxed::Box,
    utils::*,
    widget::*,
    widgets::*,
};

/// `ItemBuilder` is used to define builder methods to construct a [`Item`].
///
/// There is a default implementation of this trait for `Box<Item<S, C, D, R>>`.
pub trait ItemBuilder {
    type Style;
    type State;
    type DrawTarget;

    /// Builder method that is used to set the child of item.
    #[must_use]
    fn child(self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to insert a new background style for the given visual.
    #[must_use]
    fn style(self, visual: ButtonVisual, style: Self::Style) -> Self;

    /// Builder method that is used to set the focus style.
    #[must_use]
    fn focus_style(self, style: Self::Style) -> Self;

    // Builder method that is used to set the padding of the button. It is the space around the text of the button.
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the constraints of the button.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;

    /// Builder method that is used to set start visual of the button.
    #[must_use]
    fn visual(self, visual: impl Into<ButtonVisual>) -> Self;

    /// Builder method that is used to set if the button is checkable or not.
    #[must_use]
    fn checkable(self, is_checkable: bool) -> Self;

    /// Builder method that is used to set the value that indicates if the button is currently checked.
    #[must_use]
    fn checked(self, is_checked: bool) -> Self;

    /// Builder method that is used to set the corner radius of the buttons background.
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the value that indicates if the button is enabled.
    #[must_use]
    fn enabled(self, is_enabled: bool) -> Self;

    /// Builder method that is used to set the callback that is called after the button is tapped.
    #[must_use]
    fn on_tap<F: Fn(&mut Self::State) + 'static>(self, on_tap: F) -> Self;

    /// Builder method that is used to set the callback that is called after `is_checked` of button is changed.
    #[must_use]
    fn on_is_checked_changed<F: Fn(&mut Self::State, bool) + 'static>(
        self,
        on_is_checked_changed: F,
    ) -> Self;

    /// Builder method that is used to set is_focusable of the button.
    #[must_use]
    fn focusable(self, is_focusable: bool) -> Self;
}

impl<S, D> ItemBuilder for Box<Item<S, D>>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    type Style = PrimitiveStyle<D::Color>;
    type State = S;
    type DrawTarget = D;

    fn child(mut self, child: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_child(child);
        self
    }

    fn style(mut self, visual: ButtonVisual, style: Self::Style) -> Self {
        self.insert_style(visual, style);
        self
    }

    fn focus_style(mut self, style: Self::Style) -> Self {
        self.set_focus_style(style);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }

    fn visual(mut self, visual: impl Into<ButtonVisual>) -> Self {
        self.set_visual(visual);
        self
    }

    fn checkable(mut self, is_checkable: bool) -> Self {
        self.set_is_checkable(is_checkable);
        self
    }

    fn checked(mut self, is_checked: bool) -> Self {
        self.set_is_checked(is_checked);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }

    fn enabled(mut self, is_enabled: bool) -> Self {
        self.set_is_enabled(is_enabled);
        self
    }

    fn on_tap<F: Fn(&mut Self::State) + 'static>(mut self, on_tap: F) -> Self {
        self.set_on_tap(on_tap);
        self
    }

    fn on_is_checked_changed<F: Fn(&mut Self::State, bool) + 'static>(
        mut self,
        on_is_checked_changed: F,
    ) -> Self {
        self.set_on_is_checked_changed(on_is_checked_changed);
        self
    }

    fn focusable(mut self, is_focusable: bool) -> Self {
        self.set_is_focusable(is_focusable);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{geometry::*, mock_display::MockDisplay, pixelcolor::*};

    type TestItem = Item<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_child() {
        let stretch = TestItem::new().child(TestItem::new());
        assert!(stretch.get_child().is_some());
    }

    #[test]
    fn builder_style() {
        assert_eq!(
            TestItem::new()
                .style(
                    ButtonVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_styles()
                .get(&ButtonVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_focus_style() {
        assert_eq!(
            TestItem::new()
                .focus_style(
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_focus_style(),
            PrimitiveStyleBuilder::new()
                .fill_color(Rgb565::BLACK)
                .build()
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestItem::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestItem::new()
                .constraints(Constraints::create().width(100).height(70))
                .get_constraints(),
            Constraints::create().width(100).height(70).build()
        );
    }

    #[test]
    fn builder_is_checkable() {
        assert!(!TestItem::new().get_is_checkable());
        assert!(TestItem::new().checkable(true).get_is_checkable());
    }

    #[test]
    fn builder_is_checked() {
        assert!(!TestItem::new().get_is_checked());
        assert!(TestItem::new().checked(true).get_is_checked());
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestItem::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_is_enabled() {
        assert!(TestItem::new().get_is_enabled());
        assert!(!TestItem::new().enabled(false).get_is_enabled());
    }
}
