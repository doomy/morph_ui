use crate::{eg::prelude::*, stdlib::boxed::Box, widgets::Frame, *};

/// `FrameBuilder` is used to define builder methods to construct a [`Frame`].
///
/// There is a default implementation of this trait for `Box<Navigation<S, D>>`.
pub trait FrameBuilder {
    type State;
    type DrawTarget;

    /// Builder method that is used to set the navigation widget of [`Frame`].
    #[must_use]
    fn navigation(self, navigation: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the content widget of [`Frame`].    
    #[must_use]
    fn content(self, content: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self;

    /// Builder method that is used to set the break point that is used to define the wrap between two column and single column layout.
    #[must_use]
    fn break_point(self, break_point: u32) -> Self;

    /// Builder method that is used to set the flag that indicates if the content is displayed if the [`Frame`] breaks to single column layout.
    #[must_use]
    fn show_content(self, show_content: bool) -> Self;

    /// Builder method that is used to set the constrained width of the `Frame` area on two column layout.
    #[must_use]
    fn navigation_width(self, navigation_width: u32) -> Self;

    /// Builder method that is used to set the callback that is called after a layout break.
    #[must_use]
    fn on_layout_break<F: Fn(&mut Self::State, bool) + 'static>(self, on_layout_break: F) -> Self;
}

impl<S, D> FrameBuilder for Box<Frame<S, D>>
where
    S: 'static,
    D: DrawTarget + 'static,
{
    type State = S;
    type DrawTarget = D;

    fn navigation(mut self, navigation: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_navigation(navigation);
        self
    }

    fn content(mut self, content: Box<dyn Widget<Self::State, Self::DrawTarget>>) -> Self {
        self.set_content(content);
        self
    }

    fn break_point(mut self, break_point: u32) -> Self {
        self.set_break_point(break_point);
        self
    }

    fn show_content(mut self, show_content: bool) -> Self {
        self.set_show_content(show_content);
        self
    }

    fn navigation_width(mut self, navigation_width: u32) -> Self {
        self.set_navigation_width(navigation_width);
        self
    }

    fn on_layout_break<F: Fn(&mut Self::State, bool) + 'static>(
        mut self,
        on_layout_break: F,
    ) -> Self {
        self.set_on_layout_break(on_layout_break);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, pixelcolor::Rgb565};

    type TestNavigation = Frame<u32, MockDisplay<Rgb565>>;

    #[test]
    fn builder_navigation() {
        assert!(TestNavigation::new()
            .navigation(TestNavigation::new())
            .get_navigation()
            .is_some());
    }

    #[test]
    fn builder_content() {
        assert!(TestNavigation::new()
            .content(TestNavigation::new())
            .get_content()
            .is_some());
    }

    #[test]
    fn builder_break_point() {
        assert_eq!(
            TestNavigation::new().break_point(5).get_break_point(),
            Some(5)
        );
    }

    #[test]
    fn builder_show_content() {
        assert!(!TestNavigation::new().get_show_content());
        assert!(TestNavigation::new().show_content(true).get_show_content());
    }

    #[test]
    fn navigation_width() {
        assert_eq!(
            TestNavigation::new()
                .navigation_width(5)
                .get_navigation_width(),
            5
        );
    }
}
