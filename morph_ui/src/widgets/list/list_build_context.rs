use crate::widget::states::SelectionState;

/// `ListBuildContext` is used to create single entries of `List`.
pub struct ListBuildContext<'a, T, S> {
    pub item: &'a T,
    pub index: usize,
    pub is_selected: bool,
    pub app: &'a S,
    pub state: &'a mut SelectionState,
}
