use crate::{
    eg::{prelude::*, text::renderer::TextRenderer},
    stdlib::boxed::Box,
    widgets::list::*,
};

/// `ListBuilder` is used to define builder methods to construct a [`List`].
///
/// There is a default implementation of this trait for `Box<List<S, C, D, R>>`.
pub trait ListBuilder {
    type TextStyle;
    type Style;
    type State;
    type Item;
    type DrawTarget: DrawTarget;

    /// Builder method that is used to set a new background style .
    #[must_use]
    fn style(self, style: Self::Style) -> Self;

    /// Builder method that is used to set the style of the scroll bars.
    #[must_use]
    fn scroll_bar_style(self, visual: ScrollBarVisual, style: Self::Style) -> Self;

    /// Builder method that is used to set the size of the scroll bars.
    #[must_use]
    fn scroll_bar_size(self, size: u32) -> Self;

    /// Builder method that is used to set the margin of the scroll bars.
    #[must_use]
    fn scroll_bar_margin(self, margin: u32) -> Self;

    /// Builder method that is used to set the corner radius of the scroll bars.
    #[must_use]
    fn scroll_bar_corners(self, corners: CornerRadii) -> Self;

    // Builder method that is used to set the padding of the List. It is the space around the text of the List.
    #[must_use]
    fn padding(self, padding: impl Into<Thickness>) -> Self;

    /// Builder method that is used to set the corner radius of the list background.
    #[must_use]
    fn corners(self, corners: CornerRadii) -> Self;

    /// Builder method that is used to set the box constraints of the list.
    #[must_use]
    fn constraints(self, constraints: impl Into<Constraints>) -> Self;

    /// Builder method that is used to enable or disable vertical scrolling.
    #[must_use]
    fn ver_scroll(self, is_ver_scroll: bool) -> Self;

    /// Builder method that is used to set the selection mode of the `List`.
    #[must_use]
    fn selection_mode(self, mode: impl Into<SelectionMode>) -> Self;

    /// Builder method that is used to set the items of the `List`.
    #[must_use]
    fn items(self, items: &[Self::Item]) -> Self;

    /// Builder method that is used to set the selected index of `List`.
    #[must_use]
    fn selected_index(self, index: impl Into<Option<usize>>) -> Self;

    /// Builder method that is used to set the function to build the visual representation of an item.
    #[must_use]
    fn builder<
        F: Fn(
                &ListBuildContext<'_, Self::Item, Self::State>,
            ) -> Box<Button<SelectionState, Self::DrawTarget, Self::TextStyle>>
            + 'static,
    >(
        self,
        builder: F,
    ) -> Self;

    /// Builder method that is used to set selection changed callback.
    #[must_use]
    fn on_selection_changed<F: Fn(&mut Self::State, Option<usize>) + 'static>(
        self,
        on_selection_changed: F,
    ) -> Self;
}

impl<T, S, D, R> ListBuilder for Box<List<T, S, D, R>>
where
    T: Clone + 'static,
    S: 'static,
    D: DrawTarget + 'static,
    R: TextRenderer<Color = D::Color> + CharacterStyle + 'static,
{
    type TextStyle = R;
    type Style = PrimitiveStyle<D::Color>;
    type State = S;
    type Item = T;
    type DrawTarget = D;

    fn style(mut self, style: Self::Style) -> Self {
        self.set_style(style);
        self
    }

    fn scroll_bar_style(mut self, visual: ScrollBarVisual, style: Self::Style) -> Self {
        self.insert_scroll_bar_style(visual, style);
        self
    }

    fn scroll_bar_size(mut self, size: u32) -> Self {
        self.set_scroll_bar_size(size);
        self
    }

    fn scroll_bar_margin(mut self, margin: u32) -> Self {
        self.set_scroll_bar_margin(margin);
        self
    }

    fn scroll_bar_corners(mut self, corners: CornerRadii) -> Self {
        self.set_scroll_bar_corners(corners);
        self
    }

    fn padding(mut self, padding: impl Into<Thickness>) -> Self {
        self.set_padding(padding);
        self
    }

    fn corners(mut self, corners: CornerRadii) -> Self {
        self.set_corners(corners);
        self
    }

    fn ver_scroll(mut self, is_ver_scroll: bool) -> Self {
        self.set_is_ver_scroll(is_ver_scroll);
        self
    }

    fn selection_mode(mut self, mode: impl Into<SelectionMode>) -> Self {
        self.set_selection_mode(mode);
        self
    }

    fn items(mut self, items: &[Self::Item]) -> Self {
        self.set_items(items);
        self
    }

    fn selected_index(mut self, index: impl Into<Option<usize>>) -> Self {
        self.set_selected_index(index.into());
        self
    }

    fn builder<
        F: Fn(
                &ListBuildContext<'_, Self::Item, Self::State>,
            ) -> Box<Button<SelectionState, Self::DrawTarget, Self::TextStyle>>
            + 'static,
    >(
        mut self,
        builder: F,
    ) -> Self {
        self.set_builder(builder);
        self
    }

    fn on_selection_changed<F: Fn(&mut Self::State, Option<usize>) + 'static>(
        mut self,
        on_selection_changed: F,
    ) -> Self {
        self.set_on_selection_changed(on_selection_changed);
        self
    }

    fn constraints(mut self, constraints: impl Into<Constraints>) -> Self {
        self.set_constraints(constraints);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eg::{mock_display::MockDisplay, mono_font::*, pixelcolor::Rgb565};

    type TestList = List<u32, u32, MockDisplay<Rgb565>, MonoTextStyle<'static, Rgb565>>;

    #[test]
    fn builder_scroll_bar_style() {
        assert_eq!(
            TestList::new()
                .scroll_bar_style(
                    ScrollBarVisual::Default,
                    PrimitiveStyleBuilder::new()
                        .fill_color(Rgb565::BLACK)
                        .build()
                )
                .get_scroll_bar_styles()
                .get(&ScrollBarVisual::Default),
            Some(
                &PrimitiveStyleBuilder::new()
                    .fill_color(Rgb565::BLACK)
                    .build()
            )
        );
    }

    #[test]
    fn builder_scroll_bar_size() {
        assert_eq!(
            TestList::new().scroll_bar_size(10).get_scroll_bar_size(),
            10
        );
    }

    #[test]
    fn builder_scroll_bar_margin() {
        assert_eq!(
            TestList::new()
                .scroll_bar_margin(10)
                .get_scroll_bar_margin(),
            10
        );
    }

    #[test]
    fn builder_scroll_bar_corners() {
        assert_eq!(
            TestList::new()
                .scroll_bar_corners(CornerRadii::new(Size::new(10, 0)))
                .get_scroll_bar_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_corners() {
        assert_eq!(
            TestList::new()
                .corners(CornerRadii::new(Size::new(10, 0)))
                .get_corners(),
            CornerRadii::new(Size::new(10, 0))
        );
    }

    #[test]
    fn builder_padding() {
        assert_eq!(
            TestList::new()
                .padding(Thickness::new(10, 9, 8, 7))
                .get_padding(),
            Thickness::new(10, 9, 8, 7)
        );
    }

    #[test]
    fn builder_items() {
        assert_eq!(TestList::new().items(&[1, 2, 3]).get_items(), &[1, 2, 3]);
    }

    #[test]
    fn builder_selected_index() {
        assert_eq!(
            TestList::new().selected_index(2).get_selected_index(),
            Some(2)
        );
    }

    #[test]
    fn builder_selection_mode() {
        assert_eq!(
            TestList::new()
                .selection_mode(SelectionMode::Single)
                .get_selection_mode(),
            SelectionMode::Single
        );
    }

    #[test]
    fn builder_constraints() {
        assert_eq!(
            TestList::new()
                .constraints(Constraints::create().width(100).height(70))
                .get_constraints(),
            Constraints::create().width(100).height(70).build()
        );
    }
}
