mod key;
mod scan_codes;

pub use key::*;
pub use scan_codes::*;

#[derive(Debug, PartialEq)]
pub struct KeyEvent {
    scan_code: u8,
    key: Key,
    pressed: bool,
}

impl KeyEvent {
    pub fn new(scan_code: u8, pressed: bool) -> Self {
        Self {
            scan_code,
            key: Key::from(scan_code),
            pressed,
        }
    }

    pub fn scan_code(&self) -> u8 {
        self.scan_code
    }

    pub fn key(&self) -> Key {
        self.key
    }

    pub fn pressed(&self) -> bool {
        self.pressed
    }
}
