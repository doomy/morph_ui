use crate::stdlib::{boxed::Box, collections::VecDeque};

use downcast::Any;

/// Represents a queue of events.
#[derive(Default)]
pub struct EventQueue {
    events: VecDeque<Box<dyn Any>>,
}

impl EventQueue {
    /// Creates a new event queue.
    pub fn new() -> Self {
        EventQueue::default()
    }

    /// Enqueues a new event.
    pub fn enqueue(&mut self, event: impl Any) {
        self.events.push_back(Box::new(event));
    }

    /// Dequeues an event.
    ///
    /// Returns `None` if the queue is empty.
    pub fn dequeue(&mut self) -> Option<Box<dyn Any>> {
        self.events.pop_front()
    }

    /// Returns the number of elements in the `EventQueue`.
    pub fn len(&self) -> usize {
        self.events.len()
    }

    /// Returns `true` if the `EventQueue` is empty.
    pub fn is_empty(&self) -> bool {
        self.events.is_empty()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use crate::stdlib::string::String;

    #[derive(Debug, PartialEq)]
    struct TestEvent {
        text: String,
    }

    #[test]
    fn event_queue() {
        let mut event_queue = EventQueue::new();

        assert!(event_queue.is_empty());
        assert_eq!(event_queue.len(), 0);

        event_queue.enqueue(TestEvent {
            text: String::from("Entry 1"),
        });

        assert!(!event_queue.is_empty());
        assert_eq!(event_queue.len(), 1);

        event_queue.enqueue(TestEvent {
            text: String::from("Entry 2"),
        });

        assert!(!event_queue.is_empty());
        assert_eq!(event_queue.len(), 2);

        assert_eq!(
            event_queue
                .dequeue()
                .unwrap()
                .downcast_ref::<TestEvent>()
                .unwrap(),
            &TestEvent {
                text: String::from("Entry 1"),
            }
        );

        assert_eq!(
            event_queue
                .dequeue()
                .unwrap()
                .downcast_ref::<TestEvent>()
                .unwrap(),
            &TestEvent {
                text: String::from("Entry 2"),
            }
        );

        assert!(event_queue.is_empty());
        assert_eq!(event_queue.len(), 0);
    }
}
