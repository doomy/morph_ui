/// Scan code A key
pub const KEY_A: u8 = 0x1E;

/// Scan code B key
pub const KEY_B: u8 = 0x30;

/// Scan code C key
pub const KEY_C: u8 = 0x2E;

/// Scan code D key
pub const KEY_D: u8 = 0x20;

/// Scan code E key
pub const KEY_E: u8 = 0x12;

/// Scan code F key
pub const KEY_F: u8 = 0x21;

/// Scan code G key
pub const KEY_G: u8 = 0x22;

/// Scan code H key
pub const KEY_H: u8 = 0x23;

/// Scan code I key
pub const KEY_I: u8 = 0x17;

/// Scan code J key
pub const KEY_J: u8 = 0x24;

/// Scan code K key
pub const KEY_K: u8 = 0x25;

/// Scan code L key
pub const KEY_L: u8 = 0x26;

/// Scan code M key
pub const KEY_M: u8 = 0x32;

/// Scan code N key
pub const KEY_N: u8 = 0x31;

/// Scan code O key
pub const KEY_O: u8 = 0x18;

/// Scan code P key
pub const KEY_P: u8 = 0x19;

/// Scan code Q key
pub const KEY_Q: u8 = 0x10;

/// Scan code R key
pub const KEY_R: u8 = 0x13;

/// Scan code S key
pub const KEY_S: u8 = 0x1F;

/// Scan code T key
pub const KEY_T: u8 = 0x14;

/// Scan code U key
pub const KEY_U: u8 = 0x16;

/// Scan code V key
pub const KEY_V: u8 = 0x2F;

/// Scan code W key
pub const KEY_W: u8 = 0x11;

/// Scan code X key
pub const KEY_X: u8 = 0x2D;

/// Scan code Y key
pub const KEY_Y: u8 = 0x15;

/// Scan code Z key
pub const KEY_Z: u8 = 0x2C;

/// Scan code 0 key
pub const KEY_0: u8 = 0x0B;

/// Scan code 1 key
pub const KEY_1: u8 = 0x02;

/// Scan code 2 key
pub const KEY_2: u8 = 0x03;

/// Scan code 3 key
pub const KEY_3: u8 = 0x04;

/// Scan code 4 key
pub const KEY_4: u8 = 0x05;

/// Scan code 5 key
pub const KEY_5: u8 = 0x06;

/// Scan code 6 key
pub const KEY_6: u8 = 0x07;

/// Scan code 7 key
pub const KEY_7: u8 = 0x08;

/// Scan code 8 key
pub const KEY_8: u8 = 0x09;

/// Scan code 9 key
pub const KEY_9: u8 = 0x0A;

// Numpad keys (codes 0x70-0x79)

/// Scan code Num 0 key
pub const KEY_NUM_0: u8 = 0x70;

/// Scan code Num 1 key
pub const KEY_NUM_1: u8 = 0x71;

/// Scan code Num 2 key
pub const KEY_NUM_2: u8 = 0x72;

/// Scan code Num 3 key
pub const KEY_NUM_3: u8 = 0x73;

/// Scan code Num 4 key
pub const KEY_NUM_4: u8 = 0x74;

/// Scan code Num 5 key
pub const KEY_NUM_5: u8 = 0x75;

/// Scan code Num 6 key
pub const KEY_NUM_6: u8 = 0x76;

/// Scan code Num 7 key
pub const KEY_NUM_7: u8 = 0x77;

/// Scan code Num 8 key
pub const KEY_NUM_8: u8 = 0x78;

/// Scan code Num 9 key
pub const KEY_NUM_9: u8 = 0x79;

// Additional keys

/// Scan code Tick/tilde key
pub const KEY_TICK: u8 = 0x29;

/// Scan code Minus/underline key
pub const KEY_MINUS: u8 = 0x0C;

/// Scan code Equals/plus key
pub const KEY_EQUALS: u8 = 0x0D;

/// Scan code Backslash/pipe key
pub const KEY_BACKSLASH: u8 = 0x2B;

/// Scan code Bracket open key
pub const KEY_BRACE_OPEN: u8 = 0x1A;

/// Scan code Bracket close key
pub const KEY_BRACE_CLOSE: u8 = 0x1B;

/// Scan code Semicolon key
pub const KEY_SEMICOLON: u8 = 0x27;

/// Scan code Quote key
pub const KEY_QUOTE: u8 = 0x28;

/// Scan code Comma key
pub const KEY_COMMA: u8 = 0x33;

/// Scan code Period key
pub const KEY_PERIOD: u8 = 0x34;

/// Scan code Slash key
pub const KEY_SLASH: u8 = 0x35;

/// Scan code Backspace key
pub const KEY_BACKSPACE: u8 = 0x0E;

/// Scan code Space key
pub const KEY_SPACE: u8 = 0x39;

/// Scan code Tab key
pub const KEY_TAB: u8 = 0x0F;

/// Scan code Capslock
pub const KEY_CAPS: u8 = 0x3A;

/// Scan code Left shift
pub const KEY_SHIFT_LEFT: u8 = 0x2A;

/// Scan code Right shift
pub const KEY_SHIFT_RIGHT: u8 = 0x36;

/// Scan code Control key
pub const KEY_CONTROL_LEFT: u8 = 0x1D;

/// Scan code Alt key
pub const KEY_ALT_RIGHT: u8 = 0x38;

/// Scan code AltGr key
pub const KEY_ALT_LEFT: u8 = 0x64;

/// Scan code Enter key
pub const KEY_ENTER: u8 = 0x1C;

/// Scan code Escape key
pub const KEY_ESCAPE: u8 = 0x01;

/// Scan code F1 key
pub const KEY_F1: u8 = 0x3B;

/// Scan code F2 key
pub const KEY_F2: u8 = 0x3C;

/// Scan code F3 key
pub const KEY_F3: u8 = 0x3D;

/// Scan code F4 key
pub const KEY_F4: u8 = 0x3E;

/// Scan code F5 key
pub const KEY_F5: u8 = 0x3F;

/// Scan code F6 key
pub const KEY_F6: u8 = 0x40;

/// Scan code F7 key
pub const KEY_F7: u8 = 0x41;

/// Scan code F8 key
pub const KEY_F8: u8 = 0x42;

/// Scan code F9 key
pub const KEY_F9: u8 = 0x43;

/// Scan code F10 key
pub const KEY_F10: u8 = 0x44;

/// Scan code F11 key
pub const KEY_F11: u8 = 0x57;

/// Scan code F12 key
pub const KEY_F12: u8 = 0x58;

/// Scan code Home key
pub const KEY_HOME: u8 = 0x47;

/// Scan code Up key
pub const KEY_ARROW_UP: u8 = 0x48;

/// Scan code Left key
pub const KEY_ARROW_LEFT: u8 = 0x4B;

/// Scan code Right key
pub const KEY_ARROW_RIGHT: u8 = 0x4D;

/// Scan code Down key
pub const KEY_ARROW_DOWN: u8 = 0x50;

/// Scan code Page up key
pub const KEY_PAGE_UP: u8 = 0x49;

/// Scan code Page down key
pub const KEY_PAGE_DOWN: u8 = 0x51;

/// Scan code End key
pub const KEY_END: u8 = 0x4F;

/// Scan code Delete key
pub const KEY_DELETE: u8 = 0x53;
