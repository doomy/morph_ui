use crate::eg::geometry::Point;

/// Represents a scroll event (vertical | horizontal).
#[derive(Debug)]
pub struct ScrollEvent {
    offset: Point,
}

impl ScrollEvent {
    /// Creates a new scroll event from offset.
    pub fn new(offset: impl Into<Point>) -> Self {
        ScrollEvent {
            offset: offset.into(),
        }
    }

    /// Gets the scroll offset
    /// x => horizontal
    /// y => vertical
    pub fn offset(&self) -> Point {
        self.offset
    }
}
