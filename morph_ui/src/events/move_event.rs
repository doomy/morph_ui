use crate::eg::geometry::Point;

/// Represents a mouse move event with position.
#[derive(Debug)]
pub struct MoveEvent {
    position: Point,
}

impl MoveEvent {
    /// Creates a new move event from point.
    pub fn new(position: impl Into<Point>) -> Self {
        MoveEvent {
            position: position.into(),
        }
    }

    /// Gets the coordinates from the move event.
    pub fn position(&self) -> Point {
        self.position
    }
}
