use crate::{stdlib::string::String, utils::Cursor};

/// Defines an onscreen keyboard related event that is used to send changed text to `TextField`.
#[derive(Debug)]
pub struct KeyboardEvent {
    text: String,
    cursor: Cursor,
    id: u32,
}

impl KeyboardEvent {
    /// Creates a new drop event.
    pub fn new(text: String, cursor: Cursor, id: u32) -> Self {
        KeyboardEvent { text, cursor, id }
    }

    /// Returns the new text.
    pub fn text(&self) -> &str {
        self.text.as_str()
    }

    /// Returns new text cursor position.
    pub fn cursor(&self) -> Cursor {
        self.cursor
    }

    /// Returns id of the target widget.
    pub fn id(&self) -> u32 {
        self.id
    }
}
