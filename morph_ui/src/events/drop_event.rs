use crate::{eg::geometry::Point, stdlib::string::String};

/// Defines the kind of drop event.
#[derive(Copy, Clone, Debug)]
pub enum DropKind {
    File,
    Text,
}

/// Defines a drop event (file | text).
#[derive(Debug)]
pub struct DropEvent {
    content: String,
    kind: DropKind,
    position: Point,
}

impl DropEvent {
    /// Creates a new drop event.
    pub fn new(content: String, kind: DropKind, position: Point) -> Self {
        DropEvent {
            content,
            kind,
            position,
        }
    }

    /// Returns a reference of the content of the drop event file or text.
    pub fn content(&self) -> &String {
        &self.content
    }

    /// Returns the kind of drop event.
    pub fn kind(&self) -> DropKind {
        self.kind
    }

    /// Returns the position of the event.
    pub fn position(&self) -> Point {
        self.position
    }
}
