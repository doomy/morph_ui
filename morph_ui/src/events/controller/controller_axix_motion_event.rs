use crate::events::Axis;

/// Defines a controller axis motion event.
#[derive(Debug)]
pub struct ControllerAxisMotionEvent {
    axis: Axis,
    device: u32,
    value: i32,
}

impl ControllerAxisMotionEvent {
    /// Creates a new controller button event.
    pub fn new(axis: Axis, value: i32, device: u32) -> Self {
        ControllerAxisMotionEvent {
            axis,
            value,
            device,
        }
    }

    /// Returns the controller axis of the event.
    pub fn axis(&self) -> Axis {
        self.axis
    }

    /// Returns the axis motion value of the event.
    pub fn value(&self) -> i32 {
        self.value
    }

    /// Returns the device id of the controller.
    pub fn device(&self) -> u32 {
        self.device
    }
}
