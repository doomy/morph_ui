use crate::events::ControllerButton;

/// Defines a controller button event.
#[derive(Debug)]
pub struct ControllerButtonEvent {
    button: ControllerButton,
    pressed: bool,
    device: u32,
}

impl ControllerButtonEvent {
    /// Creates a new controller button event.
    pub fn new(button: ControllerButton, pressed: bool, device: u32) -> Self {
        ControllerButtonEvent {
            button,
            pressed,
            device,
        }
    }

    /// Returns the controller button of the event.
    pub fn button(&self) -> ControllerButton {
        self.button
    }

    /// Returns `true` if the button of the event is pressed otherwise `false`.
    pub fn pressed(&self) -> bool {
        self.pressed
    }

    /// Returns the device id of the controller.
    pub fn device(&self) -> u32 {
        self.device
    }
}
