use crate::stdlib::string::*;

/// Defines a button of a controller.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ControllerButton {
    A,
    B,
    X,
    Y,
    Back,
    Guide,
    Start,
    LeftStick,
    RightStick,
    LeftShoulder,
    RightShoulder,
    DPadUp,
    DPadDown,
    DPadLeft,
    DPadRight,
}

impl From<&str> for ControllerButton {
    fn from(s: &str) -> Self {
        match s {
            "B" => ControllerButton::B,
            "X" => ControllerButton::X,
            "Y" => ControllerButton::Y,
            "Back" => ControllerButton::Back,
            "Guide" => ControllerButton::Guide,
            "Start" => ControllerButton::Start,
            "LeftStick" => ControllerButton::LeftStick,
            "RightStick" => ControllerButton::RightStick,
            "LeftShoulder" => ControllerButton::LeftShoulder,
            "RightShoulder" => ControllerButton::RightShoulder,
            "DPadUp" => ControllerButton::DPadUp,
            "DPadDown" => ControllerButton::DPadDown,
            "DPadLeft" => ControllerButton::DPadLeft,
            "DPadRight" => ControllerButton::DPadRight,
            _ => ControllerButton::A,
        }
    }
}

impl From<String> for ControllerButton {
    fn from(s: String) -> Self {
        s.as_str().into()
    }
}

impl From<u8> for ControllerButton {
    fn from(u: u8) -> Self {
        match u {
            0 => ControllerButton::A,
            1 => ControllerButton::B,
            2 => ControllerButton::X,
            3 => ControllerButton::Y,
            4 => ControllerButton::Back,
            5 => ControllerButton::Guide,
            6 => ControllerButton::Start,
            7 => ControllerButton::LeftStick,
            8 => ControllerButton::RightStick,
            9 => ControllerButton::LeftShoulder,
            10 => ControllerButton::RightShoulder,
            11 => ControllerButton::DPadUp,
            12 => ControllerButton::DPadDown,
            13 => ControllerButton::DPadLeft,
            14 => ControllerButton::DPadRight,
            _ => ControllerButton::A,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_controller_button_from_str() {
        assert_eq!(ControllerButton::from("A"), ControllerButton::A);
        assert_eq!(ControllerButton::from("B"), ControllerButton::B);
        assert_eq!(ControllerButton::from("X"), ControllerButton::X);
        assert_eq!(ControllerButton::from("Y"), ControllerButton::Y);
        assert_eq!(ControllerButton::from("Back"), ControllerButton::Back);
        assert_eq!(ControllerButton::from("Guide"), ControllerButton::Guide);
        assert_eq!(ControllerButton::from("Start"), ControllerButton::Start);
        assert_eq!(
            ControllerButton::from("LeftStick"),
            ControllerButton::LeftStick
        );
        assert_eq!(
            ControllerButton::from("RightStick"),
            ControllerButton::RightStick
        );
        assert_eq!(
            ControllerButton::from("LeftShoulder"),
            ControllerButton::LeftShoulder
        );
        assert_eq!(
            ControllerButton::from("RightShoulder"),
            ControllerButton::RightShoulder
        );
        assert_eq!(ControllerButton::from("DPadUp"), ControllerButton::DPadUp);
        assert_eq!(
            ControllerButton::from("DPadDown"),
            ControllerButton::DPadDown
        );
        assert_eq!(
            ControllerButton::from("DPadLeft"),
            ControllerButton::DPadLeft
        );
        assert_eq!(
            ControllerButton::from("DPadRight"),
            ControllerButton::DPadRight
        );
    }
}
