use crate::stdlib::string::*;

/// Defines the axis of a controller.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Axis {
    LeftX,
    LeftY,
    RightX,
    RightY,
    TriggerLeft,
    TriggerRight,
}

impl From<&str> for Axis {
    fn from(s: &str) -> Self {
        match s {
            "LeftY" => Axis::LeftY,
            "RightX" => Axis::RightX,
            "RightY" => Axis::RightY,
            "TriggerLeft" => Axis::TriggerLeft,
            "TriggerRight" => Axis::TriggerRight,
            _ => Axis::LeftX,
        }
    }
}

impl From<String> for Axis {
    fn from(s: String) -> Self {
        s.as_str().into()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_controller_button_from_str() {
        assert_eq!(Axis::from("LeftX"), Axis::LeftX);
        assert_eq!(Axis::from("LeftY"), Axis::LeftY);
        assert_eq!(Axis::from("RightX"), Axis::RightX);
        assert_eq!(Axis::from("RightY"), Axis::RightY);
        assert_eq!(Axis::from("TriggerLeft"), Axis::TriggerLeft);
        assert_eq!(Axis::from("TriggerRight"), Axis::TriggerRight);
    }
}
