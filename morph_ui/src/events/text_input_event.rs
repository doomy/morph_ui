/// Represents a mouse move event with position.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct TextInputEvent {
    input: char,
}

impl TextInputEvent {
    /// Creates a new move event from point.
    pub fn new(input: char) -> Self {
        Self { input }
    }

    /// Gets the coordinates from the move event.
    pub fn input(&self) -> char {
        self.input
    }
}
