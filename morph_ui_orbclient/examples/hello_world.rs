use morph_ui::{
    eg::pixelcolor::{Rgb888, RgbColor},
    themes::{rgb888::*, *},
    *,
};

use morph_ui_orbclient::*;

#[derive(Debug, Default, PartialEq, Clone)]
struct AppState {
    text: String,
    theme: MorphTheme,
}

impl State for AppState {}

impl<D> ThemeHelper<D, TextStyle, MorphTheme> for AppState
where
    D: DrawTarget<Color = Rgb888> + 'static,
{
    fn theme(&self) -> &MorphTheme {
        &self.theme
    }
}

pub fn main() -> Result<(), Error> {
    let width = 100;
    let height = 100;

    App::new()
        .window(
            Window::create(AppState {
                text: "Hello World".to_string(),
                ..Default::default()
            })
            .title("hello world")
            .size(width, height)
            .centered(true)
            .resizeable(true)
            .background_color(Rgb888::BLACK)
            .ui(|app| app.label().text(app.text.as_str()))?,
        )?
        .start()?;

    Ok(())
}
