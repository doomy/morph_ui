# morph_ui OrbClient examples

## hello_world

A simple "hello world" program written with morph_ui

`Run`

```shell
cargo run --example hello_world
```

## counter

A simple counter example. A `Label` with an increment and a decrement `Button`.

`Run`

```shell
cargo run --example counter
```

## calculator

A simple calculator written with morph_ui. It is used to show how to extract `Widget` creation in functions and represents a complexer use of an app state.

`Run`

```shell
cargo run --example calculator
```

## keyboard

This example shows how to use the morph_ui on screen `Keyboard`.

`Run`

```shell
cargo run --example keyboard
```