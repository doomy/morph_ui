pub use morph_ui::{
    context::Debug,
    eg::{
        geometry::Point,
        pixelcolor::{WebColors, *},
    },
    shell::*,
    themes::{rgb888::*, *},
    widgets::*,
    *,
};

use morph_ui_orbclient::*;

#[derive(Debug, Default, PartialEq, Clone)]
struct AppState {
    text: String,
    theme: MorphTheme,
}

impl State for AppState {}

impl<D> ThemeHelper<D, TextStyle, MorphTheme> for AppState
where
    D: DrawTarget<Color = Rgb888> + 'static,
{
    fn theme(&self) -> &MorphTheme {
        &self.theme
    }
}

pub fn main() -> Result<(), Error> {
    let width = 360;
    let height = 300;

    App::new()
        .window(
            Window::create(AppState {
                text: "Keyboard".to_string(),
                ..Default::default()
            })
            .title("hello world")
            .size(width, height)
            .centered(true)
            .resizeable(true)
            .background_color(Rgb888::BLACK)
            .keyboard(|app| app.keyboard(en_us()))
            .ui(|app| {
                app.background().padding(8).child(
                    app.column()
                        .spacing(4)
                        .child(app.text_field())
                        .child(app.text_field().password_box(true)),
                )
            })?,
        )?
        .start()?;

    Ok(())
}
