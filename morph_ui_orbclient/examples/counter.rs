use morph_ui::{
    eg::pixelcolor::Rgb888,
    themes::{rgb888::*, *},
    *,
};

use morph_ui_orbclient::*;

#[derive(Debug, Default, PartialEq, Clone)]
struct AppState {
    count: i32,
    theme: MorphTheme,
}

impl State for AppState {}

impl<D> ThemeHelper<D, TextStyle, MorphTheme> for AppState
where
    D: DrawTarget<Color = Rgb888> + 'static,
{
    fn theme(&self) -> &MorphTheme {
        &self.theme
    }
}

pub fn main() -> Result<(), Error> {
    let width = 160;
    let height = 128;

    App::new()
        .window(
            Window::create(AppState::default())
                .title("counter")
                .size(width, height)
                .centered(true)
                .resizeable(true)
                .ui(|app| {
                    app.background().padding(4).child(
                        app.column()
                            .spacing(4)
                            .child(
                                app.primary_button()
                                    .text("+")
                                    .on_tap(|app: &mut AppState| app.count += 1),
                            )
                            .align(
                                app.label().text(format!("{}", app.count).as_str()),
                                "center",
                            )
                            .child(
                                app.primary_button()
                                    .text("-")
                                    .on_tap(|app: &mut AppState| app.count -= 1),
                            ),
                    )
                })?,
        )?
        .start()?;

    Ok(())
}
