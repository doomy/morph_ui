use orbclient::Renderer;

use morph_ui::{
    eg::{pixelcolor::*, text::renderer::*},
    shell::*,
    *,
};
use tiny_skia_display::*;

use crate::Error;

/// Defines a top-level window on the screen.
pub struct Window<S: State + PartialEq + Clone + 'static, C>
where
    C: PixelColor + From<<C as PixelColor>::Raw> + Into<Rgb888>,
{
    inner: orbclient::Window,
    shell: Option<Shell<S, TinySkiaDisplay<C>>>,
    has_clipboard_update: bool,
}

impl<S: State + PartialEq + Clone + 'static, C> Window<S, C>
where
    C: PixelColor + From<<C as PixelColor>::Raw> + Into<Rgb888> + 'static,
{
    pub fn create(state: S) -> WindowBuilder<S, C> {
        WindowBuilder::new(state)
    }

    #[cfg(not(target_os = "redox"))]
    fn has_clipboard_update(&self) -> bool {
        self.has_clipboard_update
    }

    // todo: workaround until clipboard update events available on orbital
    #[cfg(target_os = "redox")]
    fn has_clipboard_update(&self) -> bool {
        true
    }

    fn sync_clipboard(&mut self) {
        if self.has_clipboard_update() {
            self.has_clipboard_update = false;
            let content = self.inner.clipboard();
            if let Some(shell) = &mut self.shell {
                if shell.ctx_mut().content != content {
                    shell.ctx_mut().content = content;
                }
            }
        }

        if let Some(shell) = &mut self.shell {
            if shell.ctx_mut().internal_update && shell.ctx_mut().content != self.inner.clipboard()
            {
                self.inner.set_clipboard(shell.ctx_mut().content.as_str());
                shell.ctx_mut().internal_update = false;
            }
        }
    }

    /// Drain events and propagate the events to the shell.
    ///
    /// If it return `false` the window should be closed.
    pub fn drain_events(&mut self) -> bool {
        for event in self.inner.events() {
            if let Some(shell) = &mut self.shell {
                match event.to_option() {
                    orbclient::EventOption::Quit(_) => {
                        return false;
                    }
                    orbclient::EventOption::Key(e) => {
                        shell.key(e.scancode, e.pressed);
                    }
                    orbclient::EventOption::TextInput(e) => {
                        shell.text_input(e.character);
                    }
                    orbclient::EventOption::Mouse(e) => shell.mouse(e.x, e.y),
                    orbclient::EventOption::MouseRelative(_) => println!("no yet implemented"),
                    orbclient::EventOption::Button(e) => {
                        shell.mouse_button(e.left, e.middle, e.right);
                    }
                    orbclient::EventOption::Scroll(e) => shell.scroll(e.x as i32, e.y as i32),
                    // orbclient::EventOption::Focus(e) => self.shell.active(e.focused),
                    // orbclient::EventOption::Move(e) => self.shell.moved(e.x as f64, e.y as f64),
                    orbclient::EventOption::Resize(_) => {
                        shell.set_display(
                            TinySkiaDisplay::new(self.inner.width(), self.inner.height()).unwrap(),
                        );
                    }
                    orbclient::EventOption::Screen(_) => println!("no yet implemented"),
                    orbclient::EventOption::ClipboardUpdate(_) => self.has_clipboard_update = true,
                    orbclient::EventOption::Drop(e) => {
                        if let Some(text) = self.inner.pop_drop_content() {
                            if e.kind == orbclient::DROP_FILE {
                                shell.drop_file(text);
                            } else {
                                shell.drop_text(text);
                            }
                        }
                    }
                    orbclient::EventOption::Hover(_) => println!("no yet implemented"),
                    orbclient::EventOption::Unknown(_) => println!("no yet implemented"),
                    orbclient::EventOption::None => println!("no yet implemented"),
                    orbclient::EventOption::ControllerButton(e) => {
                        shell.controller_button(e.button.into_u8(), e.pressed, e.device)
                    }
                    _ => {}
                }
            }
        }

        true
    }

    /// Runs the inner logic of the window.
    pub fn run(&mut self) -> Result<bool, Error> {
        self.sync_clipboard();
        if !self.drain_events() {
            return Ok(false);
        }

        if let Some(shell) = &mut self.shell {
            shell.run().map_err(|_| Error::ShellRunError)?;

            if let Some(display) = shell.display_mut() {
                let len = self.inner.data().len() * std::mem::size_of::<orbclient::Color>();
                let color_data = unsafe {
                    std::slice::from_raw_parts_mut(
                        self.inner.data_mut().as_mut_ptr() as *mut u8,
                        len,
                    )
                };

                if color_data.len() == display.data().len() {
                    display.flip(color_data);
                }
            }
            self.inner.sync();
        }

        self.sync_clipboard();

        Ok(true)
    }
}

/// Window builder is used to configure a window and create it.
pub struct WindowBuilder<S: State + Clone + PartialEq + 'static, C>
where
    C: PixelColor + From<<C as PixelColor>::Raw> + Into<Rgb888>,
{
    position: (i32, i32),
    size: (u32, u32),
    title: String,
    resizeable: bool,
    borderless: bool,
    centered: bool,
    shell: Option<Shell<S, TinySkiaDisplay<C>>>,
    state: S,
    background_color: Option<C>,
    dbx: DebugContext<C>,
}

impl<S: State + Clone + PartialEq + 'static, C> WindowBuilder<S, C>
where
    C: PixelColor + From<<C as PixelColor>::Raw> + Into<Rgb888> + 'static,
{
    /// Creates a new window builder.
    fn new(state: S) -> Self {
        WindowBuilder {
            position: (0, 0),
            size: (0, 0),
            title: String::default(),
            resizeable: false,
            borderless: false,
            centered: false,
            shell: None,
            state,
            background_color: None,
            dbx: DebugContext::default(),
        }
    }

    /// Builder method that is used to specify the window position on the screen with x and y.
    ///
    /// If centered is set to `true`, position will be ignored.
    #[must_use]
    pub fn position(mut self, x: i32, y: i32) -> Self {
        self.position = (x, y);
        self
    }

    /// Builder method that is used to specify the window size with width and height.
    #[must_use]
    pub fn size(mut self, width: u32, height: u32) -> Self {
        self.size = (width, height);
        self
    }

    /// Builder method that is used to specify the window title.
    #[must_use]
    pub fn title(mut self, title: impl Into<String>) -> Self {
        self.title = title.into();
        self
    }

    /// Builder method that is used to specify the window resizsablility.
    ///
    /// If resizeable is set to `true` the window can resized otherwise the size of the window
    /// will be fixed.
    #[must_use]
    pub fn resizeable(mut self, resizeable: bool) -> Self {
        self.resizeable = resizeable;
        self
    }

    /// Builder method that is used to specify the window borderless.
    ///
    /// If borderless is set to `true` the window will be displayed without window borders, otherwise
    /// with borders.
    #[must_use]
    pub fn borderless(mut self, borderless: bool) -> Self {
        self.borderless = borderless;
        self
    }

    /// Builder method that is used to place the window in the center of the screen.
    ///
    /// If set to `true` the window will be centered on the screen an position will be ignored.
    #[must_use]
    pub fn centered(mut self, centered: bool) -> Self {
        self.centered = centered;
        self
    }

    /// Builder method to define a widget as root of the shells ui.
    ///
    /// An ui can only be set once. If the method is multiple called, the last set ui will be used.
    pub fn ui<F: Fn(&mut S) -> Box<dyn Widget<S, TinySkiaDisplay<C>>> + 'static>(
        mut self,
        ui: F,
    ) -> Result<Self, Error> {
        if self.shell.is_none() {
            self.shell = Some(Shell::new(self.state.clone()));
        }

        self.shell = Some(self.shell.unwrap().ui(ui));

        Ok(self)
    }

    /// Builder method that is used to set the on screen keyboard.
    ///
    /// If a keyboard is set each `TextField` that gets focus will open the keyboard.
    #[must_use]
    pub fn keyboard<F, R>(mut self, keyboard_builder: F) -> Self
    where
        R: TextRenderer<Color = C> + CharacterStyle + 'static,
        F: Fn(&mut S) -> Box<Keyboard<S, TinySkiaDisplay<C>, R>>,
    {
        if self.shell.is_none() {
            self.shell = Some(Shell::new(self.state.clone()));
        }

        self.shell = Some(self.shell.unwrap().keyboard(keyboard_builder));
        self
    }

    /// Used to configure debug output. Only available on debug builds.
    #[must_use]
    pub fn debug_flags(mut self, flags: &[Debug], raster_color: Option<C>) -> Self {
        self.dbx = DebugContext::new(flags, raster_color);

        self
    }

    /// Builder method used to set the background color of the shell.
    #[must_use]
    pub fn background_color(mut self, background_color: C) -> Self {
        self.background_color = Some(background_color);
        self
    }

    /// Creates a new window with the given builder settings.
    pub fn build(mut self) -> Result<Window<S, C>, Error> {
        let mut flags = Vec::new();

        if self.resizeable {
            flags.push(orbclient::WindowFlag::Resizable);
        }

        if self.borderless {
            flags.push(orbclient::WindowFlag::Front);
        }

        // used to center the window on the screen if centered is set to true
        let (x, y) = {
            if self.centered {
                let screen_size =
                    orbclient::get_display_size().map_err(|_| Error::CannotReadScreenSize)?;

                (
                    (screen_size.0 as i32 - self.size.0 as i32) / 2,
                    (screen_size.1 as i32 - self.size.1 as i32) / 2,
                )
            } else {
                (self.position.0, self.position.1)
            }
        };

        // create the window
        if let Some(inner) = orbclient::Window::new_flags(
            x,
            y,
            self.size.0,
            self.size.1,
            self.title.as_str(),
            &flags,
        ) {
            if let Some(shell) = &mut self.shell {
                shell.set_debug_context(self.dbx);
                shell.set_display(
                    TinySkiaDisplay::new(self.size.0, self.size.1)
                        .map_err(|_| Error::CannotCreateTinySkiaDisplay)?,
                );

                if let Some(background_color) = self.background_color {
                    shell.set_background_color(background_color);
                }
            }

            return Ok(Window {
                inner,
                shell: self.shell,
                has_clipboard_update: true,
            });
        }

        Err(Error::CannotCreateWindow)
    }
}

pub fn get_display_size() -> Result<(u32, u32), String> {
    orbclient::get_display_size()
}
