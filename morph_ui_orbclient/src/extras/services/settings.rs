#[cfg(not(target_arch = "wasm32"))]
use async_std::{
    fs::{create_dir_all, File},
    io::{self, ReadExt},
    prelude::*,
};

#[cfg(target_arch = "wasm32")]
use std::io;

use serde::{de::DeserializeOwned, Serialize};

use ron::de;

const FILE_FORMAT: &str = ".ron";
const DEFAULT_APP_NAME: &str = "morph_ui_orbclient_app";

/// The `Settings` service is use to save and store application settings.
/// todo: ron, save locations
#[derive(Debug, PartialEq, Clone)]
pub struct Settings {
    name: String,
}

impl Default for Settings {
    fn default() -> Self {
        Settings::new(DEFAULT_APP_NAME)
    }
}

impl Settings {
    /// Creates a new settings service,
    pub fn new(name: impl Into<String>) -> Self {
        Settings { name: name.into() }
    }

    #[cfg(target_arch = "wasm32")]
    pub fn save<S>(&self, key: &str, data: &S) -> io::Result<()>
    where
        S: Serialize,
    {
        Ok(())
    }

    #[cfg(target_arch = "wasm32")]
    pub async fn load<D>(&self, key: &str) -> io::Result<D>
    where
        D: DeserializeOwned,
    {
        Err(io::Error::new(
            io::ErrorKind::Other,
            "Not yet implemented for web.",
        ))
    }

    /// Saves the settings of the given data for the give key.
    #[cfg(not(target_arch = "wasm32"))]
    pub async fn save<S>(&self, key: &str, data: &S) -> io::Result<()>
    where
        S: Serialize,
    {
        let content = ron::ser::to_string_pretty(data, ron::ser::PrettyConfig::default())
            .map_err(|_| io::Error::new(io::ErrorKind::Other, "Cannot parse settings data"))?;

        if let Some(config_dir) = &mut dirs_next::config_dir() {
            config_dir.push(self.name.as_str());

            // create dir if not exists
            if !config_dir.exists() {
                create_dir_all(&config_dir).await?;
            }

            config_dir.push(format!("{}{}", key, FILE_FORMAT));

            let mut file = File::create(&config_dir).await?;
            file.write_all(content.as_bytes()).await?;
        }

        Ok(())
    }

    /// Loads settings of the given type for the given key.
    #[cfg(not(target_arch = "wasm32"))]
    pub async fn load<D>(&self, key: &str) -> io::Result<D>
    where
        D: DeserializeOwned,
    {
        if let Some(config_dir) = &mut dirs_next::config_dir() {
            config_dir.push(self.name.as_str());
            config_dir.push(format!("{}{}", key, FILE_FORMAT));

            let mut file = File::open(&config_dir).await?;
            let mut contents = String::new();
            file.read_to_string(&mut contents).await?;

            return de::from_str(contents.as_str())
                .map_err(|_| io::Error::new(io::ErrorKind::Other, "Cannot parse settings file"));
        }

        io::Result::Err(io::Error::new(
            io::ErrorKind::Other,
            "Cannot find user config dir",
        ))
    }
}
