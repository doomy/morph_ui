use std::{collections::BTreeMap, io::Cursor, path::Path};

use image::{imageops, io::*, *};

use morph_ui::eg::{
    draw_target::*, geometry::*, image::*, pixelcolor::Rgb888, primitives::Rectangle,
};

use crate::Error;

/// `ImageStorage` is used to manage a collection of images.
#[derive(Debug, Default, PartialEq, Clone)]
pub struct ImageStorage {
    images: BTreeMap<String, Image>,
}

impl ImageStorage {
    /// Loads a image from the given path and add it to the storage with the given key.
    pub fn load<P>(
        &mut self,
        key: impl Into<String>,
        path: P,
        size: Option<Size>,
    ) -> Result<Image, Error>
    where
        P: AsRef<Path>,
    {
        let key = key.into();
        if let Ok(image) = Image::open(path, size) {
            self.images.insert(key.clone(), image);

            return self.image(key).ok_or(Error::CannotLoadFile);
        }

        Err(Error::CannotLoadFile)
    }

    /// Returns an image if there is an image stored for the given otherwise `None`.
    pub fn image(&self, key: impl Into<String>) -> Option<Image> {
        if let Some(image) = self.images.get(&key.into()) {
            return Some(image.clone());
        }

        None
    }

    /// Returns a resized image if there is an image stored for the given otherwise `None`.
    pub fn resized_image(&self, key: impl Into<String>, size: Size) -> Option<Image> {
        if let Some(image) = self.images.get(&key.into()) {
            return Some(image.resize(size));
        }

        None
    }
}

/// Represents an image object that can be loaded from a file and can be drawn.
#[derive(Debug, Default, PartialEq, Clone)]
pub struct Image {
    rgb_image: RgbImage,
    pixels: Vec<Rgb888>,
}

impl Image {
    /// Reads a image from the given bytes.
    pub fn from_bytes(bytes: &[u8], size: Option<Size>) -> Result<Self, Error> {
        if let Ok(reader) = Reader::new(Cursor::new(bytes)).with_guessed_format() {
            if let Ok(image) = reader.decode() {
                let rgb_image = image.to_rgb8();
                let pixels = pixels(&rgb_image);

                let image = Self { rgb_image, pixels };

                if let Some(size) = size {
                    return Ok(image.resize(size));
                }

                return Ok(image);
            }
        }
        Err(Error::CannotLoadFile)
    }

    /// Opens a new image from the given path.
    #[cfg(target_arch = "wasm32")]
    pub fn open<P>(path: P, size: Option<Size>) -> Result<Self, Error>
    where
        P: AsRef<Path>,
    {
        let rgb_image = ImageBuffer::new(10, 10);
        let pixels = pixels(&rgb_image);

        let image = Self { rgb_image, pixels };

        if let Some(size) = size {
            return Ok(image.resize(size));
        }
        Ok(image)
    }

    /// Opens a new image from the given path.
    #[cfg(not(target_arch = "wasm32"))]

    pub fn open<P>(path: P, size: Option<Size>) -> Result<Self, Error>
    where
        P: AsRef<Path>,
    {
        if let Ok(img) = image::open(path) {
            let rgb_image = img.to_rgb8();
            let pixels = pixels(&rgb_image);

            let image = Self { rgb_image, pixels };

            if let Some(size) = size {
                return Ok(image.resize(size));
            }
            return Ok(image);
        }

        Err(Error::CannotLoadFile)
    }

    /// Returns a resized variant of the image.
    #[must_use]
    pub fn resize(&self, size: Size) -> Self {
        let rgb_image = imageops::resize(
            &self.rgb_image,
            size.width,
            size.height,
            imageops::FilterType::Nearest,
        );

        let pixels = pixels(&rgb_image);

        Image { rgb_image, pixels }
    }
}

impl ImageDrawable for Image {
    type Color = Rgb888;

    fn draw<D>(&self, target: &mut D) -> Result<(), D::Error>
    where
        D: DrawTarget<Color = Self::Color>,
    {
        target.fill_contiguous(&self.bounding_box(), self)
    }

    fn draw_sub_image<D>(&self, target: &mut D, area: &Rectangle) -> Result<(), D::Error>
    where
        D: morph_ui::DrawTarget<Color = Self::Color>,
    {
        self.draw(&mut target.translated(-area.top_left).clipped(area))
    }
}

impl OriginDimensions for Image {
    fn size(&self) -> Size {
        Size::new(self.rgb_image.width(), self.rgb_image.height())
    }
}

fn pixels(rgb_image: &RgbImage) -> Vec<Rgb888> {
    rgb_image
        .pixels()
        .map(|p| Rgb888::new(p[0], p[1], p[2]))
        .collect()
}

/// Iterates through the pixels of a image.
pub struct ImageIterator<'a> {
    image: &'a Image,
    index: usize,
}

impl<'a> Iterator for ImageIterator<'a> {
    type Item = Rgb888;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.image.pixels.len() {
            self.index += 1;
            return Some(self.image.pixels[self.index - 1]);
        }

        None
    }
}

impl<'a> IntoIterator for &'a Image {
    type Item = Rgb888;

    type IntoIter = ImageIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        ImageIterator {
            image: self,
            index: 0,
        }
    }
}
