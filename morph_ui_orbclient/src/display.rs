use std::marker::PhantomData;

use embedded_graphics_core::{
    pixelcolor::{Rgb888, RgbColor},
    prelude::{Dimensions, DrawTarget, Pixel, PixelColor, Point, Size},
    primitives::Rectangle,
};

#[derive(Debug, Clone)]
pub struct Display<C>
where
    C: PixelColor + From<<C as PixelColor>::Raw>,
{
    size: Size,
    frame_buffer: *mut u8,
    _pc: PhantomData<C>,
}

impl<C> Display<C>
where
    C: PixelColor + From<<C as PixelColor>::Raw>,
{
    pub fn new(size: impl Into<Size>, frame_buffer: *mut u8) -> Self {
        Self {
            size: size.into(),
            frame_buffer,
            _pc: PhantomData::default(),
        }
    }

    fn data_mut(&mut self) -> &mut [u8] {
        unsafe {
            let len = (self.size.width * self.size.height * 4) as usize;
            std::slice::from_raw_parts_mut(self.frame_buffer, len)
        }
    }
}

impl<C> DrawTarget for Display<C>
where
    C: PixelColor + From<<C as PixelColor>::Raw> + Into<Rgb888>,
{
    type Color = C;

    type Error = String;

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Pixel<Self::Color>>,
    {
        let size = self.size;
        let data = self.data_mut();
        for Pixel(p, color) in pixels.into_iter() {
            let (r, g, b, a) = rgba(color);

            if p.x >= 0 && p.y >= 0 && p.x < size.width as i32 && p.y < size.height as i32 {
                let index = (p.y as usize * size.width as usize + p.x as usize) * 4;
                data[index] = r;
                data[index + 1] = g;
                data[index + 2] = b;
                data[index + 3] = a;
            }
        }

        Ok(())
    }
}

impl<C> Dimensions for Display<C>
where
    C: PixelColor + From<<C as PixelColor>::Raw>,
{
    fn bounding_box(&self) -> Rectangle {
        Rectangle::new(Point::default(), self.size)
    }
}

#[cfg(not(target_arch = "wasm32"))]
fn rgba<C: PixelColor + Into<Rgb888>>(color: C) -> (u8, u8, u8, u8) {
    let color: Rgb888 = color.into();

    (color.b(), color.g(), color.r(), 255)
}

#[cfg(target_arch = "wasm32")]
fn rgba<C: PixelColor + Into<Rgb888>>(color: C) -> (u8, u8, u8, u8) {
    let color: Rgb888 = color.into();

    (color.r(), color.g(), color.b(), 255)
}
