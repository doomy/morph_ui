use crate::{Error, Window, WindowBuilder};

use morph_ui::{eg::pixelcolor::*, shell::State};

/// Manges the initial setup of the application, its startup and the application loop.
#[derive(Default)]
pub struct App<S: State + PartialEq + Clone + 'static, C>
where
    C: PixelColor + From<<C as PixelColor>::Raw> + Into<Rgb888>,
{
    windows: Vec<Window<S, C>>,
}

impl<S: State + PartialEq + Clone, C> App<S, C>
where
    C: PixelColor + From<<C as PixelColor>::Raw> + Into<Rgb888> + 'static,
{
    /// Creates a new app, that can be configured using the builder pattern,.
    pub fn new() -> Self {
        App {
            windows: Vec::new(),
        }
    }

    /// Builder method that is used add a new window to the app.
    pub fn window(mut self, builder: WindowBuilder<S, C>) -> Result<Self, Error> {
        self.windows.push(builder.build()?);
        Ok(self)
    }

    // Starts and runs the application (not wasm32 target)
    #[cfg(not(target_arch = "wasm32"))]
    pub fn start(mut self) -> Result<(), Error> {
        // todo: handle animation loop.
        loop {
            if !self.run()? {
                break;
            }
        }

        Ok(())
    }

    // Starts and runs the application (wasm32 target)
    #[cfg(target_arch = "wasm32")]
    pub fn start(mut self) -> Result<bool, Error> {
        orbclient::animation_loop(move || {
            if let Ok(run) = self.run() {
                return run;
            }

            return false;
        });

        Ok(true)
    }

    // runs the loop
    fn run(&mut self) -> Result<bool, Error> {
        for i in 0..self.windows.len() {
            // removes the window from the list if run returns false
            if !self.windows[i].run()? {
                self.windows.remove(i);
            }
        }

        // if no window is left, close the application.
        if self.windows.is_empty() {
            return Ok(false);
        }

        Ok(true)
    }
}
