/// Represents orbtk OrbClient specific errors.
#[derive(Clone, Debug)]
pub enum Error {
    /// Describes that it was not possible to create a new OrbClient window.
    CannotCreateWindow,
    /// Describes that it was not possible to read the screen size.
    CannotReadScreenSize,
    /// Describes that it was not possible to create a new tiny-skia display.
    CannotCreateTinySkiaDisplay,
    /// Describes an error while running the shell.
    ShellRunError,
    /// Cannot load a font.
    CannotLoadFont,
    /// Cannot load the given file.
    CannotLoadFile,
}
