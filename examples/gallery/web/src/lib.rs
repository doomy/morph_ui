#![allow(clippy::unused_unit)]

use wasm_bindgen::prelude::*;

#[wasm_bindgen(start)]
pub fn start() {
    gallery::app((1024, 720)).unwrap().start().unwrap();
}
