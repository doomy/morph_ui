use morph_ui_orbclient::Error;

fn main() -> Result<(), Error> {
    gallery::app((1024, 720))?.start()
}
