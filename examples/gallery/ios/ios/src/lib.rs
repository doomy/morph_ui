#[no_mangle]
pub extern "C" fn run() {
    env_logger::Builder::from_default_env()
        .filter_level(log::LevelFilter::Info)
        .init();

    gallery::app(morph_ui_orbclient::get_display_size().unwrap())
        .unwrap()
        .start()
        .unwrap();
}
