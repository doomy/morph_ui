// Including SDL.h is required to let SDL hook main and set up the app delegate
#include "SDL.h"
#include <stdio.h>

extern void run();

int main(int argc, char *argv[])
{
    run();
    printf("run_the_game returned");
    return 0;
}
