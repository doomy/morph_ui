# Gallery example

The gallery examples is used show how to use `morph_ui` and `morph_ui_orbclient` in a cross platform project setup. 
It also provides an overview of all default widgets provided by `morph_ui`. 

## Prerequisites

Check [README.md](https://codeberg.org/morphUI/morph_ui/src/branch/main/README.md).

## Run on Desktop

```sh
# Builds and runs the gallery on the desktop.
cargo run --bin gallery_desktop
```

## iOS

```sh
# Load sdl2 git submodule (needed only after checkout of the repo).
git submodule update --init --recursive

# Builds the gallery for aarch64.
cargo build --package gallery_ios --target aarch64-apple-ios --verbose --release

# Builds the gallery for x86_64.
cargo build --package gallery_ios --target x86_64-apple-ios --verbose --release

# Creates universal library for iOS.
lipo -create target/aarch64-apple-ios/release/libgallery_ios.a target/x86_64-apple-ios/release/libgallery_ios.a -output target/libgallery_ios.a

# Open the project with xcode to run the gallery on a simulator or iOS device.
open -a xcode examples/gallery/ios/xcode/gallery_ios.xcodeproj
```

## Web

```sh
# Navigates to the web project folder.
cd web

# Install nodejs dependencies.
npm install

# Builds the project and opens it in a new browser tab. Auto-reloads when the project changes.
npm start
```