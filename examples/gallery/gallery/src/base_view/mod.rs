use morph_ui::{eg::pixelcolor::*, themes::ThemeHelper, *};
use tiny_skia_display::TinySkiaDisplay;

use crate::*;

/// Returns a responsive base view.
pub fn base_view(
    app: &mut AppState,
    view: Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>>,
) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
    let mut column = app.column();

    if app.get_is_mobile_view() {
        column.push_align(
            app.top_bar().front(
                app.top_bar_button()
                    .text("back")
                    .on_tap(|app: &mut AppState| app.navigate_back()),
            ),
            "stretch",
        );
    }

    column.child(app.content_container().child(view))
}
