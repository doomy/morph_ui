use morph_ui::{eg::pixelcolor::*, themes::ThemeHelper, *};
use tiny_skia_display::TinySkiaDisplay;

use crate::*;

/// Returns the main view.
pub fn main_view(app: &mut AppState) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
    app.background().child(
        app.frame()
            .break_point(600)
            .show_content(app.show_content())
            .navigation(
                app.side_bar().child(
                    app.column()
                        .align(
                            app.padding().padding(Thickness::new(2, 4, 2, 4)).child(
                                app.stretch_text_field()
                                    .text(app.get_filter())
                                    .placeholder("Search")
                                    .on_text_changed(|app: &mut AppState, text: &str| {
                                        app.set_filter(text)
                                    }),
                            ),
                            "stretch",
                        )
                        .align(
                            app.stretch().child(
                                app.no_border_list()
                                    .selected_index(app.get_current_view_index())
                                    .constraints(Constraints::default())
                                    .items(app.navigation_items().as_slice())
                                    .builder(|btx: &ListBuildContext<NavigationModel, AppState>| {
                                        let index = btx.index;
                                        btx.app.button().text(btx.item.title.as_str()).on_tap(
                                            move |state: &mut SelectionState| {
                                                state.select(index);
                                            },
                                        )
                                    })
                                    .on_selection_changed(
                                        |app: &mut AppState, index: Option<usize>| {
                                            app.show_view(index.unwrap())
                                        },
                                    ),
                            ),
                            "stretch",
                        ),
                ),
            )
            .content(app.get_view())
            .on_layout_break(|app, mobile_view| app.set_is_mobile_view(mobile_view)),
    )
}
