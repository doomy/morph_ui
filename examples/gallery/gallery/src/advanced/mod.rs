mod advanced_model;
mod advanced_view;

pub use advanced_model::*;
pub use advanced_view::*;

pub const ADVANCED_VIEW: &str = "advanced_view";
