/// Provides the data for the advanced view.
#[derive(Debug, Default, PartialEq, Clone)]
pub struct AdvancedModel {
    pub items: Vec<String>,
    pub value: i32,
}
