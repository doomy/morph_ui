use morph_ui::{
    eg::pixelcolor::*,
    themes::{rgb888::material_icons, ThemeHelper},
    *,
};
use morph_ui_orbclient::image::Image;
use tiny_skia_display::TinySkiaDisplay;

use crate::*;

/// Returns the view with the overview of advanced widgets.
pub fn advanced_view(app: &mut AppState) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
    base_view(
        app,
        app.row()
            .spacing(8)
            .child(
                app.column()
                    .spacing(8)
                    .child(app.header_label().text("TextField filter"))
                    .child(
                        app.text_field()
                            .text(format!("{}", app.get_advanced_model().value).as_str())
                            .filter(&['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
                            .on_text_changed(|app: &mut AppState, text| {
                                if let Ok(value) = text.parse::<i32>() {
                                    app.get_advanced_model_mut().value = value;
                                }
                            }),
                    )
                    .child(app.header_label().text("Scroll Area"))
                    .child(
                        app.scroll_area().scroll_bar_margin(4).child(
                            app.row()
                                .child(
                                    app.column()
                                        .child(app.primary_button().text("test 0"))
                                        .child(app.primary_button().text("test 1"))
                                        .child(app.primary_button().text("test 2"))
                                        .child(app.primary_button().text("test 3"))
                                        .child(app.primary_button().text("test 4"))
                                        .child(app.primary_button().text("test 5")),
                                )
                                .child(
                                    app.column()
                                        .child(app.primary_button().text("test 0"))
                                        .child(app.primary_button().text("test 1"))
                                        .child(app.primary_button().text("test 2"))
                                        .child(app.primary_button().text("test 3"))
                                        .child(app.primary_button().text("test 4"))
                                        .child(app.primary_button().text("test 5")),
                                ),
                        ),
                    )
                    .child(app.header_label().text("Segmented"))
                    .child(
                        app.segmented()
                            .segment(material_icons::MD_PLAY_ARROW)
                            .segment(material_icons::MD_EDIT),
                    )
                    .child(app.header_label().text("Image"))
                    .child(
                        app.image()
                            //
                            .image(
                                Image::from_bytes(include_bytes!("../../assets/rust.png"), None)
                                    .unwrap(),
                            ),
                    )
                    .child(app.header_label().text("QrCodeWidget"))
                    .child(
                        app.qr_code()
                            .text("https://codeberg.org/morphUI/morph_ui")
                            .unwrap(),
                    ),
            )
            .child(
                app.column()
                    .spacing(8)
                    .child(app.header_label().text("Item"))
                    .child(
                        app.item().child(
                            app.column()
                                .spacing(4)
                                .child(
                                    app.image().image(
                                        Image::from_bytes(
                                            include_bytes!("../../assets/rust.png"),
                                            None,
                                        )
                                        .unwrap(),
                                    ),
                                )
                                .align(app.label().text("Item"), "center"),
                        ),
                    )
                    .child(app.header_label().text("List"))
                    .child(
                        app.list()
                            .items(app.get_advanced_model().items.as_slice())
                            .builder(|btx: &ListBuildContext<String, AppState>| {
                                let index = btx.index;
                                btx.app.button().text(btx.item).on_tap(
                                    move |state: &mut SelectionState| {
                                        state.select(index);
                                    },
                                )
                            }),
                    ),
            )
            .child(
                app.column()
                    .spacing(8)
                    .child(app.header_label().text("TabWidget"))
                    .child(
                        app.constraint_box()
                            .constraints(Constraints::create().width(100).height(200))
                            .child(
                                app.tab_widget()
                                    .tab(
                                        "Tab 1",
                                        app.background().child(app.label().text("Content tab 1")),
                                    )
                                    .tab(
                                        "Tab 2",
                                        app.background().child(app.label().text("Content tab 2")),
                                    )
                                    .tab(
                                        "Tab 3",
                                        app.background().child(app.label().text("Content tab 3")),
                                    ),
                            ),
                    ),
            ),
    )
}
