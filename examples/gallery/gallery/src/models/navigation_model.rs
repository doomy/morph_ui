/// Represents an entry in the navigation list.
#[derive(Debug, Default, PartialEq, Clone)]
pub struct NavigationModel {
    pub view: String,
    pub title: String,
    pub tags: Vec<String>,
}
