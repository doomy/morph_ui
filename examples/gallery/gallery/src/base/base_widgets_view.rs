use morph_ui::{eg::pixelcolor::*, themes::ThemeHelper, *};
use tiny_skia_display::TinySkiaDisplay;

use crate::*;

/// Returns the view with an overview of base morph_ui widgets.
pub fn base_widgets_view(app: &mut AppState) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
    base_view(
        app,
        app.row()
            .spacing(8)
            .child(
                app.column()
                    .spacing(4)
                    .child(app.header_label().text("Buttons"))
                    .child(app.primary_button().text("Primary"))
                    .child(app.button().text("Secondary"))
                    .child(app.highlight_button().text("Highlight"))
                    .child(app.button().text("Disabled").enabled(false))
                    .child(
                        app.button()
                            .text("Toggle")
                            .checkable(true)
                            .on_is_checked_changed(|_, checked| {
                                println!("toggle checked {}", checked);
                            }),
                    )
                    .child(app.switch().on_is_checked_changed(|_, checked| {
                        println!("switch checked {}", checked);
                    })),
            )
            .child(
                app.column()
                    .spacing(4)
                    .child(app.header_label().text("Text"))
                    .child(
                        app.text_field()
                            .placeholder("Insert text")
                            .on_enter(|_| println!("text field enter"))
                            .on_text_changed(|_, text| {
                                println!("text field text changed: {}", text);
                            }),
                    )
                    .child(
                        app.text_field()
                            .placeholder("Insert password")
                            .password_box(true)
                            .text(""),
                    ),
            ),
    )
}
