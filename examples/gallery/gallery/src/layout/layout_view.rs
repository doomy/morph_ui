use morph_ui::{eg::pixelcolor::*, themes::ThemeHelper, *};
use tiny_skia_display::TinySkiaDisplay;

use crate::*;

/// Returns the view with the overview of layout widgets.
pub fn layout_view(app: &mut AppState) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
    base_view(
        app,
        app.row().spacing(8).child(
            app.column()
                .spacing(8)
                .child(app.header_label().text("Wrap (resize window)"))
                .child(
                    app.wrap()
                        .hor_spacing(4)
                        .ver_spacing(4)
                        .child(app.button().text("1"))
                        .child(app.button().text("2"))
                        .child(app.button().text("3"))
                        .child(app.button().text("4"))
                        .child(app.button().text("5"))
                        .child(app.button().text("6"))
                        .child(app.button().text("7"))
                        .child(app.button().text("8"))
                        .child(app.button().text("9"))
                        .child(app.button().text("10"))
                        .child(app.button().text("11"))
                        .child(app.button().text("12"))
                        .child(app.button().text("13"))
                        .child(app.button().text("14"))
                        .child(app.button().text("15"))
                        .child(app.button().text("16"))
                        .child(app.button().text("17"))
                        .child(app.button().text("18"))
                        .child(app.button().text("19"))
                        .child(app.button().text("20"))
                        .child(app.button().text("21"))
                        .child(app.button().text("22"))
                        .child(app.button().text("23"))
                        .child(app.button().text("24"))
                        .child(app.button().text("25"))
                        .child(app.button().text("26"))
                        .child(app.button().text("27"))
                        .child(app.button().text("28"))
                        .child(app.button().text("29")),
                ),
        ),
    )
}
