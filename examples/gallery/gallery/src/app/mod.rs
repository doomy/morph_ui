use morph_ui::{context::Debug, eg::pixelcolor::*, themes::ThemeHelper, *};
use morph_ui_orbclient::*;

use crate::*;

mod app_state;

pub use app_state::*;

pub fn app(size: (u32, u32)) -> Result<App<AppState, Rgb888>, Error> {
    // let width = 1024;
    // let height = 720;

    let mut window = Window::create(AppState::new())
        .title("morph_ui widget gallery")
        .size(size.0, size.1)
        .centered(true)
        .resizeable(true)
        .debug_flags(
            &[Debug::DrawRaster, Debug::PrintGraph, Debug::PrintPipeline],
            Some(Rgb888::CSS_BLUE),
        )
        .ui(|app| app.background().child(main_view(app)))?;

    // if cfg!(target_os = "ios") {
    window = window.keyboard(|app| app.keyboard(en_us()));
    // }

    App::new().window(window)
}
