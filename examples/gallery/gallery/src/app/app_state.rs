use morph_ui::{
    eg::pixelcolor::*,
    themes::{rgb888::*, ThemeHelper},
    *,
};
use tiny_skia_display::TinySkiaDisplay;

use crate::*;

/// Represents the base application state.
#[derive(Debug, Default, PartialEq, Clone)]
pub struct AppState {
    pub theme: MorphTheme,
    navigation_items: Vec<NavigationModel>,
    current_view: Option<String>,
    advanced_model: AdvancedModel,
    show_content: bool,
    is_mobile_view: bool,
    filter: String,
}

impl AppState {
    pub fn new() -> Self {
        AppState {
            theme: MorphTheme::default(),
            navigation_items: vec![
                NavigationModel {
                    view: BASE_WIDGETS_VIEW.into(),
                    title: "Base".into(),
                    tags: vec![
                        "Button".into(),
                        "TextField".into(),
                        "Label".into(),
                        "Password".into(),
                        "Toggle".into(),
                        "Switch".into(),
                    ],
                },
                NavigationModel {
                    view: ADVANCED_VIEW.into(),
                    title: "Advanced".into(),
                    tags: vec![
                        "Filter".into(),
                        "Scroll".into(),
                        "Tab".into(),
                        "List".into(),
                        "Segmented".into(),
                    ],
                },
                NavigationModel {
                    view: LAYOUT_VIEW.into(),
                    title: "Layouts".into(),
                    tags: vec!["Wrap".into()],
                },
            ],
            current_view: Some(BASE_WIDGETS_VIEW.into()),
            advanced_model: AdvancedModel {
                items: vec![
                    "Item 1".into(),
                    "Item 2".into(),
                    "Item 3".into(),
                    "Item 4".into(),
                    "Item 5".into(),
                ],
                value: 0,
            },
            ..Default::default()
        }
    }

    /// Returns the value of the navigation filter.
    pub fn get_filter(&self) -> &str {
        self.filter.as_str()
    }

    /// Sets the value of the navigation filter.
    pub fn set_filter(&mut self, filter: &str) {
        self.filter = filter.into();

        if self.filter.is_empty() {
            return;
        }

        if let Some(item) = self
            .navigation_items()
            .iter()
            .find(|n| n.view.contains(self.filter.as_str()))
        {
            self.current_view = Some(item.view.clone());
        }
    }

    /// Returns the current shown view.
    pub fn get_view(&mut self) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
        match self.get_current_view() {
            Some(ADVANCED_VIEW) => advanced_view(self),
            Some(LAYOUT_VIEW) => layout_view(self),
            _ => base_widgets_view(self),
        }
    }

    /// Returns the flag that describes if the applications is shown in a mobile layout.
    pub fn get_is_mobile_view(&self) -> bool {
        self.is_mobile_view
    }

    /// If set to `true` the application will shown in a one column (mobile) layout with navigation, otherwise as
    /// two column layout.
    pub fn set_is_mobile_view(&mut self, is_mobile_view: bool) {
        self.is_mobile_view = is_mobile_view;

        if self.is_mobile_view {
            self.show_content = false;
            self.current_view = None;
        }
    }

    /// Returns a reference of the [`AdvancedModel`].
    pub fn get_advanced_model(&self) -> &AdvancedModel {
        &self.advanced_model
    }

    /// Returns a mutable reference of the [`AdvancedModel`].
    pub fn get_advanced_model_mut(&mut self) -> &mut AdvancedModel {
        &mut self.advanced_model
    }

    // navigation

    pub fn navigation_items(&self) -> Vec<NavigationModel> {
        self.navigation_items
            .iter()
            .filter(|n| {
                n.title
                    .to_lowercase()
                    .contains(self.filter.to_lowercase().as_str())
            })
            .cloned()
            .collect()
    }

    pub fn navigate_back(&mut self) {
        self.show_content = false;
        self.current_view = None;
    }

    pub fn show_view(&mut self, index: usize) {
        self.show_content = true;

        let navigation_items = self.navigation_items();

        if index < navigation_items.len() {
            self.current_view = Some(navigation_items[index].view.clone());
        } else {
            self.current_view = None;
        }
    }

    pub fn get_current_view(&self) -> Option<&str> {
        self.current_view.as_deref()
    }

    /// Returns the index of the current shown view.
    pub fn get_current_view_index(&self) -> Option<usize> {
        if let Some(current_view) = self.get_current_view() {
            return self
                .navigation_items()
                .iter()
                .position(|n| n.view.eq(current_view));
        }

        None
    }

    /// Returns `true` if navigation shows content on mobile view otherwise `false`.
    pub fn show_content(&self) -> bool {
        self.show_content
    }
}

impl State for AppState {}

impl<D> ThemeHelper<D, TextStyle, MorphTheme> for AppState
where
    D: DrawTarget<Color = Rgb888> + 'static,
{
    fn theme(&self) -> &MorphTheme {
        &self.theme
    }
}
