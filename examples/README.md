# Examples

# Counter

A simple counter application that can run on a esp32 and stm32 board with a sh1106 display.

Check [README.md](https://codeberg.org/morphUI/morph_ui/src/branch/main/examples/counter/README.md).

# Gallery

Gallery of all `morph_ui` default widgets. It can be run on desktop (Linux, macOS, Window), iOS and Web.

Check [README.md](https://codeberg.org/morphUI/morph_ui/src/branch/main/examples/gallery/README.md).
