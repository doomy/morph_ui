#![no_std]
#![no_main]
#![feature(alloc_error_handler)]
#![feature(raw_vec_internals)]

use esp32_hal::alloc::*;

use embedded_hal::blocking::i2c::{Write, WriteRead};
use esp32_hal::{
    clock_control::{self, sleep, CPUSource, ClockControl},
    dport::Split,
    dprintln,
    i2c::{self, Error, I2C},
    prelude::*,
    target::{Peripherals, I2C0},
    timer::Timer,
};
use xtensa_lx::mutex::*;

use core::panic::PanicInfo;

use morph_ui::{shell::Shell, stdlib::string::String, themes::binary::*, *};
use sh1106::{prelude::*, Builder};

#[macro_use]
extern crate alloc;

#[global_allocator]
pub static GLOBAL_ALLOCATOR: Allocator = DEFAULT_ALLOCATOR;

#[derive(Debug, Default, PartialEq, Clone)]
struct AppState {
    text: String,
    theme: MorphTheme,
}

impl ThemeHelper for AppState {
    fn theme(&self) -> &MorphTheme {
        &self.theme
    }
}

#[entry]
fn main() -> ! {
    let dp = Peripherals::take().unwrap();

    let (mut dport, dport_clock_control) = dp.DPORT.split();

    // setup clocks & watchdog
    let mut clkcntrl = ClockControl::new(
        dp.RTCCNTL,
        dp.APB_CTRL,
        dport_clock_control,
        clock_control::XTAL_FREQUENCY_AUTO,
    )
    .unwrap();

    // set desired clock frequencies
    clkcntrl
        .set_cpu_frequencies(
            CPUSource::PLL,
            80.MHz(),
            CPUSource::PLL,
            240.MHz(),
            CPUSource::PLL,
            80.MHz(),
        )
        .unwrap();

    // disable RTC watchdog
    let (clkcntrl_config, mut watchdog) = clkcntrl.freeze().unwrap();
    watchdog.disable();

    // disable MST watchdogs
    let (.., mut watchdog0) = Timer::new(dp.TIMG0, clkcntrl_config);
    let (.., mut watchdog1) = Timer::new(dp.TIMG1, clkcntrl_config);
    watchdog0.disable();
    watchdog1.disable();

    let pins = dp.GPIO.split();
    let i2c0 = i2c::I2C::new(
        dp.I2C0,
        i2c::Pins {
            sda: pins.gpio21,
            scl: pins.gpio22,
        },
        100_000,
        &mut dport,
    );

    let i2c0 = SpinLockMutex::new(i2c0);
    let display = {
        let i2c_wrapper = I2CWrapper::new(i2c0);
        let mut display: GraphicsMode<_> = Builder::new().connect_i2c(i2c_wrapper).into();

        dprintln!("after display");

        let mut rst = pins.gpio16.into_push_pull_output();
        rst.set_low().unwrap();
        sleep(10.ms());
        rst.set_high().unwrap();

        display.init().unwrap();
        dprintln!("after display init");
        display.clear();
        display.flush().unwrap();
        dprintln!("after display flash");

        display
    };

    let state = AppState {
        text: String::from("Hello morph"),
        ..Default::default()
    };

    dprintln!("after state");

    let mut shell = Shell::from_display(display, state).ui(|app| {
        app.background_container().padding(4).child(
            Column::new()
                .spacing(4)
                .child(app.header_label().text("morph_ui"))
                .child(app.label().text(app.text.clone())),
        )
    });

    if let Err(err) = shell.run() {
        dprintln!("{:?}", err);
    }

    if let Some(display) = shell.display_mut() {
        if let Err(err) = display.flush() {
            dprintln!("{:?}", err);
        }
    }

    loop {}
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    dprintln!("\n\n*** {:?}", info);
    loop {}
}

#[alloc_error_handler]
fn alloc_error_handler(layout: core::alloc::Layout) -> ! {
    panic!(
        "Error allocating  {} bytes of memory with alignment {}",
        layout.size(),
        layout.align()
    );
}

struct I2CWrapper {
    i2c: SpinLockMutex<I2C<I2C0>>,
}

impl<'a> I2CWrapper {
    fn new(i2c: SpinLockMutex<I2C<I2C0>>) -> Self {
        Self { i2c }
    }
}

impl<'a> Write for I2CWrapper {
    type Error = Error;

    fn write(&mut self, addr: u8, bytes: &[u8]) -> Result<(), Self::Error> {
        (&self.i2c).lock(|x| x.write(addr, bytes))
    }
}

impl WriteRead for I2CWrapper {
    type Error = Error;

    fn write_read(
        &mut self,
        address: u8,
        bytes: &[u8],
        buffer: &mut [u8],
    ) -> Result<(), Self::Error> {
        (&self.i2c).lock(|x| x.write_read(address, bytes, buffer))
    }
}
