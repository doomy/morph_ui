//! This example is for the STM32F103 "Blue Pill" board using I2C1.
//!
//! Wiring connections are as follows for a CRIUS-branded display:
//!
//! ```
//!      Display -> Blue Pill
//! (black)  GND -> GND
//! (red)    +5V -> VCC
//! (yellow) SDA -> PB9
//! (green)  SCL -> PB8
//! ```
//!
//! # Run on a Blue Pill
//! Run `cargo run --release`.
//!

#![no_std]
#![no_main]
#![feature(alloc_error_handler)]

extern crate alloc;

use cortex_m_rt::{entry, exception, ExceptionFrame};

use embedded_graphics::pixelcolor::BinaryColor;
use panic_semihosting as _;
use sh1106::{interface::DisplayInterface, prelude::*, Builder};
use stm32f1xx_hal::{
    delay::Delay,
    i2c::{BlockingI2c, DutyCycle, Mode},
    prelude::*,
    stm32,
};

use alloc_cortex_m::CortexMHeap;
use core::alloc::Layout;

use morph_ui::{shell::Shell, stdlib::format, themes::binary::*, widgets::*, *};

use rtt_target::{rprintln, rtt_init_print};

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

#[derive(Debug, Default, PartialEq, Clone)]
struct AppState {
    count: u32,
    theme: MorphTheme,
}

impl State for AppState {
    fn update(&mut self) {
        self.count += 1;
    }
}

impl ThemeHelper for AppState {
    fn theme(&self) -> &MorphTheme {
        &self.theme
    }
}

#[entry]
fn main() -> ! {
    rtt_init_print!();

    // Initialize the allocator BEFORE you use it
    let start = cortex_m_rt::heap_start() as usize;
    let size = 1024; // in bytes
    unsafe { ALLOCATOR.init(start, size) }

    let dp = stm32::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let mut afio = dp.AFIO.constrain(&mut rcc.apb2);

    let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);

    let scl = gpiob.pb8.into_alternate_open_drain(&mut gpiob.crh);
    let sda = gpiob.pb9.into_alternate_open_drain(&mut gpiob.crh);

    let i2c = BlockingI2c::i2c1(
        dp.I2C1,
        (scl, sda),
        &mut afio.mapr,
        Mode::Fast {
            frequency: 100.khz().into(),
            duty_cycle: DutyCycle::Ratio2to1,
        },
        clocks,
        &mut rcc.apb1,
        1000,
        10,
        1000,
        1000,
    );

    let mut display: GraphicsMode<_> = Builder::new().connect_i2c(i2c).into();

    display.init().unwrap();
    display.flush().unwrap();

    let state = AppState::default();

    let mut shell = Shell::from_display(display, state)
        .background_color(BinaryColor::Off)
        .ui(|app| {
            app.background_container().padding(4).child(
                Column::new()
                    .spacing(4)
                    .child(app.label().text("morph counter"))
                    .child(app.label().text(format!("count: {}", app.count))),
            )
        });

    let mut delay = Delay::new(cp.SYST, clocks);

    loop {
        delay.delay_ms(1_000_u16);
        run(&mut shell);
    }
}

fn run<T: DisplayInterface + 'static>(shell: &mut Shell<AppState, BinaryColor, GraphicsMode<T>>) {
    if let Err(err) = shell.run() {
        rprintln!("{:?}", err);
    }

    if let Some(display) = shell.display_mut() {
        if let Err(_) = display.flush() {
            rprintln!("cannot flush");
        }
    }
}

#[exception]
fn HardFault(ef: &ExceptionFrame) -> ! {
    panic!("{:#?}", ef);
}

#[alloc_error_handler]
fn oom(_: Layout) -> ! {
    loop {}
}
