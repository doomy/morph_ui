/// Represents orbtk OrbClient specific errors.
#[derive(Clone, Debug)]
pub enum Error {
    /// Describes that no ui is configured.
    MissingUi,

    ShellRunError,
}
