use morph_ui::{eg::pixelcolor::*, events::*, shell::Shell, stdlib::*, *};

use psp::{embedded_graphics::Framebuffer, sys::*};

use crate::error::Error;

pub struct App<S>
where
    S: State + PartialEq + Clone + 'static,
{
    shell: Shell<S, Rgb888, Framebuffer>,
    pad_data: SceCtrlData,
}

impl<S> App<S>
where
    S: Clone + PartialEq,
{
    pub fn create(state: S) -> AppBuilder<S> {
        AppBuilder::new(state)
    }

    fn drain_events(&mut self, update: &mut bool) -> bool {
        let pad_data = &mut SceCtrlData::default();

        unsafe {
            // Read button/analog input
            psp::sys::sceCtrlReadBufferPositive(pad_data, 1);
        }

        if let Some(button) = self.button(pad_data) {
            self.shell.controller_button(button.0, button.1, 0);
            *update = true;
        }

        self.pad_data = pad_data.clone();
        true
    }

    /// Runs the inner logic of the window.
    pub fn start(&mut self) -> Result<bool, Error> {
        // todo: needed?
        unsafe {
            psp::sys::sceCtrlSetSamplingCycle(0);
            psp::sys::sceCtrlSetSamplingMode(CtrlMode::Analog);
        };

        self.shell.run().map_err(|_| Error::ShellRunError)?;

        loop {
            let mut update = false;
            if !self.drain_events(&mut update) {
                return Ok(false);
            }

            if !update {
                continue;
            }

            self.shell.run().map_err(|_| Error::ShellRunError)?;
        }
    }

    fn button(&self, pad_data: &SceCtrlData) -> Option<(ControllerButton, bool)> {
        // start
        if pad_data.buttons.contains(CtrlButtons::START)
            && !self.pad_data.buttons.contains(CtrlButtons::START)
        {
            return Some((ControllerButton::Start, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::START)
            && self.pad_data.buttons.contains(CtrlButtons::START)
        {
            return Some((ControllerButton::Start, false));
        }

        // select
        if pad_data.buttons.contains(CtrlButtons::SELECT)
            && !self.pad_data.buttons.contains(CtrlButtons::SELECT)
        {
            return Some((ControllerButton::Guide, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::SELECT)
            && pad_data.buttons.contains(CtrlButtons::SELECT)
        {
            return Some((ControllerButton::Guide, false));
        }

        // up
        if pad_data.buttons.contains(CtrlButtons::UP)
            && !self.pad_data.buttons.contains(CtrlButtons::UP)
        {
            return Some((ControllerButton::DPadUp, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::LEFT)
            && self.pad_data.buttons.contains(CtrlButtons::UP)
        {
            return Some((ControllerButton::DPadUp, false));
        }

        // left
        if pad_data.buttons.contains(CtrlButtons::LEFT)
            && !self.pad_data.buttons.contains(CtrlButtons::LEFT)
        {
            return Some((ControllerButton::DPadLeft, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::LEFT)
            && self.pad_data.buttons.contains(CtrlButtons::LEFT)
        {
            return Some((ControllerButton::DPadLeft, false));
        }

        // right
        if pad_data.buttons.contains(CtrlButtons::RIGHT)
            && !self.pad_data.buttons.contains(CtrlButtons::RIGHT)
        {
            return Some((ControllerButton::DPadRight, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::RIGHT)
            && self.pad_data.buttons.contains(CtrlButtons::RIGHT)
        {
            return Some((ControllerButton::DPadRight, false));
        }

        // down
        if pad_data.buttons.contains(CtrlButtons::DOWN)
            && !self.pad_data.buttons.contains(CtrlButtons::DOWN)
        {
            return Some((ControllerButton::DPadDown, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::DOWN)
            && self.pad_data.buttons.contains(CtrlButtons::DOWN)
        {
            return Some((ControllerButton::DPadDown, false));
        }

        // left trigger
        if pad_data.buttons.contains(CtrlButtons::LTRIGGER)
            && !self.pad_data.buttons.contains(CtrlButtons::LTRIGGER)
        {
            return Some((ControllerButton::RightShoulder, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::LTRIGGER)
            && self.pad_data.buttons.contains(CtrlButtons::LTRIGGER)
        {
            return Some((ControllerButton::RightShoulder, false));
        }

        // right stick
        if pad_data.buttons.contains(CtrlButtons::RTRIGGER)
            && !self.pad_data.buttons.contains(CtrlButtons::RTRIGGER)
        {
            return Some((ControllerButton::RightShoulder, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::RTRIGGER)
            && self.pad_data.buttons.contains(CtrlButtons::RTRIGGER)
        {
            return Some((ControllerButton::RightShoulder, false));
        }

        // right stick
        if pad_data.buttons.contains(CtrlButtons::RTRIGGER)
            && !self.pad_data.buttons.contains(CtrlButtons::RTRIGGER)
        {
            return Some((ControllerButton::RightShoulder, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::RTRIGGER)
            && self.pad_data.buttons.contains(CtrlButtons::RTRIGGER)
        {
            return Some((ControllerButton::RightShoulder, false));
        }

        // triangle
        if pad_data.buttons.contains(CtrlButtons::TRIANGLE)
            && !self.pad_data.buttons.contains(CtrlButtons::TRIANGLE)
        {
            return Some((ControllerButton::Y, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::TRIANGLE)
            && self.pad_data.buttons.contains(CtrlButtons::TRIANGLE)
        {
            return Some((ControllerButton::Y, false));
        }

        // circle
        if pad_data.buttons.contains(CtrlButtons::CIRCLE)
            && !self.pad_data.buttons.contains(CtrlButtons::CIRCLE)
        {
            return Some((ControllerButton::B, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::CIRCLE)
            && self.pad_data.buttons.contains(CtrlButtons::CIRCLE)
        {
            return Some((ControllerButton::B, false));
        }

        // cross
        if pad_data.buttons.contains(CtrlButtons::CROSS)
            && !self.pad_data.buttons.contains(CtrlButtons::CROSS)
        {
            return Some((ControllerButton::A, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::CROSS)
            && self.pad_data.buttons.contains(CtrlButtons::CROSS)
        {
            return Some((ControllerButton::A, false));
        }

        // square
        if pad_data.buttons.contains(CtrlButtons::SQUARE)
            && !self.pad_data.buttons.contains(CtrlButtons::SQUARE)
        {
            return Some((ControllerButton::X, true));
        }

        if !pad_data.buttons.contains(CtrlButtons::SQUARE)
            && self.pad_data.buttons.contains(CtrlButtons::SQUARE)
        {
            return Some((ControllerButton::X, false));
        }

        None
    }
}

pub struct AppBuilder<S>
where
    S: Clone + PartialEq + 'static,
{
    shell: Option<Shell<S, Rgb888, Framebuffer>>,
    dbx: DebugContext<Rgb888>,
    background_color: Option<Rgb888>,
    state: S,
}

impl<S> AppBuilder<S>
where
    S: Clone + PartialEq + 'static,
{
    pub fn new(state: S) -> AppBuilder<S> {
        AppBuilder {
            shell: None,
            state,
            dbx: DebugContext::default(),
            background_color: None,
        }
    }

    /// Builder method to define a widget as root of the shells ui.
    ///
    /// An ui can only be set once. If the method is multiple called, the last set ui will be used.
    pub fn ui<F: Fn(&mut S) -> Box<dyn Widget<S, Framebuffer> + 'static> + 'static>(
        mut self,
        ui: F,
    ) -> Self {
        self.shell = Some(Shell::new(self.state.clone()).ui(ui));

        self
    }

    /// Used to configure debug output. Only available on debug builds.
    pub fn debug_flags(mut self, flags: &[Debug], raster_color: Option<Rgb888>) -> Self {
        self.dbx = DebugContext::new(flags, raster_color);

        self
    }

    /// Builder method used to set the background color of the shell.
    pub fn background_color(mut self, background_color: Rgb888) -> Self {
        self.background_color = Some(background_color);
        self
    }

    /// Creates a new window with the given builder settings.
    pub fn build(self) -> Result<App<S>, Error> {
        if let Some(mut shell) = self.shell {
            shell.set_debug_context(self.dbx);
            shell.set_display(Framebuffer::new());

            if let Some(background_color) = self.background_color {
                shell.set_background_color(background_color);
            }

            return Ok(App {
                shell,
                pad_data: SceCtrlData::default(),
            });
        }

        Err(Error::MissingUi)
    }
}
