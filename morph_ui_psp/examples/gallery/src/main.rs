use morph_ui::{
    eg::pixelcolor::Rgb888,
    themes::{rgb888::*, *},
    *,
};

use morph_ui_orbclient::*;
use tiny_skia_display::*;

fn digit_button(
    number: char,
    primary: bool,
    stretch: u32,
    app: &AppState,
) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
    let button = if primary {
        app.primary_stretch_button()
            .text(number.to_string().as_str())
    } else {
        app.secondary_stretch_button()
            .text(number.to_string().as_str())
    };

    app.stretch().stretch(
        button.on_tap(move |calculator: &mut AppState| calculator.digit(number)),
        stretch,
    )
}

fn op_button(
    op: char,
    primary: bool,
    stretch: u32,
    app: &AppState,
) -> Box<dyn Widget<AppState, TinySkiaDisplay<Rgb888>>> {
    let button = if primary {
        app.primary_stretch_button().text(op.to_string().as_str())
    } else {
        app.secondary_stretch_button().text(op.to_string().as_str())
    };

    app.stretch().stretch(
        button.on_tap(move |calculator: &mut AppState| calculator.op(op)),
        stretch,
    )
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct AppState {
    display: String,
    left_side: Option<f32>,
    right_side: Option<f32>,
    operation: Option<char>,
    reset: bool,
    theme: MorphTheme,
}

impl State for AppState {}

impl<D> ThemeHelper<D, TextStyle, MorphTheme> for AppState
where
    D: DrawTarget<Color = Rgb888> + 'static,
{
    fn theme(&self) -> &MorphTheme {
        &self.theme
    }
}

impl AppState {
    fn calculate(&mut self) {
        if self.left_side.is_none() || self.operation.is_none() {
            return;
        }

        if let Ok(right_side) = self.display.parse::<f32>() {
            let left_side = self.left_side.unwrap();

            match self.operation.unwrap() {
                '+' => {
                    self.display = format!("{}", left_side + right_side);
                }
                '-' => {
                    self.display = format!("{}", left_side - right_side);
                }
                '*' => {
                    self.display = format!("{}", left_side * right_side);
                }
                '/' => {
                    if left_side == 0.0 {
                        self.display = String::from("Error");
                        return;
                    }
                    self.display = format!("{}", left_side / right_side);
                }
                _ => {}
            }
        } else {
            self.display = String::from("Error");
        }
    }

    fn digit(&mut self, digit: char) {
        if self.reset {
            self.display.clear();
            self.reset = false;
        }
        self.display.push(digit);
    }

    fn op(&mut self, op: char) {
        match op {
            'C' => {
                self.left_side = None;
                self.operation = None;
                self.display.clear()
            }
            '=' => self.calculate(),
            _ => {
                if let Ok(left_side) = self.display.parse::<f32>() {
                    self.left_side = Some(left_side);
                    self.operation = Some(op);
                    self.reset = true;
                } else {
                    self.display = String::from("Error");
                }
            }
        }
    }
}

pub fn main() -> Result<(), Error> {
    let width = 124;
    let height = 200;

    App::new()
        .window(
            Window::create(AppState::default())
                .title("calculator")
                .size(width, height)
                .resizeable(true)
                .centered(true)
                .ui(|app| {
                    app.background().padding(4).child(
                        app.column()
                            .spacing(1)
                            .align(
                                app.stretch().child(
                                    app.header_label().text(app.display.as_str()).align("end"),
                                ),
                                "stretch",
                            )
                            .child(
                                app.stretch().child(
                                    app.row()
                                        .align(op_button('C', false, 3, app), "stretch")
                                        .align(op_button('/', true, 1, app), "stretch")
                                        .spacing(1),
                                ),
                            )
                            .child(
                                app.stretch().child(
                                    app.row()
                                        .align(digit_button('7', false, 1, app), "stretch")
                                        .align(digit_button('8', false, 1, app), "stretch")
                                        .align(digit_button('9', false, 1, app), "stretch")
                                        .align(op_button('*', true, 1, app), "stretch")
                                        .spacing(1),
                                ),
                            )
                            .child(
                                app.stretch().child(
                                    app.row()
                                        .align(digit_button('4', false, 1, app), "stretch")
                                        .align(digit_button('5', false, 1, app), "stretch")
                                        .align(digit_button('6', false, 1, app), "stretch")
                                        .align(op_button('-', true, 1, app), "stretch")
                                        .spacing(1),
                                ),
                            )
                            .child(
                                app.stretch().child(
                                    app.row()
                                        .align(digit_button('1', false, 1, app), "stretch")
                                        .align(digit_button('2', false, 1, app), "stretch")
                                        .align(digit_button('3', false, 1, app), "stretch")
                                        .align(op_button('+', true, 1, app), "stretch")
                                        .spacing(1),
                                ),
                            )
                            .child(
                                app.stretch().child(
                                    app.row()
                                        .align(digit_button('0', false, 2, app), "stretch")
                                        .align(digit_button('.', false, 1, app), "stretch")
                                        .align(op_button('=', true, 1, app), "stretch")
                                        // use column as spacer (later with spacer widget)
                                        .spacing(1),
                                ),
                            ),
                    )
                })?,
        )?
        .start()?;

    Ok(())
}
