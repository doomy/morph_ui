# morph_ui

morph_ui is a lightweight portable UI-Toolkit for embedded (no_std), desktop, mobile, web and PSP. Written in plain Rust.

<a href="https://codeberg.org/morphUI/morph_ui">
    <img alt="Get it on Codeberg" src="https://pages.codeberg.org/pstorch/get-it-on-blue-on-white.png" height="60">
</a>

[![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/FloVanLP/donate)

[![CI](https://ci.codeberg.org/api/badges/morphUI/morph_ui/status.svg?branch=main)](https://ci.codeberg.org/morphUI/morph_ui)
[![morphUI.codeberg.page](https://img.shields.io/badge/docs_morph_ui-latest-blue.svg)](https://morphUI.codeberg.page/doc/morph_ui)
[![morphUI.codeberg.page](https://img.shields.io/badge/docs_morph_ui_orbclient-latest-blue.svg)](https://morphUI.codeberg.page/doc/morph_ui_orbclient)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)

## Platform notes

### Embedded

On embedded (no_std) platforms morph_ui depends on `alloc` to make use of `Box`, `Vec`, `String` and `BTreeMap`. At the moment `alloc` is only available at Rust nightly. 
You can install Rust nightly with the following steps:

* Make sure Rust toolchain is installed ([install](https://www.rust-lang.org/tools/install))
* Run ```rustup toolchain install nightly```

To run an application build with morph_ui you also need to declare a `global allocator`. You can find an example how to write your own
`global allocator` at the [Rust embedded book](https://docs.rust-embedded.org/book/collections/index.html#using-alloc). But there are
also some ready to use `allocators` on crates.io e.g. for [cortex-m](https://crates.io/crates/alloc-cortex-m).

### Desktop, Mobile and Web

There is a second crate called `morph_ui_orbclient`. It provides a ready to use experience to bring your morph_ui application on desktop, mobile and web.

#### Redox

To build an run an application on Redox check the [Redox Book](https://doc.redox-os.org/book/ch05-03-compiling-program.html), please.

#### Linux, macOS and Window

`morph_ui_orbclient` depends on [OrbClient](https://gitlab.redox-os.org/redox-os/orbclient) which will build bundled with 
[SDL2](https://github.com/Rust-SDL2/rust-sdl2). To build it a Compiler is needed (like `gcc`, `clang` or MS's own compiler). 
If you have trouble to compile you application with `sdl2` bundled, you can disable the bundle feature by building your application without the default feature of `morph_ui_orbclient`. 
If you do so you have to install `sdl2` on your own: check https://github.com/Rust-SDL2/rust-sdl2 how to install it.

### Mobile

morph_ui is tested on iOS but it should be also possible to run it on Android. A template project will following soon.

#### iOS

The following tools will be needed to run morph_ui on the iOS:

* xcode ([download](https://developer.apple.com/xcode/))
* cargo-lipo ([install](https://crates.io/crates/cargo-lipo))

Setup is based on [Rust on iOS with sdl2](https://blog.aclysma.com/rust-on-ios-with-sdl2/).

### Web

On web the crate `morph_ui_orbclient` is also needed. For web it depends on [web-sys](https://crates.io/crates/web-sys). The following tools will be needed to run morph_ui on the web:

* Rust standard toolchain `rustup`, `rustc`, `cargo` ([install](https://www.rust-lang.org/tools/install))
* Rust web assembly toolchain `wasm-pack` ([install](https://rustwasm.github.io/wasm-pack/installer/))
* JavaScript package manager npm ([install](https://www.npmjs.com/get-npm))

## Examples

In the `examples` contains different examples that shows how `morph_ui` can be used on different platforms.

Check the examples [README.md](https://codeberg.org/morphUI/morph_ui/src/branch/main/examples/README.md).

## Documentation

```sh
# Builds and view the latest documentation.
cargo doc --no-deps --open
```

## Debug flags

There are some debug helpers include in morph_ui. If you run your application in debug mode you can active the helpers by using `Cmd + D` on macOS and
`Ctrl + D` on all other platforms. Debug helpers are not available in release mode. It is possible to configure the debug helpers by using the `Debug` 
flag enum:

### Activation of debug flags on Shell struct

```rust
// Active a raster that is drawn for the bounds of each widget also for the invisible widgets.
Shell::create().debug_flags(&[Debug::DrawRaster], Some(Rgb888::CSS_BLUE))

// Active the pipeline output. Prints start of loop, event handling, rendering and layout with time measure.
Shell::create().debug_flags(&[Debug::PrintPipeline], None)

// Active the output of the whole widget graph.
Shell::create().debug_flags(&[Debug::PrintGraph], None)
```

### Activation of debug flags on Window struct (morph_ui_orbclient only) 

```shell
# Active a raster that is drawn for the bounds of each widget also for the invisible widgets.
Window::create().debug_flags(&[Debug::DrawRaster, Debug::PrintPipeline, Debug::PrintGraph], Some(Rgb888::CSS_BLUE))
```

*All flags are available for both structs!*

## Notable dependencies

* [embedded-graphics](https://github.com/embedded-graphics/embedded-graphics) used for cross platform 2D rendering
* [embedded_font](https://codeberg.org/morphUI/embedded_font) font rendering (ttf and otf) for embedded-graphics
* [tiny_skia_display](https://codeberg.org/morphUI/tiny_skia_display) implementation of embedded-graphics DrawTarget based on tiny-skia
* [OrbClient](https://gitlab.redox-os.org/redox-os/orbclient) use for cross platform window and event handling

## Inspirations

* [OrbTk](https://github.com/redox-os/orbtk) a cross platform Rust UI-Toolkit based on the Entity Component System pattern
* [druid](https://github.com/linebender/druid) a data-first Rust-native UI-Toolkit
* [flutter](https://flutter.dev/) modern cross platform UI-Toolkit

## Rusty alternatives

* [embedded-gui](https://github.com/bugadani/embedded-gui) an experimental no-alloc Rust GUI toolkit built on top of embedded-graphics
* [OrbTk](https://github.com/redox-os/orbtk) a cross platform Rust UI-Toolkit based on the Entity Component System pattern
* [druid](https://github.com/linebender/druid) a data-first Rust-native UI-Toolkit
* [iced](https://github.com/hecrj/iced) a cross-platform GUI library for Rust focused on simplicity and type-safety. Inspired by Elm
* [slint](https://slint-ui.com/) Slint is a toolkit to efficiently develop fluid graphical user interfaces for any display

## Thanks to

* [Redox / OrbTk Community](https://redox-os.org/community/) to help me learn Rust, feedback on UI topics, support
* [jackpot51](https://github.com/jackpot51) as mentor, as creator of awesome Redox OS
* [jamwaffles](https://github.com/jamwaffles) as creator of embedded-graphics and support for embedded-graphics
* [bugadani](https://github.com/bugadani) as sparrings partner by embedded UI topics
